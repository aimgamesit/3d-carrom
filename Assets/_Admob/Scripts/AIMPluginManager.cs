﻿using UnityEngine;
using com.TicTok.managers;
using UnityEngine.UI;
using System.Collections;
using Pathfinding.Serialization.JsonFx;
using Firebase.Database;
using Firebase.Extensions;
using Google.Play.Review;
using Google.Play.AppUpdate;
using Google.Play.Common;
using UnityEngine.SceneManagement;
using Firebase.Analytics;
using System.IO;
using AIMCarrom;

[System.Serializable]
public class AIMPluginManager : BManager<AIMPluginManager>
{
    [System.Serializable]
    public class UpdateModel
    {
        public string appId;
        public string title;
        public string message;
        public string version;
        public bool isForceUpdate;
        public UpdateModel() { }
    }
    [Header("Popup UI")]
    public GameObject popupObject;
    public Text titleText;
    public Text msgText;
    public Button positiveBtn;
    public Button negativeBtn;
    public Text positiveText;
    public Text negativeText;
    private ReviewManager reviewManager;
    private AppUpdateManager appUpdateManager;
    private int rateUsScreenCounter = 0;

    void Start()
    {
        DontDestroyOnLoad(this);
        Debug.unityLogger.logEnabled = GameData.Instance.isLogEnabled;
        popupObject.SetActive(false);
        //CheckforUpdateFromFirebase();
        reviewManager = new ReviewManager();
        appUpdateManager = new AppUpdateManager();
        ShowUpdatePopup();
    }

    public void SendFirebaseEvent(string eventName)
    {
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventJoinGroup, FirebaseAnalytics.ParameterGroupId, eventName);
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (scene.name.Equals(GameData.Instance.MENU_SCENE))
        {
            rateUsScreenCounter++;
            if (rateUsScreenCounter >= GameData.Instance.rateUsScreenCounter)
            {
                rateUsScreenCounter = 0;
                ShowRateUsPopup();
            }
        }
    }


    #region  Rates us android Native popup
    public void ShowRateUsPopup()
    {
        StartCoroutine(OpenRateUsPopup());
    }

    private IEnumerator OpenRateUsPopup()
    {
        var requestFlowOperation = reviewManager.RequestReviewFlow();
        requestFlowOperation.Reset();
        yield return requestFlowOperation;
        var _playReviewInfo = requestFlowOperation.GetResult();
        var launchFlowOperation = reviewManager.LaunchReviewFlow(_playReviewInfo);
        yield return launchFlowOperation;
        _playReviewInfo = null; // Reset the object
    }
    #endregion
    #region Check for Update
    //{
    //    "appId": "com.br.adtest",
    //    "title": "Update Available",
    //    "message": "We have changes?",
    //    "version": "1.2.3",
    //    "isForceUpdate": true     
    //}
    #region Play store native update
    public void ShowUpdatePopup()
    {
        Debug.Log("Checking fpor update................");
        StartCoroutine(OpenUpdatePopup());
    }
    private IEnumerator OpenUpdatePopup()
    {
        PlayAsyncOperation<AppUpdateInfo, AppUpdateErrorCode> appUpdateInfoOperation = appUpdateManager.GetAppUpdateInfo();
        yield return appUpdateInfoOperation;
        if (appUpdateInfoOperation.IsSuccessful)
        {
            var appUpdateInfoResult = appUpdateInfoOperation.GetResult();
            var appUpdateOptions = AppUpdateOptions.ImmediateAppUpdateOptions(allowAssetPackDeletion: true);
            var startUpdateRequest = appUpdateManager.StartUpdate(appUpdateInfoResult, appUpdateOptions);
            yield return startUpdateRequest;
        }
        else
        {

        }
    }
    #endregion
    #region Firebase update
    private void CheckforUpdateFromFirebase()
    {
        FirebaseDatabase.DefaultInstance.GetReference("UpdateData")
          .GetValueAsync().ContinueWithOnMainThread(task =>
          {
              if (task.IsFaulted)
              {
                  Debug.Log("Firebase data error!");
              }
              else if (task.IsCompleted)
              {
                  DataSnapshot snapshot = task.Result;
                  Debug.Log("Firebase Data: " + snapshot.Value.ToString());
                  UpdateModel updateModel = JsonReader.Deserialize<UpdateModel>(snapshot.Value.ToString());
                  Debug.Log(Application.identifier + " :AVV_VERSION " + updateModel.appId);
                  if (updateModel.appId == Application.identifier)
                  {
                      bool isUpdateAvailable = IsUpdateAvailable(updateModel.version);
                      if (isUpdateAvailable)
                      {
                          ShowPopup(updateModel.title, updateModel.message, updateModel.isForceUpdate);
                      }
                  }
              }
          });
    }
    #endregion

    private bool IsUpdateAvailable(string updateVersion)
    {
        string currentVersionNumber = Application.version;
        int currentVersionLength = Application.version.Split('.').Length;
        int[] currentVersionArr = new int[currentVersionLength];

        int updateVersionLength = updateVersion.Split('.').Length;
        int[] updateVersionArr = new int[updateVersionLength];

        for (int i = 0; i < updateVersionLength; i++)
        {
            updateVersionArr[i] = int.Parse(updateVersion.Split('.')[i]);
            currentVersionArr[i] = int.Parse(currentVersionNumber.Split('.')[i]);
            Debug.Log("UPDATE: " + updateVersionArr[i] + "| CURRET: " + currentVersionArr[i]);
        }

        for (int i = 0; i < updateVersionArr.Length; i++)
        {
            Debug.Log("i. " + i + "   " + updateVersionArr[i]);
            if (updateVersionArr[i] > currentVersionArr[i])
            {
                Debug.Log("Update available!!!");
                return true;
            }
            else
            {
                Debug.LogError("Update not available!!!");
            }
        }
        return false;
    }

    public void ShowPopup(string title, string msg, bool isForceUpdate)
    {
        popupObject.SetActive(true);
        FirebaseManager.Instance.SendEvent(Constants.SHOWING_UPDATE_POPUP);
        titleText.text = title;
        msgText.text = msg;
        positiveBtn.SetActive(true);
        negativeBtn.SetActive(!isForceUpdate);
        positiveBtn.onClick.AddListener(delegate ()
        {
            FirebaseManager.Instance.SendEvent(Constants.UPDATE_NOW);
            popupObject.SetActive(isForceUpdate);
            Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
        });
        negativeBtn.onClick.AddListener(delegate ()
        {
            FirebaseManager.Instance.SendEvent(Constants.UPDATE_NOT_NOW);
            popupObject.SetActive(false);
        });
    }
    #endregion

    private IEnumerator TakeScreenshotAndShare()
    {
        yield return new WaitForEndOfFrame();
        string appUrl = "https://play.google.com/store/apps/details?id=" + Application.identifier;
        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();
        string filePath = Path.Combine(Application.temporaryCachePath, "AIMGames_SS.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());
        Destroy(ss);
        new NativeShare().AddFile(filePath)
            .SetSubject("Let's play " + Application.productName).SetText(Constants.SHARE_TEXT)
            .Share();
    }

    public void ShareApp(bool shareScreenShot)
    {
        if (shareScreenShot)
            StartCoroutine(TakeScreenshotAndShare());
        else
        {
            string appUrl = "https://play.google.com/store/apps/details?id=" + Application.identifier;
            new NativeShare()
                .SetSubject("Let's play " + Application.productName).SetText(Constants.SHARE_TEXT)
                .Share();
        }
    }

    public void RateUss()
    {
        ShowRateUsPopup();
    }
}