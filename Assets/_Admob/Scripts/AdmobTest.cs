﻿using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.UI;

public class AdmobTest : MonoBehaviour
{
    [SerializeField] Text logText;
    [SerializeField] GameObject adNativePanel;
    [SerializeField] RawImage adIcon;
    [SerializeField] RawImage adChoices;
    [SerializeField] Text adHeadline;
    [SerializeField] Text adCallToAction;
    [SerializeField] Text adAdvertiser;


    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        logText.text += "\n\n=>> "+logString;
    }

    public void ShowBannerAd(bool isTop)
    {
        AdmobAdsManager.Instance.ShowBannerAd(isTop);
        Debug.LogError("ShowBannerAd(): " + isTop);
    }

    public void ShowInterstitalAd()
    {
        AdmobAdsManager._AdClosed += delegate ()
        {
            Debug.LogError("ShowInterstitalAd(): Closed");
        };
        AdmobAdsManager.Instance.ShowInterstitial();
    }

    public void ShowRewardedVideoAd()
    {
        AdmobAdsManager._AdClosed += delegate ()
        {
            Debug.LogError("ShowRewardedVideoAd(): Closed");
        };
        AdmobAdsManager._OnRewardedReceived += delegate ()
         {
             Debug.LogError("ShowRewardedVideoAd(): _OnRewardedReceived: ");
         };
        AdmobAdsManager.Instance.ShowVideoAd();
    }

    public void ShowNativeAd()
    {
        Debug.LogError("ShowNativeAd():");
        AdmobAdsManager.Instance.ShowNativeAds(adNativePanel, adIcon, adChoices, adHeadline, adCallToAction, adAdvertiser, true);
    }

    public void HideNativeAd()
    {
        adNativePanel.SetActive(false);
    }
}