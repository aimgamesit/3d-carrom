﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using com.TicTok.managers;
using System.Collections.Generic;
using System;

public class AdmobAdsManager : BManager<AdmobAdsManager>
{
    private GameObject adNativePanel;
    private RawImage adIcon;
    private RawImage adChoices;
    private Text adHeadline;
    private Text adCallToAction;
    private Text adAdvertiser;
    private NativeAd adNative;
    private AdLoader nativeAdLoader;
    private InterstitialAd interstitialAd = null;
    private BannerView _bannerView = null;
    private AdRequest mBannerRequest = null;
    public RewardedAd rewardedAd;
    private string INTERSTIAL_ID = "";
    private string BANNER_ID = "";
    private string REWARDED_VIDEO_ID = "";
    private string NATIVE_AD_ID = "";
    private string APP_ID = "ca-app-pub-8996771506370953~1159894615";

    public delegate void AdClosed();

    public static event AdClosed _AdClosed = null;

    public delegate void OnRewardedReceived();

    public static event OnRewardedReceived _OnRewardedReceived = null;

    void Start()
    {
        DontDestroyOnLoad(this);
        Debug.unityLogger.logEnabled = GameData.Instance.isLogEnabled;
        InitAds();
    }

    private void InitAds()
    {
        if (GameData.Instance.isTestAds)
        {
            INTERSTIAL_ID = "ca-app-pub-3940256099942544/1033173712";
            BANNER_ID = "ca-app-pub-3940256099942544/6300978111";
            REWARDED_VIDEO_ID = "ca-app-pub-3940256099942544/5224354917";
            NATIVE_AD_ID = "ca-app-pub-3940256099942544/2247696110";
            APP_ID = GameData.Instance.appID;
        }
        else
        {
            GameData gData = GameData.Instance;
            INTERSTIAL_ID = gData.interstitialAdID;
            BANNER_ID = gData.bannerAdID;
            REWARDED_VIDEO_ID = gData.rewardAdID;
            NATIVE_AD_ID = gData.nativeAdID;
            APP_ID = gData.appID;
        }

        Debug.Log("Iniatilizing admob...");

        MobileAds.Initialize((initStatus) =>
        {
            InitializeNativeAd();
            LoadInterstitialAd();
            LoadRewardedAd();
            InitializeBannerAd();
        });
        StartCoroutine(AdTimer());
    }

    private IEnumerator AdTimer()
    {
        int counter = 0;
        while (GameData.Instance.adLoadTime > counter)
        {
            yield return new WaitForSeconds(1f);
            counter += 1;
        }
        LoadNextScene();
    }

    private void LoadNextScene()
    {
        if (!string.IsNullOrEmpty(GameData.Instance.MENU_SCENE))
            SceneManager.LoadScene(GameData.Instance.MENU_SCENE);
    }

    #region Initialize Banner Ads Request
    private void InitializeBannerAd()
    {
        if (_bannerView == null)
            _bannerView = new BannerView(BANNER_ID, AdSize.Banner, AdPosition.Bottom);
        if (mBannerRequest == null)
            mBannerRequest = new AdRequest.Builder().Build();

        _bannerView.OnBannerAdLoadFailed += delegate (LoadAdError error)
        {
            Debug.Log(error.GetCode() + " Banner OnAdFailedToLoad: " + error.GetMessage());
        };
    }
    #endregion

    #region Initialize InterstitalAds Request
    private void LoadInterstitialAd()
    {
        // Clean up the old ad before loading a new one.
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
            interstitialAd = null;
        }

        Debug.Log("Loading the interstitial ad.");

        // create our request used to load the ad.
        var adRequest = new AdRequest.Builder().Build();

        // send the request to load the ad.
        InterstitialAd.Load(INTERSTIAL_ID, adRequest, (InterstitialAd ad, LoadAdError error) =>
            {
                // if error is not null, the load request failed.
                if (error != null || ad == null)
                {
                    Debug.LogError("interstitial ad failed to load an ad " +
                                   "with error : " + error);
                    return;
                }

                Debug.Log("Interstitial ad loaded with response : "
                          + ad.GetResponseInfo());

                interstitialAd = ad;
                RegisterInterstitialEventHandlers(ad);
            });
    }

    private void RegisterInterstitialEventHandlers(InterstitialAd ad)
    {
        // Raised when the ad is estimated to have earned money.
        ad.OnAdPaid += (AdValue adValue) =>
        {
            Debug.Log(String.Format("Interstitial ad paid {0} {1}.",
                adValue.Value,
                adValue.CurrencyCode));
        };
        // Raised when an impression is recorded for an ad.
        ad.OnAdImpressionRecorded += () =>
        {
            Debug.Log("Interstitial ad recorded an impression.");
        };
        // Raised when a click is recorded for an ad.
        ad.OnAdClicked += () =>
        {
            Debug.Log("Interstitial ad was clicked.");
        };
        // Raised when an ad opened full screen content.
        ad.OnAdFullScreenContentOpened += () =>
        {
            Debug.Log("Interstitial ad full screen content opened.");
        };
        // Raised when the ad closed full screen content.
        ad.OnAdFullScreenContentClosed += () =>
        {
            Debug.Log("Interstitial ad full screen content closed.");
            HandleAdClose();
            LoadInterstitialAd();
        };
        // Raised when the ad failed to open full screen content.
        ad.OnAdFullScreenContentFailed += (AdError error) =>
        {
            Debug.LogError("Interstitial ad failed to open full screen content " +
                           "with error : " + error);
            HandleAdClose();
            LoadInterstitialAd();
        };
    }
    #endregion

    #region Initialize Rewarded Video Ads Request
    private void LoadRewardedAd()
    {
        // Clean up the old ad before loading a new one.
        if (rewardedAd != null)
        {
            rewardedAd.Destroy();
            rewardedAd = null;
        }

        Debug.Log("Loading the rewarded ad.");

        // create our request used to load the ad.
        var adRequest = new AdRequest.Builder().Build();

        // send the request to load the ad.
        RewardedAd.Load(REWARDED_VIDEO_ID, adRequest,
            (RewardedAd ad, LoadAdError error) =>
            {
                // if error is not null, the load request failed.
                if (error != null || ad == null)
                {
                    Debug.LogError("Rewarded ad failed to load an ad " +
                                   "with error : " + error);
                    return;
                }

                Debug.Log("Rewarded ad loaded with response : "
                          + ad.GetResponseInfo());

                rewardedAd = ad;
                RegisterRewardedEventHandlers(ad);
            });
    }

    private void RegisterRewardedEventHandlers(RewardedAd ad)
    {
        // Raised when the ad is estimated to have earned money.
        ad.OnAdPaid += (AdValue adValue) =>
        {
            Debug.Log(String.Format("Rewarded ad paid {0} {1}.",
                adValue.Value,
                adValue.CurrencyCode));
        };
        // Raised when an impression is recorded for an ad.
        ad.OnAdImpressionRecorded += () =>
        {
            Debug.Log("Rewarded ad recorded an impression.");
        };
        // Raised when a click is recorded for an ad.
        ad.OnAdClicked += () =>
        {
            Debug.Log("Rewarded ad was clicked.");
        };
        // Raised when an ad opened full screen content.
        ad.OnAdFullScreenContentOpened += () =>
        {
            Debug.Log("Rewarded ad full screen content opened.");
        };
        // Raised when the ad closed full screen content.
        ad.OnAdFullScreenContentClosed += () =>
        {
            Debug.Log("Rewarded ad full screen content closed.");
            HandleAdClose();
            LoadRewardedAd();
        };
        // Raised when the ad failed to open full screen content.
        ad.OnAdFullScreenContentFailed += (AdError error) =>
        {
            Debug.LogError("Rewarded ad failed to open full screen content " +
                           "with error : " + error);
            HandleAdClose();
            LoadRewardedAd();
        };
    }

    #endregion
    #region Native Ads Request
    private void InitializeNativeAd()
    {
        nativeAdLoader = new AdLoader.Builder(NATIVE_AD_ID).ForNativeAd().Build();
        nativeAdLoader.OnNativeAdLoaded += delegate (object sender, NativeAdEventArgs args)
        {
            this.adNative = args.nativeAd;
            ShowNativeAds(this.adNativePanel, this.adIcon, this.adChoices, this.adHeadline, this.adCallToAction, this.adAdvertiser, false);
            Debug.LogError("Native Ad OnUnifiedNativeAdLoaded");
        };
        nativeAdLoader.OnAdFailedToLoad += delegate (object sender, AdFailedToLoadEventArgs args)
        {
            Debug.LogError("Native Ad OnAdFailedToLoad: " + args.LoadAdError.GetMessage());
        };
        nativeAdLoader.LoadAd(new AdRequest.Builder().Build());
    }

    public void ShowNativeAds(GameObject adNativePanel, RawImage adIcon, RawImage adChoices, Text adHeadline, Text adCallToAction, Text adAdvertiser, bool isLoadAd)
    {
        if (nativeAdLoader != null && isLoadAd)
            nativeAdLoader.LoadAd(new AdRequest.Builder().Build());
        if (this.adNative != null && adNativePanel != null)
        {
            this.adNativePanel = adNativePanel;
            this.adIcon = adIcon;
            this.adChoices = adChoices;
            this.adHeadline = adHeadline;
            this.adCallToAction = adCallToAction;
            this.adAdvertiser = adAdvertiser;

            if (this.adIcon != null)
            {
                this.adIcon.texture = this.adNative.GetIconTexture();
                this.adNative.RegisterIconImageGameObject(this.adIcon.gameObject);
            }
            if (this.adChoices != null)
            {
                this.adChoices.texture = this.adNative.GetAdChoicesLogoTexture();
                this.adNative.RegisterAdChoicesLogoGameObject(this.adChoices.gameObject);
            }
            if (this.adHeadline != null)
            {
                this.adHeadline.text = this.adNative.GetHeadlineText();
                this.adNative.RegisterHeadlineTextGameObject(this.adHeadline.gameObject);
            }
            if (this.adAdvertiser != null)
            {
                this.adAdvertiser.text = this.adNative.GetAdvertiserText();
                this.adNative.RegisterAdvertiserTextGameObject(this.adAdvertiser.gameObject);
            }
            if (this.adCallToAction != null)
            {
                this.adCallToAction.text = this.adNative.GetCallToActionText();
                this.adNative.RegisterCallToActionGameObject(this.adCallToAction.gameObject);
            }
            adNativePanel.SetActive(true);
        }
    }
    #endregion

    public void ShowVideoAd()
    {
        if (rewardedAd != null && rewardedAd.CanShowAd())
        {
            const string rewardMsg = "Rewarded ad rewarded the user. Type: {0}, amount: {1}.";

            if (rewardedAd != null && rewardedAd.CanShowAd())
            {
                rewardedAd.Show((Reward reward) =>
                {
                    // TODO: Reward the user.
                    if (_OnRewardedReceived != null)
                    {
                        _OnRewardedReceived();
                        _OnRewardedReceived = null;
                    }
                    Debug.Log(String.Format(rewardMsg, reward.Type, reward.Amount));
                });
            }
        }
        else
        {
            LoadRewardedAd();
            HandleAdClose();
        }
    }
    public bool IsVideoAdAvailable()
    {
        if (rewardedAd != null)
        {
            Debug.Log("mRewardedBasedVideoAd.IsLoaded(): " + rewardedAd.CanShowAd());
            if (!rewardedAd.CanShowAd())
                LoadRewardedAd();
        }
        return rewardedAd == null ? false : rewardedAd.CanShowAd();
    }

    public void ShowBannerAd(bool aIsTop)
    {
        if (_bannerView == null)
            InitializeBannerAd();
        if (_bannerView != null)
        {
            //mBannerView.Hide();
            _bannerView.SetPosition(aIsTop ? AdPosition.Top : AdPosition.Bottom);
            _bannerView.LoadAd(mBannerRequest);
            _bannerView.Show();
        }
    }

    public void HideBannerAd()
    {
        if (_bannerView != null)
            _bannerView.Hide();
    }

    public void ShowInterstitial()
    {
        if (interstitialAd == null)
        {
            HandleAdClose();
        }
        else if (interstitialAd.CanShowAd())
        {
            interstitialAd.Show();
        }
        else
        {
            HandleAdClose();
            LoadInterstitialAd();
        }
    }

    private void HandleAdClose()
    {
        if (_AdClosed != null)
        {
            _AdClosed();
            _AdClosed = null;
        }
    }
}