﻿using UnityEngine;
using com.aquimo.util;

namespace com.game.util
{
	public abstract class BZone : MonoBehaviour ,IZone
	{
		public virtual void refreshZoneState (bool state)
		{
			resetZoneState ();

			gameObject.SetActive (state);
		}

		public virtual void resetZoneState ()
		{
			
		}

	}

}