﻿using UnityEngine;
using com.aquimo.util;

namespace com.aquimo.data
{
	// Load Data from resources folder and if not,create asset file .
	public abstract class BData<T> : ScriptableObject, IData where T : BData<T> //Force to implement BDATA
	{
		static T _Instance;

		public static T Instance {
			get {
				if (_Instance == null) {
					_Instance = C.LoadScriptableObject<T> ();
					_Instance.onInstanceCreated ();
				}
				return _Instance;
			}
		}

		public virtual void onInstanceCreated ()
		{
			
		}

		public abstract string getResrcPath ();

	}



}