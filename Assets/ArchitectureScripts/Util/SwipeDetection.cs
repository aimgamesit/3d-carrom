﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.aquimo.util;

namespace com.aquimo.util
{
	#region Singleton
	public partial class SwipeDetection : MonoBehaviour
	{
		static SwipeDetection _Instance;

		public static SwipeDetection Instance {
			get {
				if (_Instance == null) {
					GameObject gObj = new GameObject (typeof(SwipeDetection).Name);
					_Instance = gObj.AddComponent<SwipeDetection> ();
				}
				return _Instance;
			}
		}

	}
	#endregion

	public partial class SwipeDetection : MonoBehaviour
	{
		public List<ISwipeDetection> swipeListenerList = new List<ISwipeDetection> ();

		public static void PopSwipeListener (ISwipeDetection swipeListener)
		{
			if (_Instance == null)
				return;
			
			if (swipeListener == null) {
				Debug.LogWarning ("Swipe Listener is null");
				return;
			}

			Instance.swipeListenerList.Remove (swipeListener);

			if (Instance.swipeListenerList.Count < 1) {
				Object.Destroy (Instance.gameObject);
				_Instance = null;
			}
		}

	}

	#region Swipe Logic
	public partial class SwipeDetection : MonoBehaviour
	{
		//Constants
		private float fSensitivity = 15;
		
		//VARIABLES
		//distance calculation
		private SwipeDirection sSwipeDirection;

		//string to receive swipe output (Debug Only)
		private float fInitialX;
		private float fInitialY;
		private float fFinalX;
		private float fFinalY;
		private int iTouchStateFlag;
		//flag to check
		
		private float inputX;
		//x-coordinate
		private float inputY;
		//y-coordinate
		private float slope;
		//slope (m) of the the
		private float fDistance;
		//magnitude of distance between two positions

		public bool halfDownScreen = false;
		
		// Use this for initialization
		void Start ()
		{
			fInitialX = 0.0f;
			fInitialY = 0.0f;
			fFinalX = 0.0f;
			fFinalY = 0.0f;
			
			inputX = 0.0f;
			inputY = 0.0f;
			
			iTouchStateFlag = 0;
			sSwipeDirection = SwipeDirection.Null;
		}

		void Update ()
		{
			if (iTouchStateFlag == 0 && Input.GetMouseButtonDown (0)) {
			
				if (!halfDownScreen) {//state 1 of swipe control
					fInitialX = Input.mousePosition.x;	//get the initial x mouse/ finger value
					fInitialY = Input.mousePosition.y;	//get the initial y mouse/ finger value

					sSwipeDirection = SwipeDirection.Null;
					iTouchStateFlag = 1;
				} else if (Input.mousePosition.y < Screen.width * 0.8f) {
					fInitialX = Input.mousePosition.x;	//get the initial x mouse/ finger value
					fInitialY = Input.mousePosition.y;	//get the initial y mouse/ finger value

					sSwipeDirection = SwipeDirection.Null;
					iTouchStateFlag = 1;
				}
			}		
			if (iTouchStateFlag == 1) {	//state 2 of swipe control
				fFinalX = Input.mousePosition.x;
				fFinalY = Input.mousePosition.y;
				
				sSwipeDirection = swipeDirection ();	//get the swipe direction

				if (sSwipeDirection != SwipeDirection.Null) {
					swipeListenerList.ForEach (x => x.onSwipe (sSwipeDirection));
				}

				if (sSwipeDirection != SwipeDirection.Null)
					iTouchStateFlag = 2;
			}//end of state 1		
			if (iTouchStateFlag == 2 || Input.GetMouseButtonUp (0)) {	//state 3 of swipe control
				//sSwipeDirection = SwipeDirection.Null;
				iTouchStateFlag = 0;
			}
		}

		private SwipeDirection swipeDirection ()
		{
			inputX = fFinalX - fInitialX;
			inputY = fFinalY - fInitialY;
			slope = inputY / inputX;

			fDistance = Mathf.Sqrt (Mathf.Pow ((fFinalY - fInitialY), 2) + Mathf.Pow ((fFinalX - fInitialX), 2));
			
			if (fDistance <= (Screen.width / fSensitivity))//higher the dividing factor higher the sensitivity
				return SwipeDirection.Null;
			else if (inputX > 0 && inputY >= 0 && slope < 1 && slope >= 0) {//1st octant  RIGHT
				return SwipeDirection.Right;
			} else if (inputX > 0 && inputY <= 0 && slope > -1 && slope <= 0) {//8th octant  RIGHT
				return SwipeDirection.Right;
			} else if (inputX < 0 && inputY >= 0 && slope > -1 && slope <= 0) {//4th octant  LEFT
				return SwipeDirection.Left;
			} else if (inputX < 0 && inputY <= 0 && slope >= 0 && slope < 1) {//5th octant  LEFT
				return SwipeDirection.Left;
			} else if (inputX >= 0 && inputY > 0 && slope > 1) {//2nd octant  TOP
				return SwipeDirection.Top;
			} else if (inputX <= 0 && inputY > 0 && slope < -1) {//3rd octant  TOP
				return SwipeDirection.Top;
			} else if (inputX >= 0 && inputY < 0 && slope < -1) {//7th octant  Bottom
				return SwipeDirection.Bottom;
			} else if (inputX <= 0 && inputY < 0 && slope < 1) {//6th octant  Bottom
				return SwipeDirection.Bottom;
			}


			return SwipeDirection.Null;	
		}

		public enum SwipeDirection
		{
			//no swipe detected
			Null = 0,

			//swipe right detected
			Right = 1,

			//swipe left detected
			Left = 2,

			//swipe Top detected
			Top = 3,

			//swipe Bottom detected
			Bottom = 4,
		}
	}
	#endregion

	public interface ISwipeDetection
	{
		void onSwipe (SwipeDetection.SwipeDirection direction);
	}

}
