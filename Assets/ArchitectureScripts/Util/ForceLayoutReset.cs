﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class ForceLayoutReset : MonoBehaviour {

	void OnEnable()
	{
		StartCoroutine (ForceReorder ());
	}

	IEnumerator ForceReorder()
	{
		yield return null;
		LayoutRebuilder.ForceRebuildLayoutImmediate (GetComponent<RectTransform> ());
	}

	public void ResetSize()
	{
		StartCoroutine (ForceReorder ());
	}
}
