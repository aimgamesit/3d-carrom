﻿using UnityEngine;

namespace com.aquimo.util
{
	public class TranslateObject : MonoBehaviour
	{
		public float speed = 15;
		public float waitTime = 2.0f;

		bool moveBack;
		float timeCount;
		public Transform target;

		public float initDistance = -1;

		public void startTranslate (Transform target)
		{
			timeCount = 0;
			enabled = true;
			moveBack = true;
			this.target = target;
			if (initDistance == -1) {
				initDistance = Vector3.Distance (transform.position, target.position);
			}
		}

		void LateUpdate ()
		{
			if (target == null)
				return;

			if (moveBack) {
				
				if (Vector3.Distance (transform.position, target.position) < 10) {
					transform.Translate (Vector3.back * speed * Time.deltaTime, Space.Self);
				} else {
					timeCount += Time.deltaTime;
					moveBack = waitTime > timeCount;
				}

			} else {
				if (Vector3.Distance (transform.position, target.position) > initDistance)
					transform.Translate (Vector3.forward * speed * Time.deltaTime, Space.Self);
				else
					enabled = false;
			}
		}
	}
}