﻿
#if UNITY_EDITOR
using System.IO;
using UnityEngine;
using com.aquimo.util;
using com.aquimo.data;

namespace com.aquimo.editor
{
	class Tools : ScriptableObject
	{
		[UnityEditor.MenuItem ("Tools/ClearPlayerPrefs")]
		static void ClearPlayerPrefs ()
		{
			PlayerPrefs.DeleteAll ();
		}

		[UnityEditor.MenuItem ("Tools/ClearCache")]
		static void ClearCache ()
		{
			Caching.ClearCache ();
		}
	}

}
#endif