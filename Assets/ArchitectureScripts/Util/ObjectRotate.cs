﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotate : MonoBehaviour
{
    public bool x, y, z;
    [Range(.0F, 100.0F)]
    public float speed = 5f;

    void Update()
    {
        float x_Val = transform.eulerAngles.x + speed;
        float y_Val = transform.eulerAngles.y + speed;
        float z_Val = transform.eulerAngles.z - speed;

        if (x & y)
            transform.eulerAngles = new Vector3(x_Val, y_Val, 0);
        else if (x & z)
            transform.eulerAngles = new Vector3(x_Val, 0, z_Val);
        else if (y & z)
            transform.eulerAngles = new Vector3(0, y_Val, z_Val);
        else if (x)
            transform.eulerAngles = new Vector3(x_Val, 0, 0);
        else if (y)
            transform.eulerAngles = new Vector3(0, y_Val, 0);
        else if (z)
            transform.eulerAngles = new Vector3(0, 0, z_Val);
    }
}
