using UnityEngine;
using System.Collections.Generic;
using com.aquimo.util;
using System.Linq;
using UnityEngine.UI;

public static class ExtensionMethods
{
	public static void SetActiveInHierarchy (this Transform transform)
	{
		transform.SetActive (true);
		if (!transform.gameObject.activeInHierarchy)
			transform.parent.SetActiveInHierarchy ();
	}

	public static void SetParentReset (this Transform transform, Transform parent)
	{
		transform.SetParent (parent, false);
		transform.Reset ();       
	}

	public static void SwitchState (this Transform transform, Transform nextState)
	{
		transform.gameObject.SetActive (!transform.gameObject.activeSelf);
		nextState.gameObject.SetActive (!nextState.gameObject.activeSelf);
	}

	public static void ResetZone (this Transform transform)
	{
		IZone mZone = transform.GetComponent<IZone> ();
		if (mZone != null) {
			mZone.resetZoneState ();
		}
	}

	public static void ResetChilds (this Transform transform)
	{
		foreach (Transform child in transform) {
			child.Reset ();
		}
	}

	public static void Destroy (this System.Threading.Thread thread)
	{
		try {
			if (thread != null)
				thread.Abort ();
		} catch (System.Exception e) {
			Debug.Log (e.StackTrace);
		}

		thread = null;
	}

	public static T GetOrAddComponent<T> (this GameObject gObj) where T : Component
	{
		T tObj = gObj.GetComponent<T> ();

		if (tObj == null) {
			tObj = gObj.AddComponent<T> ();			
		}

		return tObj;
	}

	public static void updateTexture (this Transform mObj, Texture texture, int index = 0)
	{
		Renderer mRenderer = mObj.GetComponent<Renderer> ();
		if (mRenderer != null) {
			if (mRenderer.materials.Length > index)
				mRenderer.materials [index].mainTexture = texture;
		}
	}

	public static void Reset (this Transform transform)
	{
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
		transform.localScale = Vector3.one;
	}




	public static Vector3 GetForwardPosition (this Transform transform, Transform target)
	{
		return new Vector3 (transform.position.x, transform.position.y, target.position.z);
	}

	public static void SetRagDoll (this Transform tranform, bool isKinematic)
	{
		Rigidbody[] rBodys = tranform.GetComponentsInChildren<Rigidbody> ();

		foreach (var rBody in rBodys) {
			rBody.isKinematic = isKinematic;
		}
	}

	public static Toggle GetActive (this ToggleGroup aGroup)
	{
		return aGroup.ActiveToggles ().FirstOrDefault ();
	}

	public static void SetLayer (this Transform transform, int index)
	{
		transform.gameObject.layer = index;
		foreach (Transform child in transform) {
			if (child.childCount > 0)
				child.SetLayer (index);
			child.gameObject.layer = index;
		}
	}

	public static Transform GetLastActiveChild (this Transform transform)
	{
		int childCount = transform.childCount;

		for (int i = childCount - 1; i >= 0; i--) {
			Transform mChild = transform.GetChild (i);

			if (mChild.gameObject.activeSelf)
				return mChild;
		}

		return null;
	}

	public static Vector3 GetOffsetPosition (this Transform obj, Transform target, Vector3 offset, Vector3 direction)
	{
		float x = direction.x == 0 ? obj.position.x : target.position.x + offset.x;
		float y = direction.y == 0 ? obj.position.y : target.position.y + offset.y;
		float z = direction.z == 0 ? obj.position.z : target.position.z + offset.z;
		return new Vector3 (x, y, z);
	}

	public static System.DateTime SetHours (this System.DateTime from, int hour, int minutes = 0, int seconds = 0)
	{
		return new System.DateTime (from.Year, from.Month, from.Day, hour, minutes, seconds);
	}

	public static System.DateTime Next (this System.DateTime from, System.DayOfWeek dayOfWeek)
	{
		int start = (int)from.DayOfWeek;
		int target = (int)dayOfWeek;
		if (target <= start)
			target += 7;
		return from.AddDays (target - start);
	}

	public static bool IsInSameWeek (this System.DateTime time, System.DateTime currentTIme)
	{
		return currentTIme.Subtract (time).Days <= 6;//OPT
	}

	public static int GetWeekDayValue (this System.DateTime time)
	{
		return (int)time.DayOfWeek;
	}

	public static void SetScrollViewDown (this RectTransform rTransform)
	{
		float x = rTransform.anchoredPosition3D.x;
		float y = -1 * (rTransform.sizeDelta.y / 2.0f);
		float z = rTransform.anchoredPosition3D.z;
		rTransform.anchoredPosition3D = new Vector3 (x, y, z);
		Debug.Log ("Scroll Position " + rTransform.anchoredPosition3D);
	}

	public static Transform GetLastChild (this Transform transform)
	{
		if (transform.childCount == 0)
			return null;

		return transform.GetChild (transform.childCount - 1);
	}

	public static void SetNextActiveChildAtLast (this Transform transform)
	{
		Transform mChild = transform.GetLastChild ();
		if (mChild != null) {
			mChild.SetAsFirstSibling ();
			mChild.SetActive (false);
		}

		int childCount = transform.childCount;

		for (int i = childCount - 1; i >= 0; i--) {
			mChild = transform.GetChild (i);

			if (mChild.gameObject.activeSelf)
				break;

			mChild.SetAsFirstSibling ();
		}
	}

	public static void Show (this Transform transform)
	{
		transform.SetActive (true);
	}


	public static void Hide (this Transform  transform)
	{
		transform.SetActive (false);
	}

	public static void StartSyncTarget (this Transform transform, Transform target)
	{
		transform.gameObject.AddComponent<SyncTransform> ().target = target;
	}

	public static void StopSyncTarget (this Transform transform)
	{
		SyncTransform sTransform = transform.GetComponent<SyncTransform> ();

		if (sTransform != null)
			Object.Destroy (sTransform);
	}

	public static void SetActive (this Behaviour mBehaviour, bool isActive)
	{
		mBehaviour.gameObject.SetActive (isActive);
	}

	public static void SetActive (this Transform transform, bool isActive)
	{
		transform.gameObject.SetActive (isActive);
	}

	public static bool isLastSibling (this Transform transform)
	{
		if (transform.parent == null)
			return false;

		return transform.GetSiblingIndex () == (transform.parent.childCount - 1);
	}

	public static void ResetOnlyParent (this Transform transform)
	{
		List<Transform> childList = new List<Transform> (); 
		int childCount = transform.childCount;
		for (int i = 0; i < childCount; i++) {
			Transform child = transform.GetChild (0);
			child.SetParent (null, true);
			childList.Add (child);
		}

		transform.Reset ();

		foreach (Transform child in childList) {
			child.SetParent (transform, true);
		} 
	}

	public static void CopyAll (this Transform transform, Transform target)
	{
		transform.position = target.position;
		transform.rotation = target.rotation;
		transform.localScale = target.localScale;
	}

	public static void CopyAll (this Transform transform, WorldInfo target)
	{
		transform.position = target.position;
		transform.rotation = target.rotation;
		transform.localScale = target.scaling;
	}

	public static void CopyLocalAll (this Transform transform, WorldInfo target)
	{
		transform.localPosition = target.position;
		transform.localRotation = target.rotation;
		transform.localScale = target.scaling;
	}

	public static void DestroyChildChilds (this Transform transform)
	{
		foreach (Transform child in transform) {
			child.DestroyChilds ();
		}
	}

	public static void DestroyChilds (this Transform transform)
	{
		int childCount = transform.childCount;
		for (int i = 0; i < childCount; i++) {
			Object.Destroy (transform.GetChild (i).gameObject);
		}
	}

	public static Transform GetFromChilds (this Transform transform, string name)
	{
		foreach (Transform child in transform) {
			if (child.name.Equals (name)) {
				return child;
			}
			if (child.childCount > 0) {
				Transform innerChild = child.GetFromChilds (name);
				if (innerChild != null) {
					return innerChild;
				}
			}   
		}
		return null;
	}

	public static bool IsChildExist (this Transform transform, string tag)
	{
		return transform.GetTagChild (tag) != null;
	}

	public static int GetActiveChildCount (this Transform transform)
	{
		int activeChildCount = 0;
		int totalChild = transform.childCount;//

		for (int i = 0; i < totalChild; i++) {
			if (transform.GetChild (i).gameObject.activeSelf)
				activeChildCount++;
		}

		return activeChildCount;
	}

	public static Transform GetTagChild (this Transform transform, string tag)
	{
		foreach (Transform child in transform) {
			if (child.tag.Equals (tag)) {
				return child;
			}
			if (child.childCount > 0) {
				Transform innerChild = child.GetTagChild (tag);
				if (innerChild != null) {
					return innerChild;
				}
			}   
		}
		return null;
	}

	public static void TriggerAnim (this Animator animator, string id)
	{
		animator.ResetTrigger (id);//

		animator.SetTrigger (id);//
	}

	public static T GetOrAddComponent<T> (this Component child) where T: Component
	{
		T result = child.GetComponent<T> ();
		if (result == null) {
			result = child.gameObject.AddComponent<T> ();
		}
		return result;
	}

	public static bool IsVisibleFrom (this Renderer renderer, Camera camera)
	{
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes (camera);
		return GeometryUtility.TestPlanesAABB (planes, renderer.bounds);
	}

	public static bool isInArray (this string mValue, params string[] values)
	{
		return (values == null) ? false : values.Contains (mValue);
	}


	public static void SetLayerMaxWeight (this Animator animator, bool isReset = false, params string[] values)
	{
		if (isReset)
			animator.ResetLayerWeight ();

		foreach (var item in values) {
			animator.SetLayerWeight (item, 1);
		}
	}

	//	public static void SetLayerWeight (this Animator animator, bool isReset = false, params KeyValue[] keyValues)
	//	{
	//		if (isReset)
	//			animator.ResetLayerWeight ();
	//
	//		foreach (var item in keyValues) {
	//			animator.SetLayerWeight (item.key, item.value);
	//		}
	//	}

	public static void SetLayerWeight (this Animator animator, string name, int weight, bool resetOther = false)
	{
		int index = animator.GetLayerIndex (name);
		if (index < 0) {
			Debug.LogWarning ("Layer Name is not defined " + name);
			return;
		}

		if (resetOther)
			animator.ResetLayerWeight ();

		animator.SetLayerWeight (index, weight);
	}

	public static void ResetLayerWeight (this Animator animator)
	{
		int count = animator.layerCount;
		for (int i = 0; i < count; i++) {
			animator.SetLayerWeight (i, 0);
		}
	}

}

public static class QuaternionExtensions
{
	public static Quaternion Pow (this Quaternion input, float power)
	{
		float inputMagnitude = input.Magnitude ();
		Vector3 nHat = new Vector3 (input.x, input.y, input.z).normalized;
		Quaternion vectorBit = new Quaternion (nHat.x, nHat.y, nHat.z, 0)
			.ScalarMultiply (power * Mathf.Acos (input.w / inputMagnitude))
			.Exp ();
		return vectorBit.ScalarMultiply (Mathf.Pow (inputMagnitude, power));
	}

	public static Quaternion Exp (this Quaternion input)
	{
		float inputA = input.w;
		Vector3 inputV = new Vector3 (input.x, input.y, input.z);
		float outputA = Mathf.Exp (inputA) * Mathf.Cos (inputV.magnitude);
		Vector3 outputV = Mathf.Exp (inputA) * (inputV.normalized * Mathf.Sin (inputV.magnitude));
		return new Quaternion (outputV.x, outputV.y, outputV.z, outputA);
	}

	public static float Magnitude (this Quaternion input)
	{
		return Mathf.Sqrt (input.x * input.x + input.y * input.y + input.z * input.z + input.w * input.w);
	}

	public static Quaternion ScalarMultiply (this Quaternion input, float scalar)
	{
		return new Quaternion (input.x * scalar, input.y * scalar, input.z * scalar, input.w * scalar);
	}
}


public static class Vector3Extensions
{
	public static Vector3 ChangeXTo(this Vector3 input, float value)
	{
		return new Vector3 (value, input.y, input.z);
	}

	public static Vector3 ChangeYTo(this Vector3 input, float value)
	{
		return new Vector3 (input.x, value, input.z);
	}

	public static Vector3 ChangeZTo(this Vector3 input, float value)
	{
		return new Vector3 (input.x, input.y, value);
	}

	public static string ConvertToString(this Vector3 input)
	{
		return "( "+input.x.ToString()+","+input.y.ToString()+","+input.z.ToString()+" )";
	}
}