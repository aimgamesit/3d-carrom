﻿using UnityEngine;

namespace com.aquimo.util
{
	public class SyncTransform : MonoBehaviour
	{
		public Transform target;

		public SyncValue syncValues = SyncValue.All;

		// Update is called once per frame
		void Update ()
		{
			if (target == null)
				return;
			
			if ((syncValues & SyncValue.None) == SyncValue.None)
				return;

			if ((syncValues & SyncValue.Position) == SyncValue.Position)
				transform.position = target.position;

			if ((syncValues & SyncValue.Rotation) == SyncValue.Rotation)
				transform.rotation = target.rotation;

			if ((syncValues & SyncValue.Scaling) == SyncValue.Scaling)
				transform.localScale = target.localScale;
		}
	}

	[System.Flags]
	public enum SyncValue : byte
	{
		None = 2,
		All = 28,
		Position = 4,
		Rotation = 8,
		Scaling = 16,
	}


}