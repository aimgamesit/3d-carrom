using System;

namespace com.aquimo.util
{
	public static class MoneyFormat
	{
		public static string toMillionFormat (double floatNumber)
		{
			long thousand = 1000L;
			long million = 1000000L;
			long billion = 1000000000L;
			long trillion = 1000000000000L;
			long number = (long)Math.Round (floatNumber);
			if ((number >= thousand) && (number < million)) {
				float fraction = calculateFraction (number, thousand);
				return fraction.ToString () + "K";
			} else if ((number >= million) && (number < billion)) {
				float fraction = calculateFraction (number, million);
				return fraction.ToString () + "M";
			} else if ((number >= billion) && (number < trillion)) {
				float fraction = calculateFraction (number, billion);
				return fraction.ToString () + "B";
			}
			return number.ToString ();
		}
	
		public static float calculateFraction (long number, long divisor)
		{
			long truncate = (long)(number * 10L + (divisor / 2L)) / divisor;
			float fraction = (float)truncate * 0.10F;
			return fraction;
		}
	}
}