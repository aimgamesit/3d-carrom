﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace com.aquimo.util
{
	public class VideoLauncher : MonoBehaviour
	{
		public string videoPath;

		public UnityEvent onCompleteEvent;

		public bool onStart;

		// Use this for initialization
		void Start ()
		{
			if (onStart)
				StartCoroutine (playMovie (videoPath, onCompleteEvent));
		}

		public void playVideo (string path, UnityAction onComplete)
		{
			StartCoroutine (path, onComplete);
		}

		IEnumerator playMovie (string path, UnityAction onComplete)
		{
			startPlayer (path);
			yield return new WaitForEndOfFrame ();
			yield return new WaitForEndOfFrame ();
			onComplete.Invoke ();
		}

		IEnumerator playMovie (string path, UnityEvent onComplete)
		{
			startPlayer (path);
			yield return new WaitForEndOfFrame ();
			yield return new WaitForEndOfFrame ();
			onComplete.Invoke ();
		}

		void startPlayer (string path)
		{
			#if !UNITY_WEBGL
			Handheld.PlayFullScreenMovie (path, Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFill);
			#endif
		}

	}
}
