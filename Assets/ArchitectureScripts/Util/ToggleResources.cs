﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace com.aquimo.util
{
	public class ToggleResources : MonoBehaviour
	{
		public List<string> resourcePaths;
		private List<GameObject> loadedObjs = new List<GameObject> ();

		void OnEnable ()
		{
			StartCoroutine (loadResources ());
		}


		//TODO Optimize it for async
		IEnumerator loadResources ()
		{
			foreach (string resourcePath in resourcePaths) {

				GameObject gameObj = Resources.Load<GameObject> (resourcePath);
				if (gameObj != null) {
					loadedObjs.Add (Instantiate (gameObj) as GameObject);
				} else {
					Debug.LogWarning ("Loading Object through resource path fails.  " + resourcePath);  
				} 
			}

			yield return null;
		}

		void OnDisable ()
		{
			foreach (GameObject gameObj in loadedObjs) {
				if (gameObj != null)
					DestroyObject (gameObj);
			}
			Resources.UnloadUnusedAssets ();
		}
	}
}
