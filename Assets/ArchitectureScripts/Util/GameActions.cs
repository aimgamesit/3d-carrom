using UnityEngine;
using UnityEngine.Events;

namespace com.aquimo.util
{
	public delegate T GameAction<T,U> (U obj);

	public class GameEvents<T> : UnityEvent<T>
	{
		
		public GameEvents ()
		{

		}
		
	}
}
