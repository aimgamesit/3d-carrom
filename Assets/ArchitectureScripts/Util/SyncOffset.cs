﻿using UnityEngine;
using System.Collections;


namespace com.aquimo.util
{
	public class SyncOffset : MonoBehaviour
	{
		public Transform target;

		Vector3 mOffset = Vector3.zero;

		Vector3 direction = Vector3.one;

		void OnEnable ()
		{
			if (target != null)
				mOffset = transform.position - target.position;
		}

		void Update ()
		{
			if (target != null)
				transform.position = transform.GetOffsetPosition (target, mOffset, direction);
		}

		public void setDirection (Vector3 mDirection)
		{
			this.direction = mDirection;
		}

		public Vector3 getOffset ()
		{
			return mOffset;
		}
	}
}