using System;
using UnityEngine;
using com.aquimo.util;
using System.Collections;

namespace com.aquimo.football.util
{
	public static class M
	{

		#region Throw Logic

		public static Vector3 GetBallisticVelocity (Rigidbody rBody, Vector3 target)
		{
			float time = 0;
			return GetBallisticVelocity (rBody, target, time);
		}

		public static Vector3 GetBallisticVelocity (Rigidbody rBody, Vector3 target, float time)
		{
			var direction = target - rBody.transform.position;  // get target direction
		
			var height = direction.y + .3f;  // get height difference 
			direction.y = 0;  // retain only the horizontal direction
			var distance = direction.magnitude;  // get horizontal distance
		
//			float angle = Mathf.Atan (height / distance);
//			float angle = Mathf.Abs (Mathf.Atan ((4 * height) / (distance)));// Mathf.Atan ((9.8f * (time * time)) / (2 * distance));
			float angle = 0.3f;

			direction.y = (distance) * Mathf.Tan (angle); 
			distance += height / Mathf.Tan (angle);  
		
		
			float rawVelocity = distance * Physics.gravity.magnitude / Mathf.Sin (2 * angle);
			var vel = Mathf.Sqrt (Mathf.Abs (rawVelocity));
		
//			float tTime = (2 * vel * Mathf.Sin (angle)) / Physics.gravity.magnitude;
			float pTime = (direction.magnitude) / (vel * Mathf.Cos (angle));
			time = pTime;

			return vel * direction.normalized;
		}

		#endregion

		public static bool IsWithinTime (DayTime startDayTime, DayTime endDayTime, DateTime cTime)
		{
			return IsWithinTime (startDayTime, endDayTime, new TimeStamp (cTime, cTime), cTime);
		}

		public static bool IsWithinTime (DayTime startDayTime, DayTime endDayTime, TimeStamp mTime, DateTime cTime)
		{
			if (endDayTime.day == Day.AnyDay) {
				return mTime.endTime.Subtract (cTime).TotalSeconds > 0; 
			}

			if (!mTime.startTime.IsInSameWeek (cTime)) {
				return false;
			}

			int cDayValue = cTime.GetWeekDayValue ();//
			bool isWithInLeagueDay = (cDayValue >= startDayTime.dayValue) && cDayValue <= endDayTime.dayValue;//
			if (!isWithInLeagueDay)
				return false;//

			bool isWithInStartTime = cDayValue != startDayTime.dayValue || cTime.Hour >= startDayTime.time;
			bool isWithInEndTime = cDayValue != endDayTime.dayValue || cTime.Hour <= endDayTime.time;
			bool isWithInLeagueTime = isWithInStartTime && isWithInEndTime;        

			return isWithInLeagueTime;
		}

		#region Tackling Logic

		public static float GetCollisionAngle (Vector3 other, Vector3 hitobject, bool eulerAngle = true)
		{
			float angle = 0;
			Vector3 pointB = hitobject - other;
			angle = Mathf.Atan2 (pointB.x, pointB.z);

			if (eulerAngle)
				return (360 - ((angle * 180) / Mathf.PI)) % 360;
			
			return angle * Mathf.Rad2Deg;
		}

		public static int GetZoneIndex (float angle, float anglePerDivision = 22.5f, int totalZone = 16)
		{
			float quadrant = (angle / anglePerDivision) % totalZone + 1;
			int zoneIndex = (int)(quadrant / 2);  
			zoneIndex = (zoneIndex == (totalZone / 2)) ? 0 : zoneIndex;
			return zoneIndex;
		}

		#endregion

		#region Ball Direction

		public static Vector3 GetBallTargetPosition (Rigidbody rBody, Transform target, float velocity, float angle = 0)
		{
			Vector3 ballPosition = rBody.transform.position;//

			Vector3 direction = GetDirection (ballPosition, target.position, Vector3.right);

			return ballPosition + (direction * getCoveredDistance (velocity, angle, rBody.transform.position.y));
		}

		public static Vector3 GetDirection (Vector3 from, Vector3 to, Vector3 axis)
		{
			Vector3 targetDir = to - from;
			float rAngle = Mathf.Deg2Rad * Vector3.Angle (targetDir, axis);
			return new Vector3 (1 * Mathf.Cos (rAngle), 0, 1 * Mathf.Sin (rAngle));//
		}

		static float getCoveredDistance (float initialVelocity, float angle, float height)
		{
			float y = height;//

			float v = initialVelocity;
			float g = Mathf.Abs (Physics.gravity.y);

			float sin = Mathf.Sin (angle);
			float cos = Mathf.Cos (angle);

			return ((v * cos) / g) * ((v * sin) + (Mathf.Sqrt ((square (v) * square (sin)) + (2 * g * y))));
		}

		static float square (float value)
		{
			return value * value;
		}

		#endregion

		public	static IEnumerator ScaleOverTime (Transform obj, float time)
		{
			Vector3 originalScale = obj.localScale;
			Vector3 destinationScale = new Vector3 (0.1f, 0.1f, 0.1f);

			float currentTime = 0.0f;

			do {
				obj.localScale = Vector3.Lerp (originalScale, destinationScale, currentTime / time);
				currentTime += Time.deltaTime;
				yield return null;
			} while (currentTime <= time);

			//Destroy (obj);
		}
	
	}

	public struct EventAnimInfo
	{
		public byte eventType;

		public int animIndex;

		public EventAnimInfo (byte eventType, int animIndex)
		{
			this.eventType = eventType;
			this.animIndex = animIndex;
		}

		public EventAnimInfo (byte eventType)
		{
			this.eventType = eventType;
			this.animIndex = 0;
		}
		
	}

}



