﻿using UnityEngine;

namespace com.aquimo.util
{
	[ExecuteInEditMode]
	public class ScriptedChild : MonoBehaviour
	{
		public bool syncInEditor;

		public bool isValueInit;
		public Transform parent;

		public Vector3 syncPosition = Vector3.one;
		public Vector3 syncRotation = Vector3.one;

		Vector3 cPosition, cRotation;

		//Logic Variables
		Matrix4x4 parentMatrix;

		//		Vector3 startParentPosition;
		Quaternion startParentRotationQ;

		Vector3 startChildPosition;
		Quaternion startChildRotationQ;

		void Update ()
		{
			if (parent == null)
				return;

			if (!syncInEditor && !Application.isPlaying)
				return;

			if (!isValueInit)
				initValue ();

			parentMatrix = Matrix4x4.TRS (getPosition (), getRotation (), parent.lossyScale);

			transform.position = parentMatrix.MultiplyPoint3x4 (startChildPosition);

			transform.rotation = (getRotation () * Quaternion.Inverse (startParentRotationQ)) * startChildRotationQ;
		}

		void initValue ()
		{
			isValueInit = true;

			//startParentPosition = parent.position;
			startParentRotationQ = parent.rotation;

			startChildPosition = transform.position;
			startChildRotationQ = transform.rotation;
		}

		[ContextMenu ("Update Current Value")]
		void storeCurrentValue ()
		{
			cPosition = parent.position;
			cRotation = parent.rotation.eulerAngles;
		}

		Vector3 getPosition ()
		{
			float x = syncPosition.x == 0 ? cPosition.x : parent.position.x;
			float y = syncPosition.y == 0 ? cPosition.y : parent.position.y;
			float z = syncPosition.z == 0 ? cPosition.z : parent.position.z;
			return new Vector3 (x, y, z);
		}

		Quaternion getRotation ()
		{
			float x = syncRotation.x == 0 ? cRotation.x : parent.rotation.eulerAngles.x;
			float y = syncRotation.y == 0 ? cRotation.y : parent.rotation.eulerAngles.y;
			float z = syncRotation.z == 0 ? cRotation.z : parent.rotation.eulerAngles.z;
			return Quaternion.Euler (x, y, z);
		}


	}
}
