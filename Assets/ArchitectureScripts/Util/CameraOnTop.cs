﻿using UnityEngine;
using System.Collections;

namespace com.aquimo.util
{
	[RequireComponent(typeof(Camera))]
	public class CameraOnTop : MonoBehaviour
	{
		Camera uiCam;

		void Start ()
		{
			uiCam = GetComponent<Camera> ();
			float maxDepth = uiCam.depth;
			foreach (Camera cam in  Camera.allCameras) {
				if (maxDepth < cam.depth) {
					maxDepth = cam.depth;
				}
			}
			uiCam.depth = maxDepth + 1;
			uiCam.backgroundColor = Color.black;
		}

		void Update ()
		{
			if (Camera.allCamerasCount == 1) {
				uiCam.clearFlags = CameraClearFlags.Color;
			} else {
				uiCam.clearFlags = CameraClearFlags.Depth;
			}
		}
	}
	
}