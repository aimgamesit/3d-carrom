﻿using UnityEngine;
using System.Collections;
using com.aquimo.util;

public class ShakeCamera : MonoBehaviour
{

	public bool shakePosition;
	public bool shakeRotation;

	public float shakeDecay = 0.02f;
	public float shakeIntensity = 0.12f;

	private Vector3 OriginalPos;
	private Quaternion OriginalRot;

	private bool isShakeRunning = false;

	[ContextMenu ("Shake")]
	public void init (bool shakePosition = false)
	{
		if (isShakeRunning) {
			Debug.LogWarning ("Shake Already in process.");
			return;
		}

		this.shakePosition = shakePosition;
		this.shakeRotation = !shakePosition;

		OriginalPos = transform.position;
		OriginalRot = transform.rotation;
		StartCoroutine (process ());
	}

	IEnumerator process ()
	{
		if (!isShakeRunning) {
			isShakeRunning = true;
			float currentShakeIntensity = shakeIntensity;

			while (currentShakeIntensity > 0) {
				if (shakePosition) {
					transform.position = OriginalPos + Random.insideUnitSphere * currentShakeIntensity;
				}
				if (shakeRotation) {
					transform.rotation = new Quaternion (OriginalRot.x + Random.Range (-currentShakeIntensity, currentShakeIntensity) * .2f,
						OriginalRot.y + Random.Range (-currentShakeIntensity, currentShakeIntensity) * .2f,
						OriginalRot.z + Random.Range (-currentShakeIntensity, currentShakeIntensity) * .2f,
						OriginalRot.w + Random.Range (-currentShakeIntensity, currentShakeIntensity) * .2f);
				}
				currentShakeIntensity -= shakeDecay;
				yield return null;
			}

			isShakeRunning = false;
		}
	}
}