﻿using UnityEngine;
using System.Collections;

namespace com.aquimo.util
{
	public class PassColliderEvents : MonoBehaviour
	{
		[Tooltip ("No need to put parent and child")]
		public Transform[]
			eventListeners;

		void OnTriggerEnter (Collider hitCollider)
		{
			foreach (var eventListener in eventListeners) {
				IPassTriggerEvent tEvent = eventListener.GetComponent<IPassTriggerEvent> ();
				if (tEvent != null) {
					tEvent.OnEnterTrigger (transform, hitCollider);
				}
			}
		}
	}

	public interface IPassTriggerEvent
	{
		void OnEnterTrigger (Transform passObj, Collider other);
	}
}