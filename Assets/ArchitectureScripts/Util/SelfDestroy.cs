﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace com.aquimo.util
{
    public class SelfDestroy : MonoBehaviour
    {
        public bool isFadeOut = false;
        public bool isFadeIn = false;
        public Text text;
        private float cTime = 0;
        public float time = 1.0f;

        void Update()
        {
            if (cTime > time)
            {
                if (!isFadeOut && !isFadeIn)
                    Destroy(gameObject);
            }

            cTime += Time.deltaTime;

            if (isFadeOut)
                StartCoroutine(FadeTo(0.0f, 2.0f));
            if (isFadeIn)
                StartCoroutine(FadeTo(1.0f, 2.0f));
        }

        IEnumerator FadeTo(float aValue, float aTime)
        {
            float alpha = text.color.a;
            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
            {
                Color newColor = new Color(text.color.r, text.color.g, text.color.b, Mathf.Lerp(alpha, aValue, t));
                text.color = newColor;
                yield return null;
            }
            Destroy(gameObject);
        }
    }
}