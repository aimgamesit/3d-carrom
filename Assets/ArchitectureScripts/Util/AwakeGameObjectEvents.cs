﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace com.aquimo.util
{
	public class AwakeGameObjectEvents : MonoBehaviour
	{
		public UnityEvent gameObjectEvent;
		public EventMethod eventMethod;

		void OnEnable ()
		{
			invokeEventMethod (EventMethod.OnEnable);
		}

		// Use this for initialization
		void Awake ()
		{
			invokeEventMethod (EventMethod.Awake);
		}

		void Start ()
		{
			invokeEventMethod (EventMethod.Start);
		}


		void invokeEventMethod (EventMethod invokeMethod)
		{
			if (eventMethod == invokeMethod)
				gameObjectEvent.Invoke ();
		}

		public void DoNotDestroyOnLoad ()
		{
			DontDestroyOnLoad (this.gameObject);
		}

		public void LoadLevel (string levelName)
		{
			SceneManager.LoadScene (levelName);
		}

		public void LoadLevel (int levelIndex)
		{
			SceneManager.LoadScene (levelIndex);
		}

		public void AcitveInUnityEditor (GameObject target)
		{
			#if UNITY_EDITOR
			target.SetActive (true);
			#endif
		}

	}

	public enum EventMethod
	{
		OnEnable,
		Awake,
		Start,
	}


}