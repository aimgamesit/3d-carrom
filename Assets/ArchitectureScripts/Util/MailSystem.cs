﻿using UnityEngine;
using System.Collections;
using System;

#if UNITY_IOS
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
#endif
namespace com.aquimo.football.util
{
	public class MailSystem : MonoBehaviour
	{
		
		public static MailSystem instance;

		void Start ()
		{
			instance = this;
		}

		public void SendMail (string toId,
		                      string ccId,
		                      string subject,
		                      string message,
		                      string fromId = "aquimollc@gmail.com")
		{
			#if UNITY_IOS
			MailMessage mail = new MailMessage ();

			mail.From = new MailAddress (fromId);
			//		for (int i = 0; i < toId.Length; i++)
			//		{
			//			mail.To.Add (toId [i]);
			//		}
			mail.To.Add (toId);
			mail.CC.Add (ccId);
			mail.Subject = subject;
			mail.Body = message;

			SmtpClient smtpServer = new SmtpClient ("smtp.gmail.com");
			smtpServer.Port = 587;
			smtpServer.Credentials = new System.Net.NetworkCredential (fromId, "aquimo123") as ICredentialsByHost;
			smtpServer.EnableSsl = true;
			ServicePointManager.ServerCertificateValidationCallback = 
				delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{
				return true;
			};
			smtpServer.Send (mail);
			Debug.Log ("success");
			#endif
		}

	}
}
