﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public static class MInput
{

	public static bool IsPointerOnUI {
		get {
			return EventSystem.current.IsPointerOverGameObject ();
		}	
	}

	public static  bool GetMouseButtonDown (int button)
	{
		return !IsPointerOnUI && Input.GetMouseButtonDown (0);
	}

	public static  bool GetMouseButtonUp (int button)
	{
		return !IsPointerOnUI && Input.GetMouseButtonUp (0);
	}

}
