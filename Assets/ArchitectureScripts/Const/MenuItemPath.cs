﻿using UnityEngine;
using System.Collections;

namespace com.aquimo.football.util
{
    public class MenuItemPath : MonoBehaviour
    {
        public const string TICTOK_PLAY_DATA = "TicTok/PlayData";
        public const string RT_EVENT_DATA = "TicTok/RTEventData";
        public const string TICTOK_NETWORK_DATA = "TicTok/Network";
        public const string API_DATA = "API/APIData";
        public const string GAME_DATA = "GameData/GameData";
    }
}