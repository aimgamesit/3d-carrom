﻿internal static class GameObjTags
{
    internal const string PLAYER_TAG = "player";
    internal const string ROCKET_ENEMY = "rocket_enemy";
    internal const string BIG_ENEMY = "big_enemy";
    internal const string COIN_TAG = "coin";
    internal const string ENEMY_QUEST = "enemy_quest";
    internal const string ENEMY_SMOKE = "enemy_smoke";
    internal static string RED_ENEMY = "red_enemy";
    internal static string RED_ROCKET_ENEMY = "red_rocket_enemy";
}