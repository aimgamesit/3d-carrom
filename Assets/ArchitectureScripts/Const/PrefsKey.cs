﻿using UnityEngine;

public class PrefsKey
{
    public const string SOUND_EFFECT = "sound_state";
    public const string TUTORIAL_COUNT = "TUTORIAL_COUNT";

    #region Save and Get Functions
    public static void SetInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
        PlayerPrefs.Save();
    }
    public static int GetInt(string key, int defaultValue = 0) => PlayerPrefs.GetInt(key, defaultValue);

    public static void SetString(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
        PlayerPrefs.Save();
    }
    public static string GetString(string key, string defaultValue = "") => PlayerPrefs.GetString(key, defaultValue);
    #endregion
}