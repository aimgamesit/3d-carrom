﻿using com.aquimo.util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldDataInfo : IInfo
{
    public int selectedPlayerIndex = -1;
    public int selectedEnemyIndex = 0;
    public int currentEnvId = 0;
    public bool isPlayWithOneVsOne = false;
    public int killed = 0;
    public bool isTrainingRoom = false;
}