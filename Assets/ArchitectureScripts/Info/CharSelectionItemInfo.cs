﻿using UnityEngine;
using UnityEngine.UI;

public class CharSelectionItemInfo : MonoBehaviour
{
    public Image profileImg;
    public Text name;
    public Slider powerSlider;
    public Slider speedSlider;
    public Slider jumpSlider;
    public Button clickButton;
}
