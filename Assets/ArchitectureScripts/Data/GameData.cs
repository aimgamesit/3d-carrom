﻿using UnityEngine;
using com.aquimo.data;
using com.aquimo.football.util;
using System.Collections.Generic;
using UnityEngine.UI;
using AIMCarrom;

#region Load Data from resources folder and if not create asset file .
public partial class GameData : BData<GameData>
{
    public override string getResrcPath()
    {
        return ResrcPath.API_DATA;
    }
#if UNITY_EDITOR

    [UnityEditor.MenuItem(MenuItemPath.GAME_DATA)]
    public static void Edit()
    {
        UnityEditor.Selection.activeObject = Instance;
    }
#endif
}
#endregion

public partial class GameData
{
    public string CARROM_GAME_SCENE = "CarromGameScene";
    public string PUZZLE_GAME_SCENE = "BlockPuzzleGame";
    public string HEXAGON_GAME_SCENE = "HexagonScene";
    public string LUDO_GAME_SCENE = "LudoGameScene";
    public string STACK_BALL_GAME_SCENE = "StackBall3DGame";
    public string MENU_SCENE = "Menu";
    [Header("Game Setting")]
    [Range(.01f, 1.0f)]
    public float popupScaleTime = 0.2f;
    public int loginCoins = 2000;
    public int rateUsScreenCounter = 2;
    [Header("AdMob Settings")]
    public bool isLogEnabled = true;
    public bool isTestAds = true;
    public int adLoadTime = 3;
    public int freeReward = 100;
    public int queenCoins = 500;
    public string interstitialAdID = "";
    public string bannerAdID = "";
    public string rewardAdID = "";
    public string nativeAdID = "";
    public string appID = "ca-app-pub-8996771506370953~1159894615";
    [Header("Runtime Game Data"), Space(5)]
    public int selectedThemeIndex;
    public AIDifficultyType aIDifficultyType;
    [Header("Carrom Game Data"), Space(5)]
    public List<ThemeData> themeData;
    public ShopItemDataModel[] pucks;
    public ShopItemDataModel[] strikers;
    public ShopItemDataModel[] avatars;
    public Sprite defaultAvatar;
    [Header("Block Puzzle Game Data"), Space(5)]
    public Sprite[] boxSprites;
    public Sprite emptyBoxSprite;
  
    public int undoCount;
    public AIMLudo.Utility.ColorIndex colorIndex;
}

[System.Serializable]
public struct ThemeData
{
    public string themeName;
    public int themeEntryFee;
    public int themePrize;
    public Sprite bg;
    public Color glowColor;
}
[System.Serializable]
public struct ShopItemDataModel
{
    public Sprite sprite;
    public int buyCoins;
}