﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class MerqueeUI : MonoBehaviour
{
    [SerializeField] RectTransform content;
    [SerializeField] private Text mainText;
    [SerializeField] private Text duplicateText;

    public float speed;
    Coroutine marqueeRoutine;
    private float marqueeRectWidth;

    void Start()
    {

    }

    private void OnEnable()
    {
        //Debug.LogFormat("" + mainText.GetComponent<LayoutElement>().minWidth);
        //Debug.LogFormat("" + content.parent.GetComponent<RectTransform>().rect.width);

        //if (mainText.GetComponent<LayoutElement>().minWidth <= content.parent.GetComponent<RectTransform>().rect.width)
        //{
        //    if (marqueeRoutine != null)
        //        StopCoroutine(marqueeRoutine);
        //    return;
        //}

        if (marqueeRoutine != null)
            StopCoroutine(marqueeRoutine);
        marqueeRoutine = StartCoroutine(Marquee());

        //CancelInvoke(nameof(UpdateText));
        //Invoke(nameof(UpdateText), 0.5f);

    }


    private void UpdateText()
    {
        StringBuilder stringBuilder = new StringBuilder(mainText.text);
        stringBuilder.Append("\t");
        mainText.text = stringBuilder.ToString();
    }

    IEnumerator Marquee()
    {
        yield return new WaitForSeconds(0.5f);
        float ratio = ((float)Screen.width / (float)Screen.height);
        //var minWidth = ratio * 280f;
        var minWidth = content.transform.parent.GetComponent<RectTransform>().sizeDelta.x * 1.5f;
        mainText.GetComponent<LayoutElement>().minWidth = minWidth;
        duplicateText.GetComponent<LayoutElement>().minWidth = minWidth;
        if (marqueeRectWidth == 0)
            marqueeRectWidth = mainText.GetComponent<RectTransform>().sizeDelta.x * (ratio > 0.7f ? 3 : 2) + GetComponent<HorizontalLayoutGroup>().spacing + GetComponent<HorizontalLayoutGroup>().padding.left;
        content.sizeDelta = new Vector2(marqueeRectWidth, content.sizeDelta.y);
        content.anchoredPosition = new Vector2(0, content.anchoredPosition.y);
        yield return new WaitForSeconds(2);
        GetComponent<HorizontalLayoutGroup>().childScaleWidth = true;


        while (true)
        {
            duplicateText.text = mainText.text;


            if (Mathf.Abs(content.anchoredPosition.x) > content.sizeDelta.x / 2)
            {
                content.anchoredPosition = new Vector2(0, content.anchoredPosition.y);
            }
            content.Translate(Vector3.left * speed * Time.deltaTime);

            yield return null;
        }


    }

    private void OnDisable()
    {
        if (marqueeRoutine != null)
            StopCoroutine(marqueeRoutine);
    }
}
