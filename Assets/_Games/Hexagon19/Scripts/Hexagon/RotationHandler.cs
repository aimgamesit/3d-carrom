﻿using UnityEngine;
using System.Collections;

public class RotationHandler : MonoBehaviour
{
	public bool mIsLeft = true;
	public static bool mIsRotating = true;
	public float mSpeed = 0f;

	void Start ()
	{
	
	}

	void Update ()
	{
		if (!mIsRotating)
			return;
		
		if (mIsLeft)
			transform.Rotate (0, 0, Time.deltaTime * mSpeed, Space.World);
		else
			transform.Rotate (0, 0, -Time.deltaTime * mSpeed, Space.World);
	}
}