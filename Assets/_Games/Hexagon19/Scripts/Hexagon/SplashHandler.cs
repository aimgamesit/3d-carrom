﻿using UnityEngine;
using System.Collections;

namespace HexagonBlockNS
{
	public class SplashHandler : MonoBehaviour
	{
		public string mNextSceneName = "Main_Menu_Scene";

		void Start ()
		{
			StartCoroutine (LoadingNextScene ());
		}

		private IEnumerator LoadingNextScene ()
		{
			int counter = 1;
			while (counter < 4) {
				yield return  new WaitForSeconds (1f);
				counter++;
			}
			Application.LoadLevel (mNextSceneName);
		}
	}
}