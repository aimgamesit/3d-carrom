﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour
{
    public AudioClip[] _AudioClips;
    public AudioSource bgAudioSource;
    public AudioSource[] audioSource;

    public void PlayOneShot(int aIndexOfClip)
    {
        if (AIMUtils.GetSoundStatus() == AIMUtils.SOUND_OFF)
            return;
        try
        {
            if (audioSource[0].isPlaying)
            {
                audioSource[1].clip = _AudioClips[aIndexOfClip];
                audioSource[1].Play();
            }
            else
            {
                audioSource[0].clip = _AudioClips[aIndexOfClip];
                audioSource[0].Play();
            }
        }
        catch (System.Exception e)
        {

        }
    }

    public void PlayBackgroundMusic(bool aIsLoop)
    {
        if (AIMUtils.GetSoundStatus() == AIMUtils.SOUND_OFF)
            return;

        if (bgAudioSource.isPlaying)
            return;

        bgAudioSource.loop = aIsLoop;
        bgAudioSource.Play();
    }

    public void StopBackgroundMusic()
    {
        bgAudioSource.Stop();
    }
}