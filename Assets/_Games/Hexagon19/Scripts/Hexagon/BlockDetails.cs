﻿using UnityEngine;
using System.Collections;

namespace HexagonBlockNS
{
    [System.Serializable]
    public class BlockDetails
    {
        public GameObject mBlockObj = null;
        public int mHexaNo = -1;
    }
}