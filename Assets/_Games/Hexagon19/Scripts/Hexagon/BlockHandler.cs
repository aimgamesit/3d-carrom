﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HexagonBlockNS
{
    public class BlockHandler : MonoBehaviour
    {
        public static int GRID_X = 5, GRID_Y = 5, MIN_TILE_COLOR = 1, MAX_TILE_COLOR = 30;
        private const float BLOCK_MOVE_SPEED = 6f, ROTATION_ANGLE = 60f;
        private Vector3 mStartPos = Vector3.zero, mObjPos = Vector3.zero;
        private bool mIsDragging = false;
        public static int mHexaCounter = 1, mMaxBlock = 2;
        private int mIsComboCount = 0, mComboCounter = 0;
        public List<int> mBlockHexaColorNo = new List<int>();
        public GameObject mComboAnimObj;

        void Start()
        {
            mStartPos = transform.position;
            int no = Random.Range(1, 4);
            float rotateAngle = 0f;
            if (no == 1)
                rotateAngle = 120.0f;
            else if (no == 2)
                rotateAngle = 180.0f;
            else if (no == 3)
                rotateAngle = 240.0f;

            RotateObject(rotateAngle, false);
            Color32 transparentColor = new Color32(255, 255, 255, 0);
            for (int ind = 0; ind < gameObject.transform.childCount; ind++)
            {
                mHexaCounter = Random.Range(MIN_TILE_COLOR, mMaxBlock + 1);
                gameObject.transform.GetChild(ind).GetComponent<Image>().sprite = GamePlayHandler.Instance.hexagonSprites[mHexaCounter - 1];
                mBlockHexaColorNo.Add(mHexaCounter);
                gameObject.transform.GetChild(ind).transform.GetChild(0).GetComponent<Text>().text = mHexaCounter + "";
                Vector3 rotation = gameObject.transform.GetChild(ind).transform.GetChild(0).transform.eulerAngles;
                rotation.z -= -rotateAngle;
                gameObject.transform.GetChild(ind).transform.GetChild(0).transform.eulerAngles = rotation;
            }
        }

        public void OnTouchBegin()
        {
            mIsDragging = false;
        }

        public void OnTouchTaped()
        {
            if (mIsDragging)
                return;
            RotateObject(ROTATION_ANGLE, true);
        }

        private void RotateObject(float aAngle, bool aIsChieldRotate)
        {
            GamePlayHandler.Instance.PlayOneShotPlayer(7);
            Vector3 rotation = gameObject.transform.eulerAngles;
            rotation.z -= aAngle;//0,-60,-120
            gameObject.transform.eulerAngles = rotation;
            if (aIsChieldRotate)
            {
                for (int ind = 0; ind < gameObject.transform.childCount; ind++)
                {
                    rotation = gameObject.transform.GetChild(ind).transform.GetChild(0).transform.eulerAngles;
                    rotation.z -= -aAngle;
                    gameObject.transform.GetChild(ind).transform.GetChild(0).transform.eulerAngles = rotation;
                }
            }
        }

        public void OnTouchDrag()
        {
            mObjPos = Input.mousePosition;
            mObjPos.y += 80;
            mObjPos.z = 0;
            mObjPos = Camera.main.ScreenToWorldPoint(mObjPos);
            mObjPos.z = 0;
            gameObject.transform.position = mObjPos;
            gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, 0.0f);
            mIsDragging = true;
        }

        public void OnTouchEnd()
        {
            BlockExist();
        }

        private void BlockExist()
        {
            //Test();
            //Debug.Log("Checking For Block EExisting.................");

            int firstBlock = -1, secondBlock = -1;

            for (int ind = 0; ind < GamePlayHandler.Instance.mColliderBlockArray.Count; ind++)
            {
                for (int ind1 = 0; ind1 < transform.childCount; ind1++)
                {
                    if (!GamePlayHandler.Instance.mBlocksArray[ind].mBlockObj.activeSelf && GamePlayHandler.Instance.mColliderBlockArray[ind].activeSelf)
                    {
                        //Debug.Log(ind1 + " :NAME:" + transform.GetChild(ind1).name + "  :" + ind + " : " + GamePlayHandler.mInstance.mBlocksArray[ind].mBlockObj.activeSelf + ":" + GamePlayHandler.mInstance.mColliderBlockArray[ind].activeSelf);
                        if (GamePlayHandler.Instance.mColliderBlockArray[ind].GetComponent<BoxCollider2D>().bounds.Contains(transform.GetChild(ind1).transform.position))
                        {
                            Debug.Log(ind + "===================" + ind1);
                            if (transform.GetChild(ind1).name.Equals("B_0"))
                            {
                                firstBlock = ind;
                                //Debug.Log(firstBlock + " NAME:   " + transform.GetChild(ind1).name);
                            }
                            else if (transform.GetChild(ind1).name.Equals("B_1"))
                            {
                                secondBlock = ind;
                                //Debug.Log(secondBlock + " NAME:   " + transform.GetChild(ind1).name);
                            }
                        }
                    }
                }
            }

            if ((firstBlock == -1 || secondBlock == -1) && transform.childCount == 2)
            {
                //Debug.Log(firstBlock + " 22222 Not Exist  " + secondBlock);
                transform.position = mStartPos;
                return;
            }
            else if (firstBlock == -1)
            {
                //Debug.Log(firstBlock + " 111111 Not Exist  " + secondBlock);
                transform.position = mStartPos;
                return;
            }

            if (transform.childCount == 1)
            {
                if (firstBlock != -1)
                {
                    //Debug.Log("One Exist");
                    GamePlayHandler.Instance.mBlocksArray[firstBlock].mBlockObj.SetActive(true);
                    GamePlayHandler.Instance.mBlocksArray[firstBlock].mHexaNo = mBlockHexaColorNo[0];

                    GamePlayHandler.Instance.mBlocksArray[firstBlock].mBlockObj.transform.GetChild(0).GetComponent<Text>().text = mBlockHexaColorNo[0] + "";
                    GamePlayHandler.Instance.mBlocksArray[firstBlock].mBlockObj.GetComponent<Image>().sprite = GamePlayHandler.Instance.hexagonSprites[mBlockHexaColorNo[0] - 1];

                    List<int> blockIndex = new List<int>();
                    List<int> blockHexaColorNos = new List<int>();
                    blockIndex.Add(firstBlock);

                    blockHexaColorNos.Add(mBlockHexaColorNo[0]);

                    StartCoroutine(CheckAround(blockIndex, blockHexaColorNos));
                }
            }
            else if (transform.childCount == 2)
            {
                if (firstBlock != -1 && secondBlock != -1)
                {
                    // Debug.Log("Two Exist");
                    GamePlayHandler.Instance.mBlocksArray[firstBlock].mBlockObj.SetActive(true);
                    GamePlayHandler.Instance.mBlocksArray[firstBlock].mHexaNo = mBlockHexaColorNo[0];

                    GamePlayHandler.Instance.mBlocksArray[firstBlock].mBlockObj.transform.GetChild(0).GetComponent<Text>().text = mBlockHexaColorNo[0] + "";

                    GamePlayHandler.Instance.mBlocksArray[secondBlock].mBlockObj.SetActive(true);
                    GamePlayHandler.Instance.mBlocksArray[secondBlock].mHexaNo = mBlockHexaColorNo[1];

                    GamePlayHandler.Instance.mBlocksArray[secondBlock].mBlockObj.transform.GetChild(0).GetComponent<Text>().text = mBlockHexaColorNo[1] + "";

                    GamePlayHandler.Instance.mBlocksArray[firstBlock].mBlockObj.GetComponent<Image>().sprite = GamePlayHandler.Instance.hexagonSprites[mBlockHexaColorNo[0] - 1];

                    GamePlayHandler.Instance.mBlocksArray[secondBlock].mBlockObj.GetComponent<Image>().sprite = GamePlayHandler.Instance.hexagonSprites[mBlockHexaColorNo[1] - 1];

                    List<int> blockIndex = new List<int>();
                    List<int> blockHexaColorNos = new List<int>();
                    blockIndex.Add(firstBlock);
                    blockIndex.Add(secondBlock);

                    blockHexaColorNos.Add(mBlockHexaColorNo[0]);
                    blockHexaColorNos.Add(mBlockHexaColorNo[1]);

                    StartCoroutine(CheckAround(blockIndex, blockHexaColorNos));
                }
            }
        }

        public static bool IsLeft(int aIndexToSearch)
        {
            bool status = false;
            int[] leftIndexes = { 0, 6, 10, 16, 21 };

            for (int ind = 0; ind < leftIndexes.Length; ind++)
            {
                if (aIndexToSearch == leftIndexes[ind])
                {
                    status = true;
                    ind = leftIndexes.Length + 5;
                }
            }
            return status;
        }

        public static bool IsRight(int aIndexToSearch)
        {
            bool status = false;
            int[] rightIndexes = { 3, 9, 14, 19, 23 };

            for (int ind = 0; ind < rightIndexes.Length; ind++)
            {
                if (aIndexToSearch == rightIndexes[ind])
                {
                    status = true;
                    ind = rightIndexes.Length + 5;
                }
            }
            return status;
        }

        public static bool IsIndexExist(int aIndexToSearch)
        {
            bool status = false;
            int[] rightIndexes = { 0, 4, 5, 15, 34, 20, 24 };

            for (int ind = 0; ind < rightIndexes.Length; ind++)
            {
                if (aIndexToSearch == rightIndexes[ind])
                {
                    status = true;
                    ind = rightIndexes.Length + 5;
                }
            }
            return status;
        }

        private IEnumerator CheckAround(List<int> aBlockIndex, List<int> aBlockHexaColorNos)
        {
            List<int> aroundFoundedIndexes = new List<int>();
            for (int index = 0; index < aBlockIndex.Count; index++)
            {
                int aIndex = aBlockIndex[index];
                int aBlockHexaColorNo = aBlockHexaColorNos[index];

                List<int> aroundIndexes = new List<int>();

                int rowNo = GetRowNo(aIndex, GRID_X, GRID_Y);
                //				Debug.Log (aBlockIndex.Count + "  Checking Around >>>>>>>>>  " + aBlockHexaColorNo);

                if (IsLeft(aIndex))
                {

                    aroundIndexes.Add(aIndex + 1);//For Right

                    aroundIndexes.Add(aIndex - GRID_X);//For Up
                    aroundIndexes.Add(aIndex + GRID_X);//For Down

                    if (rowNo % 2 == 0)
                    {//For Even Digonal
                        aroundIndexes.Add((aIndex - GRID_X) + 1);//For Up
                        aroundIndexes.Add((aIndex + GRID_X) + 1);//For Down

                        if (rowNo > 2)
                        {
                            aroundIndexes.Add((aIndex - GRID_X) - 1);
                        }
                    }
                    else
                    {
                        if (rowNo >= 3)
                            aroundIndexes.Add((aIndex - GRID_X) - 1);//For Up

                        if (rowNo <= 2)
                        {
                            aroundIndexes.Add((aIndex + GRID_X) - 1);
                        }
                    }
                }
                else if (IsRight(aIndex))
                {

                    aroundIndexes.Add(aIndex - 1);//For Left

                    aroundIndexes.Add(aIndex - GRID_X);//For Up
                    aroundIndexes.Add(aIndex + GRID_X);//For Down
                    if (rowNo % 2 == 0)
                    {//For Even Digonal
                        if (aIndex < GRID_X)
                            aroundIndexes.Add((aIndex + GRID_X) + 1);//For Down
                        else if (rowNo == GRID_X - 1)
                            aroundIndexes.Add((aIndex - GRID_X) + 1);//For Up
                    }
                    else
                    {//For Odd Digonal
                        aroundIndexes.Add((aIndex - GRID_X) - 1);//For Up
                        aroundIndexes.Add((aIndex + GRID_X) - 1);//For Down
                    }
                }
                else
                {

                    aroundIndexes.Add(aIndex - 1);//For Left
                    aroundIndexes.Add(aIndex + 1);//For Right

                    aroundIndexes.Add(aIndex - GRID_X);//For Up
                    aroundIndexes.Add(aIndex + GRID_X);//For Down

                    if (rowNo % 2 == 0)
                    {//For Even Digonal
                        aroundIndexes.Add((aIndex - GRID_X) + 1);//For Up
                        aroundIndexes.Add((aIndex + GRID_X) + 1);//For Down
                    }
                    else
                    {//For Odd Digonal
                        aroundIndexes.Add((aIndex - GRID_X) - 1);//For Up
                        aroundIndexes.Add((aIndex + GRID_X) - 1);//For Down
                    }
                }
                bool isFound = false, isSame = false;

                for (int ind = 0; ind < aroundIndexes.Count; ind++)
                {
                    if (aroundIndexes[ind] > -1 && aroundIndexes[ind] < GRID_X * GRID_Y)
                    {
                        //					Debug.Log (aroundIndexes [ind] + " Check:  " + GamePlayHandler.mInstance.mBlocksArray [aroundIndexes [ind]].mHexaNo + " :  " + aBlockHexaColorNo);
                        if (GamePlayHandler.Instance.mBlocksArray[aroundIndexes[ind]].mHexaNo == aBlockHexaColorNo && GamePlayHandler.Instance.mColliderBlockArray[aroundIndexes[ind]].activeSelf)
                        {
                            aroundFoundedIndexes.Add(aroundIndexes[ind]);
                            isFound = true;
                            //							Debug.Log (aBlockHexaColorNo + "  Found at Another:  " + aroundIndexes [ind]);
                        }
                    }
                }
                if (aroundFoundedIndexes.Count > 0 && isFound)
                {
                    //					Debug.Log (aBlockHexaColorNo + "  Adddededdeded:  " + aIndex);
                    aroundFoundedIndexes.Add(aIndex);
                }
            }

            if (aroundFoundedIndexes.Count > 1)
            {
                int tileColorRandomNo = -1;

                for (int ind = 0; ind < transform.childCount; ind++)
                {
                    transform.GetChild(ind).GetComponent<Image>().color = new Color32(255, 255, 255, 0);
                    transform.GetChild(ind).transform.GetChild(0).GetComponent<Text>().color = new Color32(255, 255, 255, 0);
                }

                for (int ind = 0; ind < aroundFoundedIndexes.Count; ind++)
                {
                    if (tileColorRandomNo < GamePlayHandler.Instance.mBlocksArray[aroundFoundedIndexes[ind]].mHexaNo)
                        tileColorRandomNo = GamePlayHandler.Instance.mBlocksArray[aroundFoundedIndexes[ind]].mHexaNo;
                }
                int score = aroundFoundedIndexes.Count / 2;
                int comboScore = score;
                score--;
                score += tileColorRandomNo;
                GamePlayHandler.Instance.PlayOneShotPlayer(4);

                StartCoroutine(GamePlayHandler.Instance.SetScoreOnGame(score, aroundFoundedIndexes[0]));
                tileColorRandomNo++;

                if (tileColorRandomNo > MAX_TILE_COLOR)
                    tileColorRandomNo = MIN_TILE_COLOR;

                if (mMaxBlock < tileColorRandomNo)
                    mMaxBlock = tileColorRandomNo;
                for (int ind = 0; ind < aroundFoundedIndexes.Count; ind++)
                {
                    if (ind != 0)
                    {
                        int aIndex = aroundFoundedIndexes[ind];

                        Vector3 startPos = GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position;
                        GamePlayHandler.Instance.mColliderBlockArray[aIndex].transform.SetAsLastSibling();
                        while (Vector3.Distance(GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position, GamePlayHandler.Instance.mBlocksArray[aroundFoundedIndexes[0]].mBlockObj.transform.position) > BLOCK_MOVE_SPEED * Time.deltaTime)
                        {
                            GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position = Vector3.MoveTowards(GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position, GamePlayHandler.Instance.mBlocksArray[aroundFoundedIndexes[0]].mBlockObj.transform.position, BLOCK_MOVE_SPEED * Time.deltaTime);
                            yield return 0;
                        }

                        GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position = startPos;
                        GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.SetActive(false);
                    }
                    GamePlayHandler.Instance.mBlocksArray[aroundFoundedIndexes[ind]].mHexaNo = -1;
                }
                bool isSame = false;
                if (transform.childCount == 2 && mBlockHexaColorNo[0] == mBlockHexaColorNo[1])
                    isSame = true;
                if (comboScore >= 2 && !isSame)
                {
                    mComboCounter++;
                    GameObject obj = Instantiate(mComboAnimObj, GamePlayHandler.Instance.transform);
                    obj.transform.GetChild(0).GetComponent<Text>().text = "+" + mComboCounter;
                    obj.transform.SetAsLastSibling();
                }
                yield return new WaitForSeconds(.0001f);

                int randomNo = 0;
                GamePlayHandler.Instance.SetBlockData(aroundFoundedIndexes[randomNo], tileColorRandomNo);
                List<int> blockIndex1 = new List<int>();
                List<int> blockHexaColorNos1 = new List<int>();
                blockIndex1.Add(aroundFoundedIndexes[randomNo]);

                blockHexaColorNos1.Add(tileColorRandomNo);
                yield return new WaitForSeconds(0.4f);

                mIsComboCount++;
                if (mIsComboCount >= 2)
                {
                    mComboCounter++;
                    GameObject obj = Instantiate(mComboAnimObj, GamePlayHandler.Instance.transform);
                    obj.transform.GetChild(0).GetComponent<Text>().text = "+" + mComboCounter;
                    obj.transform.SetAsLastSibling();
                }

                StartCoroutine(CheckAround(blockIndex1, blockHexaColorNos1));
            }
            else
            {
                yield return new WaitForSeconds(.0001f);

                Destroy(gameObject);
                GamePlayHandler.Instance.CreateNewBox();
            }
        }

        public IEnumerator MoveAtSpeedCoroutine(int aIndex, Transform endPosition, float speed)
        {
            GamePlayHandler.Instance.mColliderBlockArray[aIndex].transform.SetAsLastSibling();
            Vector3 startPos = GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position;
            while (Vector3.Distance(GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position, endPosition.position) > speed * Time.deltaTime)
            {
                GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position = Vector3.MoveTowards(GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position, endPosition.position, speed * Time.deltaTime);
                yield return 0;
            }
            Debug.Log("Reached!  " + GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.name);
            GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.transform.position = startPos;
            GamePlayHandler.Instance.mBlocksArray[aIndex].mBlockObj.SetActive(false);
        }

        private int GetRowNo(int aIndex, int aGridRows, int aGridColms)
        {
            int counter = 0, status = -1;
            for (int ind = 0; ind < aGridRows; ind++)
            {
                for (int ind1 = 0; ind1 < aGridColms; ind1++)
                {
                    //					Debug.Log (counter + "    To Be Found........ " + aIndex);
                    if (counter == aIndex)
                    {
                        //						Debug.Log (counter + "    To Be Founded>>>>>>> " + aIndex);
                        status = ind;
                        ind1 = aGridColms + 2;
                    }
                    counter++;
                    if (status != -1)
                    {
                        ind = aGridRows + 2;
                    }
                }
            }
            return status;
        }

        public void Test()
        {
            for (int ind = 0; ind < GamePlayHandler.Instance.mColliderBlockArray.Count; ind++)
            {
                for (int ind1 = 0; ind1 < transform.childCount; ind1++)
                {
                    if (!GamePlayHandler.Instance.mBlocksArray[ind].mBlockObj.activeSelf && GamePlayHandler.Instance.mColliderBlockArray[ind].activeSelf)
                    {
                        Debug.Log(ind1 + " :NAME:" + transform.GetChild(ind1).name + "  :" + ind + " : " + GamePlayHandler.Instance.mBlocksArray[ind].mBlockObj.activeSelf + ":" + GamePlayHandler.Instance.mColliderBlockArray[ind].activeSelf);
                        if (GamePlayHandler.Instance.mColliderBlockArray[ind].GetComponent<BoxCollider2D>().bounds.Contains(transform.GetChild(ind1).transform.position))
                        {
                            Debug.Log(ind + "===================" + ind1);
                            if (transform.GetChild(ind1).name.Equals("B_0"))
                            {
                                Debug.Log(" NAME:   " + transform.GetChild(ind1).name);
                            }
                            else if (transform.GetChild(ind1).name.Equals("B_1"))
                            {
                                Debug.Log(" NAME:   " + transform.GetChild(ind1).name);
                            }
                        }
                    }
                }
            }
        }
    }
}