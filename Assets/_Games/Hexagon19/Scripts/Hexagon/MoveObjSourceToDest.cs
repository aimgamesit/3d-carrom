﻿using UnityEngine;
using System.Collections;

public class MoveObjSourceToDest : MonoBehaviour
{
	public Transform mDestinationTransform = null;
	public float mSpeed = 500f;

	void Start ()
	{
		StartCoroutine (MoveAtSpeedCoroutine (mDestinationTransform));
	}

	void Update ()
	{
		
	}

	public IEnumerator MoveAtSpeedCoroutine (Transform endPosition)
	{
		while (Vector3.Distance (transform.position, endPosition.position) > mSpeed * Time.deltaTime) {
			transform.position = Vector3.MoveTowards (transform.position, endPosition.position, mSpeed * Time.deltaTime);
			yield return 0;
		}
//		Debug.Log ("Reached!   " + gameObject.name);
		Destroy (gameObject);
	}
}