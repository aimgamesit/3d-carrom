﻿using UnityEngine;
using System.Collections;

public class AnimationEventHandler : MonoBehaviour
{
	public delegate void AnimationDelegate ();

	public static event AnimationDelegate _AnimationOverEvent=null;

	public void AnimationOverHandler ()
	{
		if (_AnimationOverEvent != null)
			_AnimationOverEvent ();
		_AnimationOverEvent = null;
	}
}