﻿using UnityEngine;
using System.Collections;

public class AIMUtils
{
	private const string BEST_INFINITY_SCORE_NAME = "BEST_INFINITY_SCORE_PREFAB";
	private const string POINS_PREFAB_NAME = "COINS_PREFAB_NAME_PREFAB";
	private const string MODE_PREFAB_NAME = "MODE_PREFAB_NAME";
	public const string SOUND_STATUS_REFAB_NAME = "SOUND_STATUS_REFAB_NAME";

	public const int SOUND_ON = 1, SOUND_OFF = 0;
    public const int REWARDED_Points=5;

	public static void SetBestScore (int aScore)
	{
		PlayerPrefs.SetInt (BEST_INFINITY_SCORE_NAME, aScore);
	}

	public static int GetBestScore ()
	{
		return PlayerPrefs.GetInt (BEST_INFINITY_SCORE_NAME, 0);
	}

	public static void SetDeletePoints (int aCoins)
	{
		PlayerPrefs.SetInt (POINS_PREFAB_NAME, aCoins);
	}

	public static int GetDeletePoints ()
	{
		return PlayerPrefs.GetInt (POINS_PREFAB_NAME, 10);
	}

	public static void SetSoundStatus (int aStatus)
	{
		PlayerPrefs.SetInt (SOUND_STATUS_REFAB_NAME, aStatus);
	}

	public static int GetSoundStatus ()
	{
		return PlayerPrefs.GetInt (SOUND_STATUS_REFAB_NAME, 1);
	}
}