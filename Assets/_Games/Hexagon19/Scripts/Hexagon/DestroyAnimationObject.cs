﻿using UnityEngine;
using System.Collections;

public class DestroyAnimationObject : MonoBehaviour
{
    public AudioSource audioSource;
    public bool mIsTimer = false;
    public float mDestroyTime = 0;

    void Start()
    {
        if (AIMUtils.GetSoundStatus() == AIMUtils.SOUND_ON && audioSource != null)
            audioSource.Play();

        if (mIsTimer)
            StartCoroutine(DestroyOtherObject());
    }

    private IEnumerator DestroyOtherObject()
    {
        float value = 0;
        Color32 color = new Color32(0, 0, 0, 255);
        while (value < mDestroyTime / 10)
        {
            value += mDestroyTime / 100;

            color.a -= 18;
            transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().color = color;
            yield return new WaitForSeconds(0.1f);
        }
        Destroy(gameObject);
    }

    public void DestroyAnimObject()
    {
        Destroy(gameObject);
    }
}