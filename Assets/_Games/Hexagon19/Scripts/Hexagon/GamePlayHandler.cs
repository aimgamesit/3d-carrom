﻿using AIMCarrom;
using com.TicTok.managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HexagonBlockNS
{
    public class GamePlayHandler : BManager<GamePlayHandler>
    {
        [SerializeField] GameObject loadingScreen;
        public AudioController oneShotAudioPlayer;
        [SerializeField] Toggle soundToggle;
        [Header("Ads")]
        [SerializeField] GameObject adNativePanel;
        [SerializeField] RawImage adIcon;
        [SerializeField] RawImage adChoices;
        [SerializeField] Text adHeadline;
        [SerializeField] Text adCallToAction;
        [SerializeField] Text adAdvertiser;
        [Space(20)]
        public Sprite[] hexagonSprites;
        public AIMPluginManager mAIMPluginManager;
        public Transform mNewPuzzlePos, mNewBlockContainer;
        public List<GameObject> mColliderBlockArray = new List<GameObject>();
        public List<BlockDetails> mBlocksArray = new List<BlockDetails>();
        private int mSingleBlockCounter = 0, mCurrentScore = 0, mVideoAdCounter = 0;
        private GameObject mCurrentHexa = null, mCurrentBoxes = null;
        public GameObject mGameCanvas, mDlgResumeButton, mBestScoreAnimation;
        public GameObject mHexaPrefab, mGameOverAnimObj;
        public GameObject[] blocks;
        public Button mDeleteButton;
        public Text mCurrentScoreEditText, mBestScoreEditText, mGameOverPassAnimEditText;
        private bool mIsScoreNotificationShown = false, mIsEvent = true;
        public GameObject objj;

        void Start()
        {
            loadingScreen.SetActive(true);
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            BlockHandler.mHexaCounter = 1;
            BlockHandler.mMaxBlock = 2;
            mGameCanvas.SetActive(false);

            mGameOverAnimObj.SetActive(false);
            mBestScoreAnimation.SetActive(false);

            StartCoroutine(SetScoreOnGame(0, 0));

            StartCoroutine(InitializeHexa());
            Invoke(nameof(HideLoading), .5f);
            soundToggle.isOn = AIMUtils.GetSoundStatus() == 0 ? true : false;
            SetSoundValue(soundToggle.isOn);
            soundToggle.onValueChanged.AddListener((bool isOn) =>
            {
                SetSoundValue(isOn);
            });
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        private void HideLoading() => loadingScreen.SetActive(false);

        private void SetSoundValue(bool isON)
        {
            AIMUtils.SetSoundStatus(isON ? AIMUtils.SOUND_OFF : AIMUtils.SOUND_ON);
            if (AIMUtils.GetSoundStatus() == AIMUtils.SOUND_ON)
                oneShotAudioPlayer.PlayBackgroundMusic(true);
            else
                oneShotAudioPlayer.StopBackgroundMusic();
        }

        private IEnumerator InitializeHexa()
        {
            mCurrentHexa = Instantiate(mHexaPrefab, transform);
            yield return new WaitForSeconds(0.0001f);
            mColliderBlockArray.Clear();
            mBlocksArray.Clear();

            for (int ind = 0; ind < mCurrentHexa.transform.childCount; ind++)
            {
                mColliderBlockArray.Add(mCurrentHexa.transform.GetChild(ind).gameObject);

                BlockDetails blockDetails = new BlockDetails();

                blockDetails.mBlockObj = mColliderBlockArray[ind].transform.GetChild(0).gameObject;

                blockDetails.mBlockObj.transform.GetChild(0).GetComponent<Text>().text = "0";

                mBlocksArray.Add(blockDetails);

                blockDetails.mBlockObj.SetActive(false);

                if (BlockHandler.IsIndexExist(ind))
                    mColliderBlockArray[ind].SetActive(false);
            }
            if (mNewBlockContainer != null)
                mNewBlockContainer.transform.SetAsLastSibling();

            yield return new WaitForSeconds(0.01f);
            if (mGameCanvas.activeSelf)
                mGameCanvas.SetActive(false);

            CreateNewBox();
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Escape) && mIsEvent)
                StartCoroutine(HandleEscape());
        }

        public void ONPauseBtnClicked()
        {
            PlayOneShotPlayer(0);
            StartCoroutine(HandleEscape());
            FirebaseManager.Instance.SendEvent(Constants.HEXAGON_PAUSE);
        }

        private IEnumerator LoadScene()
        {
            yield return new WaitForEndOfFrame();
            SceneManager.LoadSceneAsync(GameData.Instance.MENU_SCENE);
            FirebaseManager.Instance.SendEvent(Constants.HEXAGON_GO_TO_HOME);
        }

        private IEnumerator HandleEscape()
        {
            mIsEvent = false;
            if (mGameCanvas.activeSelf)
            {
                RotationHandler.mIsRotating = true;
                oneShotAudioPlayer.StopBackgroundMusic();
                loadingScreen.SetActive(true);
                StartCoroutine(LoadScene());
                //AdmobAdsManager._AdClosed += delegate ()
                //{
                //};
                //AdmobAdsManager.Instance.ShowInterstitial();
            }
            else
            {
                RotationHandler.mIsRotating = false;
                AdmobAdsManager.Instance.ShowNativeAds(adNativePanel, adIcon, adChoices, adHeadline, adCallToAction, adAdvertiser, true);
                mGameCanvas.SetActive(true);
                mDlgResumeButton.SetActive(true);
                SetScoreOnDialog(true);
            }
            yield return new WaitForSeconds(.2f);
            mIsEvent = true;
        }

        public IEnumerator SetScoreOnGame(int aScoreToAdd, int aIdleIndex)
        {
            int score = AIMUtils.GetBestScore();

            if (aScoreToAdd == 0)
            {
                mCurrentScoreEditText.GetComponent<Text>().text = mCurrentScore.ToString("000");
                mBestScoreEditText.GetComponent<Text>().text = score.ToString("000");
            }
            for (int ind = 0; ind < aScoreToAdd; ind++)
            {
                mCurrentScore++;
                mColliderBlockArray[aIdleIndex].transform.SetAsLastSibling();
                mCurrentScoreEditText.GetComponent<Text>().text = mCurrentScore.ToString("000");
                yield return new WaitForSeconds(0.06f);
                if (mCurrentScore >= score)
                    CheckAndSetBestScore();
            }
        }

        private void CheckAndSetBestScore()
        {
            mBestScoreEditText.GetComponent<Text>().text = mCurrentScore.ToString("000");
            AIMUtils.SetBestScore(mCurrentScore);
            if (mCurrentScore <= 1)
                mIsScoreNotificationShown = true;
            if (!mIsScoreNotificationShown)
            {
                mIsScoreNotificationShown = true;
                //PlayOneShotPlayer(1);
                mBestScoreAnimation.SetActive(true);
                mBestScoreAnimation.GetComponent<AudioSource>().Play();
                AnimationEventHandler._AnimationOverEvent += delegate ()
                {
                    mBestScoreAnimation.SetActive(false);
                };
                Debug.Log("Congratulation !!!");
            }
        }

        public void SetBlockData(int index, int tileColorRandomNo)
        {
            mBlocksArray[index].mBlockObj.SetActive(true);
            mBlocksArray[index].mHexaNo = tileColorRandomNo;
            mBlocksArray[index].mBlockObj.transform.GetChild(0).GetComponent<Text>().text = tileColorRandomNo + "";
            mBlocksArray[index].mBlockObj.GetComponent<Image>().sprite = hexagonSprites[tileColorRandomNo - 1];
            mBlocksArray[index].mBlockObj.GetComponent<Image>().sprite = hexagonSprites[tileColorRandomNo - 1];
        }

        public void CreateNewBox()
        {
            int no = Random.Range(1, 3);
            if (no == 2)
                mSingleBlockCounter++;
            if (mSingleBlockCounter == 2)
            {
                no = 1;
                mSingleBlockCounter = 0;
            }
            bool isTwo = IsGameOver(2);
            if (isTwo)
                no = 2;
            mCurrentBoxes = Instantiate(blocks[no - 1], mNewPuzzlePos);
            if (no == 2 && IsGameOver(1))
                StartCoroutine(IsGameOverOrComplete());
            else if (no == 1 && IsGameOver(2))
                StartCoroutine(IsGameOverOrComplete());
            PlayOneShotPlayer(5);
        }

        public void PlayOneShotPlayer(int aSoundIndex)
        {
            oneShotAudioPlayer.PlayOneShot(aSoundIndex);
        }

        private IEnumerator IsGameOverOrComplete()
        {
            RotationHandler.mIsRotating = false;
            yield return new WaitForSeconds(1f);
            mGameOverAnimObj.SetActive(true);
            mGameOverPassAnimEditText.GetComponent<Text>().text = "GAME OVER";
            mGameOverAnimObj.transform.SetAsLastSibling();
            PlayOneShotPlayer(6);
            AnimationEventHandler._AnimationOverEvent += delegate ()
            {
                BlockHandler.mMaxBlock = 2;
                mGameCanvas.SetActive(true);
                AdmobAdsManager.Instance.ShowNativeAds(adNativePanel, adIcon, adChoices, adHeadline, adCallToAction, adAdvertiser, true);
                mDlgResumeButton.SetActive(false);
                SetScoreOnDialog(false);
                mGameOverAnimObj.SetActive(false);
                Destroy(mCurrentBoxes);
                Destroy(mCurrentHexa);
            };
        }

        private void SetScoreOnDialog(bool aIsPause)
        {
            if (!mGameCanvas.activeSelf)
            {
                mGameCanvas.SetActive(true);
                AdmobAdsManager.Instance.ShowNativeAds(adNativePanel, adIcon, adChoices, adHeadline, adCallToAction, adAdvertiser, true);
            }
            if (aIsPause)
                GameObject.Find("GameStatusText").GetComponent<Text>().text = "GAME PAUSE";
            else
                GameObject.Find("GameStatusText").GetComponent<Text>().text = "GAME OVER";
            //Local Scores
            GameObject.Find("DlgBestScoreText").GetComponent<Text>().text = "Best Score: " + AIMUtils.GetBestScore().ToString("00");
            GameObject.Find("DlgCurrentScoreText").GetComponent<Text>().text = "Score: " + mCurrentScore.ToString("00");
        }

        private IEnumerator DeleteBlocks()
        {
            if (mCurrentBoxes != null && !mCurrentBoxes.GetComponent<Animator>().GetBool("FadeOut"))
            {
                yield return new WaitForSeconds(0.0001f);
                mCurrentBoxes.GetComponent<Animator>().SetBool("FadeIn", false);
                mCurrentBoxes.GetComponent<Animator>().SetBool("FadeOut", true);

                yield return new WaitForSeconds(0.10f);
                Destroy(mCurrentBoxes);
                yield return new WaitForSeconds(0.2f);
                CreateNewBox();
                int totaPoints = AIMUtils.GetDeletePoints();
                totaPoints -= AIMUtils.REWARDED_Points;
                AIMUtils.SetDeletePoints(totaPoints);
                //GameObject.Find("DeleteButtonCoinText").GetComponent<Text>().text = totaPoints.ToString("00");
            }
        }

        public void OnCancelBtnClicked()
        {
            //mIsEvent = true;
            PlayOneShotPlayer(0);
            StartCoroutine(HandleEscape());
        }

        public void OnResumeBtnClicked()
        {
            PlayOneShotPlayer(0);
            RotationHandler.mIsRotating = true;
            mGameCanvas.SetActive(false);
            FirebaseManager.Instance.SendEvent(Constants.HEXAGON_RESUME);
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }
        public void OnRestartBtnClicked()
        {
            PlayOneShotPlayer(0);
            AdmobAdsManager._AdClosed += delegate ()
            {
                RotationHandler.mIsRotating = true;
                mCurrentScore = 0;
                StartCoroutine(SetScoreOnGame(0, 0));
                Destroy(mCurrentHexa);
                Destroy(mCurrentBoxes);
                StartCoroutine(InitializeHexa());
            };
            AdmobAdsManager.Instance.ShowInterstitial();
            FirebaseManager.Instance.SendEvent(Constants.HEXAGON_RESTART);
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        public void OnBlockDeleteBtnClicked()
        {
            PlayOneShotPlayer(0);
            mVideoAdCounter++;
            if (Application.internetReachability != NetworkReachability.NotReachable && mVideoAdCounter == 8)
            {
                mVideoAdCounter = 0;
                mIsVideoWatched = false;
                AdmobAdsManager._AdClosed += delegate ()
                {
                    if (!mIsVideoWatched)
                    {
                        Debug.Log("Close Received Video|||||||||||||||||||||");
                        mIsVideoWatched = true;
                        StartCoroutine(DeleteBlocks());
                    }
                };
                AdmobAdsManager.Instance.ShowInterstitial();
                AdmobAdsManager.Instance.ShowBannerAd(false);
            }
            else
            {
                StartCoroutine(DeleteBlocks());
            }
            FirebaseManager.Instance.SendEvent(Constants.HEXAGON_DELETE_BLOCK);
        }

        private bool mIsVideoWatched = false;

        private bool IsGameOver(int aTotalBlocks)
        {
            if (aTotalBlocks == 1)
            {
                for (int ind = 0; ind < mBlocksArray.Count; ind++)
                {
                    if (BlockHandler.IsIndexExist(ind))
                        continue;

                    if (!mBlocksArray[ind].mBlockObj.activeSelf)
                    {
                        return false;
                    }
                }
            }
            else if (aTotalBlocks == 2)
            {
                if (CanRectBlockExist(mBlocksArray, BlockHandler.GRID_X, BlockHandler.GRID_Y))
                {
                    return false;
                }
                else if (CanLeftDigoBlockExist(mBlocksArray, BlockHandler.GRID_X, BlockHandler.GRID_Y))
                {
                    return false;
                }
                else if (CanRightDigoBlockExist(mBlocksArray, BlockHandler.GRID_X, BlockHandler.GRID_Y))
                {
                    return false;
                }
            }
            return true;
        }

        private bool CanRectBlockExist(List<BlockDetails> aBlocksArray, int aRows, int aCols)
        {
            int indexCounter = 0;
            for (int ind = 0; ind < aRows; ind++)
            {
                for (int ind1 = 0; ind1 < aCols - 1; ind1++)
                {
                    if (mColliderBlockArray[indexCounter].activeSelf && mColliderBlockArray[indexCounter + 1].activeSelf)
                    {
                        //						Debug.Log (indexCounter + "   :   " + (indexCounter + 1));
                        if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[indexCounter + 1].mBlockObj.activeSelf)
                        {
                            return true;
                        }
                    }
                    indexCounter++;
                }
                indexCounter++;
            }
            return false;
        }

        private bool CanLeftDigoBlockExist(List<BlockDetails> aBlocksArray, int aRows, int aCols)
        {
            int col = aCols;
            int indexCounter = 0, nextIndexCounter = 0;

            for (int ind = 0; ind < aRows - 1; ind++)
            {
                if (ind % 2 == 0)
                    col = aCols - 1;
                else
                    col = aCols;

                for (int ind1 = 0; ind1 < col; ind1++)
                {
                    nextIndexCounter = indexCounter + 6;

                    //					Debug.Log (indexCounter + "   :   " + nextIndexCounter);

                    if (ind % 2 == 0)
                    {
                        if (nextIndexCounter < mColliderBlockArray.Count && mColliderBlockArray[indexCounter].activeSelf && mColliderBlockArray[nextIndexCounter].activeSelf)
                        {
                            //							Debug.Log (indexCounter + "   :EVEN:   " + nextIndexCounter);
                            if (ind >= 2)
                            {
                                if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[nextIndexCounter].mBlockObj.activeSelf && !BlockHandler.IsRight(indexCounter))
                                {
                                    //									Debug.Log (indexCounter + "   :Return ind >=2  EVEN:   " + nextIndexCounter);
                                    return true;
                                }
                            }
                            else
                            {
                                if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[nextIndexCounter].mBlockObj.activeSelf)
                                {
                                    //									Debug.Log (indexCounter + "   :Return ind >=2(else)  EVEN:   " + nextIndexCounter);
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        nextIndexCounter = indexCounter + 5;
                        if (nextIndexCounter < mColliderBlockArray.Count && mColliderBlockArray[indexCounter].activeSelf && mColliderBlockArray[nextIndexCounter].activeSelf)
                        {
                            //							Debug.Log (indexCounter + "   :ODD:   " + nextIndexCounter);
                            if (ind >= 2)
                            {
                                if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[nextIndexCounter].mBlockObj.activeSelf && !BlockHandler.IsRight(indexCounter))
                                {
                                    //									Debug.Log (indexCounter + "   :Return ind >=2  ODD:   " + nextIndexCounter);
                                    return true;
                                }
                            }
                            else
                            {
                                if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[nextIndexCounter].mBlockObj.activeSelf)
                                {
                                    //									Debug.Log (indexCounter + "   :Return ind >=2(else)  ODD:   " + nextIndexCounter);
                                    return true;
                                }
                            }
                        }
                    }
                    indexCounter++;
                }
                if (ind % 2 == 0)
                    indexCounter++;
            }
            //			Debug.Log (indexCounter + "   :Return FINAL:   " + nextIndexCounter);
            return false;
        }

        public void OnLeaderboardBtnCLicked()
        {
            Social.ShowLeaderboardUI();
        }

        private bool CanRightDigoBlockExist(List<BlockDetails> aBlocksArray, int aRows, int aCols)
        {
            int col = aCols;
            int indexCounter = 0, nextIndexCounter = 0;

            for (int ind = 0; ind < aRows - 1; ind++)
            {
                if (ind % 2 == 0)
                    col = aCols;
                else
                    col = aCols - 1;

                for (int ind1 = 0; ind1 < col; ind1++)
                {
                    nextIndexCounter = indexCounter + 5;

                    //					Debug.Log (indexCounter + "   :   " + nextIndexCounter);
                    if (ind % 2 == 0)
                    {
                        if (nextIndexCounter < mColliderBlockArray.Count && mColliderBlockArray[indexCounter].activeSelf && mColliderBlockArray[nextIndexCounter].activeSelf)
                        {
                            //							Debug.Log (indexCounter + "   :EVEN:   " + nextIndexCounter);
                            if (ind >= 2)
                            {
                                if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[nextIndexCounter].mBlockObj.activeSelf)
                                {
                                    //									Debug.Log (indexCounter + "   :Return ind >=2  EVEN:   " + nextIndexCounter);
                                    return true;
                                }
                            }
                            else
                            {
                                if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[nextIndexCounter].mBlockObj.activeSelf && !BlockHandler.IsLeft(indexCounter))
                                {
                                    //									Debug.Log (indexCounter + "   :Return ind >=2(else)  EVEN:   " + nextIndexCounter);
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        nextIndexCounter = indexCounter + 4;
                        if (nextIndexCounter < mColliderBlockArray.Count && mColliderBlockArray[indexCounter].activeSelf && mColliderBlockArray[nextIndexCounter].activeSelf)
                        {
                            //							Debug.Log (indexCounter + "   :ODD:   " + nextIndexCounter);
                            if (ind >= 2)
                            {
                                if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[nextIndexCounter].mBlockObj.activeSelf)
                                {
                                    //									Debug.Log (indexCounter + "   :Return ind >=2  ODD:   " + nextIndexCounter);
                                    return true;
                                }
                            }
                            else
                            {
                                if (!aBlocksArray[indexCounter].mBlockObj.activeSelf && !aBlocksArray[nextIndexCounter].mBlockObj.activeSelf && !BlockHandler.IsLeft(indexCounter))
                                {
                                    //									Debug.Log (indexCounter + "   :Return ind >=2(else)  ODD:   " + nextIndexCounter);
                                    return true;
                                }
                            }
                        }
                    }
                    indexCounter++;
                }
                if (ind % 2 == 0)
                    indexCounter++;
            }
            //			Debug.Log (indexCounter + "   :Return FINAL:   " + nextIndexCounter);
            return false;
        }

        public void ChangeImage(string aGameObjectName, string aFileWithPath)
        {
            Texture2D texture = Resources.Load(aFileWithPath) as Texture2D;
            GameObject.Find(aGameObjectName).GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
        }
    }
}