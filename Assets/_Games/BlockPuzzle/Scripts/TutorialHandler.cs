using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TutorialHandler : MonoBehaviour
{
    public GameObject tutorialObj, basePanel;
    public bool isTutorialRunning = false;
    public bool isDragCompleted = false;
    public int tutorialCount = 1;
    public Animator animator;
    public Text msg;
    public Button nextBtn;
    private const string DRAG = "Drag";
    private const string TAP = "Tap";
    readonly string[] testMsg =  { "<size=22><color=yellow><b>How to play</b></color></size>\n\n<size=13><color=white><b>* Hold finger and drag to empty block</b></color></size>",
                            "<size=22><color=yellow><b>How to play</b></color></size>\n\n<size=13><color=white><b>* Tap to rotate block puzzle</b></color></size>"};
    public enum AnimType : byte { Drag, tap }

    void Start()
    {
        basePanel.SetActive(false);
        if (PrefsKey.GetInt(PrefsKey.TUTORIAL_COUNT, 0) < tutorialCount)
            Invoke(nameof(StartTutorial), 1f);
        else
            tutorialObj.SetActive(false);
    }

    private void StartTutorial()
    {
        basePanel.SetActive(true);
        basePanel.transform.localScale = Vector3.zero;
        basePanel.transform.DOScale(Vector3.one, .2f);
        isTutorialRunning = true;
        PlayAnim(AnimType.Drag);
        msg.text = testMsg[0];
        msg.transform.localScale = Vector3.zero;
        msg.transform.DOScale(Vector3.one, .2f);
        nextBtn.onClick.AddListener(delegate () {
            PrefsKey.SetInt(PrefsKey.TUTORIAL_COUNT, PrefsKey.GetInt(PrefsKey.TUTORIAL_COUNT, 0) + 1);
            HidePanel();
        });
    }

    public void OnBtnCLicked(bool isDraged)
    {
        if (!isTutorialRunning)
            return;
        if (isDraged)
            isDragCompleted = true;
        if (!isDraged && !isDragCompleted)
            return;
        Debug.Log(animator.GetBool(TAP)+" cc: " + animator.GetBool(DRAG));
        if (isDraged)
        {
            msg.transform.DOScale(Vector3.zero, .2f).OnComplete(delegate () {
                msg.text = testMsg[1];
                msg.transform.DOScale(Vector3.one, .2f); 
            });
            PlayAnim(AnimType.tap);
        }
        else
        {
            PrefsKey.SetInt(PrefsKey.TUTORIAL_COUNT, PrefsKey.GetInt(PrefsKey.TUTORIAL_COUNT, 0) + 1);
            HidePanel();
        }
    }

    private void HidePanel()
    {
        basePanel.transform.DOScale(Vector3.zero, .2f).OnComplete(delegate ()
        {
            basePanel.SetActive(false);
            tutorialObj.SetActive(false);
        });
    }

    public void PlayAnim(AnimType animType)
    {
        animator.SetBool(DRAG, false);
        animator.SetBool(TAP, false);
        animator.SetBool(animType == AnimType.Drag ? DRAG : TAP, true);
    }
}