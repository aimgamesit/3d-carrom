﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class BackObject : MonoBehaviour
{
    public bool simpleDisable = true;
    public UnityEvent OnBackPressed;

    void Awake()
    {
        if (simpleDisable)
        {
            OnBackPressed.AddListener(delegate ()
            {
                gameObject.SetActive(false);
            });
        }
    }

    void OnEnable()
    {
        Invoke("Push", .2f);
    }

    private void Push() => BackButtonHandler.Instance.backStack.Push(this);

    void OnDisable()
    {
        if (BackButtonHandler.Instance.backStack != null && BackButtonHandler.Instance.backStack.Count > 0)
            BackButtonHandler.Instance.backStack.Pop();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(BackObject))]
public class BackObjectEditor : Editor
{
    override public void OnInspectorGUI()
    {
        serializedObject.Update();
        var backObj = target as BackObject;
        var prop = serializedObject.FindProperty("OnBackPressed");

        backObj.simpleDisable = EditorGUILayout.Toggle("Simple Disable", backObj.simpleDisable);

        if (!backObj.simpleDisable)
        {
            EditorGUILayout.PropertyField(prop, true);
        }

        if (GUI.changed)
        {
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif