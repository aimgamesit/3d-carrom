﻿using UnityEngine;
using System.Collections;

namespace BlockPuzzleNS
{
	public class PatternCreateHandler : MonoBehaviour
	{
		public static ArrayList mObjectsOfShape = new ArrayList ();

		void Start ()
		{
//		Debug.Log ("PATTERN CREATED");
			InitializeBozPatterns ();
		}

		private void InitializeBozPatterns ()
		{
			for (int ind = 1; ind < 13; ind++) {
				if (ind == 1) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (13);

					obj.mShape2.Add (13);

					obj.mShape3.Add (13);

					obj.mShape4.Add (13);
					mObjectsOfShape.Add (obj);
				} else if (ind == 2) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (13);
					obj.mShape1.Add (14);

					obj.mShape2.Add (8);
					obj.mShape2.Add (13);

					obj.mShape3.Add (13);
					obj.mShape3.Add (14);

					obj.mShape4.Add (8);
					obj.mShape4.Add (13);
					mObjectsOfShape.Add (obj);
				} else if (ind == 3) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (12);
					obj.mShape1.Add (13);
					obj.mShape1.Add (14);

					obj.mShape2.Add (8);
					obj.mShape2.Add (13);
					obj.mShape2.Add (18);

					obj.mShape3.Add (12);
					obj.mShape3.Add (13);
					obj.mShape3.Add (14);

					obj.mShape4.Add (8);
					obj.mShape4.Add (13);
					obj.mShape4.Add (18);
					mObjectsOfShape.Add (obj);
				} else if (ind == 4) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (12);
					obj.mShape1.Add (13);
					obj.mShape1.Add (17);
					obj.mShape1.Add (18);

					obj.mShape2.Add (12);
					obj.mShape2.Add (13);
					obj.mShape2.Add (17);
					obj.mShape2.Add (18);

					obj.mShape3.Add (12);
					obj.mShape3.Add (13);
					obj.mShape3.Add (17);
					obj.mShape3.Add (18);

					obj.mShape4.Add (12);
					obj.mShape4.Add (13);
					obj.mShape4.Add (17);
					obj.mShape4.Add (18);
					mObjectsOfShape.Add (obj);
				} else if (ind == 5) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (13);
					obj.mShape1.Add (14);
					obj.mShape1.Add (18);

					obj.mShape2.Add (8);
					obj.mShape2.Add (9);
					obj.mShape2.Add (14);

					obj.mShape3.Add (9);
					obj.mShape3.Add (13);
					obj.mShape3.Add (14);

					obj.mShape4.Add (13);
					obj.mShape4.Add (18);
					obj.mShape4.Add (19);
					mObjectsOfShape.Add (obj);
				} else if (ind == 6) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (8);
					obj.mShape1.Add (13);
					obj.mShape1.Add (18);
					obj.mShape1.Add (19);

					obj.mShape2.Add (12);
					obj.mShape2.Add (13);
					obj.mShape2.Add (14);
					obj.mShape2.Add (17);

					obj.mShape3.Add (7);
					obj.mShape3.Add (8);
					obj.mShape3.Add (13);
					obj.mShape3.Add (18);

					obj.mShape4.Add (9);
					obj.mShape4.Add (12);
					obj.mShape4.Add (13);
					obj.mShape4.Add (14);
					mObjectsOfShape.Add (obj);
				} else if (ind == 7) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (11);
					obj.mShape1.Add (12);
					obj.mShape1.Add (13);
					obj.mShape1.Add (14);

					obj.mShape2.Add (3);
					obj.mShape2.Add (8);
					obj.mShape2.Add (13);
					obj.mShape2.Add (18);

					obj.mShape3.Add (11);
					obj.mShape3.Add (12);
					obj.mShape3.Add (13);
					obj.mShape3.Add (14);

					obj.mShape4.Add (3);
					obj.mShape4.Add (8);
					obj.mShape4.Add (13);
					obj.mShape4.Add (18);
					mObjectsOfShape.Add (obj);
				} else if (ind == 8) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (7);
					obj.mShape1.Add (12);
					obj.mShape1.Add (17);
					obj.mShape1.Add (18);
					obj.mShape1.Add (19);

					obj.mShape2.Add (7);
					obj.mShape2.Add (8);
					obj.mShape2.Add (9);
					obj.mShape2.Add (12);
					obj.mShape2.Add (17);

					obj.mShape3.Add (7);
					obj.mShape3.Add (8);
					obj.mShape3.Add (9);
					obj.mShape3.Add (14);
					obj.mShape3.Add (19);

					obj.mShape4.Add (9);
					obj.mShape4.Add (14);
					obj.mShape4.Add (19);
					obj.mShape4.Add (17);
					obj.mShape4.Add (18);
					mObjectsOfShape.Add (obj);
				} else if (ind == 9) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (7);
					obj.mShape1.Add (8);
					obj.mShape1.Add (9);
					obj.mShape1.Add (12);
					obj.mShape1.Add (13);
					obj.mShape1.Add (14);
					obj.mShape1.Add (17);
					obj.mShape1.Add (18);
					obj.mShape1.Add (19);

					obj.mShape2.Add (7);
					obj.mShape2.Add (8);
					obj.mShape2.Add (9);
					obj.mShape2.Add (12);
					obj.mShape2.Add (13);
					obj.mShape2.Add (14);
					obj.mShape2.Add (17);
					obj.mShape2.Add (18);
					obj.mShape2.Add (19);

					obj.mShape3.Add (7);
					obj.mShape3.Add (8);
					obj.mShape3.Add (9);
					obj.mShape3.Add (12);
					obj.mShape3.Add (13);
					obj.mShape3.Add (14);
					obj.mShape3.Add (17);
					obj.mShape3.Add (18);
					obj.mShape3.Add (19);

					obj.mShape4.Add (7);
					obj.mShape4.Add (8);
					obj.mShape4.Add (9);
					obj.mShape4.Add (12);
					obj.mShape4.Add (13);
					obj.mShape4.Add (14);
					obj.mShape4.Add (17);
					obj.mShape4.Add (18);
					obj.mShape4.Add (19);
					mObjectsOfShape.Add (obj);
				} else if (ind == 10) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (13);
					obj.mShape1.Add (17);
					obj.mShape1.Add (18);
					obj.mShape1.Add (19);

					obj.mShape2.Add (8);
					obj.mShape2.Add (13);
					obj.mShape2.Add (14);
					obj.mShape2.Add (18);

					obj.mShape3.Add (7);
					obj.mShape3.Add (8);
					obj.mShape3.Add (9);
					obj.mShape3.Add (13);

					obj.mShape4.Add (8);
					obj.mShape4.Add (12);
					obj.mShape4.Add (13);
					obj.mShape4.Add (18);
					mObjectsOfShape.Add (obj);
				} else if (ind == 11) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (12);
					obj.mShape1.Add (13);
					obj.mShape1.Add (18);

					obj.mShape2.Add (8);
					obj.mShape2.Add (12);
					obj.mShape2.Add (13);

					obj.mShape3.Add (8);
					obj.mShape3.Add (13);
					obj.mShape3.Add (14);

					obj.mShape4.Add (8);
					obj.mShape4.Add (9);
					obj.mShape4.Add (13);
					mObjectsOfShape.Add (obj);
				} else if (ind == 12) {
					ShapeArray obj = new ShapeArray ();
					obj.mShape1.Add (9);
					obj.mShape1.Add (12);
					obj.mShape1.Add (13);
					obj.mShape1.Add (14);

					obj.mShape2.Add (7);
					obj.mShape2.Add (12);
					obj.mShape2.Add (17);
					obj.mShape2.Add (18);

					obj.mShape3.Add (7);
					obj.mShape3.Add (8);
					obj.mShape3.Add (9);
					obj.mShape3.Add (12);

					obj.mShape4.Add (8);
					obj.mShape4.Add (9);
					obj.mShape4.Add (14);
					obj.mShape4.Add (19);
					mObjectsOfShape.Add (obj);
				}
			}
		}
	}

}