﻿using UnityEngine;
using System.Collections;

public class AlertHandler : MonoBehaviour
{
	private string toastString;
	private string input;
	private	AndroidJavaObject currentActivity, context;
	private AndroidJavaClass UnityPlayer;

	void Start ()
	{
		if (Application.platform == RuntimePlatform.Android) {
			UnityPlayer = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
			currentActivity = UnityPlayer.GetStatic<AndroidJavaObject> ("currentActivity");
			context = currentActivity.Call<AndroidJavaObject> ("getApplicationContext");
		}
		showToastOnUiThread ("Hello This is an example!");
	}

	public void showToastOnUiThread (string toastString)
	{
		this.toastString = toastString;
		currentActivity.Call ("runOnUiThread", new AndroidJavaRunnable (showToast));
	}

	void showToast ()
	{
		AndroidJavaClass Toast = new AndroidJavaClass ("android.widget.Toast");
		AndroidJavaObject javaString = new AndroidJavaObject ("java.lang.String", toastString);
		AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject> ("makeText", context, javaString, Toast.GetStatic<int> ("LENGTH_SHORT"));
		toast.Call ("show");
	}
}