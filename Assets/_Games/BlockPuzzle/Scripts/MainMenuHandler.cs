﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace BlockPuzzleNS
{
    public class MainMenuHandler : MonoBehaviour
    {
        public Text scoreText;
        public GameObject adNativePanel;
        public RawImage adIcon;
        public RawImage adChoices;
        public Text adHeadline;
        public Text adCallToAction;
        public Text adAdvertiser;
        public Toggle volumeToggle;
        public GameObject exitPopup;

        void Start()
        {
            volumeToggle.isOn = PrefsKey.GetInt(PrefsKey.SOUND_EFFECT, 1) == 1 ? true : false;
            HandleBackgroundMosic(volumeToggle.isOn);
            volumeToggle.onValueChanged.AddListener(delegate (bool status)
            {
                PrefsKey.SetInt(PrefsKey.SOUND_EFFECT, status ? 1 : 0);
                HandleBackgroundMosic(status);

            });
            AdmobAdsManager.Instance.ShowBannerAd(false);
            scoreText.text = PlayerPrefs.GetInt("BestScorePrefeb", 0).ToString();
        }

        private void HandleBackgroundMosic(bool isPlay)
        {
            if (isPlay)
                AudioManager.Instance.PlayMusic(AudioManager.background);
            else
                AudioManager.Instance.StopMusic();
        }

        public void OnExitBtnCLicked(bool isPlaySound)
        {
            if (isPlaySound)
                AudioManager.Instance.PlaySFXAtPosition(AudioManager.click_sound);
            exitPopup.SetActive(true);
            AdmobAdsManager.Instance.ShowNativeAds(adNativePanel, adIcon, adChoices, adHeadline, adCallToAction, adAdvertiser, true);
        }

        public void OnYesBtnClicked()
        {
            AudioManager.Instance.PlaySFXAtPosition(AudioManager.click_sound);
            AdmobAdsManager._OnRewardedReceived+= delegate ()
            {
                Application.Quit();
            };
            AdmobAdsManager._AdClosed += delegate ()
            {
                Application.Quit();
            };
            AdmobAdsManager.Instance.ShowVideoAd();
        }

        public void OnNoBtnClicked()
        {
            AdmobAdsManager._AdClosed += delegate ()
            {
                exitPopup.SetActive(false);
            };
            AdmobAdsManager.Instance.ShowInterstitial();
            AudioManager.Instance.PlaySFXAtPosition(AudioManager.click_sound);
        }

        public void OnPlayBtnClick()
        {
            SceneManager.LoadScene("AIMGamePlay");
            AudioManager.Instance.PlaySFXAtPosition(AudioManager.click_sound);
        }

        public void OnRateUsBtnClick()
        {
            //AIMPluginManager.Instance.RateUs();
            AudioManager.Instance.PlaySFXAtPosition(AudioManager.click_sound);
        }

        public void OnShareBtnClick()
        {
            //AIMPluginManager.Instance.ShareApp("Check out the Latest Block Puzzle Game at: ");
            AudioManager.Instance.PlaySFXAtPosition(AudioManager.click_sound);
        }

        public void OnWatchAdsBtnClicked()
        {
            //AudioManager.instance.PlaySFXAtPosition(AudioManager.click_sound);
            AdmobAdsManager._OnRewardedReceived += delegate () { };
            AdmobAdsManager._AdClosed += delegate () { };
            AdmobAdsManager.Instance.ShowVideoAd();
        }
        public void OnLeaderboardBtnClicked()
        {
            Social.ShowLeaderboardUI();
        }
    }
}