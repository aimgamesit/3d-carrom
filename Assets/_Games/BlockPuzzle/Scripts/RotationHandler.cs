﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockPuzzleNS
{
	public class RotationHandler : MonoBehaviour
	{
		public float mSpeed = 0.0f;
		public bool mIsForward = false;

		void Start ()
		{
		
		}

		void Update ()
		{
			if (mIsForward)
				transform.Rotate (Vector3.forward * mSpeed * Time.deltaTime, Space.World);
			else
				transform.Rotate (Vector3.back * mSpeed * Time.deltaTime, Space.World);
		}
	}
}