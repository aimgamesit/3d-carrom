﻿using AIMCarrom;
using com.TicTok.managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BlockPuzzleNS
{
    public class GamePlayHandler : BManager<GamePlayHandler>
    {
        public GameObject loadingScreen, adNativePanel;
        public RawImage adIcon;
        public RawImage adChoices;
        public Text adHeadline;
        public Text adCallToAction;
        public Text adAdvertiser;
        public GameObject boxesPrefab;
        private const int BUTTON_CANCEL = 1, BUTTON_RESTART = 2, BUTTON_RESUME = 3, BUTTON_SCORE = 6;
        public GameObject mPopupCanvas, mRefreshImage, mGameOverImage;
        public GameObject mBestScoreNotificationImage, mBlockDestroyPrefab, mResumeButton;
        public Text mPopupTitle, mPopupBestScore, mPopupCurrScore;
        public Transform mCreatePuzzlesTrans;
        public GameObject[] mShowImage = new GameObject[64];
        public GameObject[] mShowFilledImage = new GameObject[64];
        public Toggle volumeToggle;
        public bool mIsEscape = true, mIsGameOver = false;
        private int mAvailablePuzzleBoxes = 0, mCurrentScore = 0, mBestScore = 0;

        void Start()
        {
            loadingScreen.SetActive(true);
            Application.targetFrameRate = 60;
            mBestScore = GetBestScore();
            mBestScore++;

            for (int ind = 0; ind < mShowImage.Length; ind++)
            {
                mShowFilledImage[ind] = GameObject.Find("ShowFillesImg_" + (ind + 1));
                mShowFilledImage[ind].SetActive(false);
            }
            for (int ind = 0; ind < mShowImage.Length; ind++)
            {
                mShowImage[ind] = GameObject.Find("ShowImage_ (" + (ind + 1) + ")");
                mShowImage[ind].SetActive(false);
            }
            GetPatternWithoutSelection();
            StartCoroutine(SetScores(0));
            volumeToggle.isOn = PrefsKey.GetInt(PrefsKey.SOUND_EFFECT, 1) == 1;
            HandleBackgroundMosic(volumeToggle.isOn);
            Invoke(nameof(HideLoading), .5f);
            volumeToggle.onValueChanged.AddListener(delegate (bool status)
            {
                PrefsKey.SetInt(PrefsKey.SOUND_EFFECT, status ? 1 : 0);
                HandleBackgroundMosic(status);

            });
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        private void HideLoading() => loadingScreen.SetActive(false);

        private void HandleBackgroundMosic(bool isPlay)
        {
            if (isPlay)
                AudioManager.Instance.PlayMusic(AudioManager.background);
            else
                AudioManager.Instance.StopMusic();
        }

        private void GetPatternWithoutSelection()
        {
            for (int ind = 1; ind <= 64; ind++)
                GameObject.Find("PuzzleBox_ (" + ind + ")").GetComponentInChildren<Image>().sprite = GameData.Instance.emptyBoxSprite;
            StartCoroutine(RestartGame());
            mPopupCanvas.SetActive(false);
        }

        private IEnumerator RestartGame()
        {
            StartCoroutine(SetScores(0));
            if (!mPopupCanvas.activeSelf)
            {
                mRefreshImage.SetActive(true);
                float value = 0.0f;
                while (value <= 1.0f)
                {
                    mRefreshImage.GetComponent<Image>().fillAmount = value;
                    value += 0.06f;
                    yield return new WaitForSeconds(0.0001f);
                }
                mRefreshImage.GetComponent<Image>().fillAmount = 1.0f;
            }
            StartCoroutine(DestroyBoxes());
            for (int ind = 0; ind < mShowImage.Length; ind++)
            {
                mShowImage[ind].SetActive(false);
                mShowFilledImage[ind].SetActive(false);
            }
            StartCoroutine(InitializeBox());
        }

        private IEnumerator InitializeBox()
        {
            if (mRefreshImage.activeSelf)
            {
                float value = 1.0f;
                while (value >= 0.0f)
                {
                    mRefreshImage.GetComponent<Image>().fillAmount = value;
                    value -= 0.06f;
                    yield return new WaitForSeconds(0.0001f);
                }
                mRefreshImage.GetComponent<Image>().fillAmount = 0.0f;
                mRefreshImage.SetActive(false);
            }

            for (int ind = 1; ind <= 3; ind++)
            {
                yield return new WaitForSeconds(0.00001f);
                GameObject obj = Instantiate(boxesPrefab, mCreatePuzzlesTrans);
                obj.name = "Boxes_" + ind;
                //				Debug.Log (obj.name + "       <><><>");
                if (ind == 1)
                    obj.GetComponent<RectTransform>().anchoredPosition = GameObject.Find("PuzzleBoxPos_1").GetComponent<RectTransform>().anchoredPosition;
                else if (ind == 2)
                    obj.GetComponent<RectTransform>().anchoredPosition = GameObject.Find("PuzzleBoxPos_2").GetComponent<RectTransform>().anchoredPosition;
                else
                    obj.GetComponent<RectTransform>().anchoredPosition = GameObject.Find("PuzzleBoxPos_3").GetComponent<RectTransform>().anchoredPosition;
                obj = null;
            }

            if (mPopupCanvas.activeSelf)
                mPopupCanvas.SetActive(false);
            yield return new WaitForSeconds(0.4f);
            DestroyImmediate(GameObject.Find("Boxes_1").GetComponent<Animator>());
            DestroyImmediate(GameObject.Find("Boxes_2").GetComponent<Animator>());
            DestroyImmediate(GameObject.Find("Boxes_3").GetComponent<Animator>());
            mAvailablePuzzleBoxes = 3;
        }
        public TutorialHandler tutorialHandler;
        public void EndTouch(GameObject aObj)
        {
            List<int> boxesIndex = new List<int>();
            for (int ind = 1; ind <= 64; ind++)
            {
                for (int ind1 = 0; ind1 < aObj.transform.childCount; ind1++)
                {
                    if (GameObject.Find("PuzzleBox_ (" + ind + ")").GetComponent<BoxCollider2D>().bounds.Contains(aObj.transform.GetChild(ind1).transform.position))
                    {
                        if (!mShowImage[ind - 1].activeSelf)
                            boxesIndex.Add(ind);
                    }
                }
            }
            if (boxesIndex.Count == aObj.transform.childCount)
            {
                int boxNo = aObj.transform.GetComponent<PatternHandler>().mBoxNo;
                for (int ind = 0; ind < boxesIndex.Count; ind++)
                {
                    int index = boxesIndex[ind];
                    mShowImage[index - 1].SetActive(true);

                    mShowImage[index - 1].GetComponent<Image>().sprite = GameData.Instance.boxSprites[boxNo - 1];

                    //GameObject blockExistObj = Instantiate(mBlockExistPrefab, GameObject.Find("PuzzleImage").transform);
                    //blockExistObj.GetComponent<RectTransform>().anchoredPosition = GameObject.Find("PuzzleBox_ (" + index + ")").GetComponent<RectTransform>().anchoredPosition;
                }
                AudioManager.Instance.PlaySFXAtPosition(AudioManager.new_blocks_entry);
                StartCoroutine(SetScores(boxesIndex.Count));
                Debug.Log("Block Placed!!!!!!!!");
                tutorialHandler.OnBtnCLicked(true);
                Destroy(aObj);
                mAvailablePuzzleBoxes -= 1;
            }
            else
            {
                int no = int.Parse(aObj.name.Substring(6, 1));
                if (no == 1)
                    aObj.GetComponent<RectTransform>().anchoredPosition = GameObject.Find("PuzzleBoxPos_1").GetComponent<RectTransform>().anchoredPosition;
                else if (no == 2)
                    aObj.GetComponent<RectTransform>().anchoredPosition = GameObject.Find("PuzzleBoxPos_2").GetComponent<RectTransform>().anchoredPosition;
                else
                    aObj.GetComponent<RectTransform>().anchoredPosition = GameObject.Find("PuzzleBoxPos_3").GetComponent<RectTransform>().anchoredPosition;
                aObj.transform.localScale = new Vector3(0.4318008f, 0.4318008f, 0.4318008f);
                aObj = null;
            }
            if (mAvailablePuzzleBoxes == 0)
                StartCoroutine(InitializeBox());
            if (!mIsGameOver)
            {
                StartCoroutine(CheckHorizontalBoxes());
                StartCoroutine(CheckVerticalBoxes());
            }
        }

        private IEnumerator IsGameOver()
        {
            bool status = false;
            yield return new WaitForSeconds(0.1f);
            if (!mIsGameOver)
            {
                if (mCreatePuzzlesTrans.childCount == 6)
                {
                    //Debug.Log ("B: 333333333");
                    if (mCreatePuzzlesTrans.GetChild(3).GetComponent<PatternHandler>().IsGameOver() && mCreatePuzzlesTrans.GetChild(4).GetComponent<PatternHandler>().IsGameOver() && mCreatePuzzlesTrans.transform.GetChild(5).GetComponent<PatternHandler>().IsGameOver())
                        status = true;
                }
                else if (mCreatePuzzlesTrans.childCount == 5)
                {
                    //Debug.Log ("B: 222222222");
                    if (mCreatePuzzlesTrans.GetChild(3).GetComponent<PatternHandler>().IsGameOver() && mCreatePuzzlesTrans.GetChild(4).GetComponent<PatternHandler>().IsGameOver())
                        status = true;
                }
                else if (mCreatePuzzlesTrans.childCount == 4)
                {
                    //Debug.Log ("B: 111111111");
                    if (mCreatePuzzlesTrans.GetChild(3).GetComponent<PatternHandler>().IsGameOver())
                        status = true;
                }
            }
            if (status)
            {
                mIsGameOver = true;
                status = false;
                //Debug.Log ("GAME OVEER!");
                yield return new WaitForSeconds(1.0f);
                mGameOverImage.SetActive(true);
                AudioManager.Instance.PlaySFXAtPosition(AudioManager.game_over);
                AnimationEventHandler._AnimationGameOverEvent += delegate ()
                {
                    //RequestMmitAdsFull._mClosedAd += delegate() {
                    mGameOverImage.SetActive(false);
                    HandleGamePopup(false);
                    StartCoroutine(DestroyBoxes());
                    mIsGameOver = false;
                    //};
                    //RequestMmitAdsFull.GetInstance ().ShowInterstitial ();
                };
            }
        }

        private IEnumerator DestroyBoxes()
        {
            yield return new WaitForSeconds(0.00001f);
            DestroyImmediate(GameObject.Find("Boxes_1"));
            yield return new WaitForSeconds(0.00001f);
            DestroyImmediate(GameObject.Find("Boxes_2"));
            yield return new WaitForSeconds(0.00001f);
            DestroyImmediate(GameObject.Find("Boxes_3"));
        }

        private void HandleGamePopup(bool aIsPause)
        {
            if (aIsPause)
                StartCoroutine(ShowGAmeOverScreen(aIsPause));
            else
            {
                AdmobAdsManager._AdClosed += delegate ()
                {
                    StartCoroutine(ShowGAmeOverScreen(aIsPause));
                };
                AdmobAdsManager.Instance.ShowInterstitial();
            }
        }

        private IEnumerator ShowGAmeOverScreen(bool aIsPause)
        {
            yield return new WaitForSeconds(.1f);
            mPopupCanvas.SetActive(true);
            if (aIsPause)
            {
                mPopupTitle.text = "Game Pause";
                mResumeButton.SetActive(true);
            }
            else
            {
                mPopupTitle.text = "Game Over";
                mResumeButton.SetActive(false);
            }
            mPopupBestScore.text = "Best Score: " + GetBestScore().ToString("00");
            mPopupCurrScore.text = "Current Score: " + mCurrentScore.ToString("00");
            AdmobAdsManager.Instance.ShowNativeAds(adNativePanel, adIcon, adChoices, adHeadline, adCallToAction, adAdvertiser, true);
        }

        private IEnumerator CheckHorizontalBoxes()
        {
            int count = 0;
            int startInd = 0;
            for (int ind = 0; ind < 8; ind++)
            {
                for (int ind1 = startInd; ind1 < (8 * (ind + 1)); ind1++)
                {
                    if (mShowImage[ind1].activeSelf)
                        count++;
                }
                if (count == 8)
                {
                    Color color = GetBoxColor(BoxDetails.mBoxNo);
                    AudioManager.Instance.PlaySFXAtPosition(AudioManager.Block_on_place);
                    StartCoroutine(SetScores(10));
                    for (int ind1 = startInd; ind1 < (8 * (ind + 1)); ind1++)
                    {
                        HideUpperBox(ind1, color);
                        mShowFilledImage[ind1].SetActive(false);
                        yield return new WaitForSeconds(0.01f);
                    }
                }
                count = 0;
                startInd += 8;
            }
        }

        private IEnumerator CheckVerticalBoxes()
        {
            int count = 0;
            int index = 0;
            for (int ind = 0; ind < 8; ind++)
            {
                index = ind;
                for (int ind1 = 0; ind1 < 8; ind1++)
                {
                    if (mShowImage[index].activeSelf)
                        count++;
                    index += 8;
                }
                if (count == 8)
                {
                    Color color = GetBoxColor(BoxDetails.mBoxNo);
                    AudioManager.Instance.PlaySFXAtPosition(AudioManager.Block_on_place);
                    index = ind;
                    StartCoroutine(SetScores(10));
                    for (int ind1 = 0; ind1 < 8; ind1++)
                    {
                        HideUpperBox(index, color);
                        mShowFilledImage[index].SetActive(false);
                        index += 8;
                        yield return new WaitForSeconds(0.01f);
                    }
                }
                count = 0;
            }
            yield return new WaitForSeconds(0.5f);
            if (!mIsGameOver)
            {
                //Debug.Log ("Checking...");
                StartCoroutine(IsGameOver());
            }
        }

        private IEnumerator SetScores(int aAddScore)
        {
            yield return new WaitForSeconds(0.3f);
            if (aAddScore == 0)
            {
                mCurrentScore = 0;
                GameObject.Find("CurrentScoreText").GetComponent<Text>().text = "00";
                GameObject.Find("BestScoreText").GetComponent<Text>().text = GetBestScore().ToString("00");
            }
            else
            {
                for (int ind = 0; ind < aAddScore; ind++)
                {
                    mCurrentScore += 1;
                    if (mBestScore == mCurrentScore && mBestScore != 1)
                        StartCoroutine(HandleScoreNotification());

                    GameObject.Find("CurrentScoreText").GetComponent<Text>().text = mCurrentScore.ToString("00");
                    if (mCurrentScore > GetBestScore())
                    {
                        SetBestScore(mCurrentScore);
                        GameObject.Find("BestScoreText").GetComponent<Text>().text = GetBestScore().ToString("00");
                    }
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }

        private IEnumerator HandleScoreNotification()
        {
            mBestScoreNotificationImage.SetActive(true);
            AudioManager.Instance.PlaySFXAtPosition(AudioManager.alert_sound);
            yield return new WaitForSeconds(4.8f);
            mBestScoreNotificationImage.SetActive(false);
        }

        private void HideUpperBox(int aInd, Color aColor)
        {
            GameObject obj = Instantiate(mBlockDestroyPrefab, GameObject.Find("PuzzleImage").transform);
            ParticleSystem.MainModule main = obj.GetComponent<ParticleSystem>().main;
            main.startColor = aColor;
            obj.GetComponent<RectTransform>().anchoredPosition = GameObject.Find("PuzzleBox_ (" + (aInd + 1) + ")").GetComponent<RectTransform>().anchoredPosition;
            mShowImage[aInd].SetActive(false);
        }

        private Color GetBoxColor(int aBoxNo)
        {
            Debug.Log("BOx No = " + aBoxNo);
            float R = 0.0F;
            float G = 0.0F;
            float B = 0.0F;
            float A = 1.0F;
            if (aBoxNo == 1)
            {
                R = 250.0F;
                G = 0.0F;
                B = 110.0F;
            }
            else if (aBoxNo == 2)
            {
                R = 255;
                G = 150;
                B = 0.0F;
            }
            else if (aBoxNo == 3)
            {
                R = 0.0F;
                G = 25.0F;
                B = 240.0F;
            }
            else if (aBoxNo == 4)
            {
                R = 0.0F;
                G = 120.0F;
                B = 40.0F;
            }
            return new Color(R, G, B, A);
        }

        public void ButtonClicked(int aButtonId)
        {
            switch (aButtonId)
            {
                case BUTTON_RESTART:
                    {
                        AdmobAdsManager._AdClosed += delegate ()
                        {
                            mBestScore = GetBestScore();
                            mBestScore++;
                            StartCoroutine(RestartGame());
                        };
                        AdmobAdsManager.Instance.ShowInterstitial();
                        FirebaseManager.Instance.SendEvent(Constants.BLOCK_RESTART);
                        AdmobAdsManager.Instance.ShowBannerAd(false);
                        break;
                    }
                case BUTTON_RESUME:
                    {
                        mPopupCanvas.SetActive(false);
                        FirebaseManager.Instance.SendEvent(Constants.BLOCK_RESUME);
                        AdmobAdsManager.Instance.ShowBannerAd(false);
                        break;
                    }
                case BUTTON_CANCEL:
                    {
                        StartCoroutine(HandleEscape());
                        break;
                    }
                case BUTTON_SCORE:
                    {
                        Social.ShowLeaderboardUI();
                        break;
                    }
            }
            AudioManager.Instance.PlaySFXAtPosition(AudioManager.click_sound);
        }
        private IEnumerator LoadScene()
        {
            yield return new WaitForEndOfFrame();
            SceneManager.LoadSceneAsync(GameData.Instance.MENU_SCENE);
            FirebaseManager.Instance.SendEvent(Constants.BLOCK_GO_TO_HOME);
        }

        private IEnumerator HandleEscape()
        {
            mIsEscape = false;
            if (mPopupCanvas.activeSelf)
            {
                HandleBackgroundMosic(false);
                loadingScreen.SetActive(true);
                StartCoroutine(LoadScene());
            }
            else
            {
                HandleGamePopup(true);
            }
            yield return new WaitForSeconds(0.3f);
            mIsEscape = true;
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Escape) && mIsEscape)
                StartCoroutine(HandleEscape());
        }

        private void SetBestScore(int aScore)
        {
            //LoginManager.instance.SetScore(aScore);
            PlayerPrefs.SetInt("BestScorePrefeb", aScore);
        }

        public static int GetBestScore()
        {
            return PlayerPrefs.GetInt("BestScorePrefeb", 0);
        }
    }
}