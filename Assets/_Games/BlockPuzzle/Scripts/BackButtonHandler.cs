﻿using System.Collections.Generic;
using UnityEngine;

public class BackButtonHandler : MonoBehaviour
{
    public static BackButtonHandler Instance;
    public Stack<BackObject> backStack = new Stack<BackObject>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(Instance);
            Instance = this;
        }
    }

#if UNITY_ANDROID || UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (backStack.Count != 0)
            {
                backStack.Peek().OnBackPressed.Invoke();
            }
        }
    }
#endif
}