﻿using UnityEngine;
using System.Collections;

namespace BlockPuzzleNS
{
	public class AnimationEventHandler : MonoBehaviour
	{
		public delegate void AnimationOverEventDelegate ();

		public static event AnimationOverEventDelegate _AnimationOverEvent=null;
		public static event AnimationOverEventDelegate _AnimationOverEvent_Second=null;
		public static event AnimationOverEventDelegate _LatterAnimationOverEvent=null;
		public static event AnimationOverEventDelegate _AnimationGameOverEvent=null;

		void Start ()
		{

		}

		void Update ()
		{

		}

		public void AnimationOverHandler ()
		{
			if (_AnimationOverEvent != null)
				_AnimationOverEvent ();
			_AnimationOverEvent = null;
		}

		public void AnimationGameOverHandler ()
		{
			if (_AnimationGameOverEvent != null)
				_AnimationGameOverEvent ();
			_AnimationGameOverEvent = null;
		}

		public void AnimationOverHandlerSecond ()
		{
			if (_AnimationOverEvent_Second != null)
				_AnimationOverEvent_Second ();
			_AnimationOverEvent_Second = null;
		}

		public void LatterAnimationOverHandler ()
		{
			if (_LatterAnimationOverEvent != null)
				_LatterAnimationOverEvent ();
			_LatterAnimationOverEvent = null;
		}

	}
}