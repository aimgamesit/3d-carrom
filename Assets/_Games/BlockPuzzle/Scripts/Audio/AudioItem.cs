﻿using UnityEngine;

namespace BlockPuzzleNS
{
    [System.Serializable]
    public class AudioItem
    {
        public string name;
        [Range(0f, 1f)]
        public float volume = 1f;
        public bool loop;
        public AudioClip clip;
    }
}