﻿using com.TicTok.managers;
using UnityEngine;

namespace BlockPuzzleNS
{
    public class AudioManager : BManager<AudioManager>
    {
        public AudioSource bgMusicAudioSource;
        public AudioItem[] AudioList;
        private float musicVolume = 1f;

        #region Audio name
        public const string alert_sound = "alert_sound";
        public const string background = "background";
        public const string Block_on_place = "Block_on_place";
        public const string click_sound = "click_sound";
        public const string game_over = "game_over";
        public const string Line_full_complete = "Line_full_complete";
        public const string new_blocks_entry = "new_blocks entry";
        public const string transition = "transition";
        #endregion

        void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        public void PlayBtnClickSound() => PlaySFXAtPosition(click_sound);

        public void PlaySFXAtPosition(string name)
        {
            if (PrefsKey.GetInt(PrefsKey.SOUND_EFFECT, 1) == 0)
                return;
            foreach (AudioItem audioItem in AudioList)
            {
                if (audioItem.name == name)
                {
                    //create gameobject for the audioSource
                    GameObject audioObj = new GameObject();
                    audioObj.transform.parent = transform;
                    audioObj.name = name;
                    AudioSource audiosource = audioObj.AddComponent<AudioSource>();

                    //audio source settings
                    audiosource.clip = audioItem.clip;
                    audiosource.spatialBlend = 1.0f;
                    audiosource.minDistance = 4f;
                    audiosource.volume = audioItem.volume;
                    //audiosource.outputAudioMixerGroup = source.outputAudioMixerGroup;
                    audiosource.loop = audioItem.loop;
                    audiosource.Play();

                    //Destroy on finish
                    if (!audioItem.loop && audiosource.clip != null)
                        audioObj.AddComponent<SelfDestroy>().time = audiosource.clip.length;
                }
            }
        }

        public void PlayMusic(string name)
        {
            foreach (AudioItem audioItem in AudioList)
            {
                if (audioItem.name == name)
                {
                    bgMusicAudioSource.clip = audioItem.clip;
                    bgMusicAudioSource.loop = true;
                    bgMusicAudioSource.volume = audioItem.volume * musicVolume;
                    bgMusicAudioSource.Play();
                }
            }
        }

        public void StopMusic()
        {
            if (bgMusicAudioSource != null)
                bgMusicAudioSource.Stop();
        }
    }
}