﻿using UnityEngine;
using System.Collections;

namespace BlockPuzzleNS
{
    public class ShapeArray
    {
        public ArrayList mShape1 = new ArrayList();
        public ArrayList mShape2 = new ArrayList();
        public ArrayList mShape3 = new ArrayList();
        public ArrayList mShape4 = new ArrayList();
    }
}