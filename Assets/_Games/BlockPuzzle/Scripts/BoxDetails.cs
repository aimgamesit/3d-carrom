﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlockPuzzleNS
{
    public class BoxDetails : MonoBehaviour
    {
        private List<int> mShowImageBoxesIndex = new List<int>();
        private List<int> mFilledImageBoxesIndex = new List<int>();
        private Vector3 mScale = new Vector3(1, 1, 1);
        private Vector3 mBoxesPos = Vector3.zero;
        private Vector3 mStartPos = Vector3.zero;
        private int mIndex = 0, DISTANCE = 2;
        private Vector3 mRotation = new Vector3(0, 0, 0);
        private bool mIsTapped = false, mIsDragging = false;
        public static int mBoxNo = -1;

        void Start()
        {
            mStartPos = Camera.main.ScreenToWorldPoint(gameObject.transform.position);
        }

        public void BeginTouch()
        {
            if (GamePlayHandler.Instance.mIsGameOver)
                return;
            mIsTapped = true;
        }

        public void DragObj()
        {
            if (GamePlayHandler.Instance.mIsGameOver)
                return;

            if (!mIsDragging)
            {
                if (Mathf.Abs(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - mStartPos.x) > DISTANCE || Mathf.Abs(Camera.main.ScreenToWorldPoint(Input.mousePosition).y - mStartPos.y) > DISTANCE)
                {
                    mIsTapped = false;
                    mIsDragging = true;
                }
                else
                {
                    mIsTapped = true;
                    return;
                }
            }

            mBoxesPos = Input.mousePosition;
            mBoxesPos.y += 260.0f;
            //     gameObject.transform.position = mBoxesPos;

            mBoxesPos.z = 0;
            mBoxesPos = Camera.main.ScreenToWorldPoint(mBoxesPos);
            mBoxesPos.z = 0;
            gameObject.transform.position = mBoxesPos;
            gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, 0.0f);

            gameObject.transform.localScale = mScale;
            for (int ind = 0; ind < mShowImageBoxesIndex.Count; ind++)
            {
                mIndex = mShowImageBoxesIndex[ind];
                GamePlayHandler.Instance.mShowImage[mIndex - 1].SetActive(false);
            }
            for (int ind = 0; ind < mFilledImageBoxesIndex.Count; ind++)
            {
                mIndex = mFilledImageBoxesIndex[ind];
                GamePlayHandler.Instance.mShowFilledImage[mIndex].SetActive(false);
            }
            mShowImageBoxesIndex.Clear();
            //		mFilledImageBoxesIndex.Clear ();
            for (int ind = 1; ind <= 64; ind++)
            {
                for (int ind1 = 0; ind1 < transform.childCount; ind1++)
                {
                    if (!mShowImageBoxesIndex.Contains(ind))
                    {
                        if (GameObject.Find("PuzzleBox_ (" + ind + ")").GetComponent<BoxCollider2D>().bounds.Contains(transform.GetChild(ind1).transform.position))
                        {
                            if (!GamePlayHandler.Instance.mShowImage[ind - 1].activeSelf)
                                mShowImageBoxesIndex.Add(ind);
                        }
                    }
                }
            }
            if (mShowImageBoxesIndex.Count == transform.childCount)
            {
                int boxNo = transform.GetComponent<PatternHandler>().mBoxNo;
                for (int ind = 0; ind < mShowImageBoxesIndex.Count; ind++)
                {
                    mIndex = mShowImageBoxesIndex[ind];
                    mBoxNo = boxNo;
                    GamePlayHandler.Instance.mShowImage[mIndex - 1].SetActive(true);
                    GamePlayHandler.Instance.mShowImage[mIndex - 1].GetComponent<Image>().sprite = GameData.Instance.boxSprites[boxNo - 1];
                    //PatternHandler.ChangeImage(, "BlockPuzzle/game_play/boxs/" + );
                }
            }
            mFilledImageBoxesIndex.Clear();
            CheckHorizontal();
            CheckVertical();
        }

        private void CheckHorizontal()
        {
            int count = 0;
            int startInd = 0;
            for (int ind = 0; ind < 8; ind++)
            {
                for (int ind1 = startInd; ind1 < (8 * (ind + 1)); ind1++)
                {
                    if (GamePlayHandler.Instance.mShowImage[ind1].activeSelf)
                        count++;
                }
                if (count == 8)
                {
                    int boxNo = transform.GetComponent<PatternHandler>().mBoxNo;
                    //				mFilledImageBoxesIndex.Clear ();
                    for (int ind1 = startInd; ind1 < (8 * (ind + 1)); ind1++)
                    {
                        mFilledImageBoxesIndex.Add(ind1);
                        GamePlayHandler.Instance.mShowFilledImage[ind1].SetActive(true);
                        GamePlayHandler.Instance.mShowFilledImage[ind1].GetComponent<Image>().sprite = GameData.Instance.boxSprites[boxNo - 1];
                        //  PatternHandler.ChangeImage(GamePlayHandler.mShowFilledImage[ind1], "BlockPuzzle/game_play/boxs/" + boxNo);
                    }
                }
                count = 0;
                startInd += 8;
            }
        }

        private void CheckVertical()
        {
            int count = 0, aIndex = 0;
            for (int ind = 0; ind < 8; ind++)
            {
                aIndex = ind;
                for (int ind1 = 0; ind1 < 8; ind1++)
                {
                    if (GamePlayHandler.Instance.mShowImage[aIndex].activeSelf)
                        count++;
                    aIndex += 8;
                }
                if (count == 8)
                {
                    int boxNo = transform.GetComponent<PatternHandler>().mBoxNo;
                    //				mFilledImageBoxesIndex.Clear ();
                    aIndex = ind;
                    for (int ind1 = 0; ind1 < 8; ind1++)
                    {
                        mFilledImageBoxesIndex.Add(aIndex);
                        GamePlayHandler.Instance.mShowFilledImage[aIndex].SetActive(true);
                        GamePlayHandler.Instance.mShowFilledImage[aIndex].GetComponent<Image>().sprite = GameData.Instance.boxSprites[boxNo - 1];
                        // PatternHandler.ChangeImage(GamePlayHandler.mShowFilledImage[aIndex], "BlockPuzzle/game_play/boxs/" + boxNo);
                        aIndex += 8;
                    }
                }
                count = 0;
            }
        }

        public void EndTouch()
        {
            if (GamePlayHandler.Instance.mIsGameOver)
                return;

            bool isLeft = false;
            mIsDragging = false;
            if (mIsTapped)
            {
                if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x >= mStartPos.x)
                {
                    isLeft = true;
                    mRotation.z = gameObject.transform.eulerAngles.z - 90.0f;
                }
                else
                {
                    isLeft = false;
                    mRotation.z = gameObject.transform.eulerAngles.z + 90.0f;
                }
                gameObject.transform.eulerAngles = mRotation;
                for (int ind = 0; ind < gameObject.transform.childCount; ind++)
                {
                    if (isLeft)
                        mRotation.z = gameObject.transform.GetChild(ind).transform.eulerAngles.z + 90;
                    else
                        mRotation.z = gameObject.transform.GetChild(ind).transform.eulerAngles.z - 90;
                    gameObject.transform.GetChild(ind).transform.eulerAngles = mRotation;
                    AudioManager.Instance.PlaySFXAtPosition(AudioManager.click_sound);
                }
                GamePlayHandler.Instance.tutorialHandler.OnBtnCLicked(false);
                return;
            }
            for (int ind = 0; ind < mShowImageBoxesIndex.Count; ind++)
            {
                mIndex = mShowImageBoxesIndex[ind];
                GamePlayHandler.Instance.mShowImage[mIndex - 1].SetActive(false);
            }
            GamePlayHandler.Instance.EndTouch(gameObject);
        }
    }
}