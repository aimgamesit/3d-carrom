﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlockPuzzleNS
{
    public class PatternHandler : MonoBehaviour
    {
        private const int ROW = 5, COL = 5;
        private int CROP_ROW_ONE = -1, CROP_COL_ONE = -1, CROP_ROW_TWO = -1, CROP_COL_TWO = -1, CROP_ROW_THREE = -1, CROP_COL_THREE = -1, CROP_ROW_FOUR = -1, CROP_COL_FOUR = -1;
        public int mBoxNo = 1, mCurrentPattern = 0;
        private MatrixOfPatternArray mMatrixOfPatternArray = new MatrixOfPatternArray();
        private CropedMatrixArray mCropedMatrixArray = new CropedMatrixArray();

        class MatrixOfPatternArray
        {
            public int[,] mMatrixOfPattern1 = new int[ROW, COL];
            public int[,] mMatrixOfPattern2 = new int[ROW, COL];
            public int[,] mMatrixOfPattern3 = new int[ROW, COL];
            public int[,] mMatrixOfPattern4 = new int[ROW, COL];
        }

        class CropedMatrixArray
        {
            public int[,] mCropMatrix1 = null;
            public int[,] mCropMatrix2 = null;
            public int[,] mCropMatrix3 = null;
            public int[,] mCropMatrix4 = null;
        }

        void Start()
        {
            StartCoroutine(InitializeStart());
        }

        private IEnumerator InitializeStart()
        {
            mCurrentPattern = Random.Range(0, PatternCreateHandler.mObjectsOfShape.Count);
            MakeMatrixOfPatterns();

            yield return new WaitForSeconds(0.00001f);

            ShapeArray shapeArray = (PatternCreateHandler.mObjectsOfShape[mCurrentPattern]) as ShapeArray;
            mBoxNo = Random.Range(1, 5);
            ArrayList indexes = new ArrayList();
            int patterNo = Random.Range(0, 4);
            if (patterNo == 1)
                indexes = shapeArray.mShape1;
            else if (patterNo == 2)
                indexes = shapeArray.mShape2;
            else if (patterNo == 3)
                indexes = shapeArray.mShape3;
            else
                indexes = shapeArray.mShape4;

            int x = 0, y = 0;
            for (int ind = 1; ind <= 25; ind++)
            {
                Vector2 pos = GetRowCol(ind - 1);
                if (indexes.Contains(ind))
                    transform.GetChild(ind - 1).gameObject.GetComponent<Image>().sprite = GameData.Instance.boxSprites[mBoxNo-1];
                else
                    Destroy(transform.GetChild(ind - 1).gameObject);
            }
            yield return new WaitForSeconds(0.00001f);
            CreateCropMatrix();
        }

        private void CreateCropMatrix()
        {
            CROP_ROW_ONE = GetRows(mMatrixOfPatternArray.mMatrixOfPattern1);
            CROP_COL_ONE = GetColoumns(mMatrixOfPatternArray.mMatrixOfPattern1);
            Vector2 startPos = GetStartPos(mMatrixOfPatternArray.mMatrixOfPattern1);
            mCropedMatrixArray.mCropMatrix1 = new int[CROP_ROW_ONE, CROP_COL_ONE];

            int x = 0, y = 0;
            for (int row = (int)startPos.x; row < CROP_ROW_ONE + (int)startPos.x; row++)
            {
                for (int col = (int)startPos.y; col < CROP_COL_ONE + (int)startPos.y; col++)
                {
                    mCropedMatrixArray.mCropMatrix1[x, y] = mMatrixOfPatternArray.mMatrixOfPattern1[row, col];
                    //				//Debug.Log (mCropedMatrix [x, y] + " > " + row + "   " + col);
                    y++;
                }
                x++;
                y = 0;
            }
            CROP_ROW_TWO = GetRows(mMatrixOfPatternArray.mMatrixOfPattern2);
            CROP_COL_TWO = GetColoumns(mMatrixOfPatternArray.mMatrixOfPattern2);
            startPos = GetStartPos(mMatrixOfPatternArray.mMatrixOfPattern2);
            mCropedMatrixArray.mCropMatrix2 = new int[CROP_ROW_TWO, CROP_COL_TWO];
            x = y = 0;
            for (int row = (int)startPos.x; row < CROP_ROW_TWO + (int)startPos.x; row++)
            {
                for (int col = (int)startPos.y; col < CROP_COL_TWO + (int)startPos.y; col++)
                {
                    mCropedMatrixArray.mCropMatrix2[x, y] = mMatrixOfPatternArray.mMatrixOfPattern2[row, col];
                    //				//Debug.Log (mCropedMatrix [x, y] + " > " + row + "   " + col);
                    y++;
                }
                x++;
                y = 0;
            }


            CROP_ROW_THREE = GetRows(mMatrixOfPatternArray.mMatrixOfPattern3);
            CROP_COL_THREE = GetColoumns(mMatrixOfPatternArray.mMatrixOfPattern3);
            startPos = GetStartPos(mMatrixOfPatternArray.mMatrixOfPattern3);
            mCropedMatrixArray.mCropMatrix3 = new int[CROP_ROW_THREE, CROP_COL_THREE];
            x = y = 0;
            for (int row = (int)startPos.x; row < CROP_ROW_THREE + (int)startPos.x; row++)
            {
                for (int col = (int)startPos.y; col < CROP_COL_THREE + (int)startPos.y; col++)
                {
                    mCropedMatrixArray.mCropMatrix3[x, y] = mMatrixOfPatternArray.mMatrixOfPattern3[row, col];
                    //				//Debug.Log (mCropedMatrix [x, y] + " > " + row + "   " + col);
                    y++;
                }
                x++;
                y = 0;
            }
            CROP_ROW_FOUR = GetRows(mMatrixOfPatternArray.mMatrixOfPattern4);
            CROP_COL_FOUR = GetColoumns(mMatrixOfPatternArray.mMatrixOfPattern4);
            startPos = GetStartPos(mMatrixOfPatternArray.mMatrixOfPattern4);
            mCropedMatrixArray.mCropMatrix4 = new int[CROP_ROW_FOUR, CROP_COL_FOUR];
            x = y = 0;
            for (int row = (int)startPos.x; row < CROP_ROW_FOUR + (int)startPos.x; row++)
            {
                for (int col = (int)startPos.y; col < CROP_COL_FOUR + (int)startPos.y; col++)
                {
                    mCropedMatrixArray.mCropMatrix4[x, y] = mMatrixOfPatternArray.mMatrixOfPattern4[row, col];
                    //				//Debug.Log (mCropedMatrixArray.mCropMatrix4 [x, y] + " > " + row + "   " + col);
                    y++;
                }
                x++;
                y = 0;
            }

        }

        private void MakeMatrixOfPatterns()
        {
            ArrayList shape1 = (PatternCreateHandler.mObjectsOfShape[mCurrentPattern] as ShapeArray).mShape1;
            ArrayList shape2 = (PatternCreateHandler.mObjectsOfShape[mCurrentPattern] as ShapeArray).mShape2;
            ArrayList shape3 = (PatternCreateHandler.mObjectsOfShape[mCurrentPattern] as ShapeArray).mShape3;
            ArrayList shape4 = (PatternCreateHandler.mObjectsOfShape[mCurrentPattern] as ShapeArray).mShape4;

            for (int ind = 1; ind <= 25; ind++)
            {
                Vector2 pos = GetRowCol(ind - 1);
                if (shape1.Contains(ind))
                    mMatrixOfPatternArray.mMatrixOfPattern1[(int)pos.x, (int)pos.y] = 1;
                else
                    mMatrixOfPatternArray.mMatrixOfPattern1[(int)pos.x, (int)pos.y] = 0;

                if (shape2.Contains(ind))
                    mMatrixOfPatternArray.mMatrixOfPattern2[(int)pos.x, (int)pos.y] = 1;
                else
                    mMatrixOfPatternArray.mMatrixOfPattern2[(int)pos.x, (int)pos.y] = 0;

                if (shape3.Contains(ind))
                    mMatrixOfPatternArray.mMatrixOfPattern3[(int)pos.x, (int)pos.y] = 1;
                else
                    mMatrixOfPatternArray.mMatrixOfPattern3[(int)pos.x, (int)pos.y] = 0;

                if (shape4.Contains(ind))
                    mMatrixOfPatternArray.mMatrixOfPattern4[(int)pos.x, (int)pos.y] = 1;
                else
                    mMatrixOfPatternArray.mMatrixOfPattern4[(int)pos.x, (int)pos.y] = 0;
            }
        }

        public bool IsGameOver()
        {
            //Debug.Log (gameObject.name);
            if (IsShapeOneOver() && IsShapeTwoOver() && IsShapeThreeOver() && IsShapeFourOver())
                return true;
            else
                return false;
        }

        private bool IsShapeOneOver()
        {
            int x = 0, y = 0, index = 0, totalOne = 0, counter = 0;

            for (int row = 0; row < CROP_ROW_ONE; row++)
            {
                for (int col = 0; col < CROP_COL_ONE; col++)
                {
                    if (mCropedMatrixArray.mCropMatrix1[row, col] == 1)
                        totalOne++;
                }
            }
            for (int ind = 0; ind < 9 - CROP_ROW_ONE; ind++)
            {
                for (int ind1 = 0; ind1 < 9 - CROP_COL_ONE; ind1++)
                {
                    x = 0;
                    for (int row = ind; row < CROP_ROW_ONE + ind; row++)
                    {
                        for (int col = ind1; col < CROP_COL_ONE + ind1; col++)
                        {
                            if (mCropedMatrixArray.mCropMatrix1[x, y] == 1)
                            {
                                index = GetNoOfIndexes(row, col);
                                if (!GamePlayHandler.Instance.mShowImage[index - 1].activeSelf)
                                    counter++;
                            }
                            y++;
                        }
                        if (counter == totalOne)
                        {
                            //Debug.Log ("ONE FALSE!");
                            return false;
                        }
                        x++;
                        y = 0;
                    }
                    counter = 0;
                }
            }
            if (counter == totalOne)
            {
                //Debug.Log ("ONE FALSE");
                return false;
            }
            else
            {
                //Debug.Log ("ONE TRUE");
                return true;
            }
        }

        private bool IsShapeTwoOver()
        {
            int x = 0, y = 0, index = 0, totalOne = 0, counter = 0;

            for (int row = 0; row < CROP_ROW_TWO; row++)
            {
                for (int col = 0; col < CROP_COL_TWO; col++)
                {
                    if (mCropedMatrixArray.mCropMatrix2[row, col] == 1)
                        totalOne++;
                }
            }
            for (int ind = 0; ind < 9 - CROP_ROW_TWO; ind++)
            {
                for (int ind1 = 0; ind1 < 9 - CROP_COL_TWO; ind1++)
                {
                    x = 0;
                    for (int row = ind; row < CROP_ROW_TWO + ind; row++)
                    {
                        for (int col = ind1; col < CROP_COL_TWO + ind1; col++)
                        {
                            if (mCropedMatrixArray.mCropMatrix2[x, y] == 1)
                            {
                                index = GetNoOfIndexes(row, col);
                                if (!GamePlayHandler.Instance.mShowImage[index - 1].activeSelf)
                                    counter++;
                            }
                            y++;
                        }
                        if (counter == totalOne)
                        {
                            //Debug.Log ("TWO FALSE!");
                            return false;
                        }
                        x++;
                        y = 0;
                    }
                    counter = 0;
                }
            }
            if (counter == totalOne)
            {
                //Debug.Log ("TWO FALSE");
                return false;
            }
            else
            {
                //Debug.Log ("TWO TRUE");
                return true;
            }
        }

        private bool IsShapeThreeOver()
        {
            int x = 0, y = 0, index = 0, totalOne = 0, counter = 0;

            for (int row = 0; row < CROP_ROW_THREE; row++)
            {
                for (int col = 0; col < CROP_COL_THREE; col++)
                {
                    if (mCropedMatrixArray.mCropMatrix3[row, col] == 1)
                        totalOne++;
                }
            }
            for (int ind = 0; ind < 9 - CROP_ROW_THREE; ind++)
            {
                for (int ind1 = 0; ind1 < 9 - CROP_COL_THREE; ind1++)
                {
                    x = 0;
                    for (int row = ind; row < CROP_ROW_THREE + ind; row++)
                    {
                        for (int col = ind1; col < CROP_COL_THREE + ind1; col++)
                        {
                            if (mCropedMatrixArray.mCropMatrix3[x, y] == 1)
                            {
                                index = GetNoOfIndexes(row, col);
                                if (!GamePlayHandler.Instance.mShowImage[index - 1].activeSelf)
                                    counter++;
                            }
                            y++;
                        }
                        if (counter == totalOne)
                        {
                            //Debug.Log ("THREE FALSE!");
                            return false;
                        }
                        x++;
                        y = 0;
                    }
                    counter = 0;
                }
            }
            if (counter == totalOne)
            {
                //Debug.Log ("THREE FALSE");
                return false;
            }
            else
            {
                //Debug.Log ("THREE TRUE");
                return true;
            }
        }

        private bool IsShapeFourOver()
        {
            int x = 0, y = 0, index = 0, totalOne = 0, counter = 0;

            for (int row = 0; row < CROP_ROW_FOUR; row++)
            {
                for (int col = 0; col < CROP_COL_FOUR; col++)
                {
                    if (mCropedMatrixArray.mCropMatrix4[row, col] == 1)
                        totalOne++;
                }
            }
            for (int ind = 0; ind < 9 - CROP_ROW_FOUR; ind++)
            {
                for (int ind1 = 0; ind1 < 9 - CROP_COL_FOUR; ind1++)
                {
                    x = 0;
                    for (int row = ind; row < CROP_ROW_FOUR + ind; row++)
                    {
                        for (int col = ind1; col < CROP_COL_FOUR + ind1; col++)
                        {
                            if (mCropedMatrixArray.mCropMatrix4[x, y] == 1)
                            {
                                index = GetNoOfIndexes(row, col);
                                if (!GamePlayHandler.Instance.mShowImage[index - 1].activeSelf)
                                    counter++;
                            }
                            y++;
                        }
                        if (counter == totalOne)
                        {
                            //Debug.Log ("FOUR FALSE!");
                            return false;
                        }
                        x++;
                        y = 0;
                    }
                    counter = 0;
                }
            }
            if (counter == totalOne)
            {
                //Debug.Log ("FOUR FALSE");
                return false;
            }
            else
            {
                //Debug.Log ("FOUR TRUE");
                return true;
            }
        }

        private int GetNoOfIndexes(int aRow, int aCol)
        {
            int counter = 0;
            for (int row = 0; row < 8; row++)
            {
                for (int col = 0; col < 8; col++)
                {
                    counter++;
                    if (row == aRow && col == aCol)
                        return counter;
                }
            }
            return counter;
        }

        private Vector2 GetStartPos(int[,] aMatrixOfPattern)
        {
            int y = 25;
            int x = 25;
            for (int row = 0; row < ROW; row++)
            {
                for (int col = 0; col < COL; col++)
                {
                    if (aMatrixOfPattern[row, col] == 1)
                        if (y > col)
                            y = col;
                }
            }
            int a = 0;
            for (int row = 0; row < ROW; row++)
            {
                for (int col = 0; col < COL; col++)
                {
                    if (aMatrixOfPattern[col, row] == 1)
                        if (x > a)
                            x = a;
                    a++;
                }
                a = 0;
            }
            return new Vector2(x, y);
        }

        private int GetColoumns(int[,] aMatrixOfPattern)
        {
            int rows = 0, count = 0;
            for (int row = 0; row < ROW; row++)
            {
                for (int col = 0; col < COL; col++)
                {
                    if (aMatrixOfPattern[row, col] == 1)
                    {
                        count += 1;
                        if (count > rows)
                            rows = count;
                    }
                }
                count = 0;
            }
            return rows;
        }

        private int GetRows(int[,] aMatrixOfPattern)
        {
            int cols = 0, count = 0;
            for (int row = 0; row < COL; row++)
            {
                for (int col = 0; col < COL; col++)
                {
                    if (aMatrixOfPattern[col, row] == 1)
                    {
                        count += 1;
                        if (count > cols)
                            cols = count;
                    }
                }
                count = 0;
            }
            return cols;
        }

        private Vector2 GetRowCol(int aIndex)
        {
            return new Vector2((aIndex / 5), (Mathf.Abs(aIndex % 5)));
        }

        public static void ChangeImage(GameObject aGameObject, string aFileWithPath)
        {
            //			Debug.Log (aGameObject + "   :     " + aFileWithPath);
            Texture2D texture = Resources.Load(aFileWithPath) as Texture2D;
            aGameObject.GetComponent<UnityEngine.UI.Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
        }
    }
}