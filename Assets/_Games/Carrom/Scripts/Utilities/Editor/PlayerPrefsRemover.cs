﻿using System.IO;
using UnityEngine;
using UnityEditor;
using AIMCarrom;

namespace CarromPool
{
	public class PlayerPrefsRemover : Editor
	{

		[MenuItem("Tools/PlayerPrefs Remover")]
		private static void RemovePlayerPrefs()
		{
			File.Delete(Application.persistentDataPath + "/" + Constants.USER_FILE_NAME + ".dat");
			File.Delete(Application.persistentDataPath + "/" + Constants.CARROM_USER_FILE_NAME + ".dat");
            //File.Delete(Application.persistentDataPath + Constant.GAME_VALUES_FILE_NAME );
            PlayerPrefs.DeleteAll();
			Debug.LogFormat("<color=magenta>PLAYER PREFS DELETED</color>");
		}
	}
}