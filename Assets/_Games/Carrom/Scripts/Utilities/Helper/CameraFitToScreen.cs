﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class CameraFitToScreen : MonoBehaviour
{
    Camera cam;

    float camSizeMax = 45f;
    float camSizeMin = 38.5f;

    float screenRatioMin = 18f / 39f;
    //float screenRatioMax = 9f / 16f;

    private void Awake()
    {
        cam = Camera.main;
    }

    void Start()
    {
#if !UNITY_EDITOR
        float currentScreen = ((float)Screen.width / (float)Screen.height);
        var camSize = Mathf.Clamp(camSizeMax + (screenRatioMin - currentScreen) * 54f, camSizeMin, camSizeMax);
        cam.fieldOfView = camSize;
#endif
    }

#if UNITY_EDITOR
    private void Update()
    {
        float currentScreen = ((float)Screen.width / (float)Screen.height);
        var camSize = Mathf.Clamp(camSizeMax + (screenRatioMin - currentScreen), camSizeMin, camSizeMax);
        Camera.main.orthographicSize = camSize;
    }
#endif
}
