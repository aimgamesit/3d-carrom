﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AIMCarrom
{
    public class Loader : MonoBehaviour
    {
        [SerializeField] string gameSceneName;
        [SerializeField] UnityEngine.UI.Image loadFillImage;

        Coroutine loadSceneCoroutine;

        private void Start()
        {
            loadFillImage.fillAmount = 0;
            LoadGameScene();
        }

        void LoadGameScene()
        {
            if (loadSceneCoroutine != null)
                StopCoroutine(loadSceneCoroutine);

            loadSceneCoroutine = StartCoroutine(LoadGameSceneCoroutine());
        }

        IEnumerator LoadGameSceneCoroutine(float loadTime = 2f)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(gameSceneName);
            asyncLoad.allowSceneActivation = false;

            float timeElapsed = 0;

            while (timeElapsed <= loadTime && !asyncLoad.isDone)
            {
                loadFillImage.fillAmount = Mathf.Clamp(timeElapsed / loadTime, 0, 1);
                timeElapsed += Time.deltaTime;

                if (asyncLoad.progress >= 0.9f && timeElapsed > loadTime)
                    asyncLoad.allowSceneActivation = true;

                yield return null;
            }

            //while ()
            //{
            //    loadFillImage.fillAmount = asyncLoad.progress / 0.9f;
            //    yield return null;

            //    if (asyncLoad.progress >= 0.9f)
            //        asyncLoad.allowSceneActivation = true;
            //}
        }
    }
}
