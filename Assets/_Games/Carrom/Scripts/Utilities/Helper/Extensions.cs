﻿using System.Security.Cryptography;
using UnityEngine;
using System.Diagnostics;

namespace AIMCarrom
{
    public static class Extensions
    {
        private static System.Random ran = new System.Random();

        public static string GetUniqueID(this string collection)
        {
            string id = System.DateTime.Now.Ticks.ToString();
            id = id.Substring(id.Length - 5, 5);
            byte[] rno;
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rno = new byte[6];
                rng.GetBytes(rno);
            }

            int rnum = System.Math.Abs(System.BitConverter.ToInt32(rno, 0));

            string text = rnum.ToString().Substring(0, 3);
            return collection + id + text;
        }


        public static string ToStringAbb(this long number, bool showDecimal = true)
        {
            string[] abbreviations = { "T", "B", "M", "K" };
            long[] abbreviationsLength = { 1_000_000_000_000, 1_000_000_000, 1_000_000, 1_000 };
            bool found = false;
            float decimalNumber = 0;
            int i = 0;
            for (; i < abbreviations.Length; i++)
            {
                if (number >= abbreviationsLength[i])
                {
                    decimalNumber = (float)((float)number / ((float)abbreviationsLength[i] / 10f)) / 10f;
                    found = true;
                    break;
                }
            }

            if (found)
            {
                decimalNumber = (float)System.Math.Round(decimalNumber, 1);
                var x = decimalNumber - System.Math.Truncate(decimalNumber);
                showDecimal = x > 0;
            }

            return found ? (showDecimal ? decimalNumber.ToString("0.0") : decimalNumber.ToString("0")) + abbreviations[i] : number.ToString();
        }

        public static string ToStringAbb(this int number, bool showDecimal = true)
        {
            string[] abbreviations = { "B", "M", "K" };
            int[] abbreviationsLength = { 1_000_000_000, 1_000_000, 1_000 };
            bool found = false;
            float decimalNumber = 0;
            int i = 0;
            for (; i < abbreviations.Length; i++)
            {
                if (number >= abbreviationsLength[i])
                {
                    decimalNumber = (float)((float)number / ((float)abbreviationsLength[i] / 10f)) / 10f;
                    found = true;
                    break;
                }
            }

            if (found)
            {
                decimalNumber = (float)System.Math.Round(decimalNumber, 1);
                var x = decimalNumber - System.Math.Truncate(decimalNumber);
                showDecimal = x > 0;
            }

            return found ? (showDecimal ? decimalNumber.ToString("0.0") : decimalNumber.ToString("0")) + abbreviations[i] : number.ToString();
        }

        public static void Shuffle<T>(this System.Collections.Generic.IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = ran.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static Texture2D GetCroppedImage(string img)
        {
            var userTexture = new Texture2D(2, 2, TextureFormat.RGBA32, false);
            userTexture.LoadImage(System.Convert.FromBase64String(img));

            int size = Mathf.Min(userTexture.width, userTexture.height);

            var userTextureCropped = new Texture2D(size, size, TextureFormat.RGBA32, false);
            userTextureCropped.SetPixels(userTexture.GetPixels(0, userTexture.height - size, size, size));
            userTextureCropped.Apply();
            return userTextureCropped;
        }

        public static bool IsConnectedToInternet()
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }

        public static bool Contains(this string source, string toCheck, System.StringComparison comp)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }

        public static float TruncateUptoDecimal(this float val, int uptoDecimal)
        {
            int n = 1;
            for (int i = 0; i < uptoDecimal; i++)
            {
                n *= 10;
            }
            val = Mathf.Round(val * n) / n;
            return val;
        }

        public static void PrintMethodStack(string methodFrom = "")
        {
            StackTrace stackTrace = new StackTrace();           // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

            // write call stack method names
            string s = methodFrom;
            foreach (StackFrame stackFrame in stackFrames)
            {
                s += stackFrame.GetMethod().Name + " -> ";
            }
            UnityEngine.Debug.Log(s);   // write method name
        }
    }
    public class Vector3Serializable
    {
        public float x;
        public float y;
        public float z;

        #region CONSTRUCTOR
        public Vector3Serializable(Vector3 position)
        {
            x = position.x;
            y = position.y;
            z = position.z;
        }
        #endregion CONSTRUCTOR

        #region PUBLIC METHODS
        public Vector3 GetPosition()
        {
            return new Vector3(x, y, z);
        }

        public void SetPosition(Vector3 position)
        {
            x = position.x;
            y = position.y;
            z = position.z;
        }
        #endregion PUBLIC METHODS

        #region OVERRIDDEN METHODS
        public override string ToString()
        {
            return string.Format("[ POSITION: X: {0} Y: {1} Z: {2} ]{3}", x, y, z, System.Environment.NewLine);
        }
        #endregion OVERRIDDEN METHODS
    }
}