﻿using UnityEngine;
using UnityEngine.EventSystems;

public class GraphicRaycastObject : MonoBehaviour
{
    public bool IsMouseOnGraphics;
    public static GraphicRaycastObject Instance;

    private void Awake()
    {
        if (Instance)
            Destroy(this);
        else
            Instance = this;
    }

    private void Update()
    {
#if UNITY_EDITOR
        IsMouseOnGraphics = EventSystem.current.IsPointerOverGameObject();
#else
        if (Input.touchCount > 0)
        {
            foreach (Touch touch in Input.touches)
            {
                IsMouseOnGraphics = EventSystem.current.IsPointerOverGameObject(touch.fingerId);
            }
        }
        else
            IsMouseOnGraphics = false;
#endif
    }
}
