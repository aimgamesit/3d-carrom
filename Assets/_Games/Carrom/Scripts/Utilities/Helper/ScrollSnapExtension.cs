﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    public class ScrollSnapExtension : ScrollSnap
    {
        public System.Action<int> onScrollPageChanged;
        [SerializeField] Transform elementsParent;

        protected override IEnumerator Start()
        {
            yield return StartCoroutine(base.Start());
            yield return new WaitForSeconds(1.5f);

            elementsParent.GetComponent<ContentSizeFitter>().enabled = false;
            elementsParent.GetComponent<HorizontalLayoutGroup>().enabled = false;
        }

        private void ScaleNextPrevPages(int currentPage)
        {
            if (currentPage < _pages - 1)
                _listContainerTransform.GetChild(currentPage + 1).GetChild(0).localScale = Vector3.one * 0.75f;

            if (currentPage > 0)
                _listContainerTransform.GetChild(currentPage - 1).GetChild(0).localScale = Vector3.one * 0.75f;

            _listContainerTransform.GetChild(currentPage).GetChild(0).localScale = Vector3.one;
        }

        public override void PageChanged(int currentPage)
        {
            base.PageChanged(currentPage);
            //ScaleNextPrevPages(currentPage);
            onScrollPageChanged?.Invoke(currentPage);
        }

        public override void OnDrag(PointerEventData eventData)
        {
            base.OnDrag(eventData);
        }
    }
}
