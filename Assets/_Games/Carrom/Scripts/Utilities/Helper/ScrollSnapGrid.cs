﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEngine.UI.Extensions
{
    public class ScrollSnapGrid : ScrollSnap
    {
        [SerializeField] RectTransform scrollRect;
        [SerializeField] float itemHeight;
        private float itemsVisible;
        protected override IEnumerator Start()
        {
            yield return StartCoroutine(base.Start());
            itemsVisible = scrollRect.rect.size.y / itemHeight;
            Debug.LogFormat("items Visible : {0}", itemsVisible);
        }

        public override void UpdateListItemsSize()
        {
            float size = 0;
            float currentSize = 0;
            if (direction == ScrollSnap.ScrollDirection.Horizontal)
            {
                size = _scrollRectTransform.rect.width / itemsVisible;
                currentSize = _listContainerRectTransform.rect.width / _itemsCount;
            }
            else
            {
                size = _scrollRectTransform.rect.height / (scrollRect.rect.size.y / itemHeight);
                currentSize = _listContainerRectTransform.rect.height / _itemsCount;
            }

            _itemSize = size;

            if (LinkScrolrectScrollSensitivity)
            {
                _scroll_rect.scrollSensitivity = _itemSize;
            }

            if (AutoLayoutItems && currentSize != size && _itemsCount > 0)
            {
                if (direction == ScrollSnap.ScrollDirection.Horizontal)
                {
                    foreach (var tr in _listContainerTransform)
                    {
                        GameObject child = ((Transform)tr).gameObject;
                        if (child.activeInHierarchy)
                        {
                            var childLayout = child.GetComponent<LayoutElement>();

                            if (childLayout == null)
                            {
                                childLayout = child.AddComponent<LayoutElement>();
                            }

                            childLayout.minWidth = _itemSize;
                        }
                    }
                }
                else
                {
                    foreach (var tr in _listContainerTransform)
                    {
                        GameObject child = ((Transform)tr).gameObject;
                        if (child.activeInHierarchy)
                        {
                            var childLayout = child.GetComponent<LayoutElement>();

                            if (childLayout == null)
                            {
                                childLayout = child.AddComponent<LayoutElement>();
                            }

                            childLayout.minHeight = _itemSize;
                        }
                    }
                }
            }
        }
    }
}