﻿using System.Collections.Generic;
using static ShopManager;

namespace AIMCarrom
{
    

    [System.Serializable]
    public class PlayerProfileData
    {
        public string playerName;
        public string uniqueID;
        public string email;
        public int gamesPlayed;
        public int gamesWon;
        public int winStreak;
        private long currentGold;
        public string imageString;
        public string language;
        public string country;
        public int bet;

        public long TotalCoins
        {
            get { return currentGold; }
            set
            {
                UnityEngine.Debug.Log(currentGold + " TOTAL COINS: " + value);
                currentGold = value > 0 ? value : 0;
            }
        }
    }
}