﻿using UnityEngine;
using UnityEngine.UI;

namespace AIMCarrom
{
    public class InputFieldsCleaner : MonoBehaviour
    {
        [SerializeField] Text[] inputFields;

        private void OnEnable()
        {
            CleanInputFields();
        }

        void CleanInputFields()
        {
            foreach (var item in inputFields)
            {
                item.text = string.Empty;
            }
        }
    }
}