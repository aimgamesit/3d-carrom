﻿using System.Collections.Generic;

namespace AIMCarrom
{
    [System.Serializable]
    public struct User
    {
        public string _id;
        public string email;
        public string deviceId;
        public string userName;
        public string imageUrl;
        public string name;
        public int gamesPlayed;
        public int gamesWon;
        public int winStreak;
        public int currentGold;
        public int totalGold;
        public string country;
        public int bet;

        public int currentStriker;
        public int currentPuck;
        public List<int> boughtStriker;
        public List<int> boughtPucks;

        public int bestTournamentRank;
        public int lastTournamentRank;
        public int tournamentsPlayed;
        public int tournamentsWon;

        public int currentCue;
        public List<int> boughtCues;
    }

    [System.Serializable]
    public struct PlayerScore
    {
        public string playerId;
        public bool hasQueen;
        public int myTotalPucksPotted;
        public int myPucks;
        public int otherPucks;
    }

    public enum AccuracyType
    {
        BASIC = 0,
        COMMON = 1,
        EPIC = 2,
        RARE = 3,
        LEGENDARY = 4
    }

    [System.Serializable]
    public class StrikerAccuracyInfo
    {
        public float maxDirectionOffsetError;
        public float maxForceOffsetError;
        public List<StrikerProperties> data;
    }
    [System.Serializable]
    public class StrikerProperties
    {
        public AccuracyType accuracyType;
        public float directionDeflectionPercentage;
        public float forceDeflectionPercentage;

        public StrikerProperties(AccuracyType type)
        {
            switch (type)
            {
                case AccuracyType.BASIC:
                    directionDeflectionPercentage = 0.4f;
                    forceDeflectionPercentage = 0.35f;
                    break;
                case AccuracyType.COMMON:
                    directionDeflectionPercentage = 0.3f;
                    forceDeflectionPercentage = 0.25f;
                    break;
                case AccuracyType.EPIC:
                    directionDeflectionPercentage = 0.2f;
                    forceDeflectionPercentage = 0.15f;
                    break;
                case AccuracyType.RARE:
                    directionDeflectionPercentage = 0.1f;
                    forceDeflectionPercentage = 0.05f;
                    break;
                case AccuracyType.LEGENDARY:
                    directionDeflectionPercentage = 0;
                    forceDeflectionPercentage = 0;
                    break;
                default:
                    directionDeflectionPercentage = 0;
                    forceDeflectionPercentage = 0;
                    break;
            }

        }
    }
}