﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeMoreScript : MonoBehaviour
{
    System.Action ToExecute;

    public void AddListenerToButton(System.Action toAddAction)
    {
        ToExecute = toAddAction;
    }
    public void OnPressButton()
    {
        Debug.Log("See More Clicked");
        ToExecute?.Invoke();
    }
}