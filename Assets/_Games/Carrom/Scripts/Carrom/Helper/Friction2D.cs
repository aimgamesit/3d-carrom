﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIMCarrom
{
    public class Friction2D : MonoBehaviour
    {
        private Rigidbody2D carromBody;
        private float frictionCoeff = 0.45f;
        private float minVelocity = 0.05f;

        void Start()
        {
            carromBody = GetComponent<Rigidbody2D>();
        }

        //[ServerCallback]
        private void FixedUpdate()
        {
            if (carromBody.velocity.magnitude > minVelocity)
                carromBody.AddForce(frictionCoeff * Constants.NORMAL_FORCE * carromBody.mass * carromBody.velocity.normalized * -1);
            else
            {
                carromBody.velocity = Vector3.zero;
                carromBody.angularVelocity = 0;
            }
        }
    }
}