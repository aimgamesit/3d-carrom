﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AIMCarrom
{
    public class GridController : MonoBehaviour
    {
        [SerializeField] Transform CarromMenParent;
        public CarromMenColor MyColour;
        List<CarromMenHandler>[] PriorityLists;
        Action <List<CarromMenHandler>[]> AfterPrioritySet;
        void Start()
        {
            PriorityLists = new List<CarromMenHandler>[5];
        }
        public void AddPriorities(Action <List<CarromMenHandler>[]> ReturnList)
        {
            AfterPrioritySet = ReturnList;
            AddFifthPriority();
            AddToFirstPriority();
            AddSecondPriority();
            AddThirdPriority();
            AddFourthPriority();
        }
        void AddToFirstPriority()
        {
            PriorityLists[0] = new List<CarromMenHandler>();
            PriorityLists[0].Clear();
            foreach(Transform TR in CarromMenParent)
            {
                if (TR.position.y > transform.GetChild(0).position.y)
                {
                    if(TR.position.x > transform.GetChild(4).position.x || TR.position.x < transform.GetChild(3).position.x)
                    {
                        if (IsMyCoin(TR.GetComponent<CarromMenHandler>()))
                        {
                            PriorityLists[0].Add(TR.GetComponent<CarromMenHandler>());
                            PriorityLists[4].Remove(TR.GetComponent<CarromMenHandler>());
                        }
                    }
                }
            }
            Debug.Log(PriorityLists[0].Count);
        }
        void AddSecondPriority()
        {
            PriorityLists[1] = new List<CarromMenHandler>();
            PriorityLists[1].Clear();
            foreach(Transform TR in CarromMenParent)
            {
                if(TR.position.y< transform.GetChild(2).position.y)
                {
                    if (TR.position.x > transform.GetChild(4).position.x || TR.position.x < transform.GetChild(3).position.x)
                    {
                        if (IsMyCoin(TR.GetComponent<CarromMenHandler>()))
                        {
                            PriorityLists[1].Add(TR.GetComponent<CarromMenHandler>());
                            PriorityLists[4].Remove(TR.GetComponent<CarromMenHandler>());
                        }
                    }
                }
            }
            Debug.Log(PriorityLists[1].Count);
        }
        void AddThirdPriority()
        {
            PriorityLists[2] = new List<CarromMenHandler>();
            PriorityLists[2].Clear();
            foreach (Transform TR in CarromMenParent)
            {
                if (TR.position.y > transform.GetChild(1).position.y)
                {
                    if (TR.position.x > transform.GetChild(4).position.x || TR.position.x < transform.GetChild(3).position.x)
                    {
                        if (!PriorityLists[0].Contains(TR.GetComponent<CarromMenHandler>()))
                        {
                            if (IsMyCoin(TR.GetComponent<CarromMenHandler>()))
                            {
                                PriorityLists[2].Add(TR.GetComponent<CarromMenHandler>());
                                PriorityLists[4].Remove(TR.GetComponent<CarromMenHandler>());
                            }
                        }
                    }
                }
            }
            Debug.Log(PriorityLists[2].Count);
        }
        void AddFourthPriority()
        {
            PriorityLists[3] = new List<CarromMenHandler>();
            PriorityLists[3].Clear();
            foreach (Transform TR in CarromMenParent)
            {
                if (TR.position.y > transform.GetChild(2).position.y && TR.position.y < transform.GetChild(1).position.y)
                {
                    if (TR.position.x < transform.GetChild(4).position.x && TR.position.x > transform.GetChild(3).position.x)
                    {
                        if (IsMyCoin(TR.GetComponent<CarromMenHandler>()))
                        {
                            PriorityLists[3].Add(TR.GetComponent<CarromMenHandler>());
                            PriorityLists[4].Remove(TR.GetComponent<CarromMenHandler>());
                        }
                    }
                }
            }
            Debug.Log(PriorityLists[3].Count);
            Debug.Log(PriorityLists[4].Count);
            AfterPrioritySet?.Invoke(PriorityLists);
        }
        void AddFifthPriority()
        {
            PriorityLists[4] = new List<CarromMenHandler>();
            PriorityLists[4].Clear();
            foreach (Transform TR in CarromMenParent)
            {
                if (IsMyCoin(TR.GetComponent<CarromMenHandler>()))
                {
                    PriorityLists[4].Add(TR.GetComponent<CarromMenHandler>());
                }
            }
            Debug.Log(PriorityLists[4].Count);
        }
        bool IsMyCoin(CarromMenHandler CM)
        {
            if (CM.pocketed)
                return false;
            return CM.carromMen.colorType == MyColour || CM.carromMen.colorType == CarromMenColor.RED;
        }
    }
}