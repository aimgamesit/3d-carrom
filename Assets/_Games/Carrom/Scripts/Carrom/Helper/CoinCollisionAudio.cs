﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace AIMCarrom
{
    public enum CollisionSoundType
    {
        Puck = 1,
        Striker,
        Wall
    }

    public class CoinCollisionAudio : MonoBehaviour
    {
        private AudioSource audioSource;
        public AudioSource AudioSource => audioSource;
        private Rigidbody2D rbRigidBody;
        [SerializeField] private AudioClip[] audioClips;
        private string[] tags = { Constants.CARROM_MAN_TAG, Constants.STRIKER_TAG, Constants.CARROMBOARD_WALLS_TAG };

        float transformSpeed;
        float compareTime = 0.02f;

        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            rbRigidBody = GetComponent<Rigidbody2D>();

            //Commented as isHeadless is a parameter is NetworkManager.cs under Mirror
            //if (!Mirror.NetworkManager.isHeadless)
            {
                CheckTranformVelocity();
            }
        }
        
        IEnumerator CheckVelocityCoroutine()
        {
            Vector3 MyPos = transform.position;
            yield return new WaitForSeconds(compareTime);
            transformSpeed = (transform.position - MyPos).magnitude / compareTime;
            CheckTranformVelocity();
        }

        void CheckTranformVelocity()
        {
            StartCoroutine(CheckVelocityCoroutine());
        }

        public void PlayCollisionSound(float volume, string soundTag)
        {
            if (audioClips.Length <= 0)
                return;
            //audioSource.clip = audioClips[(int)soundTag];
            int id = tags.ToList().FindIndex(t => t == soundTag);
            audioSource.clip = audioClips[id];
            audioSource.volume = volume;
            audioSource.Play();
            //Debug.LogFormat("VOLUME : {0}", volume);
        }
    }
}
