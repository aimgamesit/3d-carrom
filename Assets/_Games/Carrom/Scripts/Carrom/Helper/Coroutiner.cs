﻿using UnityEngine;
using System.Collections;

public class Coroutiner
{
    public static int test = 0;
    public static GameObject StartCoroutine(IEnumerator iterationResult)
    {
        //Create GameObject with MonoBehaviour to handle task.
        GameObject routineHandlerObj = new GameObject(test.ToString());
        test++;

        CoroutinerInstance routineHandler = routineHandlerObj.AddComponent<CoroutinerInstance>();
        routineHandler.ProcessWork(iterationResult);
        return routineHandlerObj;
    }
}

public class CoroutinerInstance : MonoBehaviour
{
    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public Coroutine ProcessWork(IEnumerator iterationResult)
    {
        return StartCoroutine(DestroyWhenComplete(iterationResult));
    }

    public IEnumerator DestroyWhenComplete(IEnumerator iterationResult)
    {
        yield return StartCoroutine(iterationResult);
        Destroy(this.gameObject);
    }
}