﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AIMCarrom
{
    public class GridScrollListFitter : MonoBehaviour
    {
        [SerializeField] RectTransform scrollRect;
        [SerializeField] GameObject dummyGrid;
        Transform gridParent;
        GameObject currentGrid;
        GridLayoutGroup dummyGridLayout;

        [SerializeField] GameObject gridPrefab;
        [SerializeField] GameObject itemPrefab;
        [SerializeField] GameObject indicatorObj;
        [SerializeField] Transform indicatorParent;

        int columsCount = 3;
        [SerializeField] int spawnItemsCount = 30;
        float scrollRectHeight;
        float itemHeight;
        int totalItemsPossible;

        List<GameObject> totalItems = new List<GameObject>();
        //List<GameObject> grids = new List<GameObject>();

        private void Start()
        {
            scrollRectHeight = Mathf.Abs(scrollRect.rect.size.y);
            dummyGridLayout = dummyGrid.GetComponent<GridLayoutGroup>();
            gridParent = dummyGrid.transform.parent;

            RectTransform itemRect = itemPrefab.GetComponent<RectTransform>();
            itemHeight = dummyGridLayout.cellSize.y + (dummyGridLayout.spacing.y / 2f);

            totalItemsPossible = (int)Mathf.Floor(scrollRectHeight / (itemHeight)) * columsCount;

            currentGrid = dummyGrid;

            SpawnItems(spawnItemsCount);
        }

        void SpawnItems(int count)
        {
            for (int i = 0; i < count; ++i)
            {
                if (currentGrid.transform.childCount >= totalItemsPossible)
                {
                    var newGrid = Instantiate(gridPrefab, gridParent);// spawn another grid
                    //if (!grids.Contains(newGrid))
                    //    grids.Add(newGrid);
                    Instantiate(indicatorObj, indicatorParent);// spawn another indicator
                    currentGrid = newGrid;// make it currentGrid
                }
                GameObject spawnedItem = SpawnItem();
                if (!totalItems.Contains(spawnedItem))
                totalItems.Add(spawnedItem);
            }            
        }

        GameObject SpawnItem()
        {
            var item = Instantiate(itemPrefab, currentGrid.transform);

            // temp
            

            return item;
        }

        void RemoveItem(int index)
        {
            var item = totalItems[index];
            totalItems.RemoveAt(index);
            Destroy(item);

            int itemIndexInGrid = ((index + 1) % totalItemsPossible) - 1;
            int itemGridIndex = (index + 1) / totalItemsPossible;
            Debug.LogFormat("Item {0} from grid {1} is deleted", itemIndexInGrid, itemGridIndex);

            while (itemGridIndex < currentGrid.transform.GetSiblingIndex())
            {
                Transform firstItemInNextGrid = gridParent.transform.GetChild(itemGridIndex + 1).GetChild(0); // get first element of next grid
                firstItemInNextGrid.SetParent(gridParent.transform.GetChild(itemGridIndex));
                firstItemInNextGrid.SetAsLastSibling(); // make it last sibling of current grid

                //for (int i = itemIndexInGrid+1; i <= (totalItemsPossible-itemIndexInGrid+1); i++)
                //{
                    
                //}
                ++itemGridIndex;
            }

            if (currentGrid.transform.childCount <= 0)
            {
                Destroy(currentGrid);
                currentGrid = gridParent.transform.GetChild(gridParent.transform.childCount - 1).gameObject;
            }
        }
    }
}
