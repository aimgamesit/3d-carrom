﻿using System.Collections;
using UnityEngine;
using NaughtyAttributes;

namespace AIMCarrom
{
    public class CoinTumble : MonoBehaviour
    {
        // [SerializeField] [Slider(0, 2000)] private float rotationMultiplier = 1000;
        [SerializeField] [Slider(0, 1)] private float tumbleSimulationDuration = 0.3f;
        [SerializeField] [Slider(0, 10)] private float velocityMagnitudeMin = 5;

        private Coroutine tumblingCoroutine;
        public System.Action onTumbleFinish;
        public bool isTumbling;


        public void OnPot(GameObject pot, GameObject coin)
        {
            //Debug.Log("On Pot Called!!");
            if (tumblingCoroutine != null)
                StopCoroutine(tumblingCoroutine);

            tumblingCoroutine = StartCoroutine(StartTumbling(pot, coin));
        }

        private void OnEnable()
        {
            isTumbling = false;
        }

        public IEnumerator StartTumbling(GameObject pot, GameObject coin)
        {
            isTumbling = true;
            Rigidbody2D carromBody = coin.GetComponent<Rigidbody2D>();
            Vector3 potPos = pot.transform.position;
            potPos.z = 0;
            Vector3 ownPos = transform.position;
            ownPos.z = 0;
            Vector3 potDirection = (potPos - ownPos).normalized;
            Vector3 axis = Quaternion.AngleAxis(-90, Vector3.forward) * potDirection;

            float velocityMagnitude = Mathf.Clamp(carromBody.velocity.magnitude, velocityMagnitudeMin, Mathf.Infinity);

            yield return new WaitForSeconds(0.1f / velocityMagnitude);

            float time = 0;
            Vector3 initialPosition = transform.position;
            Vector3 potpos = pot.transform.position;
            Vector3 ground = (pot.transform.forward);// + potDirection * 0.2f) * velocityMagnitude * 0.5f;
            carromBody.velocity = Vector3.zero;
            //Debug.Log(new Vector3(potpos.x, potpos.y, transform.position.z));
            while (time < tumbleSimulationDuration)
            {
                transform.position = Vector3.Lerp(initialPosition, new Vector3(potpos.x, potpos.y, transform.position.z), (time) * 5);
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, Mathf.Min(0.4f, ground.z)), 20);
                // transform.Rotate(axis, Time.fixedDeltaTime * (1 / (velocityMagnitude)) * rotationMultiplier, Space.World);
                yield return new WaitForEndOfFrame();
                carromBody.velocity = Vector3.zero;
                time += Time.deltaTime;
            }

            isTumbling = false;
            onTumbleFinish?.Invoke();
        }

        public void StopTumble()
        {
            if (tumblingCoroutine != null)
                StopCoroutine(tumblingCoroutine);

            isTumbling = false;
        }
    }
}

