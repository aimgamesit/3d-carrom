﻿using UnityEngine;

public class ScrollItemSmoothScale : MonoBehaviour
{
    public Transform referenceTransform;

    float minAlpha = 0.6f;
    float setValue;

    void Update()
    {
        if(referenceTransform == null) 
            return;

        setValue = Mathf.Lerp(1, minAlpha, Mathf.Abs(transform.position.x - referenceTransform.position.x));
        transform.GetChild(0).localScale = Vector3.one * setValue;

        GetComponent<CanvasGroup>().alpha = setValue;
        if (setValue > 0.86f && transform.GetSiblingIndex() != transform.parent.childCount - 1)
        {
            transform.SetAsLastSibling();
        }
    }
}
