﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIMCarrom
{
    public class PocketDetector : MonoBehaviour
    {
        private AudioSource audioSource;

        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
        }


        public void PlayPocketSound(float volume)
        {
            audioSource.volume = volume * volume;
            audioSource.Play();
        }
    }
}