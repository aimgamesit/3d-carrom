﻿using NaughtyAttributes;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;

namespace AIMCarrom
{
    public class StrikerControls : MonoBehaviour
    {
        public System.Action onStrikerShoot;
        public delegate void StrikerShootDelegate();
        public bool strikerOverLapping;
        #region Striker variables
        private bool strikerClicked;
        private Camera mainCamera;
        private Vector3 strikerReleasePosition;
        private Vector3 strikerReleaseDirection;
        internal CircleCollider2D circleCollider2D;
        internal Rigidbody2D strikerRigidBody;
        internal StrikerHandler strikerHandler;
        private float slingDistance;
        private float forceMagnitude;
        public GameObject strikerPrefab;
        public Transform strikerParent;
        public GameObject striker;
        [SerializeField] private LayerMask strikerLayer;
        [SerializeField] private LayerMask carromMenLayer;
        [SerializeField] private Slider[] strikerSliders;
        [SerializeField] [Slider(0f, 200f)] private float strikerReleaseForceMultiplier;
        [SerializeField] [Slider(0f, 1f)] private float maxSlingDistance;
        [SerializeField] [Slider(0f, 1f)] private float validSlingDistanceRatio;
        Collider2D currentOverlappingPuckCollider;
        Collider2D firstOverlappingPuckCollider;
        Coroutine strikerJumpCoroutine;
        Coroutine sizeArrowCoroutine;
        private float jumpSpeed = 8f;
        public List<StrikerPositionData> strikerSlidePositions;
        public List<StrikerDirectionData> strikerShootDirections;
        internal bool strikerPocketed;
        #endregion

        #region Arrow Sprite Variables
        private Vector2 arrowInitialSize;
        private Vector2 arrowInitialFrontSize;
        private Vector2 arrowInitialBackSize;
        private Vector2 arrowInitialCircleSize;

        [SerializeField] private GameObject directionArrow;
        [SerializeField] private SpriteRenderer arrowSprite;
        [SerializeField] private SpriteRenderer arrowSpriteFront;
        [SerializeField] private SpriteRenderer arrowSpriteBack;
        [SerializeField] private SpriteRenderer arrowSpriteCircle;
        [SerializeField] [Slider(0f, 50f)] private float spriteWidthMultiplier;
        [SerializeField] private float directionArrowHeight = -0.6f;
        [SerializeField] private float strikerMoveHaloSize = 0.65f;
        internal bool opponentInShot;
        #endregion

        private void Awake()
        {
            mainCamera = Camera.main;
            arrowInitialSize = arrowSprite.size;
            arrowInitialFrontSize = arrowSpriteFront.size;
            arrowInitialBackSize = arrowSpriteBack.size;
            arrowInitialCircleSize = arrowSpriteCircle.size;
            strikerSlidePositions = new List<StrikerPositionData>();
            strikerShootDirections = new List<StrikerDirectionData>();
        }

        public void SetUpStriker()
        {
            GameObject[] strikers = GameObject.FindGameObjectsWithTag("Striker");
            for (int i = 0; i < strikers.Length; i++)
                Destroy(strikers[i]);
            Debug.Log("Destroyed Strikers");
            striker = Instantiate(strikerPrefab, strikerParent);
            striker.transform.localRotation = Quaternion.identity;
            striker.transform.localPosition = Constants.STRIKER_DEFAULT_LOCAL_POSITION;
            Debug.Log($"Striker: { striker.name}");
            StrikerRef();
        }

        public void StrikerRef()
        {
            strikerRigidBody = striker.GetComponent<Rigidbody2D>();
            strikerHandler = striker.GetComponent<StrikerHandler>();
            circleCollider2D = striker.GetComponent<CircleCollider2D>();
            SetStrikerGFX();
        }

        public bool StrikerUpdated => startIndex >= strikerSlidePositions.Count;

        int startIndex = 1;
        bool movingSlider;
        public void StrikerPositionFromNetwork(Vector3 strikerPosition, float timeStamp, int state, bool hasCollided, string collObj)
        {
            strikerSlidePositions.Add(new StrikerPositionData(strikerPosition, timeStamp, state, hasCollided, collObj));
            //Debug.Log($"Added new striker Pos: count: {strikerSlidePositions.Count}, state: {state}, moving Slider:{movingSlider}");
            if (strikerSlidePositions.Count == 1 && movingSlider)
                movingSlider = false;
            StartCoroutine(StartSliderSimulation());
        }

        IEnumerator StartSliderSimulation()
        {
            yield return new WaitForSeconds(1f);
            if (!updatingDirection && !movingSlider && (startIndex) < strikerSlidePositions.Count)
            {
                //Debug.Log($"Starting Slider movement: {movingSlider}, startIndex: {startIndex}, count{strikerSlidePositions.Count}");
                if (!GameManager.Instance.IsMyTurn)
                {
                    StopCoroutine(MoveSliderFromNetwork());
                    StartCoroutine(MoveSliderFromNetwork());
                }
            }
            yield return new WaitForSeconds(1f);
        }

        IEnumerator MoveSliderFromNetwork(bool reduceTime = false)
        {
            Debug.Log($"Is My Turn:{GameManager.Instance.IsMyTurn}");

            strikerHandler.inShot = true;
            //Debug.Log($"Striker InShot set to :{strikerHandler.inShot}");
            Debug.Log($"Moving slider from index: {startIndex} to {strikerSlidePositions.Count}");
            bool collisionChecked = false;
            for (int k = startIndex; k < strikerSlidePositions.Count; k++)
            {
                movingSlider = true;
                float duration = (reduceTime) ? 0.15f : strikerSlidePositions[k].timeStamp - strikerSlidePositions[k - 1].timeStamp;
                //Debug.Log($"k {k},Target state:{strikerSlidePositions[k].state}, Moving striker from : {striker.transform.position} to {strikerSlidePositions[k].position} in seconds: {duration}, has collided: {strikerSlidePositions[k].hasCollided} with :{strikerSlidePositions[k].collisionObject}");
                float t = 0;
                reduceTime = false;
                //Debug.Log($"current Index:{k}, position Count:{strikerSlidePositions.Count}");
                collisionChecked = false;
                Vector3 initPos = striker.transform.position;
                while (t < duration)
                {
                    striker.transform.position = Vector3.Lerp(initPos, strikerSlidePositions[k].position, t / duration);
                    t += Time.deltaTime;
                    if (Vector3.Distance(striker.transform.position, strikerSlidePositions[k].position) < 1f && !collisionChecked)
                    {
                        collisionChecked = true;
                        CheckCollision(k);
                        //t = duration;
                    }
                    yield return new WaitForSeconds(0.001f);
                }
                striker.transform.position = strikerSlidePositions[k].position;
                if (!collisionChecked)
                    CheckCollision(k);
                if (strikerSlidePositions[k].state == 4)
                {
                    StartCoroutine(GameManager.Instance.GamePlayHandler.CheckIfCanChangeTurn());

                }
                startIndex = k + 1;
                if (startIndex < strikerSlidePositions.Count)
                {
                    if (strikerSlidePositions[startIndex].state == 3 && strikerSlidePositions[k].state == 2)
                    {
                        movingSlider = false;
                        StartCoroutine(MoveDirectionFromNetwork());
                        break;
                    }
                }
            }
            movingSlider = false;
            Debug.Log($"Striker updated: new startIndex{startIndex}, count: {strikerSlidePositions.Count}");

            yield return null;
        }

        void CheckCollision(int k)
        {
            if (strikerSlidePositions[k].hasCollided)
            {
                CarromMenHandler coin = GameManager.Instance.GamePlayHandler.coins.Find(c => c.gameObject.name == strikerSlidePositions[k].collisionObject);
                if (coin != null)
                {
                    StartCoroutine(coin.StartPositionSimulation(true));
                    Debug.Log($"Starting simulation for {coin.name} from striker");
                }
                else if (strikerSlidePositions[k].collisionObject.Contains("Holes"))
                {
                    strikerHandler.OnPot();
                    strikerPocketed = true;
                }
                else
                    Debug.Log($"striker collided but could not find {strikerSlidePositions[k].collisionObject}");
            }
        }
 
        void Update()
        {
            if (!GameManager.Instance.HasGameStarted || (!GameManager.Instance.IsMyTurn /*&& !GameManager.Instance.isLocal*/) || GameManager.Instance.IsGameInSimulation)
                return;
            if (Input.GetMouseButtonDown(0) && !IsPointOverlappingWith(striker.transform.position, carromMenLayer) && !GameManager.Instance.GamePlayHandler.oppoWaitScreenActive)
            {
                Ray ray = mainCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, mainCamera.farClipPlane));
                RaycastHit2D hit2D = Physics2D.GetRayIntersection(ray, 100, strikerLayer);

                if (hit2D.collider)
                {
                    strikerClicked = true;
                    strikerReleasePosition = UniformMousePosition(Input.mousePosition);
                    directionArrow.transform.position = new Vector3(striker.transform.position.x, striker.transform.position.y, directionArrowHeight);
                    directionArrow.SetActive(true);
                    strikerHandler.RemoveHalo();
                    Debug.Log("Striker Hit!!");
                    //GameManager.Instance.UiManager.TogglePlayerSlider(false);
                }
            }

            if (strikerClicked)
            {
                strikerReleaseDirection = (UniformMousePosition(Input.mousePosition) - strikerReleasePosition) * (GameManager.Instance.GamePlayHandler.MyIndex == 0 ? -1 : 1);
                float rawSlingDistance = strikerReleaseDirection.magnitude;
                slingDistance = Mathf.Clamp(rawSlingDistance, 0, maxSlingDistance);

                slingDistance /= maxSlingDistance;      // SCALE THE VALUE BETWEEN THE RANGE O AND 1
                slingDistance = slingDistance > validSlingDistanceRatio ? slingDistance : 0;

                Vector2 arrowSizeTemp = arrowInitialSize * slingDistance * spriteWidthMultiplier;
                SetArrowDirection(strikerReleaseDirection.normalized, arrowSizeTemp.y, false);
                GameManager.Instance.localPlayer.MoveDirection(strikerReleaseDirection.normalized, arrowSizeTemp.y, 1);
            }

            if (Input.GetMouseButtonUp(0) && strikerClicked)
            {
                strikerClicked = false;

                if (GameManager.Instance.StrikerAccuracyInfo != null)
                {
                    //Vector2 screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, striker.transform.position.z - Camera.main.transform.position.z));
                    //Debug.Log($"Screen Width: {screenWidth}");
                    Debug.DrawRay(striker.transform.position, strikerReleaseDirection.normalized, Color.red, 30f);
                    Vector2 dir = strikerReleaseDirection.normalized;

                    float directionOffset = GameManager.Instance.StrikerAccuracyInfo.data[Player.PlayerData.currentStriker].directionDeflectionPercentage;
                    MoveDirection(strikerReleaseDirection.normalized, 0, 2);

                    if (Random.value > 0.5f)
                    {
                        directionOffset *= -1;
                    }
                    Vector2 newForceDir = new Vector2(dir.x + (dir.x * directionOffset), dir.y);
                    strikerReleaseDirection = newForceDir;

                    Debug.DrawRay(striker.transform.position, newForceDir.normalized, Color.cyan, 30f);
                }
                // STRIKER FORCE IS DIRECTLY PROPORTIONAL TO THE SQUARE OF THE MAGNITUDE OF STRIKER RELEASE DISTANCE
                if (slingDistance > 0)
                {
                    circleCollider2D.enabled = true;
                    forceMagnitude = slingDistance * slingDistance * strikerReleaseForceMultiplier;
                    if (GameManager.Instance.StrikerAccuracyInfo != null)
                    {
                        float forceOffset = GameManager.Instance.StrikerAccuracyInfo.data[Player.PlayerData.currentStriker].forceDeflectionPercentage;
                        if (Random.value > 0.5f)
                        {
                            forceOffset *= -1;
                        }
                        forceMagnitude = forceMagnitude + forceMagnitude * forceOffset;
                        forceMagnitude = Mathf.Clamp(forceMagnitude, 0, strikerReleaseForceMultiplier);
                    }
                    //Commented as isHeadless is a parameter is NetworkManager.cs under Mirror
                    //if (!NetworkManager.isHeadless)
                    {
                        Debug.Log("Shoot called");
                        GameManager.Instance.localPlayer.Shoot(striker.transform.position, strikerReleaseDirection.normalized * forceMagnitude, GameManager.Instance.GamePlayHandler.MyIndex);
                    }
                    strikerHandler.RemoveHalo();
                }
                else
                {
                    strikerHandler.SetStrikerPositionValid(true);
                }
                directionArrow.SetActive(false);
            }
        }

        public void Shoot(Vector3 pos, Vector2 force, int index, bool isRpcCall = false)
        {
            // Debug.Log($"Shooting Striker: is RPC call :{isRpcCall}");
            //if (!isRpcCall|| MultiplayerManager.isOffline)
            {
                circleCollider2D.enabled = true;
                //Debug.Log($"Striker Shoot at pos: {pos} with force: {force}");
                striker.transform.position = pos;
                strikerRigidBody.AddForce(force);
                onStrikerShoot?.Invoke();
                strikerHandler.inShot = true;
                strikerHandler.strikerCount = 0;
                StartCoroutine(strikerHandler.SendStrikerPosition());
                RpcShoot(force, striker.transform.position);
            }
        }

        //[ClientRpc]
        public void RpcShoot(Vector2 force, Vector3 pos)
        {
            //Debug.Log("Rpc Shoot!");
            //striker.GetComponent<Smooth.SmoothSyncMirror>().enabled = true;
            circleCollider2D.enabled = true;
            striker.transform.position = pos;
            directionArrow.SetActive(false);
            strikerHandler.RemoveHalo();
            StartCoroutine(GameManager.Instance.GamePlayHandler.OnStrikerShooted());
            strikerSliders[0].interactable = strikerSliders[1].interactable = false;
            //  strikerRigidBody.AddForce(force);
        }



        public void OnStrikerReset(bool addDelay = false, bool hasGameStarted = false)
        {
            StartCoroutine(ResetWait(addDelay));
        }

        IEnumerator ResetWait(bool addDelay, bool hasGameStarted = true)
        {
            Debug.Log($"RESET WAIT {hasGameStarted} ");
            if (hasGameStarted)
            {
                strikerOverLapping = false;
                if (circleCollider2D)
                    circleCollider2D.enabled = false;
                //strikerRigidBody.gameObject.SetActive(true);
                if (strikerRigidBody)
                {
                    strikerRigidBody.velocity = Vector3.zero;
                    strikerRigidBody.angularVelocity = 0;
                }
                if (strikerHandler)
                {
                    strikerHandler.ResetStriker();
                }
                if (strikerSliders != null && strikerSliders.Length > 1)
                {
                    strikerSliders[0].value = 0;
                    strikerSliders[1].value = 0;
                }
                SetStrikerPosition(0, true);
                lastStrikerPosition = 0;
                sliderValueIsIncreasing = false;
                strikerSlidePositions.Clear();
                strikerShootDirections.Clear();
                strikerPocketed = false;
                strikerHandler.SetStrikerPositionValid(true);
                strikerHandler.SetHaloSize(1);
                SetArrowDirection(Vector3.one, 0, true);
                OnSliderMouseUp();
                SetStrikerGFX();
                ResetSliders();
                if (addDelay)
                    yield return new WaitForSeconds(2f);
                float waitTime = 0.2f;
                yield return new WaitForSeconds(waitTime);

                if (GameManager.Instance.IsLocalGame() && !GameManager.Instance.GamePlayHandler.resultDisplayed)
                {
                    StartCoroutine(CheckBotTurn());
                }
                else
                {
                    GameManager.Instance.GamePlayHandler.ResetGamePlayParameters();
                }

                startIndex = 1;
                directionIndex = 1;

                //strikerHandler.SendPositionDetails(striker.transform.position, 1, false, "");
                StartCoroutine(GameManager.Instance.GamePlayHandler.SetOpponentState(OpponentState.MoveStarted, true));
            }
        }

        IEnumerator CheckBotTurn()
        {
            Debug.Log($"Inside CheckBotTurn { GameManager.Instance.IsLocalGame() } ");
            yield return new WaitForSeconds(0.25f);
            int aiIndex = GameManager.Instance.GamePlayHandler.playerInfo.FindIndex(t => t.playerData.isAI == true);
            Debug.LogFormat($"AI INDEX : {aiIndex} :: current turn  : {GameManager.Instance.GetCurrentIndex}");

            if (aiIndex >= 0)
            {
                //Debug.LogFormat("isBot at aiIndex: {1}", GameManager.Instance.GamePlayHandler.playerInfo[aiIndex].playerData.isAI);
                if (GameManager.Instance.GetCurrentIndex == aiIndex && GameManager.Instance.GamePlayHandler.playerInfo[aiIndex].playerData.isAI)
                {
                    var ai = strikerHandler.GetComponent<AIController>();
                    ai.TakeTurn();
                }
            }
        }

        public void ResetSliders()
        {
            Debug.Log("RESET SLIDERS");
            strikerSliders[0].value = 0;
            strikerSliders[1].value = 0;
            strikerSliders[0].interactable = GameManager.Instance.IsMyTurn;
            //strikerSliders[1].interactable = !GameManager.Instance.IsMyTurn;
        }

        public void StopStrikerAim()
        {
            strikerClicked = false;

            //Commented as isHeadless is a parameter is NetworkManager.cs under Mirror
            //if (!NetworkManager.isHeadless)
            directionArrow.SetActive(false);
        }

        /// <summary>
        /// Moves the Striker based on the position of the slider.
        /// </summary>
        public void OnStrikerSliderValueChanged()
        {
            if (GameManager.Instance.IsGameInSimulation)
                return;

            //Commented as isHeadless is a parameter is NetworkManager.cs under Mirror
            //if (!NetworkManager.isHeadless)
            {
                var sliderValue = strikerSliders[GameManager.Instance.GetCurrentIndex].value * (GameManager.Instance.GamePlayHandler.MyIndex == 0 ? 1 : -1);
                //if (!MultiplayerManager.isOffline)
                MoveStriker(sliderValue, GameManager.Instance.GamePlayHandler.MyIndex, false, 0.15f);
                //GameManager.Instance.localPlayer.MoveStriker(sliderValue, GameManager.Instance.GamePlayHandler.MyIndex, false, 0.15f);
            }
        }

        public void MoveStriker(float val, int index, bool isInstant, float lerpRate = 0.15f)
        {
            if (index == GameManager.Instance.GetCurrentIndex)
                ChangeStrikerPosition(val, isInstant, lerpRate);
        }

        public void ChangeStrikerPosition(float val, bool isInstant, float lerpRate = 0.15f)
        {
            if (GameManager.Instance.IsGameInSimulation)
                return;

            //Commented as isHeadless is a parameter is NetworkManager.cs under Mirror
            //if (!NetworkManager.isHeadless)
            strikerHandler.PlayDragSound();

            //Vector3 temp = Constants.STRIKER_DEFAULT_LOCAL_POSITION;
            //if (GameManager.Instance.GetCurrentIndex == 1)
            //{
            //    temp.y *= -1;
            //}
            //temp.x = val * Constants.STRIKER_X_EXTENT;

            SetStrikerPosition(val, isInstant, lerpRate, delegate
            {
                //if(!isUsingSlider)
                //    OnSliderMouseUp();
                {
                    if (!IsPointOverlappingWith(striker.transform.position, carromMenLayer))
                        strikerHandler.SetStrikerPositionValid(true);
                }
            }); // galat hai          
        }

        /*  Uncomment if Photon Added & change it to Photon Rpc
        [ClientRpc(channel = Channels.DefaultReliable)]
        private void RpcMoveStriker(float sliderValue, bool isInstant, float lerpRate)
        {
            if (GameManager.Instance.GetCurrentIndex != GameManager.Instance.GamePlayHandler.MyIndex || GameManager.Instance.userType == UserType.SPECTATOR)
                ChangeStrikerPosition(sliderValue, isInstant, lerpRate);
        }*/

        float lastStrikerPosition;
        bool sliderValueIsIncreasing;
        private void SetStrikerPosition(float val, bool isInstant = true, float lerpTime = 0.1f, System.Action onMoveComplete = null)
        {
            if (circleCollider2D)
                circleCollider2D.enabled = false;
            Vector3 temp = Constants.STRIKER_DEFAULT_LOCAL_POSITION;
            //Debug.LogError(GameManager.Instance.GetCurrentIndex);
            if (GameManager.Instance.GetCurrentIndex == 1)
            {
                temp.y *= -1;
            }
            temp.x = val * Constants.STRIKER_X_EXTENT;
            //lerpTime = Vector3.Magnitude(striker.transform.position - temp) * 20 * Time.deltaTime;

            if (isInstant)
                striker.transform.position = temp;
            else
                striker.transform.DOLocalMove(temp, lerpTime).OnComplete(() => onMoveComplete?.Invoke());

            bool sliderValueReverted = false;
            if (sliderValueIsIncreasing && (temp.x - lastStrikerPosition) < 0)
            {
                sliderValueReverted = true;
                sliderValueIsIncreasing = false;
            }
            else if (!sliderValueIsIncreasing && (temp.x - lastStrikerPosition) > 0)
            {
                sliderValueReverted = true;
                sliderValueIsIncreasing = true;
            }
            //Debug.Log($"Updated Striker Position: {temp}");
            lastStrikerPosition = temp.x;
            striker.transform.localRotation = Quaternion.identity;
            strikerOverLapping = IsPointOverlappingWith(striker.transform.position, carromMenLayer);
            strikerHandler.SetStrikerPositionValid(!strikerOverLapping);
        }

        float strikerPosTemp;
        private bool isUsingSlider;

        #region AUTO SLIDE STRIKER

        public bool IsPointOverlappingWith(Vector2 point, LayerMask layer)
        {
            currentOverlappingPuckCollider = Physics2D.OverlapCircle(point, Constants.STRIKER_RADIUS, layer);
            return currentOverlappingPuckCollider != null;
        }

        public Vector2 FindEmpty(Vector2 point, bool isMove, System.Action onFindEmpty = null)
        {
            if (!currentOverlappingPuckCollider)
                return point;
            firstOverlappingPuckCollider = currentOverlappingPuckCollider;
            bool isStrikerInRight = point.x - currentOverlappingPuckCollider.transform.position.x >= 0;
            int dir = isStrikerInRight ? 1 : -1;

            Vector2 newPoint = GetClosestPointToCollider(dir, point, currentOverlappingPuckCollider);
            int iterationCount = 0;
            int maxIterations = 20;
            while (IsPointOverlappingWith(newPoint, carromMenLayer) || Mathf.Abs(newPoint.x) > Constants.STRIKER_X_EXTENT)
            {
                if (Mathf.Abs(newPoint.x) <= Constants.STRIKER_X_EXTENT)
                {
                    newPoint = GetClosestPointToCollider(dir, newPoint, currentOverlappingPuckCollider);
                    continue;
                }
                dir *= -1;
                currentOverlappingPuckCollider = firstOverlappingPuckCollider;
                newPoint = GetClosestPointToCollider(dir, point, currentOverlappingPuckCollider);
                ++iterationCount;
                if (iterationCount > maxIterations)
                {
                    Debug.LogFormat("Max iterations reached | returning zeroTH position");
                    return point;
                }
            }
            //UpdateStrikerPos(newPoint);

            if (strikerJumpCoroutine != null)
                StopCoroutine(strikerJumpCoroutine);

            if (isMove)
            {
                float jumpTime = (Vector2.Distance(striker.transform.position, newPoint) / jumpSpeed) * 1.5f;

                float val = Mathf.Clamp(newPoint.x / Constants.STRIKER_X_EXTENT, -1, 1);
                striker.transform.DOMove(newPoint, jumpTime).SetEase(Ease.OutCubic).OnComplete(() =>
                {
                    onFindEmpty?.Invoke();
                    UpdateStrikerPos(newPoint);
                });
                return newPoint;
            }
            return newPoint;
        }

        Vector2 GetClosestPointToCollider(int direction, Vector2 fromPoint, Collider2D collider_)
        {
            float y_ = Mathf.Abs(collider_.transform.position.y - fromPoint.y);
            float contactDistance = Constants.PUCK_RADIUS + Constants.STRIKER_RADIUS + 0.04f;
            float xDistance = Mathf.Sqrt(Mathf.Pow(contactDistance, 2) - Mathf.Pow(y_, 2)) * direction;
            xDistance = collider_.transform.position.x + xDistance;

            return new Vector2(xDistance, fromPoint.y);
        }

        #endregion

        private void UpdateStrikerPos(Vector2 newPlace)
        {
            //striker.transform.position = newPlace;

            strikerOverLapping = IsPointOverlappingWith(newPlace, carromMenLayer);
            strikerHandler.SetStrikerPositionValid(!strikerOverLapping);
            if (/*!NetworkManager.isHeadless &&*/ GameManager.Instance.IsMyTurn)
            {
                //strikerSliders[GameManager.Instance.GetCurrentIndex].value = /*(GameManager.Instance.GetCurrentIndex == 0 ? 1 : -1) **/ (newPlace.x / Constants.STRIKER_X_EXTENT);
                OnStrikerSliderValueChanged();
            }
            int sliderId = GameManager.Instance.GetCurrentIndex;
            //Debug.LogFormat("slider {0} moving", sliderId);
            strikerSliders[sliderId].value = (GameManager.Instance.GamePlayHandler.MyIndex == 0 ? 1 : -1) * (newPlace.x / Constants.STRIKER_X_EXTENT);
        }

        int directionIndex = 1;
        bool updatingDirection;
        IEnumerator MoveDirectionFromNetwork()
        {
            //Debug.Log($"changing direction from index: {directionIndex} to {strikerShootDirections.Count}");
            for (int k = directionIndex; k < strikerShootDirections.Count; k++)
            {
                updatingDirection = true;
                //Debug.Log($"current Index:{k}, direction Count:{strikerShootDirections.Count}, direction:{ strikerShootDirections[k].direction}, size: { strikerShootDirections[k].size} ");
                bool lastDirUpdate = false;
                if (opponentInShot && strikerShootDirections[k].size == 0)
                    lastDirUpdate = true;


                RpcSetArrowDirection(strikerShootDirections[k].direction, strikerShootDirections[k].size, lastDirUpdate);

                float duration = strikerShootDirections[k].timeStamp - strikerShootDirections[k - 1].timeStamp;
                yield return new WaitForSeconds(duration);

                directionIndex = k + 1;
            }
            updatingDirection = false;
            if (opponentInShot)
            {
                opponentInShot = false;
                StartCoroutine(MoveSliderFromNetwork(true));
                strikerHandler.RemoveHalo();
                StartCoroutine(GameManager.Instance.GamePlayHandler.SetOpponentState(OpponentState.Replay));
            }
            //Debug.Log($"Striker updated: new DirectionIndex{directionIndex}");
            yield return null;
        }

        public void StrikerDirectionFromNetwork(Vector3 strikerDir, float size, float timeStamp, int state)
        {
            strikerShootDirections.Add(new StrikerDirectionData(strikerDir, size, timeStamp, state));
            //Debug.Log($"Added new striker Direction: count: {strikerShootDirections.Count}, state: {state}, moving Slider:{movingSlider}");
            StartCoroutine(StartDirectionSimulation());
        }
        IEnumerator StartDirectionSimulation()
        {
            yield return new WaitForSeconds(1f);
            if (/*(state == 2) && */(!movingSlider && !updatingDirection && directionIndex < strikerShootDirections.Count))
            {
                StopCoroutine(MoveDirectionFromNetwork());
                StartCoroutine(MoveDirectionFromNetwork());
            }
            yield return new WaitForSeconds(1f);
        }

        StrikerDirectionData lastSentDirection = new StrikerDirectionData(Vector3.zero, 0, 0, 0);
        public void MoveDirection(Vector3 dir, float size, int state = 1)
        {
            //if (!GameManager.Instance.IsLocalGame() && GameManager.Instance.IsMyTurn)
            //{
            //    //Debug.Log($"Dir: {dir}, last Dir value: {lastSentDirection.direction}, are same: {dir == lastSentDirection.direction}");
            //    //Debug.Log($"Size: {size}, last Size value: {lastSentDirection.size}, are same: {size == lastSentDirection.size}");
            //    //Debug.Log($"State: {state}, last State value: {lastSentDirection.state}, are same: {state == lastSentDirection.state}");

            //    if (dir == lastSentDirection.direction && size == lastSentDirection.size && state == lastSentDirection.state)
            //    {
            //        //Debug.Log("Same Direction!!");
            //        return;
            //    }
            //    object[] content = new object[] { dir, size, Time.time, state }; // Array contains the target position and the IDs of the selected units
            //    RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
            //    PhotonNetwork.RaiseEvent(Constants.STRIKER_SHOOT_DIRECTION_EVENT_CODE, content, raiseEventOptions, SendOptions.SendReliable);
            //    lastSentDirection.direction = dir;
            //    lastSentDirection.size = size;
            //    lastSentDirection.state = state;
            //    //Debug.Log($"Sending Event {Constants.STRIKER_SHOOT_DIRECTION_EVENT_CODE} with striker direction value: {content[0]}, size {content[1]}, timestamp: {content[2]}, state:{content[3]}");
            //}
        }

        //below function is executed at opponent end only
        public void RpcSetArrowDirection(Vector3 dir, float size, bool lastDirUpdate = false)
        {
            //Debug.Log($"RpcSetArrowDirection=> Dir :{dir}, size: {size}");
            //if (/*GameManager.Instance.GamePlayHandler.MyIndex != GameManager.Instance.GetCurrentIndex ||*/ GameManager.Instance.userType == UserType.SPECTATOR)
            {
                if (size > 0 || lastDirUpdate)
                {
                    directionArrow.transform.localPosition = new Vector3(striker.transform.position.x, striker.transform.position.y, directionArrowHeight);
                    directionArrow.SetActive(true);
                    strikerHandler.RemoveHalo();
                    SetArrowDirection(dir, size, false);
                }
                else
                {
                    directionArrow.SetActive(false);
                    strikerHandler.SetStrikerPositionValid(true);
                }
            }
        }

        private void SetArrowDirection(Vector3 dir, float size, bool isImmediate)
        {
            //Debug.LogFormat("SetArrowDirection");
            if (sizeArrowCoroutine != null)
                StopCoroutine(sizeArrowCoroutine);
            sizeArrowCoroutine = StartCoroutine(SetArrowDirectionCoroutine(dir, size, 0.25f, isImmediate));
        }

        IEnumerator SetArrowDirectionCoroutine(Vector3 dir, float size, float lerpRate, bool isImmediate)
        {
            //Debug.LogFormat("SetArrowDirectionCoroutine");
            Vector2 arrowSizeTemp = arrowInitialSize;
            if (!isImmediate)
            {
                while (true)
                {
                    Vector2 sizeVector = new Vector2(arrowInitialSize.x, size);
                    if (arrowSizeTemp.y - size <= 0.05f)
                        break;
                    arrowSizeTemp = Vector2.Lerp(arrowSizeTemp, sizeVector, lerpRate);

                    arrowSprite.size = Vector2.Lerp(arrowSprite.size, sizeVector, lerpRate);
                    arrowSpriteBack.size = Vector2.Lerp(arrowSpriteBack.size, new Vector2(arrowInitialBackSize.x, sizeVector.y), lerpRate);
                    arrowSpriteFront.size = Vector2.Lerp(arrowSpriteFront.size, new Vector2(arrowInitialFrontSize.x, sizeVector.y), lerpRate);
                    arrowSpriteCircle.size = Vector2.Lerp(arrowSpriteCircle.size, new Vector2(sizeVector.y, sizeVector.y), lerpRate);
                    directionArrow.transform.up = Vector2.Lerp(directionArrow.transform.up, dir, lerpRate);

                    yield return null;
                }
            }

            arrowSizeTemp = new Vector2(arrowInitialSize.x, size);
            arrowSprite.size = arrowSizeTemp;
            arrowSpriteBack.size = new Vector2(arrowInitialBackSize.x, arrowSizeTemp.y);
            arrowSpriteFront.size = new Vector2(arrowInitialFrontSize.x, arrowSizeTemp.y);
            arrowSpriteCircle.size = new Vector2(arrowSizeTemp.y, arrowSizeTemp.y);
            directionArrow.transform.up = dir;
        }

        private void StrikerOverLappingChanged(bool newOverlap)
        {
            //Commented as isHeadless is a parameter is NetworkManager.cs under Mirror
            //if (!NetworkManager.isHeadless)
            strikerHandler.SetStrikerPositionValid(newOverlap);
        }

        public void OnSliderMouseDown(Slider slider)
        {
            if (slider.IsInteractable())
                strikerHandler.SetHaloSize(strikerMoveHaloSize);
            isUsingSlider = true;
        }

        public void OnSliderMouseUp()
        {
            isUsingSlider = false;
            strikerHandler.SetHaloSize(1);
            if (IsPointOverlappingWith(striker.transform.position, carromMenLayer))
            {
                FindEmpty(striker.transform.position, true);
            }
        }

        /// <summary>
        /// Call this method when the striker is changed.
        /// </summary>
        private void OnStrikerChanged()
        {
            // Update the striker gameobject
            // Update the striker rigidbody
            // Update the things that related to the striker. 
        }


        /// <summary>
        /// Makes the mouse position independent of resolution.
        /// </summary>
        /// <returns>Uniform mouse position.</returns>
        private Vector3 UniformMousePosition(Vector3 mousePosition)
        {
            // return the normalized mouse position (between 0 to 1) relative to the screen
            //will be 0 if the mouse is on the very left
            //will be 1 if the mouse is on the very right

            float mouseRatioX = mousePosition.x / Screen.width;
            float mouseRatioY = mousePosition.y / Screen.height;

            return new Vector3(mouseRatioX - 0.5f, mouseRatioY - 0.5f, 0f);
        }

        public IEnumerator StopGame(bool hasGameStarted)
        {
            yield return new WaitForSeconds(2f);
            if (hasGameStarted)
            {
                OnStrikerReset(false, hasGameStarted);
                strikerRigidBody.gameObject.SetActive(false);
                strikerHandler.StopGame();

                strikerClicked = false;

                if (strikerJumpCoroutine != null)
                    StopCoroutine(strikerJumpCoroutine);

                //Commented as isHeadless is a parameter is NetworkManager.cs under Mirror
                //if (!NetworkManager.isHeadless)
                {
                    directionArrow.SetActive(false);
                    strikerHandler.SetHaloSize(1);
                }
            }
        }

        public void SetStrikerGFX()
        {
            int strikerIndex = GameManager.Instance.GamePlayHandler.playerInfo[GameManager.Instance.GetCurrentIndex].playerData.currentSelectedStriker;
            strikerHandler.SetStrikerGFX(strikerIndex);
            GameManager.Instance.UiManager.ChangeSliderStriker();
        }
    }
}

public class StrikerPositionData
{
    public Vector3 position;
    public float timeStamp;
    public int state;
    public bool hasCollided;
    public string collisionObject;
    public StrikerPositionData(Vector3 strikerPosition, float timeStamp, int state, bool hasCollided, string collObj)
    {
        this.position = strikerPosition;
        this.timeStamp = timeStamp;
        this.state = state;
        this.hasCollided = hasCollided;
        this.collisionObject = collObj;
    }
}

public class StrikerDirectionData
{
    public Vector3 direction;
    public float size;
    public float timeStamp;
    public int state;

    public StrikerDirectionData(Vector3 strikerPosition, float size, float timeStamp, int state)
    {
        this.direction = strikerPosition;
        this.size = size;
        this.timeStamp = timeStamp;
        this.state = state;
    }
}
