﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace AIMCarrom
{
    public class CarromMenPlacement : MonoBehaviour      //Initially Inheriting Network Behaviour
    {
        [SerializeField] private LayerMask carromMenLayer;
        [SerializeField] public Transform carromMenParent;
        [SerializeField] private GameObject queenCarromMan;
        [SerializeField] private GameObject whiteCarromMan;
        [SerializeField] private GameObject blackCarromMan;
        [SerializeField] private GameObject caromMenCollection;

        [SerializeField] private int firstLayerMenAmount;
        [SerializeField] private int maxLayers;
        [SerializeField] private float centerRadius;

        internal List<CarromMenHandler> carromMenList = new List<CarromMenHandler>();
        internal List<CarromMenHandler> carromMenResetList = new List<CarromMenHandler>();

        Coroutine placeObjectsCoroutine;
        public System.Action onPlaceObjectsDone;

        void Start()
        {

        }

        /*  initial function in NetworkServer.cs
         internal static void SpawnObject(GameObject obj, NetworkConnection ownerConnection)
        {
            if (!active)
            {
                Debug.LogError("SpawnObject for " + obj + ", NetworkServer is not active. Cannot spawn objects without an active server.");
                return;
            }

            NetworkIdentity identity = obj.GetComponent<NetworkIdentity>();
            if (identity == null)
            {
                Debug.LogError("SpawnObject " + obj + " has no NetworkIdentity. Please add a NetworkIdentity to " + obj);
                return;
            }
            identity.Reset();
            identity.connectionToClient = (NetworkConnectionToClient)ownerConnection;

            // special case to make sure hasAuthority is set
            // on start server in host mode
            if (ownerConnection is ULocalConnectionToClient)
                identity.hasAuthority = true;

            identity.OnStartServer();

            if (LogFilter.Debug) Debug.Log("SpawnObject instance ID " + identity.netId + " asset ID " + identity.assetId);

            identity.RebuildObservers(true);
        }
         */

        internal static void SpawnObject(GameObject obj)
        {                            

        }

        internal bool CheckForPrefab(GameObject obj)
        {
#if UNITY_EDITOR
#if UNITY_2018_3_OR_NEWER
            return UnityEditor.PrefabUtility.IsPartOfPrefabAsset(obj);
#elif UNITY_2018_2_OR_NEWER
            return (UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(obj) == null) && (UnityEditor.PrefabUtility.GetPrefabObject(obj) != null);
#else
            return (UnityEditor.PrefabUtility.GetPrefabParent(obj) == null) && (UnityEditor.PrefabUtility.GetPrefabObject(obj) != null);
#endif
#else
            return false;
#endif
        }

        internal bool VerifyCanSpawn(GameObject obj)
        {
            if (CheckForPrefab(obj))
            {
                Debug.LogErrorFormat("GameObject {0} is a prefab, it can't be spawned. This will cause errors in builds.", obj.name);
                return false;
            }

            return true;
        }

        internal void PlaceObjects(int rotation)
        {
            PlaceObjectsCoroutine(rotation);
            //if (placeObjectsCoroutine != null)
            //    StopCoroutine(placeObjectsCoroutine);

            //placeObjectsCoroutine = StartCoroutine(PlaceObjectsCoroutine(rotation));
        }

        void PlaceObjectsCoroutine(int rotation)
        {
            Debug.Log("Placing Carrom Objects!!");
            int id = 0;
            //carromMenParent.transform.eulerAngles = Vector3.zero;
            //carromMenParent.transform.localEulerAngles = new Vector3(0, 0, rotation);
            for (int i=0; i<carromMenList.Count; i++)
            {
                Destroy(carromMenList[i].gameObject);
            }
            carromMenList.Clear();
            Debug.Log("Deleted any preloaded Objects!!");

            var queen = Instantiate(queenCarromMan, carromMenParent);
            queen.transform.localRotation = Quaternion.Euler(Vector3.zero);
            queen.transform.localPosition = new Vector3(0, 0, 0);
            queen.name = "Queen";
            CarromMenHandler carromMenHandler = queen.GetComponent<CarromMenHandler>();
            AddCarromMen(id++, CarromMenColor.RED, carromMenHandler);
            //Call spawn queeen function is user is masterClient on Photon
            //NetworkServer.Spawn(queen);
            carromMenList.Add(carromMenHandler);
            float startTime = Time.time;
            for (int j = 0; j < maxLayers; j++)
            {
                int currentLayerMax = firstLayerMenAmount * (int)Mathf.Pow(2, j);
                float f = (Mathf.PI * 2) / currentLayerMax;

                for (int i = 0; i < currentLayerMax; i++)
                {
                    float angle = (f * i);
                    float radius = centerRadius * (j + 1) * ((j != 0 && i % 2 != 0) ? (Mathf.Pow(3, 0.5f)) / 2 : 1);
                    Vector2 temp = new Vector2(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle));
                    Vector3 pos = new Vector3(temp.x, temp.y, 0.1f);
                    if ((i & 1) == 0)
                    {
                        var temp_GameObject = Instantiate(whiteCarromMan, carromMenParent);
                        temp_GameObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                        //temp_GameObject.transform.localPosition = pos;
                        temp_GameObject.transform.position = pos;
                        carromMenHandler = temp_GameObject.GetComponent<CarromMenHandler>();
                        AddCarromMen(id++, CarromMenColor.WHITE, carromMenHandler);
                        //NetworkServer.Spawn(temp_GameObject);
                        /*  Use this code to replace above line objects dont spawn
                         * if (VerifyCanSpawn(temp_GameObject))
                        {
                            SpawnObject(temp_GameObject);
                        }*/
                        temp_GameObject.name = "WhiteCoin" + j.ToString() + i;
                        //Debug.Log($"{temp_GameObject.name} instantiated at position: {pos} , temp: {temp}, radius: {radius}, angle: {angle}");
                    }
                    else
                    {
                        var temp_GameObject = Instantiate(blackCarromMan, carromMenParent);
                        temp_GameObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                        //temp_GameObject.transform.localPosition = pos;
                        temp_GameObject.transform.position = pos;
                        carromMenHandler = temp_GameObject.GetComponent<CarromMenHandler>();
                        AddCarromMen(id++, CarromMenColor.BLACK, carromMenHandler);
                        //NetworkServer.Spawn(temp_GameObject);
                        temp_GameObject.name = "BlackCoin" + j.ToString() + i;
                        //Debug.Log($"{temp_GameObject.name} instantiated at position: {pos} , temp: {temp}, radius: {radius}, angle: {angle}");                        
                    }

                    carromMenList.Add(carromMenHandler);
                    //if (id % 3 == 0)
                    //    yield return null;
                }
            }
            carromMenParent.transform.localEulerAngles = new Vector3(0, 0, rotation);
            
            if (GameManager.Instance.IsLocalGame())
                onPlaceObjectsDone?.Invoke();
            else
            {
                GameManager.Instance.GamePlayHandler.CallEventPucksPlacementDone();
                //for (int i = 0; i < carromMenList.Count-15; i++)
                //{
                //    carromMenList[i].gameObject.SetActive(false);
                //}                
            }
            StartCoroutine(GameManager.Instance.GamePlayHandler.InitializeCoins());
        }

        internal void PlaceObjects2(int rotation)
        {
            int id = 0;
            carromMenList.Clear();
            GameObject CMCollection = Instantiate(caromMenCollection) as GameObject;
            CMCollection.transform.parent = carromMenParent.parent;
            CMCollection.transform.localScale = Vector3.one;
            CMCollection.transform.localPosition = new Vector3(0,-0.039f,0);
            CMCollection.transform.localEulerAngles = new Vector3(0, rotation, 0);
            //NetworkServer.Spawn(CMCollection);
            foreach (Transform TR in CMCollection.transform)
            {
                CarromMenHandler carromMenHandler = TR.GetComponent<CarromMenHandler>();
                carromMenList.Add(carromMenHandler);
                //NetworkServer.Spawn(carromMenHandler.gameObject);
            }
        }

        public void AddCarromMen(int id, CarromMenColor colorType, CarromMenHandler cmh)
        {
            CarromMen carromMen = new CarromMen { id = id };
            carromMen.SetColorType(colorType);
            cmh.SetValues(carromMen);
        }

        public void AddCarromMenToResetList(CarromMenHandler carromMenHandler)
        {
            if (!carromMenResetList.Contains(carromMenHandler))
            {
                carromMenResetList.Add(carromMenHandler);
            }
        }

        public void RemoveCarromMenFromResetList(CarromMenHandler carromMenHandler)
        {
            if (carromMenResetList.Contains(carromMenHandler))
            {
                carromMenResetList.Remove(carromMenHandler);
            }
        }

        internal void ResetCarromMen()
        {
            //Debug.Log($"Reset Carrom Men: {carromMenResetList.Count}");
            if (carromMenResetList.Count > 0)
            {
                StartCoroutine(ResetCarromMenCoroutine());
            }
        }

        IEnumerator ResetCarromMenCoroutine()
        {
            yield return new WaitForSeconds(0.1f);
            var list = GetEmptyArea(carromMenResetList.Count);
            for (int i = 0; i < carromMenResetList.Count; ++i)
            {
                carromMenResetList[i].gameObject.GetComponent<Rigidbody2D>().simulated = true;
                carromMenResetList[i].gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                carromMenResetList[i].gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
                carromMenResetList[i].gameObject.transform.localRotation = Quaternion.identity;
                carromMenResetList[i].gameObject.SetActive(true);
                carromMenResetList[i].gameObject.transform.localPosition = list[i];
                //carromMenResetList[i].gameObject.transform.DOLocalMove(list[i], 0.5f);
                carromMenResetList[i].gameObject.GetComponent<CarromMenHandler>().ResetCarromMen();
                yield return null;
            }
            Debug.LogFormat("<color=red>RESET CARROMMEN DONE</color>");
            carromMenResetList.Clear();
            GameManager.Instance.UiManager.UpdateScore();
        }

        internal IEnumerator StopGame()
        {
            yield return new WaitForSeconds(2f);
            RemoveObjects();
        }

        internal void RemoveObjects()
        {
            GameObject temp_GameObject = null;
            for (int i = 0; i < carromMenParent.childCount; ++i)
            {
                temp_GameObject = carromMenParent.GetChild(i).gameObject;
                CarromMenHandler carromMenHandler = temp_GameObject.GetComponent<CarromMenHandler>();
                carromMenHandler.RemoveReferences();
                Destroy(temp_GameObject);
                //NetworkServer.Destroy(temp_GameObject);
            }
            carromMenResetList.Clear();
            carromMenList.Clear();
            Debug.LogFormat("DONE DESTROYING OBJECTS");
        }

        internal List<Vector3> GetEmptyArea(int count)
        {
            List<Vector3> list = new List<Vector3>(count);
            if (!Physics2D.OverlapCircle(new Vector2(carromMenParent.position.x, carromMenParent.position.y), centerRadius / 2f, carromMenLayer))
            {
                list.Add(new Vector3(0, 0.1f, 0));
                if (list.Count >= count)
                    return list;
            }
            int j = 0;
            while (true)
            {
                int currentLayerMax = firstLayerMenAmount * (int)Mathf.Pow(2, j);
                float f = (Mathf.PI * 2) / currentLayerMax;
                int listCount = 0;
                for (int i = 0; i < currentLayerMax; i++)
                {
                    float angle = f * i;
                    float radius = centerRadius * (j + 1) * ((j != 0 && i % 2 != 0) ? (Mathf.Pow(3, 0.5f)) / 2 : 1);
                    Vector2 temp = new Vector2(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle));
                    Vector3 pos = new Vector3(temp.x, 0.1f, temp.y);
                    var checkPos = carromMenParent.TransformPoint(pos);
                    //                    Debug.Log("checkPos " + checkPos + "centerRadius/2f " + centerRadius / 2f);// + "CarromMen parent "+ new Vector2(carromMenParent.position.x, carromMenParent.position.y));
                    bool empty = Physics2D.OverlapCircle(checkPos, centerRadius / 2f, carromMenLayer);

                    if (!empty)
                    {
                        list.Add(pos);
                        listCount++;
                    }

                    if (list.Count >= count)
                        return list;
                }
                j++;
            }
            //return list;
        }
    }

}
