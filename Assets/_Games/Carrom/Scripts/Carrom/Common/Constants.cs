﻿using UnityEngine;

namespace AIMCarrom
{
    public static class Constants
    {
        public static string SHARE_TEXT = $"Hey, Download {Application.productName} and enjoy the game fun\nClick on this link to download game- https://play.google.com/store/apps/details?id={Application.identifier}.\n\n-Best gaming experience\n-New Awesome graphics\n-Smooth game play\n-Free to play carrom game";
        #region User Variables
        public const string USER_FILE_NAME = "PlayerData";
        public const string CURRENT_GAME_PORT_PREF = "currentGamePort";
        public const string SERVER_TOKEN_PREF = "serverTokenPref";
        public const string IS_LAST_GAME_TOURNAMENT_PREF = "isTournamentPref";
        public const string CURRENT_GAME_MODE_PREF = "CurrentGameModePref";
        public const string PLAYER_MATCH_PLAYED_COUNT = "MatchPlayedCount";
        public const string PLAYER_FIRST_TIME = "PLAYER_FIRST_TIME";
        #endregion

        #region Physics Variables
        public const float NORMAL_FORCE = 9.81f;
        public const int AD_REWARD_AMOUNT = 2000;
        public const float AD_FREQUENCY = 3f;
        #endregion

        #region POPUPS
        public const string POPUP_SOMETHING_WENT_WRONG_MESSAGE = "Something went wrong!";
        public const string POPUP_EMAIL_FORMAT_ERROR_TITLE = "Incorrect Format!";
        public const string POPUP_EMAIL_FORMAT_ERROR_INFO = "Email is not in the correct format.";
        public const string POPUP_EMAIL_EMPTY_ERROR_INFO = "Email can not be empty.";
        public const string POPUP_PASSWORD_EMPTY_ERROR_INFO = "Password can not be empty.";
        public const string POPUP_INVALID_CREDENTIALS = "Invalid Email/Password";
        public const string POPUP_EMAIL_EXISTS = "Email already exists.";
        public const string POPUP_NETWORK_ERROR_TITLE = "Network Error!";
        public const string POPUP_INTERNET_NOT_CONNECTED_INFO = "You are not connected to the internet.";
        public const string POPUP_GAME_DISCONNECTED = "You have been disconnected from the game.";
        public const string POPUP_LEAVE_GAME_INFO = "Do you want to leave the game?";
        public const string POPUP_PASSWORD_LENGTH_INFO = "The password should be atleast 6 characters long.";
        public const string POPUP_PASSWORD_MISMATCHED_INFO = "The password and confirm password do not match.";
        public const string POPUP_CANT_PLAY_TOURNAMENT_INFO = "Please link your account with Google/Facebook or login through an email to join others in the tournament.";
        public const string POPUP_CAN_PLAY_TOURNAMENT_WARNING = "You can also link your account with Google/Facebook (in settings screen), which will help you to retain your data once you switch mobile phone or reinstall the game.";
        public const string POPUP_ONE_NEW_MESSAGE = "You have 1 new message.";
        public const string POPUP_NO_ONE_PLAYING_GAME = "Game is finished because no one was playing.";
        public const string POPUP_PLAYER_WON_GAME = "{0} has won the match.";


        public const string POPUP_LEAVE_GAME_TITLE = "Exit Game";
        public const string POPUP_SIGNUP_TITLE = "Sign Up";
        public const string POPUP_SIGNIN_TITLE = "Sign In";
        public const string POPUP_EXIT_GAME_TITLE = "Exit Game";
        public const string POPUP_NO_ADS_TITLE = "No Ads";
        public const string POPUP_BUY_ITEM_TITLE = "Store";
        public const string POPUP_BUY_ITEM_FAILED_TITLE = "Oops!";
        public const string POPUP_GAME_UNDER_MAINTENANCE_TITLE = "Maintenance!";
        public const string POPUP_CHALLENGE_FRIEND_TITLE = "Challenge Friend";
        public const string POPUP_MATCH_FINISHED_TITLE = "Match Finished!";

        public const string POPUP_EXIT_GAME_MESSAGE = "Do you want to exit the game?";
        public const string POPUP_BUY_ITEM_MESSAGE = "Do you want to get this item?";
        public const string POPUP_BUY_ITEM_FAILED_MESSAGE = "There waas an error buying this product. Please try again later.";
        public const string POPUP_NOT_ENOUGH_COINS_PURCHASE_MESSAGE = "You do not have enough coins to complete this purchase.";
        public const string POPUP_NOT_ENOUGH_COINS_GAME_MESSAGE = "You do not have enough coins to play a game.Watch ads to get extra coins.";
        public const string POPUP_GAME_MANDATORY_UPDATE_MESSAGE = "There is a new update available. Please update to the latest version to continue.";
        public const string POPUP_GAME_TEMP_UPDATE_MESSAGE = "There is a new update available. Update the app for bug fixes.";
        public const string POPUP_LOGGED_OUT_MESSAGE_LOGIN = "You have been logged-in from another device.";
        public const string POPUP_LOGGED_OUT_MESSAGE_BLOCKED = "You have been banned by the administrator.";
        public const string POPUP_LOGGED_OUT_MESSAGE_DELETED = "Your account has been deleted by the administrator.";
        public const string POPUP_UNKNOWN_SERVER_ISSUE_MESSAGE = "The game stopped unexpectedly. Please try again.";
        public const string POPUP_GAME_UPDATE_TITLE = "Alert!";
        public const string POPUP_GAME_UNDER_MAINTENANCE_MESSAGE = "The game is under maintenance";
        public const string POPUP_GAME_EXIT_MESSAGE = "Are you sure\nyou want to exit?";
        public const string POPUP_PLAYER_WON_OFFLINE_MESSAGE = "{0} has won the match!";
        public const string POPUP_FRIEND_ALREADY_CHALLENGED_MESSAGE = "This user has already challenged you, Do you want to Accept the challenge?";
        public const string POPUP_REWARD_REWARDED_TITLE = "Hurray!";
        public const string POPUP_REWARD_REWARDED_MESSAGE = "You have received {0} coins";
        public const string POPUP_REWARD_NOT_REWARDED_TITLE = "Oops!";
        public const string POPUP_REWARD_NOT_REWARDED_MESSAGE = "Something went wrong";
        public const string POPUP_REWARD_NO_ADS_AVAILABLE = "No ads available";

        #endregion
        #region Title
        public const string TITLE_TOURNAMENT = "Tournament";
        public const string TITLE_1_VS_1_TOURNAMENT = "1 vs 1";

        #endregion
        #region PROFILE
        public const string PROFILE_DIALOG_TITLE_TEXT = "profile_update";
        public const string PROFILE_MIME_TYPE = "image/*";
        public const string PROFILE_NAME_ERROR_TEXT = "profile_name_error";
        public const string PROFILE_PIC_ERROR_TEXT = "profile_pic_error";
        #endregion PROFILE

        #region COMMON GAME VARIABLES
        public const string BOT_UNIQUE_ID = "botunqiueid.123";
        public const string _ID_FOR_BOT = "5db2874df7f9420ad8612baf";
        public const string EMAIL_REGEX = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        public static string[] SEARCHING_SCREEN_TEXT = { "Creating Room", "Setting up lights", "Finding Opponent" };
        #endregion

        #region AUDIO MIXER VARIABLES 
        public const string VOLUME_SFX = "SFXVolume";
        public const string VOLUME_MUSIC = "MusicVolume";
        #endregion
        public const string CARROM_MENU_TUTORIAL = "CarromMenuTutorial";
        public const string DEFFICULTY = "AIDefficulty";
        public const string FRIST_TIME_DATA = "FRIST_TIME_DATA";
        public const string PLAYER_DATA = "PLAYER_DATA";


        #region ------------- CARROM REGION STARTS HERE -------------

        public const string CARROM_USER_FILE_NAME = "CarromPlayerData";
        public const string CARROM_SCREENSHOT_FILE_NAME = "carrom_shared_img.png";

        #region Constant Variables
        public const float STRIKER_X_EXTENT = 2.4f;
        public static readonly Vector3 STRIKER_DEFAULT_LOCAL_POSITION = new Vector3(0, -3.102f, 0);
        //public static readonly Vector3 STRIKER_DEFAULT_LOCAL_POSITION = new Vector3(0, 0.1f, -3.102f);
        public const int MAX_COINS_OF_EACH_COLOR = 9;
        public const float MAX_ARROW_DIRECTION_SIZE = 1.512f;
        public const float PUCK_RADIUS = 0.2541f;
        public const float STRIKER_RADIUS = 0.333f;
        public const float MAX_STRIKER_VELOCITY_MAGNITUDE = 17f;
        public const float MAX_CARROM_MEN_VELOCITY_MAGNITUDE = 22f;
        public const int MAX_THEMES_COUNT = 6;
        #endregion

        #region GAME OBJECT NAMES
        public const string CARROM_WALL_LEFT = "Left";
        public const string CARROM_WALL_RIGHT = "Right";
        public const string CARROM_WALL_BOTTOM = "Bot";
        public const string CARROM_WALL_TOP = "Top";
        #endregion

        #region Tags
        public const string STRIKER_TAG = "Striker";
        public const string CARROM_MAN_TAG = "CarromMan";
        public const string POCKET_TAG = "Pocket";
        public const string CARROMBOARD_WALLS_TAG = "CarromBoardWalls";
        #endregion

        #region Layers
        public const string STRIKER_LAYER = "Striker";
        public const string POCKET_MEN_LAYER = "PocketMen";
        public const string CARROM_MEN_LAYER = "CarromMen";
        public const string CARROM_BOARD_LAYER = "CarromBoard";
        public const string CARROM_AI_STRIKER_WALKER_LAYER = "CarromAIStrikerWalker";
        #endregion

        #endregion ------------- CARROM REGION ENDS HERE -------------

        #region ------------- POOL REGION STARTS HERE -------------

        #region Layers
        public const string MAIN_BALL_LAYER = "MainBall";
        public const string BALL_LAYER = "Ball";
        public const string CANVAS_LAYER = "Canvas";
        public const string CLOTH_LAYER = "Graund";
        public const string WALL_LAYER = "Wall";
        public const string GUI_LAYER = "GUI";
        #endregion

        #endregion ------------- POOL REGION ENDS HERE -------------


        public const string REWARD_BUTTON = "Reward_Button";
        public const string BLOCK_PUZZLE_BUTTON = "Block_Puzzle_Button";
        public const string HEXAGON_19_BUTTON = "Hexagon19_Button";
        public const string CARROM_BUTTON = "Carrom_Button";
        public const string CHANGE_NAME = "Change_Name_Btn";
        public const string SHOP_BTN = "Shop_Btn";
        public const string AVATAR_BUTTON = "Avatar_Button";
        public const string EXIT_GAME = "Exit_Game";
        public const string GAME_CARROM_EXIT = "Carrom_Game_Exit";
        public const string CARROM_GAME_EXIT_YES = "Carrom_Game_Exit_Yes";
        public const string CARROM_GAME_EXIT_NO = "Carrom_Game_Exit_No";
        public const string MENU_SHARE = "Menu_Share";
        public const string SHARE_BTN = "Share_Btn";

        public const string CARROM_EASY = "Carrom_Easy";
        public const string CARROM_HARD = "Carrom_Hard";
        public const string CARROM_MEDIUM = "Carrom_Medium";
        public const string CARROM_GAME_EXIT = "Carrom_Game_Exit";
        public const string CARROM_SHARE = "Carrom_Share";
        public const string CARROM_EXIT_YES = "Carrom_Exit_Yes";
        public const string CARROM_EXIT_NO = "Carrom_Exit_No";
        public const string CARROM_REMATCH = "Carrom_Rematch";
        public const string CARROM_GO_TO_HOME = "Carrom_GoToHome";


        public const string BLOCK_GAME_EXIT = "Block_Game_Exit";
        public const string BLOCK_GO_TO_HOME = "Block_GoToHome";
        public const string BLOCK_RESUME = "Block_Resume";
        public const string BLOCK_RESTART = "Block_Restart";

        public const string HEXAGON_GAME_EXIT = "Hexagon_Game_Exit";
        public const string HEXAGON_GO_TO_HOME = "Hexagon_GoToHome";
        public const string HEXAGON_RESUME = "Hexagon_Resume";
        public const string HEXAGON_PAUSE = "Hexagon_Pause";
        public const string HEXAGON_RESTART = "Hexagon_Restart";
        public const string HEXAGON_DELETE_BLOCK = "Hexagon_Delete_Block";
        public const string UPDATE_NOW = "Update_Game_Buton";
        public const string UPDATE_NOT_NOW = "Update_Game_Not_Now";
        public const string SHOWING_UPDATE_POPUP = "Showing_Update_Popup";

        public const string LUDO_BUTTON = "Ludo_Button";
        public const string LUDO_LOCAL_PLAYER_BUTTON = "Ludo_Play_Local_Player_Button";
        public const string LUDO_PLAY_LOCAL_PLAYER_BUTTON = "Ludo_Local_Player_Button";
        public const string LUDO_PC_PLAYER_BUTTON = "Ludo_PC_Player_Button";
        public const string LUDO_PLAY_PC_PLAYER_BUTTON = "Ludo_Play_PC_Player_Button";
        public const string LUDO_GAME_EXIT = "Ludo_Game_Exit";
        public const string LUDO_EXIT_YES = "Ludo_Exit_Yes";
        public const string LUDO_EXIT_NO = "Ludo_Exit_No";
        public const string LUDO_GO_TO_HOME = "Ludo_GoToHome";

        public const string STACK_BALL_BUTTON = "Stack_Ball_Button";
        public const string STACK_BALL_FULL_ADS_BUTTON = "Stack_Ball_FullAds_Button";
    }
}