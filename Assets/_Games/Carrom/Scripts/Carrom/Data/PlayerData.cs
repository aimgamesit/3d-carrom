﻿using System.Collections.Generic;
using UnityEngine;

namespace AIMCarrom
{
    [System.Serializable]
    public class PlayerData : PlayerProfileData
    {
        public int currentStriker;
        public List<int> boughtStrikers;
        public int currentCarromMen;
        public List<int> boughtCarromMen;
        public int currentAvatar;
        public List<int> boughtAvatar;

        public PlayerData()
        {
            if (PlayerPrefs.GetInt("SETTTT", 0) == 0)
            {
                PlayerPrefs.SetInt("SETTTT", 1);
                Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!");
                uniqueID = Random.Range(100, 1000).ToString();
                playerName = "Guest_" + uniqueID;
                email = "";
                gamesPlayed = 0;
                gamesWon = 0;
                winStreak = 0;
                TotalCoins = 0;
                imageString = "";
                language = "English";
                country = "";
                currentStriker = 0;
                boughtStrikers = new List<int> { 0 };
                currentCarromMen = 0;
                boughtCarromMen = new List<int> { 0, 1 };
                currentAvatar= 0;
                boughtAvatar = new List<int> { 0, 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 };
            }
            PlayerPrefs.DeleteKey("NEW_AV");
            Debug.Log("AVATARDATA :"+ PlayerPrefs.GetInt("NEW_AV", 0));
            if (PlayerPrefs.GetInt("NEW_AV", 0) == 0)
            {
                PlayerPrefs.SetInt("NEW_AV", 1);
                boughtAvatar = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                Debug.Log( (boughtAvatar == null) +" AVATARDATA []=>:" + PlayerPrefs.GetInt("NEW_AV", 0));
            }
        }
    }
}