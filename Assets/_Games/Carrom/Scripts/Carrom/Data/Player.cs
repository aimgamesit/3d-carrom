﻿using System.Collections.Generic;
using UnityEngine;

namespace AIMCarrom
{
    public static class Player
    {
        private static PlayerData gameUser;

        public static PlayerData PlayerData
        {
            get
            {
                if (gameUser == null)
                    LoadData();
                return gameUser;
            }
        }

        public static void SaveDataToPrefs()
        {
            if (gameUser == null)
                LoadData();
            string playerDataJson = Pathfinding.Serialization.JsonFx.JsonWriter.Serialize(PlayerData);
            PlayerPrefs.SetString(Constants.PLAYER_DATA, playerDataJson);
            Debug.LogFormat("<color=magenta>SaveDataToPrefs(): {0}</color>", playerDataJson);
            LoadData();
        }

        private static void LoadData()
        {
            string defData = PlayerPrefs.GetString(Constants.PLAYER_DATA, "");
            if (string.IsNullOrEmpty(defData))
            {
                Debug.Log("NULLLLLLLL: "+defData);
                PlayerPrefs.SetString(Constants.PLAYER_DATA, Pathfinding.Serialization.JsonFx.JsonWriter.Serialize(new PlayerData()));
            }
            Debug.LogFormat("<color=magenta>LoadData(defData): {0}</color>", defData);
            gameUser = Pathfinding.Serialization.JsonFx.JsonReader.Deserialize<PlayerData>(PlayerPrefs.GetString(Constants.PLAYER_DATA, ""));
            Debug.Log("______AVATAR_NULL: "+(gameUser.boughtAvatar == null));
            if (gameUser.boughtAvatar == null)
            {
                gameUser.boughtAvatar = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                Debug.Log("______AVATAR_NULL(NOT NULL): " + gameUser.boughtAvatar.Count);
                SaveDataToPrefs();
            }
        }
    }
}