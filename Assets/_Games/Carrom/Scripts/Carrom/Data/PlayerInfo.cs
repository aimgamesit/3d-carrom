﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIMCarrom
{
    [System.Serializable]
    public struct PlayerInfo
    {
        public enum ColorType
        {
            BLACK,
            WHITE
        }

        public PlayerNetworkInfo playerData;
        public int playerIndex;
        public ColorType playerColor;
        public PocketedCarromMenData[] pocketedCarromMen;
        //public int[] allPocketedMen;
        public bool hasQueen;
        public string playerID;

        public PlayerInfo(PlayerNetworkInfo playerObj, int index, ColorType color, string id, PocketedCarromMenData[] pocketed, bool queenState = false)
        {
            playerData = playerObj;
            playerIndex = 0;
            playerID = id;
            pocketedCarromMen = pocketed;
            playerColor = color;
            hasQueen = queenState;
        }
    }

    [System.Serializable]
    public struct PocketedCarromMenData
    {
        public int id;
        public int turnIndex;
    }
}