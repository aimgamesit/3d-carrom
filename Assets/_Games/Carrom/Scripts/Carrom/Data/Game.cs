﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIMCarrom
{
    [System.Serializable]
    public struct Game
    {
        public int currentTurnIndex;
        public float turnTime;
        public long betGoldAmount;
        public bool isQueenCovered;
        public bool hasGameStarted;
        public bool isGameInSimulation;
        public bool queenPocketedWithoutCover;
        public float playerStartTimer;
        public bool isPlayerTimerRunning;

       // public Game ()
        //{
            //pocketedCarromMen = new List<CarromMen>();
            //playerInfo = new List<PlayerInfo>();
            //carromMenCollection = new List<CarromMen>();
       // }

    }
}