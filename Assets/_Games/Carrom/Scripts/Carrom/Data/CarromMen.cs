﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AIMCarrom
{
    public enum CarromMenColor
    {
        BLACK,
        WHITE,
        RED
    }
    
    [System.Serializable]
    public struct CarromMen
    {
        #region PUBLIC VARIABLES

        public int id;
        public CarromMenColor colorType;
        public bool inActive;

        #endregion PUBLIC VARIABLES
        internal CarromMen SetColorType(CarromMenColor ct)
        {
            colorType = ct;
            return this;
        }
    }
}