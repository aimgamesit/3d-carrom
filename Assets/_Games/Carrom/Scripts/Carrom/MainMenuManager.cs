﻿using AIMCarrom;
using AIMLudo;
using BlockPuzzleNS;
using com.TicTok.managers;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
namespace AIMCarrom
{
    public class MainMenuManager : BManager<MainMenuManager>
    {
        [SerializeField] private Text versionText;
        [SerializeField] private Image playerProfile;
        [SerializeField] private Text playerName;
        [SerializeField] private Text playerCoins;
        [SerializeField] private Text freeCoinsText;
        [SerializeField] private GameObject carromOptionScreen;
        [SerializeField] private GameObject gameSelectionScreen;
        [SerializeField] private GameObject coinAnimPrefab;
        [SerializeField] private Transform coinsAnimSourceTrans, coinsAnimDestTrans;
        [SerializeField] private AudioSource coinAudioSource;
        [SerializeField] private ShopManager shopManager;
        [SerializeField] private TutorialManager tutorialManager;
        [SerializeField] private LudoSelection ludoSelection;
        public PopupManager popupManager;
        public GameObject loadingScreen;

        void Start()
        {
            SetData();
            Screen.sleepTimeout = SleepTimeout.SystemSetting;
            Player.SaveDataToPrefs();
            loadingScreen.SetActive(false);
            SetVersionText();
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        private void SetVersionText() => versionText.text = "Version " + Application.version;


        public void PlayButtonClickSound()
        {
            if (PlayerPrefs.GetInt(Constants.VOLUME_MUSIC, 1) == -80)
                return;
            BlockPuzzleNS.AudioManager.Instance.PlaySFXAtPosition(BlockPuzzleNS.AudioManager.click_sound);
        }
        private void SetData()
        {
            carromOptionScreen.SetActive(false);
            gameSelectionScreen.SetActive(true);
            CheckANdSetDefaultCoins();
            GameData.Instance.aIDifficultyType = (AIDifficultyType)PlayerPrefs.GetInt(Constants.DEFFICULTY, 0);
            freeCoinsText.text = "Get " + GameData.Instance.freeReward + " coins";
            UpdatePlayerUI();
        }


        public void UpdatePlayerUI()
        {
            playerName.text = Player.PlayerData.playerName;
            playerCoins.text = Player.PlayerData.TotalCoins.ToString();
            playerProfile.sprite = GameData.Instance.avatars[Player.PlayerData.currentAvatar].sprite;
        }

        private IEnumerator LoadNextScene(string sceneName)
        {
            PlayButtonClickSound();
            loadingScreen.SetActive(true);
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            yield return new WaitForEndOfFrame();
            SceneManager.LoadSceneAsync(sceneName);
        }

        private void CheckANdSetDefaultCoins()
        {
            if (PlayerPrefs.GetInt(Constants.FRIST_TIME_DATA, 0) == 0)
            {
                Player.PlayerData.TotalCoins = GameData.Instance.loginCoins;
                Player.SaveDataToPrefs();
                PlayerPrefs.SetInt(Constants.FRIST_TIME_DATA, 1);
            }
        }

        public void OnToggleValueChange(int index)
        {
            PlayButtonClickSound();
            GameData.Instance.aIDifficultyType = (AIDifficultyType)index;
            PlayerPrefs.SetInt(Constants.DEFFICULTY, (int)GameData.Instance.aIDifficultyType);
        }

        #region Button click methods
        public void OnExitBtnClicked()
        {
            PlayButtonClickSound();
            popupManager.ShowPopup(false, Player.PlayerData.TotalCoins, PopupManager.PopupType.YES_NO, Constants.POPUP_EXIT_GAME_TITLE, Constants.POPUP_GAME_EXIT_MESSAGE, "YES", "NO", "OK", "", delegate (bool callback, string inputText)
            {
                Debug.Log(callback);
                if (callback)
                {
                    //AdmobAdsManager._AdClosed += delegate () {  };
                    //AdmobAdsManager.Instance.ShowInterstitial();
                    FirebaseManager.Instance.SendEvent(Constants.CARROM_EXIT_YES);
                    Application.Quit();
                }
                else
                {
                    FirebaseManager.Instance.SendEvent(Constants.CARROM_EXIT_NO);
                    AdmobAdsManager._AdClosed += delegate () { };
                    AdmobAdsManager.Instance.ShowInterstitial();
                }
            });
            FirebaseManager.Instance.SendEvent(Constants.EXIT_GAME);
        }

        public void OnInputNameBtnClicked()
        {
            PlayButtonClickSound();
            popupManager.ShowPopup(false, Player.PlayerData.TotalCoins, PopupManager.PopupType.INPUT, "Enter Name", "", "SAVE", "CANCEL", "OK", Player.PlayerData.playerName, delegate (bool callback, string inputText)
            {
                if (callback)
                {
                    AdmobAdsManager._AdClosed += delegate ()
                    {
                        Player.PlayerData.playerName = inputText;
                        Player.SaveDataToPrefs();
                        UpdatePlayerUI();
                    };
                    AdmobAdsManager.Instance.ShowInterstitial();
                }
            });
            FirebaseManager.Instance.SendEvent(Constants.CHANGE_NAME);
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        public void OnWatchVideoBtnCLicked()
        {
            PlayButtonClickSound();
            bool isRewardReceived = false;
            Debug.Log("OnWatchVideoBtnCLicked()");
            if (AdmobAdsManager.Instance.IsVideoAdAvailable())
            {
                AdmobAdsManager._OnRewardedReceived += delegate ()
                {
                    Debug.Log("isRewardReceived  TERU: " + isRewardReceived);
                    if (!isRewardReceived)
                    {
                        isRewardReceived = true;
                        StartCoroutine(ShowCoinAnimation(GameData.Instance.freeReward));
                    }
                };
                AdmobAdsManager._AdClosed += delegate ()
                {
                    Debug.Log("isRewardReceived  FALSE: " + isRewardReceived);
                    if (!isRewardReceived)
                    {
                        isRewardReceived = true;
                    }
                };
                AdmobAdsManager.Instance.ShowVideoAd();
            }
            else
            {
                popupManager.ShowPopup(false, Player.PlayerData.TotalCoins, PopupManager.PopupType.OK, Constants.POPUP_NO_ADS_TITLE, Constants.POPUP_REWARD_NO_ADS_AVAILABLE, "", "", "OK", Player.PlayerData.playerName, delegate (bool callback, string inputText) { });
            }
            FirebaseManager.Instance.SendEvent(Constants.REWARD_BUTTON);
        }

        private IEnumerator ShowCoinAnimation(int totalRewardCoins)
        {
            coinAudioSource.Play();
            long currentCoins = Player.PlayerData.TotalCoins;
            for (int i = 0; i < totalRewardCoins / 100; i++)
            {
                playerCoins.text = currentCoins.ToString();
                currentCoins += 100;
                Instantiate(coinAnimPrefab, coinsAnimSourceTrans).transform.DOMove(coinsAnimDestTrans.position, .1f);
                yield return new WaitForSeconds(.05f);
            }

            Player.PlayerData.TotalCoins += GameData.Instance.freeReward;
            Player.SaveDataToPrefs();
            UpdatePlayerUI();
        }

        public void OnPlayBtnCLicked(int index)
        {
            GameData.Instance.aIDifficultyType = (AIDifficultyType)index;
            GameData.Instance.selectedThemeIndex = 0;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            StartCoroutine(LoadNextScene(GameData.Instance.CARROM_GAME_SCENE));
            string eventName = Constants.CARROM_EASY;
            if (index == 1)
                eventName = Constants.CARROM_MEDIUM;
            else if (index == 1)
                eventName = Constants.CARROM_HARD;
            FirebaseManager.Instance.SendEvent(eventName);
        }

        public void OnBackBtnClick()
        {
            PlayButtonClickSound();
            carromOptionScreen.SetActive(false);
            gameSelectionScreen.SetActive(true);
            FirebaseManager.Instance.SendEvent(Constants.CARROM_BUTTON);
        }

        public void OnCarromBtnClick()
        {
            PlayButtonClickSound();
            carromOptionScreen.SetActive(true);
            gameSelectionScreen.SetActive(false);
        }

        public void OnBlockBtnClick()
        {
            StartCoroutine(LoadNextScene(GameData.Instance.PUZZLE_GAME_SCENE));
            FirebaseManager.Instance.SendEvent(Constants.BLOCK_PUZZLE_BUTTON);
        }

        public void OnHexagonBtnClick()
        {
            StartCoroutine(LoadNextScene(GameData.Instance.HEXAGON_GAME_SCENE));
            FirebaseManager.Instance.SendEvent(Constants.HEXAGON_19_BUTTON);
        }

        public void OnLudoBtnClick()
        {
            //StartCoroutine(LoadNextScene(GameData.Instance.LUDO_GAME_SCENE));
            PlayButtonClickSound();
            ludoSelection.SetActive(true);
            gameSelectionScreen.SetActive(false);
            FirebaseManager.Instance.SendEvent(Constants.LUDO_BUTTON);
        }

        public void OnLudoSelectioCloseClick()
        {
            PlayButtonClickSound();
            gameSelectionScreen.SetActive(true);
            ludoSelection.SetActive(false);
        }

        public void OnStackBallBtnClick()
        {
            StartCoroutine(LoadNextScene(GameData.Instance.STACK_BALL_GAME_SCENE));
            FirebaseManager.Instance.SendEvent(Constants.STACK_BALL_BUTTON);
        }

        public void OnShopBtnCLicked(int itemType)
        {
            PlayButtonClickSound();
            shopManager.PopulateItems((ShopManager.ItemType)itemType);
            tutorialManager.PlayNextTutorial();
            FirebaseManager.Instance.SendEvent(itemType == 2 ? Constants.AVATAR_BUTTON : Constants.SHOP_BTN);
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        public void OnShareBtnCLicked()
        {
            PlayButtonClickSound();
            AIMPluginManager.Instance.ShareApp(true);
            FirebaseManager.Instance.SendEvent(Constants.MENU_SHARE);
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }
        #endregion
    }
}