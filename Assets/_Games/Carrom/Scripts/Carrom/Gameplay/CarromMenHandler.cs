﻿using System;
using System.Collections;
using NaughtyAttributes;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace AIMCarrom
{
    public class CarromMenHandler : MonoBehaviour    //Initially Inheriting NetworkBehaiour.cs
    {
        [SerializeField] [Slider(0, 50)] private float velocityToPot = 12;

        //[SyncVar]
        public CarromMen carromMen;

        private CoinTumble coinTumble;
        internal Rigidbody2D carromBody;
        //private MeshRenderer meshRenderer;
        private TransformSync transformSync;
        private Collider2D carromMenCollider;
        private Transform lastPocketPosition;
        public float radius { get; private set; }
        //[SyncVar]
        public bool pocketed;
        //[SyncVar(hook = nameof(OnCarromMenStateChange))]
        public bool isDisabled;
        private StrikerHandler striker;

        [SerializeField] private GameObject sprite;
        [SerializeField] private SpriteRenderer puckSpriteRenderer;
        [SerializeField] private SpriteRenderer puckOnPot;
        [SerializeField] private Transform lightingOverlay;
        [SerializeField] private Transform shadowTransform;

        public static System.Action<CarromMenHandler> onPocket;
        private bool isPlacementDone;
        [SerializeField] private CoinCollisionAudio coinCollisionAudio;
        private Vector3 lastPositionSent;
        bool isOfflineGame; 
        #region Collision Variables
        private Vector2 wallNormal;
        public List<CoinDetails> coinPositionUpdates;
        #endregion

        private void Awake()
        {
            coinTumble = GetComponent<CoinTumble>();
            carromBody = GetComponent<Rigidbody2D>();
            //meshRenderer = GetComponent<MeshRenderer>();
            carromMenCollider = GetComponent<Collider2D>();
            transformSync = GetComponent<TransformSync>();
            radius = carromBody.GetComponent<CircleCollider2D>().radius;
            coinPositionUpdates = new List<CoinDetails>();
            lastPositionSent = Vector3.zero;
        }
        private void Start()
        {
            striker = GameManager.Instance.GamePlayHandler.StrikerControls.strikerHandler;
            isOfflineGame = GameManager.Instance.IsLocalGame();
        }

        private void OnApplicationPause(bool pause)
        {
            if(!pause /*&& !NetworkManager.isHeadless*/)    // Uncomment for photon
                OnCarromMenStateChange(pause, isDisabled);
        }

        public void OnStartClient()
        {
            // base.OnStartClient();        //NetworkBehaviour.cs
            OnCarromMenStateChange(false, isDisabled);
        }

        private void OnEnable()
        {
            pocketed = false;
            GetComponent<CoinTumble>().onTumbleFinish += OnTumbleFinish;
            puckOnPot.gameObject.SetActive(false);
            sprite.SetActive(true);
            GameManager.Instance.GamePlayHandler.EventPucksPlacementDone += OnAllPucksPlaced;
            GameManager.Instance.GamePlayHandler.CarromMenPlacement.onPlaceObjectsDone += OnAllPucksPlaced;
            GamePlayHandler.onTurnEnd += OnTurnEnd;
        }

        private void OnDisable()
        {
            coinTumble.onTumbleFinish -= OnTumbleFinish;
            GameManager.Instance.GamePlayHandler.EventPucksPlacementDone -= OnAllPucksPlaced;
            GameManager.Instance.GamePlayHandler.CarromMenPlacement.onPlaceObjectsDone -= OnAllPucksPlaced;
            GamePlayHandler.onTurnEnd -= OnTurnEnd;
        }

        float shadowMaxDistance = 2.5f;
        //[ClientCallback]
        private void FixedUpdate()
        {
            if (striker.inShot)
            {
                Physics.Simulate(Time.fixedDeltaTime);                
            }

            if (carromBody.IsSleeping() || isDisabled /*|| NetworkManager.isHeadless*/)
                return;

            float currentRotY = -transform.localEulerAngles.z;
            Vector3 newRot = new Vector3(0, 0, currentRotY);
            lightingOverlay.localEulerAngles = newRot;
            puckOnPot.transform.localEulerAngles = newRot;

            // shadow movement
            Vector2 dir = transform.position - transform.parent.parent.position;
            float mag = Mathf.Clamp(dir.magnitude / shadowMaxDistance, 0, 1);
            shadowTransform.position = transform.position + (Vector3)dir.normalized * mag * 0.05f;
        }

        public void ResetCarromMen()
        {
            if (transformSync)
                transformSync.ResetSync();
            isDisabled = false;
            pocketed = false;
            carromMenCollider.enabled = true;
            StopCoinMovement();
            OnTurnEnd();
        }

        //Make it a photon RPC
        public void OnCarromMenStateChange(bool oldState, bool state)
        {
            //Debug.Log("OnCarromMenStateChange " + state + "  " + name);
            //meshRenderer.enabled = !state;
            //Debug.LogFormat("OnCarromMenStateChange : CARROM MEN isDisabled : {0}", state);
            puckOnPot.gameObject.SetActive(false);
            sprite.SetActive(!state);

            //GetComponent<Rigidbody2D>().simulated = !state;
            carromMenCollider.enabled = !state;
        }


        //[Server]  //Server callback, Replace by photon Rpc called for other player
        private void OnTumbleFinish()
        {
            //Debug.Log($"OnTumbleFinish()");
            //GetComponent<Rigidbody2D>().simulated = false;
            //isDisabled = true;
            //carromMenCollider.enabled = !isDisabled;

            // if (carromMen.colorType != CarromMenColor.RED)
            //      GameManager.Instance.UIFxManager.StartLerpPotCoin(carromMen.colorType, lastPocketPosition.position, (int)carromMen.colorType);
            //carromMenCollider.enabled = false;

            StartCoroutine(RpcTumble());
            
        }

        //[ClientRpc]
        private IEnumerator RpcTumble()
        {
            if(!isOfflineGame)
                yield return new WaitForSeconds(1f);
            isDisabled = true;
            carromMenCollider.enabled = !isDisabled;
            if (carromMen.colorType != CarromMenColor.RED)
                GameManager.Instance.UIFxManager.StartLerpPotCoin(CarromMenColor.BLACK, puckSpriteRenderer.sprite, lastPocketPosition.position, (int)carromMen.colorType);
            puckOnPot.gameObject.SetActive(false);
            carromMenCollider.enabled = false;

            //Debug.LogFormat("RPC TUMBLE : PUCK ON POT isDisabled : {0}", isDisabled);
            //Debug.LogFormat("<color=yellow>COLLIDER CHANGED</color>");
            //OnCarromMenStateChange(true);
            yield return null;
        }

        void Tumble()
        {
            if (carromMen.colorType != CarromMenColor.RED)
                GameManager.Instance.UIFxManager.StartLerpPotCoin(CarromMenColor.BLACK, puckSpriteRenderer.sprite, lastPocketPosition.position, (int)carromMen.colorType);
            carromMenCollider.enabled = false;
        }

        //[ServerCallback]
        private void OnTriggerStay2D(Collider2D coll)
        {
            if (!pocketed && coll.CompareTag(Constants.POCKET_TAG))
            {
                if (!isOfflineGame && !GameManager.Instance.IsMyTurn)
                    return;
                if (carromBody.velocity.magnitude < velocityToPot)
                {
                    pocketed = true;
                    coinTumble.OnPot(coll.gameObject, gameObject);
                    //Debug.Log("men velocity less than pot velocity");
                    //OnPot();
                    lastPocketPosition = coll.transform;
                    //Debug.Log($"collObject: {coll.gameObject.name}");
                    List<GameObject> holes = (coll.gameObject.name.Contains("AI")) ? GameManager.Instance.aiPockets : GameManager.Instance.carromPockets;
                    //for (int l = 0; l < holes.Count; l++)
                    //{
                    //    Debug.Log($"Pocket {l}: {holes[l].gameObject.name}");
                    //}
                    RpcPot(carromBody.velocity.magnitude, holes.IndexOf(coll.gameObject));
                }
            }
        }

        //[ServerCallback]
        private void OnCollisionStay2D(Collision2D coll)
        {
            var wall = coll.contacts.FirstOrDefault(t => t.collider.CompareTag(Constants.CARROMBOARD_WALLS_TAG));
            wallNormal = wall.normal;

            //Debug.LogFormat("NormAL : {0}", wallNormal);
        }

        //[ServerCallback]
        private void OnCollisionEnter2D(Collision2D coll)
        {
            if (!isOfflineGame && !GameManager.Instance.IsMyTurn)
                return;
            //Debug.Log($"{name} collided with {coll.gameObject.name}");

            Vector2 strikerVelocity = striker.Body.velocity;
            Vector2 reverseDirection = Vector2.Reflect(strikerVelocity, wallNormal);
            float contactAngle = Vector2.Angle(reverseDirection, wallNormal);

            //Debug.LogFormat("<color=blue>Contact Angle : </color>{0}", contactAngle);
            if(wallNormal != Vector2.zero)
                carromBody.velocity += reverseDirection * 0.4f;

            if (coll.collider.CompareTag(Constants.CARROM_MAN_TAG) || coll.collider.CompareTag(Constants.STRIKER_TAG) || coll.collider.CompareTag(Constants.CARROMBOARD_WALLS_TAG))
            {
                if (!isOfflineGame && striker.inShot && GameManager.Instance.IsMyTurn)
                {
                    //Debug.Log($"Sending details: {name} collided with {coll.gameObject.name}");
                    if(!sendingData)
                        StartCoroutine(SendCoinDetails());
                }
                float maxVelocity = gameObject.CompareTag(Constants.STRIKER_TAG) ? Constants.MAX_STRIKER_VELOCITY_MAGNITUDE : Constants.MAX_CARROM_MEN_VELOCITY_MAGNITUDE;
                float volume = carromBody.velocity.magnitude / maxVelocity;
                
                var collisionAudio = coll.collider.GetComponent<CoinCollisionAudio>();
                if (collisionAudio != null)
                {
                    int myId = gameObject.GetInstanceID();
                    int otherObjId = collisionAudio.gameObject.GetInstanceID();

                    if (myId < otherObjId)
                    {
                        //Debug.LogFormat("Played SOUND");
                        RpcPlayCollisionSound(volume * volume, coll.collider.gameObject.tag);
                        return;
                    }
                }
                RpcPlayCollisionSound(volume * volume, coll.collider.gameObject.tag);
            }
        }

        //[ServerCallback]
        private void OnCollisionExit2D(Collision2D coll)
        {
            wallNormal = Vector2.zero;
        }

        //[ClientRpc]
        public void RpcPot(float pow, int index)
        {
            if (transformSync)
            {
                transformSync.isPot = true;
                transformSync.StopRigidbody();
            }
            Debug.Log($"Count: {GameManager.Instance.carromPockets.Count}, Index: {index}");
            var pocket = GameManager.Instance.carromPockets[index];
            lastPocketPosition = pocket.transform;
            carromBody.velocity = Vector3.zero;
            // coinTumble.OnPot(pocket, gameObject);
            //if(!MultiplayerManager.isOffline)
                OnPot();

            float volume = pow / Constants.MAX_STRIKER_VELOCITY_MAGNITUDE;
            pocket.GetComponent<PocketDetector>().PlayPocketSound(volume);
        }

        private void OnPot()
        {
            puckOnPot.gameObject.SetActive(true);
            sprite.gameObject.SetActive(false);
            //Debug.LogFormat("OnPot:: IsDisabled : {0}", isDisabled);
                        
            onPocket?.Invoke(this);
            //GameManager.Instance.GamePlayHandler.OnCarromMenPocket(this);     //comment above line if uncommenting this one.                       
        }

        public void SetValues(CarromMen carrom)
        {
            carromMen = carrom;
        }

        internal void RemoveReferences()
        {
            pocketed = false;
            coinTumble.StopTumble();
        }

        internal void SetPuckGFX(int myIndex)
        {
            //int playerIndex = (int)carromMen.colorType == GameManager.Instance.GetCurrentIndex ? myIndex : 1 - myIndex; // tempDe
            int playerIndex = (int)carromMen.colorType == (int)GameManager.Instance.GamePlayHandler.playerInfo[myIndex].playerColor ? myIndex : 1 - myIndex;

            //Debug.LogFormat("####Player Index : {0}", playerIndex);
            if (carromMen.colorType != CarromMenColor.RED)
                GiveDistinctGFX(playerIndex);
        }

        void GiveDistinctGFX(int playerIndex)
        {
            puckSpriteRenderer.sprite = GameManager.Instance.UiManager.GetSprite(GameManager.Instance.playersPuckIndices[playerIndex]);
            puckOnPot.sprite = puckSpriteRenderer.sprite;
        }

        private void OnAllPucksPlaced()
        {
            StartCoroutine(OnPucksPlacedCoroutine());
        }

        IEnumerator OnPucksPlacedCoroutine()
        {
            yield return new WaitForSeconds(0.1f);

            puckOnPot.gameObject.SetActive(false);
            sprite.SetActive(true);
            isPlacementDone = true;

            // code that aligns all pucks to same rotation
            //yield return new WaitForSeconds(0.3f);
            transform.localEulerAngles = new Vector3(0, 0, Mathf.Abs(transform.parent.localEulerAngles.z) + 180);
        }

        //[ClientRpc]
        void RpcPlayCollisionSound(float vol, string tag)
        {
            coinCollisionAudio.PlayCollisionSound(vol, tag);
        }

        private void OnTurnEnd()
        {
            //Commented as isHeadless is a parameter in NetworkManager under Mirror
            //if(!NetworkManager.isHeadless)
                OnCarromMenStateChange(isDisabled, isDisabled);  
        }

        bool sendingData;
        public IEnumerator SendCoinDetails()
        {
            if (isOfflineGame)
                yield return null;
            else if (GameManager.Instance.IsMyTurn)
                yield return null;
            //Debug.Log($"striker Count:{strikerCount} sending at time {Time.time}");
            yield return new WaitForSeconds(0.15f);

            StopCoroutine(SendCoinDetails());
            if (striker.inShot)
            {
                sendingData = true;
                StartCoroutine(SendCoinDetails());
            }else
            {
                yield return new WaitForSeconds(0.4f);
                sendingData = false;
            }
        }

        internal int startIndex = 0;
        internal bool movingCoin;
        public void CoinPositionFromNetwork(string objName, Vector3 coinPosition, float timeStamp, bool hasCollided, string collObj)
        {
            coinPositionUpdates.Add(new CoinDetails(objName, coinPosition, timeStamp, hasCollided, collObj));
            //Debug.Log($"Added new Pos for :{objName} count:{coinPositionUpdates.Count}, position {coinPosition}, timeStamp: {timeStamp}, has Collided: {hasCollided} with:{collObj}");            
        }

        public IEnumerator StartPositionSimulation(bool reduceDuration = false)
        {
            yield return new WaitForSeconds(0.01f);
            Debug.Log($"startIndex: {startIndex}, count{coinPositionUpdates.Count}");
            if ((!movingCoin || carromBody.IsSleeping()) && (startIndex) < coinPositionUpdates.Count)
            {
                Debug.Log($"Starting coin movement: {movingCoin}");
                StopCoroutine(MoveCoinFromNetwork(reduceDuration));
                StartCoroutine(MoveCoinFromNetwork(reduceDuration));
            }
            yield return new WaitForSeconds(1f);
        }

        public void StopCoinMovement()
        {
            StopCoroutine(MoveCoinFromNetwork(true));
            startIndex = 0;
            coinPositionUpdates.Clear();
        }

        IEnumerator MoveCoinFromNetwork(bool reduceDuration)
        {
            Debug.Log($"Moving coin from index: {startIndex} to {coinPositionUpdates.Count}");
            bool collisionChecked = false;
            for (int k = startIndex; k < coinPositionUpdates.Count; k++)
            {
                movingCoin = true;
                float duration = (reduceDuration || k == 0) ? 0.05f : coinPositionUpdates[k].timeStamp - coinPositionUpdates[k - 1].timeStamp;

                reduceDuration = false;
                string go = name;
                //Debug.Log($"k {k}, Moving {go} from : {transform.position} to {coinPositionUpdates[k].position} in seconds: {duration}, has Collided: {coinPositionUpdates[k].hasCollided} with {coinPositionUpdates[k].collisionObject}");
                float t = 0;
                collisionChecked = false;
                Vector3 initPos = transform.position;
                //Debug.Log($"current Index:{k}, position Count:{strikerSlidePositions.Count}");
                while (t < duration)
                {
                    //Debug.Log($"Time:{Time.time}, Moving {go} in {duration-t}!!");
                    transform.position = Vector3.Lerp(initPos, coinPositionUpdates[k].position, t / duration);
                    t += Time.deltaTime;
                    if (Vector3.Distance(transform.position, coinPositionUpdates[k].position) < 0.1f && !collisionChecked)
                    {
                        collisionChecked = true;
                        CheckCollision(k);
                        //int upperLimit = (int)(coinPositionUpdates.Count * 0.7f);
                        //Debug.Log($"Reduce duration: {GameManager.Instance.GamePlayHandler.reduceUpdationTime}");
                        if (GameManager.Instance.GamePlayHandler.reduceUpdationTime /*&& k < upperLimit*/)
                            t = duration;
                    }
                        yield return new WaitForSeconds(0.0001f);
                }
                transform.position = coinPositionUpdates[k].position;
                if (!collisionChecked)
                    CheckCollision(k);
                startIndex = k + 1;                
            }
            movingCoin = false;
            Debug.Log($"Coin updated: new startIndex {startIndex}");
            yield return null;
        }

        void CheckCollision(int k)
        {
            if (coinPositionUpdates[k].hasCollided)
            {
                CarromMenHandler coin = GameManager.Instance.GamePlayHandler.coins.Find(c => c.gameObject.name == coinPositionUpdates[k].collisionObject);
                if (coin != null)
                {
                    StartCoroutine(coin.StartPositionSimulation(true));
                    Debug.Log($"Starting simulation for {coin.name} from {name}");
                }
                else if (coinPositionUpdates[k].collisionObject.Contains("Holes"))
                {
                    List<GameObject> holes = (coinPositionUpdates[k].collisionObject.Contains("AI")) ? GameManager.Instance.aiPockets : GameManager.Instance.carromPockets;
                    GameObject collObject = holes[0];
                    for (int l = 0; l < holes.Count; l++)
                    {
                        Debug.Log($"Pocket {l}: {holes[l].gameObject.name}");
                        if(coinPositionUpdates[k].collisionObject.Equals(holes[l].name))
                        {
                            collObject = holes[l];
                        }
                    }
                    coinTumble.OnPot(collObject, gameObject);
                    OnPot();
                    pocketed = true;
                    lastPocketPosition = collObject.transform;
                }
                else
                    Debug.Log($"{name} collided but could not find {coinPositionUpdates[k].collisionObject}");
            }
        }
    }
    public class CoinDetails
    {
        public string name;
        public Vector3 position;
        public float timeStamp;
        public bool hasCollided;
        public string collisionObject;
        public CoinDetails(string name, Vector3 coinPosition, float timeStamp, bool hasCollided, string collObj)
        {
            this.name = name;
            this.position = coinPosition;
            this.timeStamp = timeStamp;
            this.hasCollided = hasCollided;
            this.collisionObject = collObj;
        }
    }
}
