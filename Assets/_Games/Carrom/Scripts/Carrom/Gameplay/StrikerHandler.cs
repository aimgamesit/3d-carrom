﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using DG.Tweening;

namespace AIMCarrom
{
    public class StrikerHandler : MonoBehaviour
    {
        [SerializeField] private GameObject strikerValidHalo;
        [SerializeField] private GameObject strikerNotValidHalo;
        [SerializeField] private GameObject sprites;
        //[SerializeField] private CoinCollisionAudio coinCollisionAudio;
        [SerializeField] private SpriteRenderer strikerOnPot;
        [SerializeField] private SpriteRenderer strikerSpriteRenderer;
        [SerializeField] [Slider(0, 50)] private float velocityToPot = 6;
        [SerializeField] AudioSource StrikerMoveAudioSource;
        private CoinTumble coinTumble;
        private Rigidbody2D carromBody;
        public Rigidbody2D Body => carromBody;
        private TransformSync transformSync;
        //[SyncVar]
        private bool pocketed;
        //[SyncVar]
        private bool isDisabled;
        private bool isPlacementDone;
        private Vector3 lastStrikerPositionSent = Vector3.zero;
        [SerializeField] private CoinCollisionAudio coinCollisionAudio;
        [SerializeField] private Transform shadowTransform;

        public bool inShot { get; set; }
        public int strikerCount;
        public static System.Action onPocket;

        public List<StrikerPositionData> strikerSlidePositions;

        private void Awake()
        {
            coinTumble = GetComponent<CoinTumble>();
            carromBody = GetComponent<Rigidbody2D>();
            transformSync = GetComponent<TransformSync>();
            Physics.autoSimulation = false;
            Physics.autoSyncTransforms = true;
            Debug.Log("autoSyncTransforms " + Physics.autoSyncTransforms);
            //Commented as isHeadless is a parameter is NetworkManager.cs under Mirror
            //if (!NetworkManager.isHeadless)
            {
                var temp = transform.position;
                transform.SetParent(GameManager.Instance.GamePlayHandler.StrikerControls.strikerParent);
                transform.localPosition = temp;
                transform.localRotation = Quaternion.identity;
            }
        }

        public void ResetStriker()
        {
            //if (!MultiplayerManager.isOffline)
            {
                pocketed = false;
            }
            if (transformSync)
                transformSync.ResetSync();
            sprites.SetActive(true);
            strikerOnPot.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            sprites.SetActive(true);
            strikerOnPot.gameObject.SetActive(false);
            pocketed = false;
            coinTumble.onTumbleFinish += OnTumbleFinish;
            GameManager.Instance.GamePlayHandler.EventPucksPlacementDone += OnAllPucksPlaced;
            GameManager.Instance.GamePlayHandler.CarromMenPlacement.onPlaceObjectsDone += OnAllPucksPlaced;
        }

        private void OnDisable()
        {
            coinTumble.onTumbleFinish -= OnTumbleFinish;
            GameManager.Instance.GamePlayHandler.EventPucksPlacementDone -= OnAllPucksPlaced;
            GameManager.Instance.GamePlayHandler.CarromMenPlacement.onPlaceObjectsDone -= OnAllPucksPlaced;
        }

        float shadowMaxDistance = 2.5f;
        //[ClientCallback]
        private void FixedUpdate()
        {
            if (inShot)
            {
                Physics.Simulate(Time.fixedDeltaTime);
            }
            if (Body.IsSleeping() || isDisabled/*|| NetworkManager.isHeadless*/)
                return;
            // shadow movement

            Vector2 dir = transform.position - transform.parent.parent.position;
            float mag = Mathf.Clamp(dir.magnitude / shadowMaxDistance, 0, 1);
            shadowTransform.position = transform.position + (Vector3)dir.normalized * mag * 0.05f;
        }

        public IEnumerator SendStrikerPosition(int state = 3)
        {
            if (GameManager.Instance.IsLocalGame() || !GameManager.Instance.IsMyTurn)
                yield return null;

            strikerCount += 1;
            //Debug.Log($"striker Count:{strikerCount} sending at time {Time.time}");
            yield return new WaitForSeconds(0.1f);

            StopCoroutine(SendStrikerPosition());
            if (inShot)
            {
                StartCoroutine(SendStrikerPosition());
            }
        }

        private void OnTriggerStay2D(Collider2D coll)
        {
            if (!pocketed && coll.CompareTag(Constants.POCKET_TAG))
            {
                // Debug.Log($"Striker collided with: {coll.name}, carromBody.velocity.magnitude:{carromBody.velocity.magnitude}, velocityToPot:{velocityToPot}");
                if (carromBody.velocity.magnitude < velocityToPot)
                {
                    pocketed = true;
                    coinTumble.OnPot(coll.gameObject, gameObject);
                    //Debug.LogFormat($"striker velocity less than pot velocity: {coll.gameObject.name}, Tag: {coll.gameObject.tag}");
                    //OnPot();
                    List<GameObject> currentHoles = GameManager.Instance.carromStrikerPockets;
                    if (coll.gameObject.layer == 11)
                        currentHoles = GameManager.Instance.carromPockets;
                    else if (coll.gameObject.layer == 13)
                        currentHoles = GameManager.Instance.aiPockets;

                    //for (int i=0; i< currentHoles.Count; i++)
                    //{
                    //    Debug.Log($"i: {i}, name: {currentHoles[i].name}");
                    //}
                    RpcPot(carromBody.velocity.magnitude, currentHoles.IndexOf(coll.gameObject));
                }
            }
        }

        /*  "Collision happened on server callback"
         * [ServerCallback]*/
        private void OnCollisionEnter2D(Collision2D coll)
        {
            //Debug.Log($"Collision happened: {coll.gameObject.name}, with Tag: {coll.collider.tag}");
            //if (!isPlacementDone)
            //    return;
            if (GameManager.Instance.IsMyTurn && coll.collider.CompareTag(Constants.CARROM_MAN_TAG) || coll.collider.CompareTag(Constants.STRIKER_TAG) || coll.collider.CompareTag(Constants.CARROMBOARD_WALLS_TAG))
            {
                float maxVelocity = gameObject.CompareTag(Constants.STRIKER_TAG) ? Constants.MAX_STRIKER_VELOCITY_MAGNITUDE : Constants.MAX_CARROM_MEN_VELOCITY_MAGNITUDE;
                float volume = carromBody.velocity.magnitude / maxVelocity;
                var collisionAudio = coll.collider.GetComponent<CoinCollisionAudio>();
                if (collisionAudio != null)
                {
                    int myId = gameObject.GetInstanceID();
                    int otherObjId = collisionAudio.gameObject.GetInstanceID();

                    if (myId < otherObjId)
                    {
                        Debug.LogFormat("Played SOUND");
                        RpcPlayCollisionSound(volume * volume, coll.collider.gameObject.tag);
                        return;
                    }
                }
                RpcPlayCollisionSound(volume * volume, coll.collider.gameObject.tag);
            }
        }

        //[ClientRpc]
        public void RpcPot(float pow, int index)
        {
            Debug.Log($"Index: {index}");
            if (transformSync)
            {
                transformSync.isPot = true;
                transformSync.StopRigidbody();
            }
            var pocket = GameManager.Instance.carromStrikerPockets[index];
            //   coinTumble.OnPot(pocket, gameObject);

            //if (!GameManager.Instance.IsLocalGame())
            OnPot();

            float volume = pow / Constants.MAX_STRIKER_VELOCITY_MAGNITUDE;
            pocket.GetComponent<PocketDetector>().PlayPocketSound(volume);
        }

        private void OnTumbleFinish()
        {
            //gameObject.SetActive(false);
        }
        internal void OnPot()
        {
            sprites.SetActive(false);
            strikerOnPot.gameObject.SetActive(true);
            onPocket?.Invoke();
            //Uncomment if Photon added
            //Debug.LogFormat("IsServer : {0}", isServer);            
        }

        public void SetStrikerPositionValid(bool valid)
        {
            strikerValidHalo.SetActive(valid);
            strikerNotValidHalo.SetActive(!valid);
        }

        public void RemoveHalo()
        {
            strikerValidHalo.SetActive(false);
            strikerNotValidHalo.SetActive(false);
        }

        public void SetHaloSize(float size)
        {
            strikerValidHalo.transform.localScale = size * Vector3.one;
        }

        public void StopGame()
        {
            pocketed = false;
            coinTumble.StopTumble();
        }

        //[Client]
        public void SetStrikerGFX(int strikerIndex)
        {
            //Debug.Log($"# striker set graphics::{strikerIndex}");
            strikerSpriteRenderer.sprite = GameManager.Instance.UiManager.GetSprite(strikerIndex, true);
            strikerOnPot.sprite = strikerSpriteRenderer.sprite;
        }

        //[ClientRpc]
        void RpcPlayCollisionSound(float vol, string tag)
        {
            coinCollisionAudio.PlayCollisionSound(vol, tag);
        }

        private void OnAllPucksPlaced()
        {
            isPlacementDone = true;
        }

        public void PlayDragSound()
        {
            // float maxVol = 0.6f;
            //float currentVolume = maxVol;
            // DOTween.To(() => maxVol, x => currentVolume = x, 0, 0.5f).OnUpdate(() => { StrikerMoveAudioSource.volume = currentVolume; });
            // StrikerMoveAudioSource.volume = currentVolume;
        }
    }
}