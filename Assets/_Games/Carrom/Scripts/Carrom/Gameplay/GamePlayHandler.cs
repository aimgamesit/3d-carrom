﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace AIMCarrom
{
    [System.Serializable]
    public struct PlayerNetworkInfo
    {
        public string localName;
        public string imageURL;
        public int bet;
        public string playerID;
        public string userID;
        public int currentSelectedPuck;
        public int currentSelectedStriker;
        public bool isAI;
        public int winStreak;
    }

    public enum OpponentState
    {
        TurnHandling,
        MoveStarted,
        StrikerShot,
        Replay
    }

    public struct ConnectedPlayer
    {
        public int netID;
        public string uniqueID;
        public PlayerNetworkInfo playerObj;
    }

    public class SyncListPlayers : List<ConnectedPlayer> { }
    public class SyncListPlayerInfo : List<PlayerInfo> { }

    public class GamePlayHandler : MonoBehaviour //Initially Inheriting NetworkBehaviour
    {
        public List<int> emptyTurnsList = new List<int>() { 0, 0 }; //This List needs to be synced with players using Photon
        public SyncListPlayers connectedPlayers = new SyncListPlayers();
        public SyncListPlayerInfo playerInfo = new SyncListPlayerInfo();
        public event CoinMoveHandler<int, Vector3> OnCoinMove;
        public event CoinSleepHandler<int, Vector3> OnCoinSleep;
        public event CoinShotHandler<string> OnStartShot;
        public event CoinShotHandler<string> OnSaveEndStartReplay;
        public event CoinShotHandler<string> OnEndShot;

        public delegate void CoinShotHandler<String>(string impulse);
        public delegate void CoinMoveHandler<Int, Vector3>(int ballId, Vector3 position, Vector3 velocity, Vector3 angularVelocity);
        public delegate void CoinSleepHandler<Int, Vector3>(int ballId, Vector3 position);
        public delegate void GameStartDelegate();
        //[SyncEvent]
        public event GameStartDelegate EventGameStart;

        public delegate void SpectatorJoinedDelegate(int themeIndex);
        //[SyncEvent]
        public event SpectatorJoinedDelegate EventSpectatorJoined;

        public delegate void PucksPlacedDelegate();
        //[SyncEvent]   Use PhotonEvents in place of SyncEvent
        public event GameStartDelegate EventPucksPlacementDone;

        public delegate void TurnEndDelegate(int index);
        //[SyncEvent]
        public event TurnEndDelegate EventTurnEnd;

        public delegate void GameWinDelegate(int a, int pucksCount);
        //[SyncEvent]
        public event GameWinDelegate EventGameWin;

        public delegate void KickAllPlayerDelegate();
        //[SyncEvent]
        public event KickAllPlayerDelegate EventKickAllPlayer;

        public static Action onTurnEnd;

        [SerializeField] private int playerTimer;
        [SerializeField] private StrikerControls strikerControls;
        [SerializeField] private CarromMenPlacement carromMenPlacement;
        [SerializeField] private Transform carromMenParent;
        public List<CarromMenHandler> coins { get; private set; }
        public Transform clothSpace;
        public GameObject Striker => strikerControls.striker;
        public CarromMenPlacement CarromMenPlacement => carromMenPlacement;

        public GameManager gameManager;
        public StrikerControls StrikerControls => strikerControls;
        public PlayerInfo CurrentPlayer => playerInfo[GetCurrentIndex];

        public OpponentState opponentState;
        public bool HasGameStarted => (hasGameStarted);
        public float moveTime { get; set; }

        public float opponentTimeEventReceivedAt;
        public bool IsGameInSimulation => (isGameInSimulation);
        public int GetCurrentIndex => currentTurnIndex;
        public int GetCurrentUiIndex => GetUiIndex(currentTurnIndex);
        public int GetUiIndex(int index) => MyIndex == 0 ? index : 1 - index;
        public int MyIndex { get; set; }
        public static bool isOffline = true;
        public static bool isTournamentGame = true;
        private List<CarromMenHandler> pocketedCarromMen = new List<CarromMenHandler>();

        private Coroutine playerTimerCoroutine;
        private Coroutine checkForTurnSwitchCoroutine;
        private Coroutine startGame;
        private Coroutine checkForStartGame;
        private Coroutine findOppoenntCoroutine;
        private Coroutine gameplayTimerCoroutine;

        private float[] playerLeftTime = new float[2];
        private int maxPlayerLeftTime = 120;
        private int maxEmptyServerTime = 120;
        private int gameplayTime;
        private bool isFoul;
        private bool hasPocketed;
        internal bool oppoWaitScreenActive;
        internal bool changeTurnOnTimerEnd = true;
        public BotInfo[] botInfos;
        [SerializeField] TextAsset botInfoTextAsset;
        [SerializeField] bool practiceWithAI { get; set; }
        public bool PracticeWithAI
        {
            get
            {
                return practiceWithAI;
            }
            set
            {
                practiceWithAI = value;
            }
        }

        public int GameWonIndex => gameWonIndex;
        private int currentTurnIndex;
        private long betGoldAmount;
        private bool isQueenCovered;
        private bool hasGameStarted;
        internal bool resultDisplayed;
        private bool isGameInSimulation;
        private bool queenPocketedWithoutCover;
        private float playerStartTimer;
        private bool isPlayerTimerRunning;
        private int gameWonIndex;

        void Start()
        {
            botInfos = JsonUtility.FromJson<BotInfoArray>(botInfoTextAsset.text).infos;
            gameManager = GameManager.Instance;
        }

        public IEnumerator InitializeCoins()
        {
            Debug.Log($"InitializeCoins(): {carromMenParent.childCount}");
            yield return new WaitForSeconds(2f);
            coins = new List<CarromMenHandler>();
            for (int i = 0; i < carromMenParent.childCount; i++)
            {
                coins.Add(carromMenParent.GetChild(i).GetComponent<CarromMenHandler>());
            }
        }

        public List<string> coinsUpdated = new List<string>();
        public bool reduceUpdationTime;

        public IEnumerator SetOpponentState(OpponentState oppoState, bool addDelay = false)
        {
            if (addDelay)
                yield return new WaitForSeconds(2f);
            opponentState = oppoState;
            yield return null;
        }

        internal IEnumerator CheckIfCanChangeTurn()
        {
            for (int i = 0; i < coins.Count; i++)
            {
                while (coins[i].startIndex < coins[i].coinPositionUpdates.Count)
                {
                    if (coins[i].pocketed)
                    {
                        coins[i].StopCoinMovement();
                    }
                    else if (!coins[i].movingCoin)
                    {
                        StartCoroutine(coins[i].StartPositionSimulation(false));
                    }
                    //Debug.Log($"Waiting for {coins[i]}");
                    yield return new WaitForSeconds(0.001f);
                }
            }
            StrikerControls.strikerHandler.inShot = false;
            StartCoroutine(SetOpponentState(OpponentState.TurnHandling));
            //Debug.Log($"Striker InShot set to :{strikerHandler.inShot}");
        }

        bool CheckIsSleeping(bool forceSleep)
        {
            float minEnergy = 0.1f;
            bool isSleep = true;
            CarromMenHandler[] coins = carromMenParent.GetComponentsInChildren<CarromMenHandler>();
            foreach (CarromMenHandler coin in coins)
            {

                bool ballIsSleeping = (!Geometry.SphereInCube(coin.carromBody.position, coin.radius, clothSpace) || coin.carromBody.isKinematic || (coin.carromBody.velocity.magnitude < minEnergy && coin.radius * coin.carromBody.angularVelocity < minEnergy));
                if (!ballIsSleeping)
                {
                    isSleep = false;
                }
            }
            return isSleep;
        }

        public void PauseGamePlay(bool pause)
        {
            Debug.Log($"game play paused:{pause}");
            oppoWaitScreenActive = pause;
        }

        private IEnumerator StopMove()
        {
            StrikerControls.strikerHandler.inShot = false;
            yield return new WaitForSeconds(0.5f);
            Debug.Log($"Sending position with state 4!!");
            StartCoroutine(StrikerControls.strikerHandler.SendStrikerPosition(4));
            CarromMenHandler[] coins = carromMenParent.GetComponentsInChildren<CarromMenHandler>();
            foreach (CarromMenHandler coin in coins)
            {
                if (!coin.carromBody.isKinematic)
                {
                    coin.carromBody.velocity = Vector3.zero;
                    coin.carromBody.angularVelocity = 0f;
                    coin.carromBody.Sleep();
                }
                CallBallSleep(coin.carromMen.id, coin.carromBody.position);
            }
            moveTime = 0.0f;
        }

        public void CallBallSleep(int ballId, Vector3 position)
        {
            /*if (OnBallSleep != null)
            {
                OnBallSleep(ballId, position);
            }*/
        }
        private void OnEnable()
        {
            StrikerControls.onStrikerShoot += OnStrikerShoot;
            CarromMenHandler.onPocket += OnCarromMenPocket;
            StrikerHandler.onPocket += OnStrikerPocket;
            CarromMenPlacement.onPlaceObjectsDone += OnEventPucksPlacementDone;
        }

        private void OnDisable()
        {
            StrikerControls.onStrikerShoot -= OnStrikerShoot;
            CarromMenHandler.onPocket -= OnCarromMenPocket;
            StrikerHandler.onPocket -= OnStrikerPocket;
            CarromMenPlacement.onPlaceObjectsDone -= OnEventPucksPlacementDone;
        }

        public void Initialize(CarromMenHandler[] coins)
        {
            OnStartShot += (string data) =>
            {
                OnStartShot(data);
            };
            OnEndShot += (string data) =>
            {
                OnEndShot(data);
            };
            OnCoinMove += (int ballId, Vector3 position, Vector3 velocity, Vector3 angularVelocity) =>
            {
                if (coins[ballId].pocketed)
                {
                    //coins[ballId].OnState(BallState.MoveInPocket);
                }
                else
                {
                    //coins[ballId].OnState(BallState.Move);
                }
            };
            OnCoinSleep += (int ballId, Vector3 position) =>
            {
                //coins[ballId].OnState(BallState.EndMove);
            };
        }

        public void DisplayGameOverScreen(int winIndex, bool isPlayerLeft = false)
        {
            Debug.Log($"Display Game Over Screen called: {hasGameStarted} && resultDisplayed : {resultDisplayed}");
            if (hasGameStarted)
            {
                OnGamewin(winIndex, playerInfo[winIndex].pocketedCarromMen.Length);
            }
        }

        public void StartCarromGame()
        {
            if (!hasGameStarted)
            {
                if (gameManager.IsLocalGame())
                    StartOneVsOneGame();
            }
        }

        internal void StartOneVsOneGame(int turnId = 0)
        {
            Debug.Log($"<color=red>Starting One Vs One Game!!\n Connected Player Count: {connectedPlayers.Count}, hasGameStarted: {hasGameStarted}</color> , turnid {turnId} ");

            if (connectedPlayers.Count > 1 && !hasGameStarted)
            {
                StopCoroutineNull(startGame);
                startGame = StartCoroutine(StartGameCoroutine(turnId));
            }
            else
            {
                Debug.Log("No enough players to start game!");
            }
        }

        private const float minmumWaitForOtherPlayers = 8;
        private const float maximusWaitForOtherPlayers = 10;
        private const float defaultWaitForOtherPlayers = 9;

        internal IEnumerator FindOpponentTimerCoroutine()
        {
            Debug.Log("========================================== Waiting to Begin with AI ==========================================");
            //yield return new WaitForSeconds(GameManager.Instance.WaitForPlayerToJoinTime);
            gameManager.OpponentFound = false;
            //Debug.LogError($" IsRematch  = {gameManager.UiManager.IsRematch} , Players = {PhotonNetwork.PlayerList.Length} ");
            float waitTime = defaultWaitForOtherPlayers;
            if (minmumWaitForOtherPlayers < maximusWaitForOtherPlayers)
                waitTime = UnityEngine.Random.Range(minmumWaitForOtherPlayers, maximusWaitForOtherPlayers);
            yield return new WaitForSeconds(waitTime);
            MatchWithBot();
        }

        internal void MatchWithBot()
        {
            Debug.Log($"Opponent found: {gameManager.OpponentFound}, Connected players Count: {connectedPlayers.Count}");
            if (!gameManager.OpponentFound)
            {
                Debug.Log($"<color=red>Start MatchWithBot</color>");
                Debug.Log("========================================== Playing with AI ==========================================");
                StopFindingBot();
                StartBotGame();
            }
        }

        public void StopFindingBot()
        {
            gameManager.OpponentFound = true;
            StopCoroutine(FindOpponentTimerCoroutine());
            StopCoroutine(StartBotGameCoroutine());
            if (findOppoenntCoroutine != null)
                StopCoroutine(findOppoenntCoroutine);
        }

        public IEnumerator StartGameCoroutine(int turnId)
        {
            Debug.Log($"Starting StartGameCoroutine() :: Player Count: {connectedPlayers.Count} turn id: {turnId} ");
            playerInfo.Clear();
            Debug.LogFormat($"Player Count: {connectedPlayers.Count}");
            for (int i = 0; i < connectedPlayers.Count; i++)
            {
                Debug.LogFormat($"Connected Players: {connectedPlayers[i].playerObj.localName}");
            }
            playerInfo.Add(new PlayerInfo(connectedPlayers[0].playerObj, 0, PlayerInfo.ColorType.BLACK, connectedPlayers[0].uniqueID, new PocketedCarromMenData[] { }));
            playerInfo.Add(new PlayerInfo(connectedPlayers[1].playerObj, 1, PlayerInfo.ColorType.WHITE, connectedPlayers[1].uniqueID, new PocketedCarromMenData[] { }));

            Debug.LogFormat("<color=green>Player info 0 : {0}</color>", JsonUtility.ToJson(playerInfo[0].playerData));
            yield return new WaitForSeconds(.2f);

            StartGame(turnId);
        }

        public void StartBotGame()
        {
            Debug.Log("StartBotGame");
            StopCoroutineNull(startGame);
            StopCoroutineNull(findOppoenntCoroutine);
            startGame = StartCoroutine(StartBotGameCoroutine());
        }

        internal IEnumerator StartBotGameCoroutine()
        {
            Debug.Log("START BOT GAME COROUTINE");
            PracticeWithAI = true;
            playerInfo.Clear();
            connectedPlayers.Clear();
            PlayerNetworkInfo myDetails = new PlayerNetworkInfo { userID = Player.PlayerData.uniqueID, localName = Player.PlayerData.playerName, currentSelectedPuck = Player.PlayerData.currentCarromMen, currentSelectedStriker = Player.PlayerData.currentStriker, isAI = false };
            MyIndex = 0;
            int botPuck = UnityEngine.Random.Range(0, 6);
            if (myDetails.currentSelectedPuck == botPuck)
            {
                botPuck = (myDetails.currentSelectedPuck != 0) ? 0 : 1;
            }

            int botIndex = UnityEngine.Random.Range(0, botInfos.Length);
            var botNwInfo = new PlayerNetworkInfo { playerID = Constants._ID_FOR_BOT, userID = botInfos[botIndex].userName, localName = botInfos[botIndex].localName, currentSelectedPuck = botPuck, currentSelectedStriker = 0, isAI = true };

            PlayerInfo.ColorType myColor = (Player.PlayerData.currentCarromMen > 0) ? PlayerInfo.ColorType.WHITE : PlayerInfo.ColorType.BLACK;
            PlayerInfo.ColorType oppoColor = (myColor == PlayerInfo.ColorType.BLACK) ? PlayerInfo.ColorType.WHITE : PlayerInfo.ColorType.BLACK;

            connectedPlayers.Add(new ConnectedPlayer { playerObj = myDetails });
            connectedPlayers.Add(new ConnectedPlayer { playerObj = botNwInfo });
            playerInfo.Add(new PlayerInfo(myDetails, 0, myColor, connectedPlayers[0].uniqueID, new PocketedCarromMenData[] { })); // me
            playerInfo.Add(new PlayerInfo(botNwInfo, 1, oppoColor, Constants.BOT_UNIQUE_ID, new PocketedCarromMenData[] { })); // bot
            Debug.LogFormat("<color=cyan>Player info 0 : {0}</color>", JsonUtility.ToJson(playerInfo[0].playerData));

            gameManager.SetPlayerCamera(0);
            gameManager.OpponentFound = true;
            yield return new WaitForSeconds(.1f);

            StrikerControls.SetUpStriker();
            Debug.Log("PLAYING BOT GAME");
            gameManager.UiManager.OnOpponentFound(playerInfo);
        }

        private void OnPlayerInfoUpdate()
        {
            //if (playerInfo.Count > 1)
            gameManager.UiManager.UpdateScore();
            //gameManager.UiManager.UpdateScoreBoard();
        }

        private void Update()
        {
            if (hasGameStarted)
            {
                if (isPlayerTimerRunning)
                {
                    float time = (Time.time - playerStartTimer) / playerTimer;
                    gameManager.UiManager.UpdatePlayerTimer(GetCurrentUiIndex, Mathf.Lerp(1, 0, time));
                }
            }
        }

        public void ResetGamePlayParameters()
        {
            changeTurnOnTimerEnd = true;
            coinsUpdated.Clear();
            reduceUpdationTime = false;
            if (coins != null)
            {
                for (int i = 0; i < coins.Count; i++)
                {
                    coins[i].startIndex = 0;
                    coins[i].coinPositionUpdates.Clear();
                }
            }
        }

        public void StartGame(int turnIndex = 0)
        {
            Debug.LogFormat("--- START GAME CALLED ON SERVER ---");
            StopCoroutineNull(checkForStartGame);
            GameManager.Instance.SetPlayerCamera(MyIndex);
            StrikerControls.SetUpStriker();
            currentTurnIndex = turnIndex;
            hasGameStarted = true;
            resultDisplayed = false;
            CarromMenPlacement.PlaceObjects(UnityEngine.Random.Range(0, 359)); // UnityEngine.Random.Range(0, 359));
            StopCoroutineNull(playerTimerCoroutine);
            gameManager.SetCurrentGamePortPref();
            gameManager.aiPockets[0].transform.parent.gameObject.SetActive(GameManager.Instance.IsLocalGame());
            EventGameStart?.Invoke();
            StartCheckingGameplayTime();
            playerTimerCoroutine = StartCoroutine(PlayerTimer());
            emptyTurnsList = new List<int> { 0, 0 };
            GameManager.Instance.AudioManager.StopSfx();
            StrikerControls.OnStrikerReset(true);
        }

        public void OnGameStartEvent()
        {
            StartGameClient();
            OnTurnEndEvent(0);
        }

        private void StartGameClient()
        {
            if (GameManager.Instance.IsLocalGame())
                MyIndex = 0;
            else
                MyIndex = playerInfo.FindIndex(x => x.playerData.userID == Player.PlayerData.uniqueID);
            Debug.Log($"My Index:{MyIndex}");
            GameManager.Instance.SetPlayerCamera(MyIndex);
        }

        public void CallEventPucksPlacementDone()
        {
            EventPucksPlacementDone?.Invoke();
        }

        private void OnEventPucksPlacementDone()
        {
            Debug.Log($"On Events Pucks Placement Done Called!!");
            SetCurrentSelectedPuckForPlayers();
        }

        public GameObject FindGameObjectWithId(int coinId)
        {
            if (coinId >= 0)
            {
                CarromMenHandler[] carromMenArray = carromMenParent.GetComponentsInChildren<CarromMenHandler>();
                for (int i = 0; i < carromMenArray.Length; i++)
                {
                    if (carromMenArray[i].carromMen.id == coinId)
                        return carromMenArray[i].gameObject;
                }
            }
            else
            {
                return StrikerControls.striker.gameObject;
            }
            return null;
        }

        public void SetPucksGFX()
        {
            CarromMenHandler[] carromMenArray = carromMenParent.GetComponentsInChildren<CarromMenHandler>();
            Debug.LogFormat("Carrom Men in the list : {0}", carromMenArray.Length);
            int myIndex = 0;
            carromMenArray.ToList().ForEach(t => t.SetPuckGFX(myIndex));
        }

        public void SetCurrentSelectedPuckForPlayers()
        {
            Debug.Log($"SetCurrentSelectedPuckForPlayers()");
            var playersPucks = GameManager.Instance.playersPuckIndices;
            playersPucks[0] = playerInfo[0].playerData.currentSelectedPuck;
            playersPucks[1] = playerInfo[1].playerData.currentSelectedPuck;
            int oppIndex = GameManager.Instance.IsLocalGame() ? 1 : playerInfo.FindIndex(x => x.playerData.userID != Player.PlayerData.uniqueID);

            if (playersPucks[0] != playersPucks[1])
            {
                Debug.LogFormat("playersPucks[0] : {0}\t playersPucks[1] : {1}", GameManager.Instance.playersPuckIndices[0], GameManager.Instance.playersPuckIndices[1]);
                SetPucksGFX();
                GameManager.Instance.UiManager.UpdateScoreBoard();
                return;
            }

            if (playersPucks[0] == playersPucks[1])
            {
                if ((playersPucks[MyIndex] != (int)CarromMenColor.BLACK))
                    playersPucks[oppIndex] = (int)CarromMenColor.BLACK;
                else
                    playersPucks[oppIndex] = (int)CarromMenColor.WHITE;
            }
            GameManager.Instance.UiManager.UpdateScoreBoard();
            Debug.LogFormat("playersPucks[0] : {0}\t playersPucks[1] : {1}", GameManager.Instance.playersPuckIndices[0], GameManager.Instance.playersPuckIndices[1]);
        }

        private IEnumerator PlayerTimer()
        {
            playerStartTimer = Time.time;
            if (!GameManager.Instance.IsLocalGame() && !GameManager.Instance.IsMyTurn)
                yield return null;
            isPlayerTimerRunning = true;
            Debug.Log($"CurrentPlayer: {GetCurrentUiIndex}");
            yield return new WaitForSeconds(playerTimer);
            Debug.Log($"Timer over.Calling Change Turn: {changeTurnOnTimerEnd}, my turn: {GameManager.Instance.IsMyTurn}");
            OnTurnEnd(true);
        }

        private void OnStrikerShoot()
        {
            Debug.Log("OnStrikerShoot");
            GameManager.Instance.UiManager.SetTimerTickSound(false);
            StopCoroutineNull(playerTimerCoroutine);
            isPlayerTimerRunning = false;
        }

        public void CheckTurn()
        {
            isGameInSimulation = true;
            isPlayerTimerRunning = false;
            StopCoroutineNull(checkForTurnSwitchCoroutine);
            checkForTurnSwitchCoroutine = StartCoroutine(CheckForTurnSwitch());
        }

        private IEnumerator CheckForTurnSwitch()
        {
            yield return new WaitForSeconds(1);
        }

        private void OnStrikerPocket()
        {
            isFoul = true;
        }

        internal void OnCarromMenPocket(CarromMenHandler carromMen)
        {
            if (!queenPocketedWithoutCover && carromMen.carromMen.id == 0)
                queenPocketedWithoutCover = true;

            pocketedCarromMen.Add(carromMen);
        }

        private bool CheckForTurnEnd()
        {
            if (CarromMenPlacement?.carromMenList != null)
            {
                foreach (var item in CarromMenPlacement.carromMenList)
                {
                    if (!item.gameObject.GetComponent<CarromMenHandler>().isDisabled && (!item.gameObject.GetComponent<Rigidbody2D>().IsSleeping() || item.gameObject.GetComponent<CoinTumble>().isTumbling))
                        return false;
                }
            }
            return true;
        }

        public IEnumerator OnStrikerShooted()
        {
            float initialWaitTime = 0.2f;
            float waitStartTime = Time.time;
            float maxWaitTime = 3 - initialWaitTime;

            yield return new WaitForSeconds(initialWaitTime);
            bool doWaitForZeroMomentum = true;
            List<Rigidbody2D> pucksRigidbody = new List<Rigidbody2D>();
            Rigidbody2D strikerRigidbody2D = new Rigidbody2D();

            if (Striker)
                strikerRigidbody2D = Striker.GetComponent<Rigidbody2D>();

            if (coins != null)
            {
                foreach (CarromMenHandler puck in coins)
                    if (puck)
                        if (!puck.isDisabled)
                        {
                            Rigidbody2D rigidbody2D = puck.GetComponent<Rigidbody2D>();
                            if (rigidbody2D)
                                pucksRigidbody.Add(rigidbody2D);
                        }
            }
            //Wait untill 2.8 Seconds are passed and all the 
            while (doWaitForZeroMomentum && Time.time - waitStartTime <= maxWaitTime)
            {
                doWaitForZeroMomentum = false;
                foreach (Rigidbody2D rigid in pucksRigidbody)
                {
                    if (rigid)
                    {
                        if (!rigid.IsSleeping())
                        {
                            doWaitForZeroMomentum = true;
                            break;
                        }
                    }
                }
                if (!doWaitForZeroMomentum && strikerRigidbody2D)
                    if (strikerRigidbody2D.IsSleeping())
                        break;
                    else
                        doWaitForZeroMomentum = true;
                else
                    doWaitForZeroMomentum = true;
                yield return new WaitForSecondsRealtime(0.02f);
            }

            StrikerControls.strikerHandler.inShot = false;
            yield return new WaitForSeconds(0.5f);
            Debug.Log($"Sending position with state 4!!");
            StartCoroutine(StrikerControls.strikerHandler.SendStrikerPosition(4));
            if (GameManager.Instance.IsLocalGame())
                OnTurnEnd(false);
        }

        private void OnTurnEnd(bool isTimerEnd)
        {
            Debug.Log($"OnTurnEnd: {isTimerEnd}");
            isGameInSimulation = false;
            ValidatePocket();
            GameManager.Instance.UiManager.OnTurnEnd();
            StrikerControls.StopStrikerAim();
            onTurnEnd?.Invoke();
            if (!CheckWinningCondition())
            {
                if (isTimerEnd && !GameManager.Instance.IsLocalGame())
                {
                    IncreaseEmptyTurnsCount();
                }
                else
                    emptyTurnsList[GetCurrentIndex] = 0;
                if (resultDisplayed)
                    return;
                SwitchTurn();
                CarromMenPlacement.ResetCarromMen();
                EventTurnEnd?.Invoke(currentTurnIndex);
                StrikerControls.OnStrikerReset();
            }
        }

        int maxEmptyTurns = 4;

        void IncreaseEmptyTurnsCount()
        {
            emptyTurnsList[GetCurrentIndex] += 1;
            Debug.LogFormat("Incresed Consecutive Empty turn for player[{0}] to : {1}", GetCurrentIndex, emptyTurnsList[GetCurrentIndex]);

            int maxEmptyTurnReachedPlayer = emptyTurnsList.FindIndex(t => t >= maxEmptyTurns);
            Debug.Log("Empty Turns going on " + maxEmptyTurns);
            bool noOnePlaying = emptyTurnsList.All(t => t >= maxEmptyTurns);
            bool onePlayerPlaying = emptyTurnsList.Any(t => t == 0);

            if (maxEmptyTurnReachedPlayer >= 0 && noOnePlaying)
            {
                KickAll();
            }
            else if (maxEmptyTurnReachedPlayer >= 0 && onePlayerPlaying)
            {
                KickPlayer(maxEmptyTurnReachedPlayer);
            }
        }

        void KickPlayer(int id)
        {
            Debug.LogFormat("Empty : Game finished as player[{0}] is not playing anymore", id);
            int wonIndex = 1 - id;
            Debug.LogFormat("Empty : Player[{0}] wins!!!", wonIndex);

            OnGamewin(wonIndex, playerInfo[wonIndex].pocketedCarromMen.Length);
        }

        void KickAll()
        {
            Debug.LogFormat("Empty : Kick all. Finish game as no one is playing");
            EventKickAllPlayer?.Invoke();
        }

        internal void OnTurnEndEvent(int index)
        {
            currentTurnIndex = index;
            GameManager.Instance.UiManager.OnTurnEnd();
            StrikerControls.StopStrikerAim();
            StrikerControls.OnStrikerReset();
            GameManager.Instance.UiManager.UpdateScore();
        }

        public void SwitchTurn()
        {
            if ((!hasPocketed || isFoul) && !queenPocketedWithoutCover)
            {
                ++currentTurnIndex;
                currentTurnIndex %= 2;
            }
            Debug.Log($"New Turn Index:{currentTurnIndex}");
            isPlayerTimerRunning = false;
            isFoul = false;
            hasPocketed = false;
            StopCoroutineNull(playerTimerCoroutine);
            playerTimerCoroutine = StartCoroutine(PlayerTimer());
        }

        public void ValidatePocket()
        {
            bool isPenalty = false;
            if (isFoul)
            {
                if (pocketedCarromMen.Exists(x => (int)x.carromMen.colorType == (1 - GetCurrentIndex)))
                {
                    // Opponent's coins (if any) are awarded to him 
                    var opponentList = pocketedCarromMen.Where(x => (int)x.carromMen.colorType == (1 - GetCurrentIndex)).ToList();

                    foreach (var item in opponentList)
                    {
                        // Opponent's coins (if any) are removed from the list of coins that will return to the board. 
                        pocketedCarromMen.Remove(item);
                    }
                    if (opponentList.Count > 0)
                    {
                        if (!isQueenCovered && Constants.MAX_COINS_OF_EACH_COLOR - playerInfo[1 - GetCurrentIndex].pocketedCarromMen.Length <= opponentList.Count)
                        {
                            var opponentCoin = opponentList[0];
                            opponentList.RemoveAt(0);
                            carromMenPlacement.carromMenResetList.Add(opponentCoin);
                        }
                        AddListToPlayerPocketed(1 - GetCurrentIndex, opponentList);
                        //playerInfo[1 - GetCurrentIndex].pocketedCarromMen.AddRange(opponentList.Select(x => x.carromMen));
                    }

                }
                // Player's coin (if any) are returned to the board
                carromMenPlacement.carromMenResetList.AddRange(pocketedCarromMen);

                // Queen is returned to the board.
                // There is a chance that queen was pocketed in the previous turn and foul occurs in current turn.
                // So the pocketed carrom list doesn't include queen and we need to add it explicitly.
                //Debug.Log($"Is Foul && Queen Pocketed Without Cover: {queenPocketedWithoutCover}");
                if (queenPocketedWithoutCover)
                {
                    queenPocketedWithoutCover = false;
                    var queen = CarromMenPlacement.carromMenList.FirstOrDefault(x => x.carromMen.id == 0);
                    carromMenPlacement.AddCarromMenToResetList(queen);
                    queen.isDisabled = false;
                }

                pocketedCarromMen.Clear();
                isPenalty = true;
                //Debug.Log("Foul called by validate pocket");
            }

            else
            {
                if (pocketedCarromMen.Count > 0 || queenPocketedWithoutCover)
                {
                    var homeList = pocketedCarromMen.Where(x => (int)x.carromMen.colorType == GetCurrentIndex).ToList();
                    hasPocketed = homeList.Count > 0;

                    if (queenPocketedWithoutCover)
                    {
                        var queen = CarromMenPlacement.carromMenList.FirstOrDefault(x => x.carromMen.id == 0);
                        //Debug.Log("has pocketed: " + hasPocketed);

                        if (hasPocketed)
                        {
                            //Debug.Log("Queen Covered");
                            playerInfo[GetCurrentIndex] = new PlayerInfo(CurrentPlayer.playerData, CurrentPlayer.playerIndex, CurrentPlayer.playerColor, CurrentPlayer.playerID, CurrentPlayer.pocketedCarromMen, true);
                            isQueenCovered = true;
                            pocketedCarromMen.Remove(queen);

                        }
                        queenPocketedWithoutCover = pocketedCarromMen.Exists(x => x.carromMen.colorType == CarromMenColor.RED);
                        Debug.Log($"queenPocketedWithoutCover:{queenPocketedWithoutCover}, isQueenCovered:{isQueenCovered}");
                        if (!queenPocketedWithoutCover && !isQueenCovered)
                        {
                            carromMenPlacement.carromMenResetList.Add(queen);
                            queen.isDisabled = false;
                        }
                    }

                    else if (!isQueenCovered && Constants.MAX_COINS_OF_EACH_COLOR - CurrentPlayer.pocketedCarromMen.Length <= homeList.Count)
                    {
                        isPenalty = true;
                    }

                    var opponentList = pocketedCarromMen.Where(x => (int)x.carromMen.colorType == (1 - GetCurrentIndex)).ToList();

                    if (hasPocketed)
                    {
                        AddListToPlayerPocketed(GetCurrentIndex, homeList);
                    }

                    if (opponentList.Count > 0)
                    {
                        if (!isQueenCovered && Constants.MAX_COINS_OF_EACH_COLOR - playerInfo[1 - GetCurrentIndex].pocketedCarromMen.Length <= opponentList.Count)
                        {
                            var opponentCoin = opponentList[0];
                            opponentList.RemoveAt(0);
                            carromMenPlacement.carromMenResetList.Add(opponentCoin);
                            Debug.LogFormat("Opponent's last coin potted : {0}", carromMenPlacement.carromMenResetList.Count);
                        }
                        AddListToPlayerPocketed(1 - GetCurrentIndex, opponentList);
                    }
                    pocketedCarromMen.Clear();
                }
            }

            if (isPenalty && CurrentPlayer.pocketedCarromMen.Length > 0)
            {
                hasPocketed = false;
                Debug.Log("Penalty incurred.");
                var carromMen = CurrentPlayer.pocketedCarromMen[0].id;
                playerInfo[GetCurrentIndex] = new PlayerInfo(CurrentPlayer.playerData, CurrentPlayer.playerIndex, CurrentPlayer.playerColor, CurrentPlayer.playerID, CurrentPlayer.pocketedCarromMen.Where(m => m.id != carromMen).ToArray(), CurrentPlayer.hasQueen);
                //Commented for testing ResetPuck // Uncomment below lines for normal functionality
                var puck = CarromMenPlacement.carromMenList.FirstOrDefault(x => x.carromMen.id == carromMen);
                //carromMenPlacement.carromMenResetList.Add(puck);
                carromMenPlacement.AddCarromMenToResetList(puck);
            }
        }

        private void AddListToPlayerPocketed(int index, List<CarromMenHandler> opponentList)
        {
            Debug.Log($"Added coins to pocketed coins : {opponentList.Count} at index: {index}");
            var count = playerInfo[index].pocketedCarromMen.Length;
            var list = playerInfo[index].pocketedCarromMen;
            Array.Resize(ref list, playerInfo[index].pocketedCarromMen.Length + opponentList.Count);
            playerInfo[index] = new PlayerInfo(playerInfo[index].playerData, playerInfo[index].playerIndex, playerInfo[index].playerColor, playerInfo[index].playerID, list, playerInfo[index].hasQueen);
            for (int i = count; i < playerInfo[index].pocketedCarromMen.Length; ++i)
            {
                playerInfo[index].pocketedCarromMen[i] = new PocketedCarromMenData { id = opponentList[i - count].carromMen.id, turnIndex = GetCurrentIndex };
            }
            OnPlayerInfoUpdate();
        }

        private bool CheckWinningCondition()
        {
            Debug.Log("CheckWinningCondition()");
            bool won = false;
            // Both have 9 pockets each then the player with queen wins 
            if (playerInfo[0].pocketedCarromMen.Length == Constants.MAX_COINS_OF_EACH_COLOR && playerInfo[1].pocketedCarromMen.Length == Constants.MAX_COINS_OF_EACH_COLOR)
            {
                int wonIndex = playerInfo.FindIndex(x => x.hasQueen);
                OnGamewin(wonIndex, playerInfo[wonIndex].pocketedCarromMen.Length);
                won = true;
            }
            // Player 0 has 9 pockets 
            else if (playerInfo[0].pocketedCarromMen.Length >= Constants.MAX_COINS_OF_EACH_COLOR)
            {
                // Player 0 wins
                int queenIndex = playerInfo.FindIndex(x => x.hasQueen);
                Debug.Log($"Player 0 has max coins: {playerInfo[0].pocketedCarromMen.Length}, queen Index: {queenIndex}");

                OnGamewin(0, playerInfo[0].pocketedCarromMen.Length);
                won = true;

            }
            // Player 1 has 9 pockets
            else if (playerInfo[1].pocketedCarromMen.Length >= Constants.MAX_COINS_OF_EACH_COLOR)
            {
                // Player 1 wins
                int queenIndex = playerInfo.FindIndex(x => x.hasQueen);
                Debug.Log($"Player 1 has max coins: {playerInfo[1].pocketedCarromMen.Length}, queen Index: {queenIndex}");
                OnGamewin(1, playerInfo[1].pocketedCarromMen.Length);
                won = true;

            }
            else if ((playerLeftTime[0] > 0 && playerLeftTime[0] > maxPlayerLeftTime) || (playerLeftTime[1] > 0 && playerLeftTime[1] > maxPlayerLeftTime))
            {
                Debug.LogFormat("Players left time : {0}\t{1}", playerLeftTime[0], playerLeftTime[1]);
                if (playerLeftTime[0] > 0 && playerLeftTime[1] > 0)
                {
                    Debug.LogFormat("both player playerLeftTime");
                    GameManager.Instance.GameCompleted(playerInfo, 0, gameplayTime, false);
                    won = true;
                }
                else if (playerLeftTime[1] <= 0)
                {
                    Debug.LogFormat("player 0 playerLeftTime");
                    OnGamewin(1, playerInfo[1].pocketedCarromMen.Length);
                    won = true;
                }
                else
                {
                    Debug.LogFormat("player 1 playerLeftTime");
                    OnGamewin(0, playerInfo[0].pocketedCarromMen.Length);
                    won = true;
                }
            }
            return won;
        }

        private void OnGamewin(int index, int winPucksCount)
        {
            gameWonIndex = index;
            oppoWaitScreenActive = false;
            OnGameWinEvent(index);
            Debug.LogError("OnGameWin()||");
        }

        private void OnGameWinEvent(int index)
        {
            Debug.LogFormat($"OnGameWinEvent :: resultDisplayed:{resultDisplayed}");
            if (!resultDisplayed)
            {
                resultDisplayed = true;
                isPlayerTimerRunning = false;
                StopCheckingGameplayTime();
                GameManager.Instance.PlayerGameWon(playerInfo.ToList(), index);
            }
        }

        public void ResetGame()
        {
            Debug.Log("RESET GAME");
            gameManager.UiManager.OnTurnEnd();
            StopCoroutine(CheckForTurnSwitch());
            StopCoroutine(PlayerTimer());
            isFoul = false;
            hasPocketed = false;
            isQueenCovered = false;
            isGameInSimulation = false;
            queenPocketedWithoutCover = false;
            isPlayerTimerRunning = false;
            StartCoroutine(StrikerControls.StopGame(hasGameStarted));
            hasGameStarted = false;
            StartCoroutine(CarromMenPlacement.StopGame());
            practiceWithAI = false;
        }

        private void StopCoroutineNull(Coroutine coroutine)
        {
            if (coroutine != null)
                StopCoroutine(coroutine);
        }

        public void StartCheckingGameplayTime()
        {
            if (gameplayTimerCoroutine != null)
                StopCoroutine(gameplayTimerCoroutine);

            gameplayTime = 0;
            gameplayTimerCoroutine = StartCoroutine(CheckGameplayTime());
        }

        public void StopCheckingGameplayTime()
        {
            if (gameplayTimerCoroutine != null)
                StopCoroutine(gameplayTimerCoroutine);
            gameplayTime = 0;
        }

        IEnumerator CheckGameplayTime()
        {
            int timerUpdateInterval = 1;
            int timeElapsed = 0;
            while (true)
            {
                timeElapsed += timerUpdateInterval;
                gameplayTime = timeElapsed;

                yield return new WaitForSeconds(timerUpdateInterval);
            }
        }
    }
}