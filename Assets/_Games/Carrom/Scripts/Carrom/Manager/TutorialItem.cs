using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TutorialItem : MonoBehaviour
{
    public GameObject obj;
    public Text messageText;
    public Button closeBtn;

    public void HidePanel(Action action)
    {
        obj.transform.DOScale(Vector3.zero, GameData.Instance.popupScaleTime).OnComplete(delegate () {
            action?.Invoke();
            obj.SetActive(true);
        });
    }

    public void ShowPanel(string msgText, Action action = null)
    {
        obj.transform.localScale = Vector3.zero;
        obj.SetActive(true);
        messageText.text = msgText;
        obj.transform.DOScale(Vector3.one, GameData.Instance.popupScaleTime).OnComplete(delegate () {
            action?.Invoke();
        });
    }
}