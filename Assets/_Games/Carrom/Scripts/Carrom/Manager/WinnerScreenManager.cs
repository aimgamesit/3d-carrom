﻿using AIMCarrom;
using GoogleMobileAds.Api;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinnerScreenManager : PopupBaseManager
{
    public Text[] namesText;
    public Image[] profilePics;
    public GameObject[] winnerObjs;
    public GameObject[] queenObjs;
    public Text[] scoreText;
    public GameObject[] queenUI;
    public Text[] queenAmtText;
    public Text prizeMoneyText;

    void Awake()
    {
        HideScreen(false);
    }

    void OnEnable()
    {
    }

    public void ShowWinnerScreen(List<PlayerInfo> playerList, int winIndex, long prizeAmt, int opponentAvatarIndex)
    {
        ShowScreen(true, false);
        if (winIndex == 0)
        {
            Player.PlayerData.TotalCoins += prizeAmt / 1;
            if (playerList[0].hasQueen)
                Player.PlayerData.TotalCoins += GameData.Instance.queenCoins;
            Player.SaveDataToPrefs();
        }
        profilePics[0].sprite = GameData.Instance.avatars[Player.PlayerData.currentAvatar].sprite;
        profilePics[1].sprite = GameData.Instance.avatars[opponentAvatarIndex].sprite;
        for (int i = 0; i < winnerObjs.Length; i++)
        {
            winnerObjs[i].SetActive(false);
            queenObjs[i].SetActive(playerList[i].hasQueen);
            scoreText[i].text = playerList[i].pocketedCarromMen.Length.ToString();
            namesText[i].text = playerList[i].playerData.localName;
            queenUI[i].SetActive(playerList[i].hasQueen && winIndex == i);
            if(playerList[i].hasQueen && winIndex == i)
                queenAmtText[i].text = "+"+GameData.Instance.queenCoins.ToString();
        }
        winnerObjs[winIndex].SetActive(true);
        prizeMoneyText.text = GameData.Instance.themeData[GameData.Instance.selectedThemeIndex].themePrize.ToString();
        UiManager.Instance.UpdateUserDetails();
    }

    public void OnHomeBtnClicked()
    {
        GameManager.Instance.AudioManager.PlaySfx(SfxClips.ButtonClick);
        AdmobAdsManager._AdClosed += delegate () {
            SceneManager.LoadScene(GameData.Instance.MENU_SCENE);
            FirebaseManager.Instance.SendEvent(Constants.CARROM_GO_TO_HOME);
        };
        AdmobAdsManager.Instance.ShowInterstitial();
    }

    public void OnRematchBtnClicked()
    {
        GameManager.Instance.AudioManager.PlaySfx(SfxClips.ButtonClick);
        if (Player.PlayerData.TotalCoins < GameData.Instance.themeData[UiManager.Instance.currentSelectedTheme].themeEntryFee)
        {
            UiManager.Instance. popupManager.ShowPopup(true, Player.PlayerData.TotalCoins, PopupManager.PopupType.YES_NO, Constants.POPUP_BUY_ITEM_FAILED_TITLE, Constants.POPUP_NOT_ENOUGH_COINS_GAME_MESSAGE, "Watch now", "Home", "HOME", "", delegate (bool callback, string inputText)
            {
                if (callback)
                    OnWatchVideoBtnCLicked();
                else
                    OnHomeBtnClicked();
            });
            return;
        }
        AdmobAdsManager._AdClosed += delegate () {
            SceneManager.LoadScene(GameData.Instance.CARROM_GAME_SCENE);
        };
        AdmobAdsManager.Instance.ShowInterstitial();
        FirebaseManager.Instance.SendEvent(Constants.CARROM_REMATCH);
    }

    private void OnWatchVideoBtnCLicked()
    {
        bool isRewardReceived = false;
        Debug.Log("OnWatchVideoBtnCLicked()");
        if (AdmobAdsManager.Instance.IsVideoAdAvailable())
        {
            AdmobAdsManager._OnRewardedReceived += delegate ()
            {
                Debug.Log("isRewardReceived  TERU: " + isRewardReceived);
                if (!isRewardReceived)
                {
                    isRewardReceived = true;
                    Player.PlayerData.TotalCoins += GameData.Instance.freeReward;
                    Player.SaveDataToPrefs();
                    UiManager.Instance.UpdateUserDetails();
                    OnRematchBtnClicked();
                }
            };
            AdmobAdsManager._AdClosed += delegate () {
                Debug.Log("isRewardReceived  FALSE: " + isRewardReceived);
                if (!isRewardReceived)
                {
                    isRewardReceived = true;
                }
            };
            AdmobAdsManager.Instance.ShowInterstitial();
        }
        else
        {
            Invoke(nameof(ShowNoAdsPopup), .5f);     
        }
    }

    private void ShowNoAdsPopup()
    {
        UiManager.Instance.popupManager.ShowPopup(false, Player.PlayerData.TotalCoins, PopupManager.PopupType.OK, Constants.POPUP_NO_ADS_TITLE, Constants.POPUP_REWARD_NO_ADS_AVAILABLE, "", "", "OK", Player.PlayerData.playerName, delegate (bool callback, string inputText) { });
    }

    public void OnCLoseBtnClicked()
    {
        GameManager.Instance.AudioManager.PlaySfx(SfxClips.ButtonClick);
        HideScreen(true);
    }
}