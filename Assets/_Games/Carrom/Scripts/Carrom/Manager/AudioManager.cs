﻿using UnityEngine;
using UnityEngine.Audio;

namespace AIMCarrom
{
    public enum SfxClips
    {
        collide,
        pot,
        foul,
        win,
        lose,
        chatPopup,
        searchOpponent,
        opponentFound,
        Winner,
        ButtonClick,
        Looser,
    }

    public enum MusicClips
    {
        gamePlay,
        mainMenu,
        resultScreen
    }

    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private AudioSource sfxAudioSource;
        [SerializeField] private AudioSource musicAudioSource;

        [SerializeField] private AudioClip[] sfxClips;
        [SerializeField] private AudioClip[] musicClips;

        #region Audio Controls
        [SerializeField] private AudioMixer masterMixer;

        private int minVolume = -80;
        private int maxvolume = 1;

        #endregion

        private void Start()
        {
            masterMixer.SetFloat(Constants.VOLUME_MUSIC, maxvolume);
            masterMixer.SetFloat(Constants.VOLUME_SFX, maxvolume);
        }

        public void SetSoundVolume(bool isMute)
        {
            int value = isMute ? minVolume : maxvolume;
            Debug.Log(isMute + " ismute " + value);
            PlayerPrefs.SetInt(Constants.VOLUME_MUSIC, value);
            PlayerPrefs.SetInt(Constants.VOLUME_SFX, value);
            PlayerPrefs.Save();
            masterMixer.SetFloat(Constants.VOLUME_MUSIC, value);
            masterMixer.SetFloat(Constants.VOLUME_SFX, value);
        }

        public bool IsMuted()
        {
            Debug.Log("IsMuted() ===============> " + PlayerPrefs.GetInt(Constants.VOLUME_MUSIC, maxvolume));
            return PlayerPrefs.GetInt(Constants.VOLUME_MUSIC, maxvolume) == minVolume;
        }

        public void PlaySfx(SfxClips sfxClip, bool inLoop = false)
        {
            Debug.Log("PlaySfx:: "+ IsMuted());
            if (IsMuted())
                return;
            Debug.Log("PlaySfx:: ===================");
            if (sfxAudioSource.enabled)
            {
                if (sfxAudioSource.isPlaying)
                {
                    StopSfx();
                }
                sfxAudioSource.clip = sfxClips[(int)sfxClip];
                sfxAudioSource.loop = inLoop;
                sfxAudioSource.Play();
            }
        }

        public void PlayMusic(MusicClips musicClip)
        {
            if (musicAudioSource.enabled)
            {
                musicAudioSource.clip = musicClips[(int)musicClip];
                musicAudioSource.Play();
            }
        }

        public void EnableDisableVfx(bool state)
        {
            sfxAudioSource.enabled = state;
        }

        public void EnableDisableMusic(bool state)
        {
            musicAudioSource.enabled = state;
        }

        public void StopSfx()
        {
            if (sfxAudioSource.enabled)
            {
                if (sfxAudioSource.isPlaying)
                {
                    sfxAudioSource.Stop();
                }
            }
        }
        //public void ControlMusic(float val)
        //{
        //    float mixerVal = minVolume + (maxvolume - minVolume) * val;
        //    masterMixer.SetFloat(Constants.VOLUME_MUSIC, 100);// mixerVal);
        //    PlayerPrefs.SetFloat(Constants.VOLUME_MUSIC, 100);
        //}

        //public void ControlSFX(float val)
        //{
        //    float mixerVal = minVolume + (maxvolume - minVolume) * val;
        //    masterMixer.SetFloat(Constants.VOLUME_SFX, 100);//mixerVal);
        //    PlayerPrefs.SetFloat(Constants.VOLUME_SFX, 100);
        //}
    }
}
