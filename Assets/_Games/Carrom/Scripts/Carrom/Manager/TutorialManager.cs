using AIMCarrom;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public TutorialItem[] messages;

    readonly string[] testMsg =  { "Tap here to change <b><color=green>Profile Pic</color></b>",
                                "Tap here to change Carrom <b><color=green>Pucks, Strikers</color></b>"};
    public int tutorialCount = 2;
    private byte counter = 0;

    void Start()
    {
        Debug.Log("START________TUT: "+ PlayerPrefs.GetInt(Constants.CARROM_MENU_TUTORIAL, 0));
        if (PlayerPrefs.GetInt(Constants.CARROM_MENU_TUTORIAL, 0) < tutorialCount)
        {
            messages[counter].ShowPanel(testMsg[counter]);
            for (int i = 0; i < messages.Length; i++)
            {
                messages[i].closeBtn.onClick.AddListener(delegate () { PlayNextTutorial(); });
            }
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
 
    public void PlayNextTutorial()
    {
        Debug.Log(counter +"  ::  "+tutorialCount +" :START________TUT: " + PlayerPrefs.GetInt(Constants.CARROM_MENU_TUTORIAL, 0));
        if (PlayerPrefs.GetInt(Constants.CARROM_MENU_TUTORIAL, 0) >= tutorialCount)
            return;
        if (counter >= messages.Length)
        {
            EndTutorial();
            return;
        }

        messages[counter].HidePanel(delegate () {
            counter++;
            if (counter < messages.Length)
            {
                messages[counter].ShowPanel(testMsg[counter]);
            }
            else
            {
                EndTutorial();
            }
        });
    }

    private void EndTutorial()
    {
            gameObject.SetActive(false);
            PlayerPrefs.SetInt(Constants.CARROM_MENU_TUTORIAL, PlayerPrefs.GetInt(Constants.CARROM_MENU_TUTORIAL, 0) + 1);
    }
}