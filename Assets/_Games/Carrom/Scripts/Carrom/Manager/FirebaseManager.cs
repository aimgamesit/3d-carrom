using com.TicTok.managers;
using Firebase.Analytics;
using UnityEngine;

public class FirebaseManager : BManager<FirebaseManager>
{
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void SendEvent(string eventName)
    {
#if !UNITY_EDITOR
        Debug.Log("Send Firebase Event: " + eventName);
        FirebaseAnalytics.LogEvent(eventName);
#endif
    }
}