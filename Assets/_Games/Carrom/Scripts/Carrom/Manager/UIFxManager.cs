﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace AIMCarrom
{
    public class UIFxManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] coins;
        [SerializeField] private GameObject[] playerHUDs;
        [SerializeField] private GameObject hudPanel;
        [SerializeField] private RectTransform masterCanvas;
        [SerializeField] private float speedMultiplier = 500;

        public void StartLerpPotCoin(CarromMenColor coin, Sprite coinSprite, Vector3 initialPos, int playerIndex)
        {
            //Vector2 screenPoint = Camera.main.WorldToViewportPoint(initialPos);
            var point = WorldToCanvasPosition(masterCanvas, initialPos);
            //initialPos = screenPoint - masterCanvas.sizeDelta / 2f;

            RectTransform rectTransform = Instantiate(coins[(int)coin], hudPanel.transform).GetComponent<RectTransform>();
            rectTransform.GetComponent<Image>().sprite = coinSprite;
            StartCoroutine(LerpPotCoin(rectTransform, point, hudPanel.transform.InverseTransformPoint( playerHUDs[GameManager.Instance.GamePlayHandler.GetUiIndex(playerIndex)].transform.position)));
        }

        IEnumerator LerpPotCoin(RectTransform rectTransform, Vector3 initialPos, Vector3 finalPos)
        {
            //Debug.Log("intialPos " + initialPos);
            rectTransform.anchoredPosition = initialPos;

             float time = 0;
             while (time < 1)
             {
                 yield return null;

                rectTransform.anchoredPosition = Vector3.Lerp(initialPos, finalPos, time);
                 time += Time.deltaTime / (finalPos - initialPos).magnitude * speedMultiplier;
             }

             Destroy(rectTransform.gameObject);
        }

        private Vector2 WorldToCanvasPosition(RectTransform canvas, Vector3 position)
        {
            //Vector position (percentage from 0 to 1) considering camera size.
            //For example (0,0) is lower left, middle is (0.5,0.5)
            Vector2 temp = Camera.main.WorldToViewportPoint(position);

            //Calculate position considering our percentage, using our canvas size
            //So if canvas size is (1100,500), and percentage is (0.5,0.5), current value will be (550,250)
            temp.x *= canvas.sizeDelta.x;
            temp.y *= canvas.sizeDelta.y;

            //The result is ready, but, this result is correct if canvas recttransform pivot is 0,0 - left lower corner.
            //But in reality its middle (0.5,0.5) by default, so we remove the amount considering cavnas rectransform pivot.
            //We could multiply with constant 0.5, but we will actually read the value, so if custom rect transform is passed(with custom pivot) , 
            //returned value will still be correct.

            temp.x -= canvas.sizeDelta.x * canvas.pivot.x;
            temp.y -= canvas.sizeDelta.y * canvas.pivot.y;

            return temp;
        }

        public void ShowCoinAnimation(UITransformAnimationData animationData, System.Action onCoinAnimationDone = null)
        {
            Debug.LogFormat("Coin trail animation started");
            if (animationData.transformAniamtionCoroutine != null)
                StopCoroutine(animationData.transformAniamtionCoroutine);
            animationData.transformAniamtionCoroutine = StartCoroutine(CoinAnimationCoroutine(animationData, onCoinAnimationDone));
        }

        IEnumerator CoinAnimationCoroutine(UITransformAnimationData animationData, System.Action onCoinAnimationDone)
        {
            float timeElapsed = 0;
            while (timeElapsed < animationData.animationTime)
            {
                GameObject coinTemp = Instantiate(animationData.transformInfo.transform_.gameObject, animationData.animationParent);

                float maxOffset = animationData.transformInfo.maxPositionOffset;

                float rngX = System.Math.Abs(maxOffset) > Mathf.Epsilon ? Random.Range(-maxOffset, maxOffset) : 0;
                float rngY = System.Math.Abs(maxOffset) > Mathf.Epsilon ? Random.Range(-maxOffset, maxOffset) : 0;

                coinTemp.transform.position = new Vector3(animationData.transformInfo.fromTransform.position.x + rngX, animationData.transformInfo.fromTransform.position.y + rngY, animationData.transformInfo.fromTransform.position.z);
                Vector3 rngToPosition = new Vector3(animationData.transformInfo.toTransform.position.x + rngX, animationData.transformInfo.toTransform.position.y + rngY, animationData.transformInfo.toTransform.position.z);

                var transformInfo = new LerpTransformInfo(animationData.transformInfo);
                transformInfo.transform_ = coinTemp.transform;
                //transformInfo.toTransform.position = rngToPosition;

                LerpUITransform(transformInfo);

                timeElapsed += animationData.spawnDelta;
                yield return new WaitForSeconds(animationData.spawnDelta);
            }

            yield return new WaitForSeconds(1f);
            onCoinAnimationDone?.Invoke();

            if (animationData.transformAniamtionCoroutine != null)
                StopCoroutine(animationData.transformAniamtionCoroutine);
        }

        private void LerpUITransform(LerpTransformInfo info)
        {
            info.transform_.GetComponent<RectTransform>().DOMoveX(info.toTransform.position.x, info.lerpTime * info.xLerpFactor).SetEase(info.easeX).OnComplete(() => Destroy(info.transform_.gameObject));
            info.transform_.GetComponent<RectTransform>().DOMoveY(info.toTransform.position.y, info.lerpTime * info.yLerpFactor).SetEase(info.easeY);
            if (info.isRotated)
                info.transform_.DORotate(new Vector3(0, 0, 180), info.lerpTime);

            Image img = info.transform_.GetComponent<Image>();
            int fadeFactor = 8;
            img.DOFade(1, info.lerpTime / fadeFactor).SetEase(Ease.Linear).OnComplete(() => img.DOFade(0, info.lerpTime * (fadeFactor - 1) / fadeFactor).SetEase(info.fadeOutCurve));
        }
    }

    [System.Serializable]
    public struct LerpTransformInfo
    {
        public Transform transform_;
        public Transform fromTransform;
        public Transform toTransform;
        public AnimationCurve fadeOutCurve;
        [Range(2, 10)] public float lerpTime;
        public float maxPositionOffset;
        [Range(0, 1)] public float xLerpFactor;
        [Range(0, 1)] public float yLerpFactor;
        public Ease easeX;
        public Ease easeY; // Ease.OutQuart for faster side
        public bool isRotated;

        public LerpTransformInfo(LerpTransformInfo data)
        {
            transform_ = data.transform_;
            fromTransform = data.fromTransform;
            toTransform = data.toTransform;
            fadeOutCurve = data.fadeOutCurve;
            lerpTime = data.lerpTime;
            maxPositionOffset = data.maxPositionOffset;
            xLerpFactor = data.xLerpFactor;
            yLerpFactor = data.yLerpFactor;
            easeX = data.easeX;
            easeY = data.easeY;
            isRotated = data.isRotated;
        }
    }

    [System.Serializable]
    public struct UITransformAnimationData
    {
        public float animationTime;
        public float spawnDelta;
        public Transform animationParent;
        public Coroutine transformAniamtionCoroutine;
        public LerpTransformInfo transformInfo;
    }
}
