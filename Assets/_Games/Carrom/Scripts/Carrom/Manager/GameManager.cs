﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

namespace AIMCarrom
{
    public class GameManager : MonoBehaviour
    {
        public bool isLocal;
        public PlayerCommands localPlayer;
        public List<GameObject> carromPockets;
        public List<GameObject> carromStrikerPockets;
        public List<GameObject> aiPockets;
        public GameObject playerCamera;
        public int[] playersPuckIndices = new int[2];
        public bool OpponentFound;
        [SerializeField] private UiManager uiManager;
        [SerializeField] private UIFxManager uiFxManager;
        [SerializeField] private AudioManager audioManager;
        [SerializeField] private GamePlayHandler gamePlayHandler;
        #region Properties
        public UiManager UiManager => uiManager;
        public UIFxManager UIFxManager => uiFxManager;
        public AudioManager AudioManager => audioManager;
        public GamePlayHandler GamePlayHandler => gamePlayHandler;
        //private CarromGameManager carromGameManager;
        //public DissonanceComms DissonanceComms => trigger;
        public bool HasGameStarted => gamePlayHandler.HasGameStarted;
        //public bool IsPlayerTimerRunning => gamePlayHandler.isPlayerTimerRunning;
        public bool IsGameInSimulation => gamePlayHandler.IsGameInSimulation;
        public int GetCurrentIndex => gamePlayHandler.GetCurrentIndex;
        public PlayerInfo CurrentPlayer => gamePlayHandler.CurrentPlayer;
        public StrikerAccuracyInfo StrikerAccuracyInfo => strikerAccuracyInfo;

        public int AdFrequency { get; private set; }

        public event Action<bool> onIsTournamentSet;
        public bool isPlayerInGame;
        public bool isThirdPlayer;

        public bool IsMyTurn
        {
            get { return GetCurrentIndex == gamePlayHandler.MyIndex; }
        }
        #endregion

        public User TempUser { get; private set; }
        private StrikerAccuracyInfo strikerAccuracyInfo;
        public static GameManager Instance { private set; get; }

        private void Awake()
        {
            if (!Instance)
                Instance = this;
            else
                Destroy(gameObject);

            InitGameSettings();
        }

        public bool IsLocalGame()
        {
            if (isLocal || GamePlayHandler.PracticeWithAI) return true;
            else
                return false;
        }

        private void InitGameSettings()
        {
           // Application.targetFrameRate = 60;
        }

        private void SetSleepTime(int timeoutTime)
        {
            Debug.LogFormat("<color=magenta>Sleep time out set to : </color><color=white>{0}</color>", timeoutTime);
            Screen.sleepTimeout = timeoutTime;
        }

        //Telepathy.Client clientOnline;
        void Start()
        {
            Debug.LogFormat("<color=cyan>Application Version : </color>{0}", Application.version);
        }

        Coroutine startFriendGame;

        public IEnumerator JoinOfflineCoroutine()
        {
            yield return new WaitUntil(() => GamePlayHandler != null);
            StartCoroutine(GamePlayHandler.StartBotGameCoroutine());
            //UiManager.ToggleLoading(false);
        }

        public void ExitSearchScreen()
        {
            GamePlayHandler.StopFindingBot();
            if (startFriendGame != null)
                StopCoroutine(startFriendGame);
            Debug.Log("ExitSearchScreen");
            Debug.LogFormat("Connected Players count : {0}", GamePlayHandler.connectedPlayers.Count);
            StopGame();
        }

        public void GameCompleted(IList<PlayerInfo> players, int winIndex, int playTime, bool isPlayerLeft)
        {
            Debug.Log("GameCompleted " + winIndex);
            Debug.LogFormat("PLAY TIME : {0}", playTime);

            int pZeroMePocketed = players[0].pocketedCarromMen.Where(t => t.turnIndex == 0).Count();
            int pZeroHePocketed = players[0].pocketedCarromMen.Where(t => t.turnIndex == 1).Count();

            int pOneMePocketed = players[1].pocketedCarromMen.Where(t => t.turnIndex == 1).Count();
            int pOneHePocketed = players[1].pocketedCarromMen.Where(t => t.turnIndex == 0).Count();

            Debug.LogFormat("p0 Total pucks : {0}\tp1 total pucks : {1}", players[0].pocketedCarromMen.Length, players[1].pocketedCarromMen.Length);
            Debug.LogFormat("Player pucks :\n player[{0}] : my : {1} other : {2}" +
                "\tplayer[{3}] : my : {4} other : {5}", players[0].playerData.playerID, pZeroMePocketed, pOneHePocketed,
                                                        players[1].playerData.playerID, pOneMePocketed, pZeroHePocketed);
            if (!IsLocalGame())
            {
                PlayerScore[] playerScores = new PlayerScore[2];
                playerScores[0] = new PlayerScore { playerId = players[0].playerData.playerID, hasQueen = players[0].hasQueen, myTotalPucksPotted = players[0].pocketedCarromMen.Length, myPucks = pZeroMePocketed, otherPucks = pOneHePocketed };
                playerScores[1] = new PlayerScore { playerId = players[1].playerData.playerID, hasQueen = players[1].hasQueen, myTotalPucksPotted = players[1].pocketedCarromMen.Length, myPucks = pOneMePocketed, otherPucks = pZeroHePocketed };
            }
        }

        public void PlayerGameWon(List<PlayerInfo> playerList, int winIndex)
        {
            Debug.Log($"{ playerList[winIndex].playerData.localName } Player Game Won Called!!");
            isPlayerInGame = false;
            SetSleepTime(SleepTimeout.SystemSetting);
            uiManager.ShowWinnerScreen(playerList, winIndex);
        }

        public void OnLeaveGameByClickExitBtn(bool isPlayerLeft)
        {
            isPlayerInGame = false;
            Debug.Log($"Is Local game: {isLocal}, HasGameStarted: {HasGameStarted}");
            GamePlayHandler.DisplayGameOverScreen(1 - GamePlayHandler.MyIndex, true);
            ExitGame();
            StopGame();
        }

        public void ExitGame()
        {
            Debug.Log("Exit Game");
            StopGame();
            GamePlayHandler.StopFindingBot();
            UiManager.OnExitGame();
        }

        public void SetPlayerCamera(int index)
        {
            playerCamera.transform.eulerAngles = new Vector3(0, 0, index * 180);
            UiManager.gameplayTheme.carromBoard.flipX = index != 0;
            UiManager.gameplayTheme.carromBoard.flipY = index != 0;
        }

        public void StopGame()
        {
            Debug.Log("StopGame Called");
            gamePlayHandler.ResetGame();
        }

        public void UpdateUserStrikersAndPucks(User arg)
        {
            Player.PlayerData.currentCarromMen = arg.currentPuck;
            Player.PlayerData.currentStriker = arg.currentStriker;
            Player.PlayerData.boughtStrikers = arg.boughtStriker;
            Player.PlayerData.boughtCarromMen = arg.boughtPucks;
            Debug.Log($"<color=lime> Current Striker: {Player.PlayerData.currentStriker}</color>");
        }
 
        public void SetCurrentGamePortPref(int defaultPort = -1)
        {
            PlayerPrefs.SetInt(Constants.CURRENT_GAME_PORT_PREF, defaultPort);
        }
       
        public void SetTime(float time01, bool timerSoundCanPlay)
        {
            UiManager.SetPlayTime(time01);
            if (!timerSoundCanPlay && !IsMyTurn)
            {
                //Debug.Log("Timer Controller: <color=red>Timer tick sound stopped from RPC call</color>");
                UiManager.SetTimerTickSound(false);
            }
        }

        public int readyPlayers = 0;

        private void OnStrikerPropertiesRecieved(StrikerAccuracyInfo accuracyInfo)
        {
            strikerAccuracyInfo = accuracyInfo;
        }

        void OnAdFrequencyFetched(int frequency)
        {
            AdFrequency = frequency;
        }

        private void ShowAdsAfterGameOver(int frequency)
        {
            PlayerPrefs.SetInt(Constants.PLAYER_MATCH_PLAYED_COUNT, PlayerPrefs.GetInt(Constants.PLAYER_MATCH_PLAYED_COUNT, 0) + 1);
            if (PlayerPrefs.GetInt(Constants.PLAYER_MATCH_PLAYED_COUNT, 0) > frequency)
            {
                Debug.LogFormat("<color=cyan>FOR GAME OVER INTERSTITIAL AD SHOWN!!</color>");
                PlayerPrefs.SetInt(Constants.PLAYER_MATCH_PLAYED_COUNT, 0);
                Debug.Log("For Ads Count = " + PlayerPrefs.GetInt(Constants.PLAYER_MATCH_PLAYED_COUNT, 0));
            }
        }
    }
}