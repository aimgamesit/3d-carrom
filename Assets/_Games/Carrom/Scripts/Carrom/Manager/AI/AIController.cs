﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//using Boo.Lang.Environments;

namespace AIMCarrom
{
    public enum HitType
    {
        PUCK,
        NOTHING,
        WALKER,
        BRUTE,
        BACK_SHOT
    }

    public enum AIDifficultyType
    {
        EASY,
        MEDIUM,
        HARD
    }

    public class AIController : MonoBehaviour
    {
        CarromMenHandler[] totalPucks;
        List<CarromMenHandler> myPucks = new List<CarromMenHandler>();
        List<CarromMenHandler> backShotPucks = new List<CarromMenHandler>();
        PuckToPocketData puckToPocket;

        [SerializeField] bool useDebugMode = true;
        [SerializeField] bool followColorRule = true;
        [SerializeField] PlayerInfo.ColorType aiColor;
        [SerializeField] BackShotChecker[] backShotTesters;
        public PlayerInfo.ColorType AiColor => aiColor;

        [SerializeField] LayerMask carromBoardLayer;
        [SerializeField] LayerMask carromMenLayer;
        [SerializeField] LayerMask strikerLayer;
        [SerializeField] LayerMask aiWalkerLayer;
        [SerializeField] LayerMask carromMenPocketLayer;

        #region ACCURACY VARIABLES
        //[Header("Accuracy Fields")]
        //[SerializeField, Range(0, 1)] float accuracy = 1;
        //[SerializeField, Range(Constants.PUCK_RADIUS, Constants.PUCK_RADIUS * 2)] float maxDeviationDistance = Constants.PUCK_RADIUS;

        [Header("Difficulty Values")]
        [SerializeField] AIDifficultyType difficulty;
        [SerializeField] private AIDifficultyAttributes[] difficultyValues;
        #endregion

        int aiIndex = 1;
        Vector3 aiSliderWorldPos;
        Transform carromMenParent;
        Rigidbody2D rb;
        bool isAnyBackShotAvailable;
        bool isLastTwoWithQueen;
        bool choosingBackShot;
        BackShotPocketData chosenBackShot;
        AIDifficultyAttributes selectedDifficultyValues;

        float maxRayDistance = 12f;
        float drawLineHeight = 0.6f;
        float drawLineDuration = 1.5f;
        float strikerRadiusOffset = 0.01f;

        Coroutine jumpCoroutine;
        Coroutine takeAITurnCoroutine;

        private void Start()
        {
            Debug.Log($"AI Controller : {gameObject.name}");
            AssignDifficulty(GameData.Instance.aIDifficultyType);
            backShotTesters = FindObjectsOfType<BackShotChecker>();
            aiColor = GameManager.Instance.GamePlayHandler.playerInfo[1].playerColor;
            DebugMessage(string.Format("AI COLOR : {0}", aiColor));
            rb = GetComponent<Rigidbody2D>();
            carromMenParent = GameManager.Instance.GamePlayHandler.CarromMenPlacement.carromMenParent;

            int mul = aiIndex == 1 ? -1 : 1;
            aiSliderWorldPos = carromMenParent.parent.transform.TransformPoint(Constants.STRIKER_DEFAULT_LOCAL_POSITION) * mul;
        }

        void GetMyPucks()
        {
            if (myPucks.Count <= 0)
            {
                totalPucks = carromMenParent.GetComponentsInChildren<CarromMenHandler>();
                DebugMessage(string.Format("AI: Total pucks found : {0}", totalPucks.Length));
            }

            if (followColorRule)
                myPucks = totalPucks.Where(t => ((int)t.carromMen.colorType == (int)aiColor || t.carromMen.colorType == CarromMenColor.RED) && !t.isDisabled).ToList();
            else
                myPucks = totalPucks.Where(t => !t.isDisabled).ToList();

            DebugMessage(string.Format("AI: MyPucks count : {0}, color : {1}, followColorRule : {2}", myPucks.Count, aiColor, followColorRule));
        }

        Vector3[] GetShootDirection(Vector3 startPos, ShootDirectionData shootData, int index)
        {
            DebugMessage("Take Aim coroutine start");
            transform.position = startPos;
            rb.velocity = Vector3.zero;

            if (shootData == null)
                return new Vector3[] { Vector3.zero, Vector2.zero };

            float decc = 0.45f * Constants.NORMAL_FORCE;
            float maxForce = 8 * rb.mass * decc * decc * 8f;

            float forceMagnitude;
            float angle;

            if (shootData.hitType == HitType.BACK_SHOT) // it's a back shot
            {
                forceMagnitude = maxForce;

                Vector3 relativePuckPosition = transform.InverseTransformPoint(shootData.carromMenPosition);
                angle = Mathf.Atan2(relativePuckPosition.y, relativePuckPosition.x) * Mathf.Rad2Deg;
                GameManager.Instance.GamePlayHandler.StrikerControls.RpcSetArrowDirection(relativePuckPosition, Random.Range(1.2f, 1.5f), false);
            }
            else
            {
                Vector3 dirStrikerToPuck = (shootData.carromMenPosition - transform.position);
                Vector3 dirPuckToPocket = (shootData.pocketPosition - shootData.carromMenPosition);
                GameManager.Instance.GamePlayHandler.StrikerControls.RpcSetArrowDirection(dirStrikerToPuck, 1f, false);
                float startVelocity = Mathf.Sqrt(2 * decc * (dirStrikerToPuck.magnitude + dirPuckToPocket.magnitude));
                float dist_ = (dirPuckToPocket.magnitude > dirStrikerToPuck.magnitude ? dirPuckToPocket.magnitude / 1.2f : dirStrikerToPuck.magnitude);
                forceMagnitude = 8 * rb.mass * decc * decc * dist_;
                forceMagnitude *= 1.8f/* * (2.1f / dist_)*/; // for testing
                DebugMessage(string.Format("dist : {0} {1}", dist_, maxRayDistance / 2));
                //if (dist_ >= maxRayDistance / 2)
                //    forceMagnitude /= 1.4f;
                DebugMessage(string.Format("MaxForce : {0}", maxForce));
                forceMagnitude = Mathf.Clamp(forceMagnitude, 0, 4000);

                DebugMessage(string.Format("start Velocity : {0}\nforceMagnitude : {1}", startVelocity, forceMagnitude));

                Vector3 relativePuckPosition = transform.InverseTransformPoint(shootData.carromMenPosition);
                Vector3 relativePocketPosition = transform.InverseTransformPoint(shootData.pocketPosition);
                Vector3 relativePuckToPocketVector = transform.InverseTransformVector(dirPuckToPocket);

                float offset = 0;
                //if (Vector3.Distance(shootData.carromMenPosition, shootData.strikerPosition) < (Constants.STRIKER_RADIUS + Constants.PUCK_RADIUS + 0.2f))
                //    offset = -(Constants.PUCK_RADIUS + Constants.STRIKER_RADIUS) / 2.4f;
                //else if (Vector3.Distance(shootData.carromMenPosition, shootData.pocketPosition) < 3f)
                //    offset = -0.1f;
                //else if (Mathf.Abs(shootData.strikerPosition.y - transform.position.y) <= 0.2f)
                //    offset = -Constants.PUCK_RADIUS;
                //else
                //    offset = 0;

                shootData.carromMen.GetComponent<Collider2D>().enabled = false;
                // check if puck is very near to a wall
                RaycastHit2D hit = Physics2D.CircleCast(shootData.carromMen.transform.position, Constants.PUCK_RADIUS * 1.3f, Vector2.zero, Mathf.Infinity, carromBoardLayer | carromMenLayer);
                shootData.carromMen.GetComponent<Collider2D>().enabled = true;
                if (hit.collider != null)
                {
                    // dot product to check directions between striker to puck and puck to pot
                    float dot = Vector2.Dot((shootData.carromMenPosition - shootData.strikerPosition).normalized, (shootData.pocketPosition - shootData.carromMenPosition).normalized);
                    DebugMessage($"dot of vectors : {dot}");

                    if (hit.collider.gameObject.name == Constants.CARROM_WALL_BOTTOM && hit.collider.gameObject.layer == LayerMask.NameToLayer(Constants.CARROM_BOARD_LAYER) || (dot >= -0.4f && dot <= 0.4f))
                    {
                        offset = -Constants.PUCK_RADIUS;
                    }
                }
                else if (Vector3.Distance(shootData.carromMenPosition, shootData.pocketPosition) > 6f)
                    offset = -0.1f;

                Vector3 shootPoint = relativePuckPosition + (relativePuckToPocketVector.normalized * (Constants.PUCK_RADIUS + Constants.STRIKER_RADIUS + offset) * -1);

                int devMul = Random.Range(0, 2) == 0 ? 1 : -1;
                Vector3 deviatedPoint = new Vector3(shootPoint.x + (devMul * selectedDifficultyValues.maxDeviationDistance * (1 - selectedDifficultyValues.accuracy)), shootPoint.y, shootPoint.z);

                angle = Mathf.Atan2(deviatedPoint.y, deviatedPoint.x) * Mathf.Rad2Deg;
            }
            transform.Rotate(0, 0, angle);
            Vector3 forceToApply = transform.right * forceMagnitude;
            if (useDebugMode)
                Debug.DrawRay(transform.position, forceToApply.normalized * Vector3.Magnitude(shootData.carromMenPosition - transform.position), Color.yellow, drawLineDuration + 1);

            DebugMessage(string.Format("Shooting theChosenOne: {0}", shootData.hitType));
            DebugMessage("Take Aim coroutine end");
            return new Vector3[] { forceToApply, new Vector3(0, maxForce, 0) };
        }

        void Shoot(Vector2 force, int index)
        {
            GameManager.Instance.GamePlayHandler.StrikerControls.Shoot(transform.position, force, index);
            puckToPocket = null;
            DebugMessage("Shot successfully taken");
        }

        void JumpToPosistion(Vector3 pos, int index, System.Action onJumpComplete)
        {
            DebugMessage(string.Format("World Pos to Jump : {0}", pos));
            var local = carromMenParent.parent.InverseTransformPoint(pos);
            DebugMessage(string.Format("Local Pos to Jump : {0}", local));
            float val = 0;
            if (System.Math.Abs(local.x) > Mathf.Epsilon)
                val = Mathf.Clamp(local.x / Constants.STRIKER_X_EXTENT, -1, 1);

            //Debug.LogFormat("AI: Move AI : {0}", val);

            if (jumpCoroutine != null)
                StopCoroutine(jumpCoroutine);
            jumpCoroutine = StartCoroutine(JumpPositionCoroutine(val, index, onJumpComplete));
        }

        IEnumerator JumpPositionCoroutine(float val, int index, System.Action onComplete)
        {
            //GameManager.Instance.GamePlayHandler.StrikerControls.MoveStriker(0, index);

            //float currentVal = 0;
            //Debug.LogFormat("AI: movement started");
            //while (Mathf.Abs(currentVal - val) > 0.02f)
            //{
            //    currentVal = Mathf.Lerp(currentVal, val, 0.3f);
            //    GameManager.Instance.GamePlayHandler.StrikerControls.MoveStriker(currentVal, index);
            //    yield return new WaitForSeconds(0.125f);
            //}
            //Debug.LogFormat("AI: movement ended");
            //onComplete?.Invoke();

            GameManager.Instance.GamePlayHandler.StrikerControls.MoveStriker(val, index, false, 0.35f);
            yield return new WaitForSeconds(1.5f);
            //Debug.LogFormat("JumpPositionCoroutine mid");
            onComplete?.Invoke();
            //Debug.LogFormat("JumpPositionCoroutine end");
        }

        public void TakeTurn()
        {
            Debug.LogFormat($"<color=lime>AI TAKE TURN CALLED</color>");
            if (takeAITurnCoroutine != null)
                StopCoroutine(takeAITurnCoroutine);
            takeAITurnCoroutine = StartCoroutine(TakeTurnCoroutine());
        }

        List<CarromMenHandler> GetPucksBelowPuck(Transform puckTransform)
        {
            var pucksBlow = totalPucks.Where(t => Mathf.Abs(puckTransform.position.y - aiSliderWorldPos.y) > Mathf.Abs(t.transform.position.y - aiSliderWorldPos.y)
                                && !t.isDisabled
                                && t.transform.position.x <= Constants.STRIKER_X_EXTENT).ToList();
            DebugMessage(string.Format("Pucks below puck at {0} : {1}", puckTransform.position, pucksBlow.Count));
            return pucksBlow;
        }

        Coroutine takeAimCoroutine;

        void TakeAim(Vector3 direction, float arrowSize, System.Action onAimDone = null)
        {
            GameManager.Instance.GamePlayHandler.StrikerControls.MoveDirection(direction.normalized, arrowSize, aiIndex);
            float randomWaitTime = Random.Range(0.5f, 1.4f);
            if (takeAimCoroutine != null)
                StopCoroutine(takeAimCoroutine);
            takeAimCoroutine = StartCoroutine(TakeAimCoroutine(randomWaitTime, onAimDone));
        }

        IEnumerator TakeAimCoroutine(float waitTime, System.Action onDone)
        {
            yield return new WaitForSeconds(waitTime);
            onDone?.Invoke();
        }

        IEnumerator TakeTurnCoroutine()
        {
            DebugMessage("TakeTurnCoroutine starts");
            GetMyPucks();
            var pucksWithPocket = GetMyPucksWithPlainSightPocket(myPucks);

            // backshots
            List<CarromMenHandler> pucksWithPocketCarromMen = new List<CarromMenHandler>();
            pucksWithPocket.ForEach(t => pucksWithPocketCarromMen.Add(t.carromMen));
            backShotPucks = GetBackShotList(pucksWithPocketCarromMen);
            List<BackShotPocketData> availableBackShots = GetBackShotPocketData(backShotPucks);
            isAnyBackShotAvailable = availableBackShots.Count > 0;
            if (isAnyBackShotAvailable)
            {
                int chosenIndex;
                //bool isQueenAvailableWithCover = /*availableBackShots.Count > 1 && */availableBackShots.Any(t => t.carromMen.carromMen.colorType == CarromMenColor.RED);
                //if (isQueenAvailableWithCover)
                //    chosenIndex = availableBackShots.FindIndex(t => t.carromMen.carromMen.colorType == CarromMenColor.RED);
                //else
                //{
                int rngIndex = Random.Range(0, availableBackShots.Count);
                chosenIndex = availableBackShots.Count > 1 ? rngIndex : 0;
                //}
                chosenBackShot = availableBackShots[chosenIndex];
            }

            // every other shots
            DebugMessage(string.Format("AI: pucksWithPocket : {0}", pucksWithPocket.Count));
            var pucksWithHitData = GetHitDatas(pucksWithPocket) ?? pucksWithPocket;
            DebugMessage(string.Format("AI: pucksWithHitData : {0}", pucksWithHitData.Count));
            puckToPocket = GetPuckToPot(pucksWithHitData);
            DebugMessage($"seedhe jayegi: {puckToPocket.carromMen.carromMen.colorType}");

            if (useDebugMode)
                Debug.DrawLine(puckToPocket.shootPoint, new Vector2(puckToPocket.shootPoint.x, puckToPocket.shootPoint.y + drawLineHeight), Color.green, drawLineDuration);
            bool overlappingWithAPuck = GameManager.Instance.GamePlayHandler.StrikerControls.IsPointOverlappingWith(puckToPocket.shootPoint, carromMenLayer);

            float waitTimeOffset = Random.Range(0.1f, 1f);
            yield return new WaitForSeconds(waitTimeOffset);

            if (isAnyBackShotAvailable && puckToPocket.carromMen.carromMen.colorType != CarromMenColor.RED)
            {
                choosingBackShot = true;
                JumpToPosistion(chosenBackShot.strikerPosition, aiIndex, TakeAimAndShoot);
            }
            else if (!overlappingWithAPuck)
            {
                choosingBackShot = false;
                JumpToPosistion(puckToPocket.shootPoint, aiIndex, TakeAimAndShoot);
            }
            else
            {
                choosingBackShot = false;
                TakeAimAndShoot();
            }

            //Debug.LogFormat("TakeTurnCoroutine end");
        }

        void TakeAimAndShoot()
        {
            ShootDirectionData shootData;
            if (isAnyBackShotAvailable && choosingBackShot)
            {
                shootData = new ShootDirectionData { carromMenPosition = chosenBackShot.carromShootPoint, pocketPosition = chosenBackShot.carromShootPoint, strikerPosition = chosenBackShot.strikerPosition, hitType = chosenBackShot.hitType, carromMen = chosenBackShot.carromMen };
            }
            else
            {
                shootData = new ShootDirectionData { carromMenPosition = puckToPocket.carromMen.transform.position, pocketPosition = puckToPocket.pocketPosition, strikerPosition = puckToPocket.shootPoint, hitType = puckToPocket.hitType, carromMen = puckToPocket.carromMen };
            }
            //Debug.LogFormat("TakeAimAndShoot starts ");
            Vector3[] dirAndForce = GetShootDirection(transform.position, shootData, aiIndex);

            Vector2 forceToApply = dirAndForce[0];
            float maxForceMag = dirAndForce[1].y;
            float arrowSize = (forceToApply.magnitude / maxForceMag) * Constants.MAX_ARROW_DIRECTION_SIZE;

            TakeAim(forceToApply.normalized, arrowSize, () => Shoot(forceToApply, aiIndex));
            //Debug.LogFormat("TakeAimAndShoot ends");
        }

        PuckToPocketData GetPuckToPot(List<PuckToPocketData> puckToPocketDatas)
        {
            isLastTwoWithQueen = false;
            PuckToPocketData pocketData;
            if (puckToPocketDatas.Count <= 0)
            {
                DebugMessage("AI: data empty in GetPuckToPot() | Returning random from MyPucks");
                if (myPucks.Count == 2)
                {
                    DebugMessage("AI: Only 2 left | random shot");
                    CarromMenHandler carromMenToHit = myPucks.FirstOrDefault(t => t.carromMen.colorType == CarromMenColor.RED) ?? myPucks[Random.Range(0, 2)];
                    pocketData = new PuckToPocketData { carromMen = carromMenToHit, pocketPosition = GetPocket(carromMenToHit, transform.position).transform.position, shootPoint = transform.position };
                    return GetShootPoint(pocketData) ?? pocketData;
                }
                var puck = myPucks[Random.Range(0, myPucks.Count)];
                var pocket = GetPocket(puck, transform.position);
                pocketData = new PuckToPocketData { carromMen = puck, pocketPosition = pocket.transform.position };
                return GetShootPoint(pocketData) ?? pocketData;
            }

            if (myPucks.Count == 2)
            {
                DebugMessage("AI: Only 2 left | chosen shot");
                isLastTwoWithQueen = myPucks.Any(t => t.carromMen.colorType == CarromMenColor.RED);
                var queen = myPucks.FirstOrDefault(t => t.carromMen.colorType == CarromMenColor.RED) ?? myPucks[0];

                return puckToPocketDatas.FirstOrDefault(t => t.carromMen.carromMen.colorType == CarromMenColor.RED) ??
                        new PuckToPocketData { carromMen = queen, pocketPosition = GetPocket(queen, transform.position).transform.position, shootPoint = transform.position } ??
                        puckToPocketDatas[0];
            }

            PuckToPocketData theChosenOne;
            if (puckToPocketDatas.Count > 1 && puckToPocketDatas.Any(t => t.carromMen.carromMen.colorType == CarromMenColor.RED))
            {
                theChosenOne = puckToPocketDatas.FirstOrDefault(t => t.carromMen.carromMen.colorType == CarromMenColor.RED);
                return theChosenOne;
            }

            //theChosenOne = puckToPocketDatas.Where(t => t.hitType == HitType.WALKER).OrderByDescending(t => Vector3.Distance(t.carromMen.transform.position, t.shootPoint)).FirstOrDefault() ?? // first try direct shots
            //                    puckToPocketDatas.Where(t => t.hitType == HitType.NOTHING).OrderBy(t => Vector3.Distance(t.carromMen.transform.position, t.pocketPosition)).FirstOrDefault() ?? // then try shots beside walls
            //                    puckToPocketDatas.Where(t => t.hitType == HitType.BRUTE).OrderBy(t => Vector3.Distance(t.carromMen.transform.position, t.pocketPosition)).FirstOrDefault() ?? // then try brute force
            //                    puckToPocketDatas.FirstOrDefault(); // lastly try indirect shots

            theChosenOne = puckToPocketDatas.Where(t => t.hitType == HitType.WALKER).OrderByDescending(t => Vector3.Dot((t.carromMen.transform.position - t.shootPoint).normalized, (t.pocketPosition - t.carromMen.transform.position).normalized)).FirstOrDefault() ?? // first try direct shots
                                puckToPocketDatas.Where(t => t.hitType == HitType.NOTHING).OrderBy(t => Vector3.Distance(t.carromMen.transform.position, t.pocketPosition)).FirstOrDefault() ?? // then try shots beside walls
                                puckToPocketDatas.Where(t => t.hitType == HitType.BRUTE).OrderByDescending(t => Vector3.Dot((t.carromMen.transform.position - t.shootPoint).normalized, (t.pocketPosition - t.carromMen.transform.position).normalized)).FirstOrDefault() ?? // then try brute force
                                puckToPocketDatas.FirstOrDefault(); // lastly try indirect shots

            return theChosenOne;
        }

        List<PuckToPocketData> GetHitDatas(List<PuckToPocketData> pucksWithPocket)
        {
            if (pucksWithPocket.Count <= 0)
            {
                DebugMessage("AI: data empty in GetHitDatas()");
                return null;
            }
            List<PuckToPocketData> hitDatas = new List<PuckToPocketData>();
            foreach (var pocketData in pucksWithPocket)
            {
                //DebugMessage("inside foreach GetHitDatas");
                var pData = GetShootPoint(pocketData);
                if (pData != null)
                    hitDatas.Add(pData);
            }
            DebugMessage("outside foreach GetHitDatas");
            return hitDatas;
        }

        PuckToPocketData GetShootPoint(PuckToPocketData pocketData)
        {
            Vector3 puckToBackwardsDir = (pocketData.pocketPosition - pocketData.carromMen.transform.position).normalized * (Constants.STRIKER_RADIUS + strikerRadiusOffset) * -1; // puck to walker
            if (useDebugMode)
                Debug.DrawRay(pocketData.carromMen.transform.position/* + (2.1f * puckToBackwardsDir)*/, puckToBackwardsDir * maxRayDistance, Color.red, 3);
            //pocketData.carromMen.GetComponent<Collider2D>().enabled = false;
            RaycastHit2D hit = Physics2D.CircleCast(pocketData.carromMen.transform.position + (2.1f * puckToBackwardsDir), Constants.STRIKER_RADIUS + strikerRadiusOffset, puckToBackwardsDir.normalized, maxRayDistance, aiWalkerLayer | carromMenLayer);
            //pocketData.carromMen.GetComponent<Collider2D>().enabled = true;

            if (hit.collider == null) // hits nothing
            {
                DebugMessage("AI: Hits nothing");
                return HitsNothingTrackingBackwards(pocketData, puckToBackwardsDir);
            }

            if (hit.collider)
                DebugMessage(string.Format("AI: Hits {0}", hit.collider.name));

            if (hit.collider != null && !hit.collider.CompareTag(Constants.CARROM_MAN_TAG)) // hits walker
            {
                return HitsWalkerTrackingBackwards(pocketData, puckToBackwardsDir, hit);
            }
            //return null;
            return HitsPuckTrackingBackwards(pocketData, puckToBackwardsDir, hit); // hits puck
        }

        PuckToPocketData TryBruteForce(PuckToPocketData pocketData)
        {
            DebugMessage("Brute started");
            //// -- Steps -- ////
            /// 0. set x to -walkerExtent
            /// 1. circle cast a ray from puck to x
            /// 2. if hits walker
            /// 3.  make it shoot point
            /// 4. else
            /// 5.  increase x by puck radius 
            /// 6. try from step 1 till you find walker or x > extent

            RaycastHit2D hit;
            float x_ = 0f;
            int count = 0;

            do
            {
                x_ = -Constants.STRIKER_X_EXTENT + (Constants.PUCK_RADIUS * count);
                Vector3 selectedPoint = new Vector3(x_, aiSliderWorldPos.y, pocketData.carromMen.transform.position.z);
                Vector3 dirToSelectedPoint = Vector3.Normalize(selectedPoint - pocketData.carromMen.transform.position) * Constants.STRIKER_RADIUS;
                Vector2 rayStartPos = pocketData.carromMen.transform.position + (2.1f * dirToSelectedPoint.normalized);
                hit = Physics2D.CircleCast(rayStartPos, Constants.STRIKER_RADIUS + 0.12f, dirToSelectedPoint.normalized, maxRayDistance, carromMenLayer | aiWalkerLayer);

                if (hit.collider != null)
                {
                    if (useDebugMode)
                        Debug.DrawRay(rayStartPos, dirToSelectedPoint.normalized * maxRayDistance, Color.gray, drawLineDuration);
                    if (!hit.collider.CompareTag(Constants.CARROM_MAN_TAG) && !GameManager.Instance.GamePlayHandler.StrikerControls.IsPointOverlappingWith(selectedPoint, carromMenLayer)) // hits striker slider
                    {
                        if (useDebugMode)
                            Debug.DrawRay(rayStartPos, dirToSelectedPoint.normalized * maxRayDistance, Color.green, drawLineDuration);
                        return new PuckToPocketData { carromMen = pocketData.carromMen, pocketPosition = GetPocket(pocketData.carromMen, selectedPoint).transform.position, shootPoint = selectedPoint, hitType = HitType.BRUTE };
                    }
                    else
                    {
                        ++count;
                    }
                }
                else
                {
                    return null;
                }

            } while (/*hit.collider != null || */x_ < Constants.STRIKER_X_EXTENT);
            DebugMessage("Trying Brute done");
            return null;
        }

        PuckToPocketData HitsPuckTrackingBackwards(PuckToPocketData pocketData, Vector3 backDir, RaycastHit2D hit)
        {
            float offset = 0/*Constants.STRIKER_RADIUS * 2.5f*/;
            Transform newPuckTransform = hit.collider.transform;

            // try brute force after indirect shot
            var brute = TryBruteForce(pocketData);
            if (brute != null)
                return brute;

            // try this when brute approach doesn't give a result
            var pocketDataBehind = HitsNothingTrackingBackwards(pocketData, backDir);
            if (pocketDataBehind == null)
                return null;
            Vector3 newShootPtToPocket = pocketData.pocketPosition - pocketDataBehind.shootPoint;
            Vector3 newCarromMenToPocket = pocketData.pocketPosition - newPuckTransform.position;

            if ((newShootPtToPocket.magnitude > newCarromMenToPocket.magnitude) && (Vector3.Distance(pocketData.carromMen.transform.position, newPuckTransform.position) <= Constants.STRIKER_X_EXTENT / 2)) // newly hit carrom men is inside new shoot pt. and pocket
            {
                Vector3 oldPuckToPocketDir = pocketData.pocketPosition - pocketData.carromMen.transform.position;
                Vector3 oldPuckHitPt = pocketData.carromMen.transform.position + (oldPuckToPocketDir.normalized * (Constants.PUCK_RADIUS + Constants.STRIKER_RADIUS) * -1);
                Vector3 newPuckToOldHitPt = oldPuckHitPt - newPuckTransform.position;

                Vector3 newHitPoint = new Ray(newPuckTransform.position, newPuckToOldHitPt.normalized).GetPoint(newCarromMenToPocket.magnitude);

                PuckToPocketData newPocketData = new PuckToPocketData { carromMen = hit.collider.GetComponent<CarromMenHandler>(), pocketPosition = newHitPoint, hitType = HitType.PUCK };
                Vector3 newBackDir = newCarromMenToPocket.normalized * -1 * (Constants.STRIKER_RADIUS + strikerRadiusOffset);
                newPocketData = HitsNothingTrackingBackwards(newPocketData, newBackDir, HitType.PUCK);
                return newPocketData;
            }
            else
            {
                return HitsNothingTrackingBackwards(pocketData, backDir);
            }
        }

        PuckToPocketData HitsWalkerTrackingBackwards(PuckToPocketData pocketData, Vector3 backDir, RaycastHit2D hit)
        {
            DebugMessage(string.Format("Hits {0} while back tracking | Dang we hit the walker !!", hit.collider.gameObject));
            Vector3 shootPoint = hit.point;
            if (GameManager.Instance.GamePlayHandler.StrikerControls.IsPointOverlappingWith(shootPoint, carromMenLayer))
            {
                shootPoint = GameManager.Instance.GamePlayHandler.StrikerControls.FindEmpty(shootPoint, false);

                backDir = Vector3.Normalize(shootPoint - (pocketData.carromMen.transform.position)) * Constants.STRIKER_RADIUS;
                pocketData.carromMen.GetComponent<Collider2D>().enabled = false;
                hit = Physics2D.CircleCast(pocketData.carromMen.transform.position + (2.1f * backDir), Constants.STRIKER_RADIUS + 0.1f, backDir.normalized, maxRayDistance, aiWalkerLayer | carromMenLayer);
                pocketData.carromMen.GetComponent<Collider2D>().enabled = true;
                if (hit.collider != null)
                {
                    if (!hit.collider.CompareTag(Constants.CARROM_MAN_TAG)) // hits walker
                    {
                        if (!GameManager.Instance.GamePlayHandler.StrikerControls.IsPointOverlappingWith(shootPoint, carromMenLayer) && Vector3.Distance(hit.point, pocketData.carromMen.transform.position) > Constants.PUCK_RADIUS)
                        {
                            //shootPoint = hit.point;
                            return new PuckToPocketData { carromMen = pocketData.carromMen, pocketPosition = pocketData.pocketPosition, shootPoint = shootPoint, hitType = HitType.WALKER };
                        }
                    }
                    else // hits puck
                    {
                        return HitsPuckTrackingBackwards(pocketData, backDir, hit);
                    }
                }
                else // hits nothing
                {
                    return HitsNothingTrackingBackwards(pocketData, backDir);
                }
            }
            return new PuckToPocketData { carromMen = pocketData.carromMen, pocketPosition = pocketData.pocketPosition, shootPoint = shootPoint, hitType = HitType.WALKER };
        }

        PuckToPocketData HitsNothingTrackingBackwards(PuckToPocketData pocketData, Vector3 backDir, HitType hitType_ = HitType.NOTHING)
        {
            Vector3 backRayEndPoint = new Ray(pocketData.carromMen.transform.position/* + (2.1f * backDir)*/, backDir.normalized).GetPoint(maxRayDistance * 0.8f);
            if (useDebugMode)
                Debug.DrawLine(pocketData.carromMen.transform.position, backRayEndPoint, Color.white, drawLineDuration);
            //DebugMessage(string.Format("BackRay End point : {0}", backRayEndPoint));
            int xMul = backRayEndPoint.x < 0 ? -1 : 1;

            // if puckToPot is above striker && (pocket2 || pocket3)
            // move to inverse nothing
            if (pocketData.carromMen.transform.position.y >= transform.position.y - 0.25f && (pocketData.pocketPosition == GameManager.Instance.aiPockets[0].transform.position || pocketData.pocketPosition == GameManager.Instance.aiPockets[1].transform.position))
            {
                if ((transform.position.x - pocketData.pocketPosition.x) * (transform.position.x - backRayEndPoint.x) > 0) // pocket and back ray end point are on the same side
                    xMul *= -1;
            }
            Vector3 strikerDefaultPos = Constants.STRIKER_DEFAULT_LOCAL_POSITION;

            strikerDefaultPos = new Vector3(xMul * Constants.STRIKER_X_EXTENT, -strikerDefaultPos.y, strikerDefaultPos.z);
            DebugMessage(string.Format("estimated striker pos : {0}", strikerDefaultPos));

            Vector3 shootPoint = GameManager.Instance.GamePlayHandler.StrikerControls.FindEmpty(strikerDefaultPos, false);
            DebugMessage(string.Format("estimated striker pos2 : {0}", shootPoint));
            backDir = Vector3.Normalize(shootPoint - (pocketData.carromMen.transform.position)) * Constants.STRIKER_RADIUS;
            if (useDebugMode)
                Debug.DrawLine(pocketData.carromMen.transform.position, shootPoint, Color.cyan, drawLineDuration);

            pocketData.carromMen.GetComponent<Collider2D>().enabled = false;
            RaycastHit2D hit = Physics2D.CircleCast(pocketData.carromMen.transform.position/* + (2.1f * backDir)*/, Constants.STRIKER_RADIUS + 0.01f, backDir.normalized, maxRayDistance, aiWalkerLayer | carromMenLayer);
            pocketData.carromMen.GetComponent<Collider2D>().enabled = true;
            if (hit.collider)
                DebugMessage(string.Format("<color=red>hits {0} in checking for nothing</color>", hit.collider.name));
            if (hit.collider != null && !hit.collider.CompareTag(Constants.CARROM_MAN_TAG)) // hits walker
            {
                //var pocketDat = new PuckToPocketData { carromMen = pocketData.carromMen, pocketPosition = pocketData.pocketPosition, shootPoint = shootPoint, hitType = hitType_ };
                //return HitsWalkerTrackingBackwards(pocketDat, backDir, hit);

                //shootPoint = hit.point;
                if (!GameManager.Instance.GamePlayHandler.StrikerControls.IsPointOverlappingWith(shootPoint, carromMenLayer))
                {
                    return new PuckToPocketData { carromMen = pocketData.carromMen, pocketPosition = pocketData.pocketPosition, shootPoint = shootPoint, hitType = hitType_ };
                }
            }
            return null;
        }

        List<PuckToPocketData> GetMyPucksWithPlainSightPocket(List<CarromMenHandler> pucks)
        {
            if (pucks.Count <= 0)
            {
                DebugMessage("AI: data empty in GetMyPucksWithPlainSightPocket()");
                return null;
            }
            List<PuckToPocketData> pucksWithPocket = new List<PuckToPocketData>();
            foreach (var puck in pucks)
            {
                //DebugMessage("inside foreach GetMyPucksWithPlainSightPocket");
                GameObject pocket = GetPocket(puck, transform.position);

                Vector3 dir = (pocket.transform.position - puck.transform.position).normalized * Constants.PUCK_RADIUS; // puck to pocket
                RaycastHit2D hit = Physics2D.CircleCast(puck.transform.position + (2.1f * dir), Constants.PUCK_RADIUS/* - (0.01f * Constants.PUCK_RADIUS)*/, dir.normalized, maxRayDistance, carromMenPocketLayer | carromMenLayer);

                if (hit.collider != null)
                {
                    if (hit.collider.CompareTag(Constants.POCKET_TAG))
                    {
                        PuckToPocketData pocketData = new PuckToPocketData { carromMen = puck, pocketPosition = pocket.transform.position };

                        if (!pucksWithPocket.Contains(pocketData))
                            pucksWithPocket.Add(pocketData);
                    }
                }
            }
            DebugMessage("outside foreach GetMyPucksWithPlainSightPocket");
            return pucksWithPocket;
        }

        GameObject GetPocket(CarromMenHandler puck, Vector3 shootPoint)
        {
            return GameManager.Instance.aiPockets[GetPuckQuadrant(puck, shootPoint)];
        }

        int GetPuckQuadrant(CarromMenHandler puck, Vector3 shootPoint)
        {
            //Debug.Log($"puck: {puck.name}");
            var x_ = puck.transform.position.x - shootPoint.x;
            var y_ = puck.transform.position.y - (shootPoint.y - 0.25f);

            int quad;
            quad = x_ > 0 ? (y_ > 0 ? 1 : 2) : (y_ > 0 ? 0 : 3);
            return quad;
        }

        List<CarromMenHandler> GetBackShotList(List<CarromMenHandler> pucksWithPocket)
        {
            List<CarromMenHandler> tempList = new List<CarromMenHandler>();
            foreach (var tester in backShotTesters)
            {
                tempList.AddRange(tester.GetBackShotPucks(pucksWithPocket));
            }
            DebugMessage(string.Format($"Back List pucks count: {tempList.Count}"));
            return tempList;
        }

        List<BackShotPocketData> GetBackShotPocketData(List<CarromMenHandler> backShotList)
        {
            // Steps:
            // 0. for each puck in backShotList
            // 1. while x > -striker-extent && x <= 0
            // 1-a. Cast a ray from puckShootPoint - wallTop (x, top-wall-y)
            // 1-b. if hit wall 
            // 1-b-i. cast a reflected ray
            // 1-b-ii. if reflected ray hits striker-walker
            // 1-b-ii-I. return the striker-walker-x and angle to shoot
            // 1-c. increment x

            float rayStartX = 3.9f;
            float rayEndX = 0.2f;
            float rayRadius = Constants.STRIKER_RADIUS + 0.05f;
            List<BackShotPocketData> backPocketData = new List<BackShotPocketData>();

            foreach (var puck in backShotList)
            {
                bool isLeftSide = puck.transform.position.x < 0;
                GameObject pocketGO = GameManager.Instance.aiPockets[isLeftSide ? 0 : 1].gameObject;
                Vector3 puckToPocketDirection = (pocketGO.transform.position - puck.transform.position);
                Vector3 shootPoint = puck.transform.position + (puckToPocketDirection.normalized * (Constants.STRIKER_RADIUS + Constants.PUCK_RADIUS + 0.045f) * -1);

                puck.GetComponent<Collider2D>().enabled = false;
                // check if puck is in plain sight with pocket
                RaycastHit2D hit = Physics2D.CircleCast(puck.transform.position + (puckToPocketDirection.normalized * (Constants.PUCK_RADIUS + 0.05f)), Constants.PUCK_RADIUS + 0.05f, puckToPocketDirection.normalized, maxRayDistance, carromMenPocketLayer | carromMenLayer);
                puck.GetComponent<Collider2D>().enabled = true;
                if (hit.collider != null)
                {
                    if (useDebugMode)
                        Debug.DrawLine(puck.transform.position + (puckToPocketDirection.normalized * (Constants.PUCK_RADIUS - 0.05f)), hit.point, Color.magenta, drawLineDuration);
                    DebugMessage($"collided with {hit.collider.name}");
                    var hitCarromMen = hit.collider.GetComponent<CarromMenHandler>();
                    if (hitCarromMen != null && (int)hitCarromMen.carromMen.colorType != (int)aiColor)
                    {
                        DebugMessage($"collided with PUCK in BETWEEN");
                        continue;
                    }
                }

                Vector3 relativeShootPoint = (shootPoint - puck.transform.position).normalized;
                Vector3 maxYRay = relativeShootPoint * 3;
                float x_ = 0;
                int count = 0;

                do
                {
                    DebugMessage("Casting an incident ray");
                    // cast an incident ray
                    int boardSideMultiplier = isLeftSide ? -1 : 1;
                    x_ = boardSideMultiplier * rayStartX + (Constants.PUCK_RADIUS / 2 * count);
                    Vector2 incidentDirection = (new Vector3(x_, maxYRay.y, puck.transform.position.z) - shootPoint);
                    puck.GetComponent<Collider2D>().enabled = false;
                    hit = Physics2D.CircleCast(shootPoint, rayRadius/* - 0.05f*/, incidentDirection.normalized, maxRayDistance, carromMenLayer | carromBoardLayer);
                    puck.GetComponent<Collider2D>().enabled = true;
                    if (hit.collider != null)
                    {
                        DebugMessage($"incident ray collides with something : {hit.collider.name}");
                        var collidedCarromMen = hit.collider.GetComponent<CarromMenHandler>();
                        if (useDebugMode)
                            Debug.DrawLine(shootPoint, hit.point, collidedCarromMen == null ? Color.green : Color.red, 2);
                        if (collidedCarromMen == null)
                        {
                            DebugMessage($"Distance from collision point : {Vector3.Distance(puck.transform.position, hit.point)}");
                            float shootPointOffset = 0.1f;
                            // if collided with left or right wall, shift the shoot point a little in x-axis
                            if ((hit.collider.gameObject.name == Constants.CARROM_WALL_LEFT || hit.collider.gameObject.name == Constants.CARROM_WALL_RIGHT) && Vector3.Distance(puck.transform.position, hit.point) <= 1f)
                            {
                                int leftRightMultiplier = shootPoint.x < 0 ? 1 : -1; // if on left, go right & vice versa
                                shootPoint.x += leftRightMultiplier * shootPointOffset;
                                DebugMessage($"shoot point shifting by {leftRightMultiplier * shootPointOffset}");
                            }

                            // cast a reflected ray
                            DebugMessage("casting a reflected ray");
                            Vector3 reflectedDirection = Vector2.Reflect(incidentDirection, hit.normal);
                            hit.collider.enabled = false;
                            RaycastHit2D hitReflected = Physics2D.CircleCast(hit.point, rayRadius, reflectedDirection.normalized, maxRayDistance, carromMenLayer | aiWalkerLayer | carromBoardLayer);
                            hit.collider.enabled = true;
                            if (hitReflected.collider != null)
                            {
                                DebugMessage($"reflected ray hits {hit.collider.name} | layer : {hitReflected.collider.gameObject.layer} | layer in script : {LayerMask.NameToLayer(Constants.CARROM_BOARD_LAYER)}");
                                var collidedCarromMenReflected = hitReflected.collider.GetComponent<CarromMenHandler>();
                                if (useDebugMode)
                                    Debug.DrawLine(hit.point, hitReflected.point, collidedCarromMenReflected == null ? Color.green : Color.red, 2);
                                if (collidedCarromMenReflected == null && hitReflected.collider.gameObject.layer != LayerMask.NameToLayer(Constants.CARROM_BOARD_LAYER))
                                {
                                    // add this to shootable positions
                                    var newStrikerPos = new Vector3(hitReflected.point.x, Constants.STRIKER_DEFAULT_LOCAL_POSITION.y, Constants.STRIKER_DEFAULT_LOCAL_POSITION.z);
                                    newStrikerPos = GameManager.Instance.GamePlayHandler.StrikerControls.FindEmpty(hitReflected.point, false);
                                    BackShotPocketData backShot = new BackShotPocketData { hitType = HitType.BACK_SHOT, carromShootPoint = hit.point, puckShootPoint = shootPoint, strikerPosition = newStrikerPos, carromMen = puck };
                                    backPocketData.Add(backShot);
                                    break;
                                }
                            }
                        }
                    }
                    count = isLeftSide ? count + 1 : count - 1;
                }
                while (isLeftSide ? x_ < rayEndX : x_ > rayEndX);
            }
            DebugMessage($"Back Shot available: {backPocketData.Count}");
            return backPocketData;
        }

        public void AssignDifficulty(AIDifficultyType diff)
        {
            difficulty = diff;
            SetDifficultyValues(diff);
        }

        void SetDifficultyValues(AIDifficultyType diff)
        {
            Debug.Log($"<color=white>AI DIFFICULTY:</color> {diff}");
            int index = (int)diff;
            selectedDifficultyValues = difficultyValues[index];
        }

        void DebugMessage(string message)
        {
            if (!useDebugMode)
                return;
            Debug.LogFormat($"<color=blue>[AI Debug]: </color> {message}");
        }

        private float maxXRayDistance = 2.8f;
        void ShootXRay(Vector2 coordinate)
        {
            Vector3 coord = new Vector3(coordinate.x, coordinate.y, transform.position.z);
            Vector3 finalPt = coord + transform.position;
            if (useDebugMode)
                Debug.DrawRay(transform.position, finalPt - transform.position, Color.magenta);

            //Debug.DrawLine(transform.position, coord + transform.position, Color.magenta);
        }

        IEnumerator KeepShootingXRay()
        {
            float x_ = 0;
            float timeElapsed = 0;
            while (true)
            {
                x_ = Mathf.Sin((timeElapsed / 2) % (2 * Mathf.PI));
                float y_ = Mathf.Sin((timeElapsed / 2) % (2 * Mathf.PI) - 1);
                ShootXRay(new Vector2(x_, y_));
                timeElapsed += Time.deltaTime;
                yield return null;
            }
        }
    }

    public class PuckToPocketData
    {
        public CarromMenHandler carromMen;
        public Vector3 pocketPosition;
        public Vector3 shootPoint;
        public HitType hitType;
    }

    public class BackShotPocketData
    {
        public Vector3 strikerPosition;
        public Vector3 carromShootPoint;
        public Vector3 puckShootPoint;
        public CarromMenHandler carromMen;
        public HitType hitType;
    }

    public class ShootDirectionData
    {
        public Vector3 strikerPosition;
        public Vector3 carromMenPosition;
        public Vector3 pocketPosition;
        public CarromMenHandler carromMen;
        public HitType hitType;
    }

    [System.Serializable]
    public struct AIDifficultyAttributes
    {
        public string name;
        public float accuracy;
        public float maxDeviationDistance;
    }
}
