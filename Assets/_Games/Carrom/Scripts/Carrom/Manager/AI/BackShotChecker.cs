﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIMCarrom
{
    public class BackShotChecker : MonoBehaviour
    {
        public enum BackShotCheckerLocation
        {
            LEFT,
            RIGHT
        }

        [SerializeField] BackShotCheckerLocation location;
        [SerializeField] LayerMask carromMenLayer;
        //[SerializeField] Transform dummyPuck;

        #region PRIVATE VARIABLES
        float circleCastRadius = 3f;
        float minAngle = -20f;
        float maxAngle = 120f;
        float rayDrawDuration = 2;
        float rayDrawDistance = 0.2f;
        #endregion


        //private void Update()
        //{
        //    Debug.LogFormat($"angle : {FindAngleWith(dummyPuck)}");
        //}

        public List<CarromMenHandler> GetBackShotPucks(List<CarromMenHandler> myPucks)
        {
            List<CarromMenHandler> backShotList = new List<CarromMenHandler>();
            RaycastHit2D[] hitData = Physics2D.CircleCastAll(transform.position, circleCastRadius, Vector2.zero, 0, carromMenLayer);

            foreach (RaycastHit2D hit in hitData)
            {
                CarromMenHandler puck = hit.collider.GetComponent<CarromMenHandler>();
                if (myPucks.Contains(puck))
                {
                    float angle = FindAngleWith(puck.transform);

                    switch (location)
                    {
                        case BackShotCheckerLocation.LEFT:
                            {
                                if (angle >= minAngle && angle <= maxAngle)
                                {
                                    AddPuckToList(ref backShotList, puck);
                                }
                                break;
                            }
                        case BackShotCheckerLocation.RIGHT:
                            {
                                if (angle >= -maxAngle && angle <= -minAngle)
                                {
                                    AddPuckToList(ref backShotList, puck);
                                }
                                break;
                            }
                    }
                }
            }
            return backShotList;
        }

        void AddPuckToList(ref List<CarromMenHandler> list, CarromMenHandler puck)
        {
            if (!list.Contains(puck))
            {
                //Debug.DrawRay(puck.transform.position, (new Vector3(puck.transform.position.x + rayDrawDistance, puck.transform.position.y + rayDrawDistance, puck.transform.position.z) - puck.transform.position).normalized * rayDrawDistance, Color.black, rayDrawDuration);
                list.Add(puck);
            }
        }

        float FindAngleWith(Transform obj)
        {
            Vector3 relativePuckPos = transform.InverseTransformPoint(obj.position);
            float angle = Mathf.Atan2(relativePuckPos.x, relativePuckPos.y) * Mathf.Rad2Deg;
            return angle;
        }
    }
}