﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using AIMCarrom;

public class PopupManager : PopupBaseManager
{
    public GameObject msgObj;
    public Button yesBtnObj;
    public Button noBtnObj;
    public Button okBtnObj;
    public GameObject inputObj;
    public InputField inputField;
    public Text titleText;
    public Text msgText;
    public Text yesBtnText;
    public Text noBtnText;
    public Text okBtnText;
    public GameObject coinsObj;
    public Text coinsText;
    public GameObject errorMsg;
    [Header("Native ads")]
    public GameObject adNativePanel;
    public RawImage adIcon;
    public RawImage adChoices;
    public Text adHeadline;
    public Text adCallToAction;
    public Text adAdvertiser;
    [SerializeField] private AudioSource buttonClickAudioSource;

    public enum PopupType : byte
    {
        YES_NO,
        OK,
        INPUT
    }

    public void PlayButtonClickSound()
    {
        if (PlayerPrefs.GetInt(Constants.VOLUME_MUSIC, 1) == -80)
            return;
        if (buttonClickAudioSource.enabled)
        {
            if (buttonClickAudioSource.isPlaying)
                buttonClickAudioSource.Stop();
            buttonClickAudioSource.Play();
        }
    }

    public void ShowPopup(bool canShowCoins, long coins, PopupType popupType, string titleText = "", string msg = "", string yesBtnText = "", string noBtnText = "", string okBtnText = "", string inputText = "", Action<bool,string> actionCallback = null)
    {
        ShowScreen(true);
        yesBtnObj.gameObject.SetActive(popupType == PopupType.YES_NO || popupType == PopupType.INPUT);
        noBtnObj.gameObject.SetActive(popupType == PopupType.YES_NO || popupType == PopupType.INPUT);
        okBtnObj.gameObject.SetActive(popupType == PopupType.OK);
        msgObj.gameObject.SetActive(popupType == PopupType.OK || popupType == PopupType.YES_NO);
        inputObj.gameObject.SetActive(popupType == PopupType.INPUT);
        errorMsg.SetActive(false);
        this.yesBtnText.text = yesBtnText;
        this.noBtnText.text = noBtnText;
        this.okBtnText.text = okBtnText;
        this.msgText.text = msg;
        this.titleText.text = titleText;
        this.inputField.text = inputText;
        coinsObj.SetActive(canShowCoins);
        coinsText.text = coins.ToString();

        yesBtnObj.onClick.RemoveAllListeners();
        noBtnObj.onClick.RemoveAllListeners();
        okBtnObj.onClick.RemoveAllListeners();

        gameObject.SetActive(true);
        yesBtnObj.onClick.AddListener(() => {
            PlayButtonClickSound();
            errorMsg.SetActive(false);
            if (popupType == PopupType.INPUT)
            {
                if (string.IsNullOrEmpty(inputField.text))
                {
                    errorMsg.SetActive(true);
                    return;
                }
            }
            actionCallback?.Invoke(true,inputField.text);
            HideScreen(true);
        });
        noBtnObj.onClick.AddListener(() => {
            PlayButtonClickSound();
            actionCallback?.Invoke(false, inputField.text);
            HideScreen(true);
        });
        okBtnObj.onClick.AddListener(() => {
            PlayButtonClickSound();
            actionCallback?.Invoke(true, inputField.text);
            HideScreen(true);
        });
        AdmobAdsManager.Instance.ShowNativeAds(adNativePanel, adIcon, adChoices, adHeadline, adCallToAction, adAdvertiser, true);
    }
}