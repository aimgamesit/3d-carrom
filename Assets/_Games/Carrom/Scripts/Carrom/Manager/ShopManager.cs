﻿using AIMCarrom;
using com.aquimo.util;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : PopupBaseManager
{
    public GameObject shopItemPrefab;
    public Transform shopItemTransform;
    public Text coinsText;
    private ItemType curentTab = 0;
    public GameObject[] selectTabButtons;

    public enum ItemType
    {
        PUCKS,
        STRIKER,
        AVATAR
    }

    public void PopulateItems(ItemType itemType)
    {
        curentTab = itemType;
        for (int i = 0; i < selectTabButtons.Length; i++)
        {
            selectTabButtons[i].SetActive(false);
        }

        ShowScreen(!gameObject.activeInHierarchy);
        coinsText.text = Player.PlayerData.TotalCoins.ToString();
        C.ClearAllChilds(shopItemTransform);
        GameData gData = GameData.Instance;
        ShopItemDataModel[] shopItemDataModel;

        if (itemType == ItemType.PUCKS)
        {
            shopItemDataModel = gData.pucks;
            selectTabButtons[0].SetActive(true);
        }
        else if (itemType == ItemType.STRIKER)
        {
            shopItemDataModel = gData.strikers;
            selectTabButtons[1].SetActive(true);
        }
        else
        {
            shopItemDataModel = gData.avatars;
            selectTabButtons[2].SetActive(true);
        }

        for (int i = 0; i < shopItemDataModel.Length; i++)
        {
            int boughtIndex = 0;

            Debug.Log((Player.PlayerData.boughtStrikers == null) + "IS NULL: "+(Player.PlayerData.boughtAvatar == null));

            if (itemType == ItemType.PUCKS)
                boughtIndex = Player.PlayerData.boughtCarromMen.Find(x => x == i);
            else if (itemType == ItemType.STRIKER)
                boughtIndex = Player.PlayerData.boughtStrikers.Find(x => x == i);
            else
                boughtIndex = Player.PlayerData.boughtAvatar.Find(x => x == i);

            int currentSelected = 0;
            if (itemType == ItemType.PUCKS)
                currentSelected = Player.PlayerData.currentCarromMen;
            else if (itemType == ItemType.STRIKER)
                currentSelected = Player.PlayerData.currentStriker;
            else
                currentSelected = Player.PlayerData.currentAvatar;

            Debug.Log(i+". " + currentSelected+ " currentSelected "+ boughtIndex);
            int selectIndex = i, buyAmt = shopItemDataModel[i].buyCoins;

            ShopItemInfo shopItemInfo =  Instantiate(shopItemPrefab, shopItemTransform).GetComponent<ShopItemInfo>();
            
            shopItemInfo.buyButton.onClick.RemoveAllListeners();
            shopItemInfo.fullBuyButton.onClick.RemoveAllListeners();
            shopItemInfo.selectButton.onClick.RemoveAllListeners();
            if (itemType == ItemType.AVATAR)
            {
                shopItemInfo.itemImgFull.sprite = shopItemDataModel[i].sprite;
                shopItemInfo.itemImg.gameObject.SetActive(false);
            }
            else
            {
                shopItemInfo.itemImg.sprite = shopItemDataModel[i].sprite;
                shopItemInfo.itemImgFull.gameObject.SetActive(false);
            }
            shopItemInfo.lockObj.SetActive(boughtIndex == i);

            shopItemInfo.selectText.text = "Selected";
            shopItemInfo.selectedText.text = "Select";
            shopItemInfo.buyCoinsText.text = buyAmt.ToString();

            shopItemInfo.selectText.gameObject.SetActive(currentSelected == i);
            shopItemInfo.selectedText.gameObject.SetActive(currentSelected != i && boughtIndex == i);

            shopItemInfo.selectButton.interactable = (currentSelected != i && boughtIndex == i);
            shopItemInfo.lockObj.SetActive(boughtIndex != i);

            shopItemInfo.buyButton.onClick.AddListener(() => {
                OnBuyBtnClick(buyAmt, itemType, selectIndex);
            });
            shopItemInfo.fullBuyButton.onClick.AddListener(() => {
                OnBuyBtnClick(buyAmt, itemType, selectIndex);
            });

            shopItemInfo.selectButton.onClick.AddListener(() => {
                MainMenuManager.Instance.PlayButtonClickSound();
                SelectItem(itemType, selectIndex);
            });
        }
    }

    private void OnBuyBtnClick(int buyAmt, ItemType itemType, int index)
    {
        MainMenuManager.Instance.PlayButtonClickSound();
        if (Player.PlayerData.TotalCoins < buyAmt)
        {
            MainMenuManager.Instance.popupManager.ShowPopup(true, Player.PlayerData.TotalCoins, PopupManager.PopupType.YES_NO, Constants.POPUP_BUY_ITEM_FAILED_TITLE, Constants.POPUP_NOT_ENOUGH_COINS_PURCHASE_MESSAGE + "\nWatch video ad to get extra coins.", "Watch now", "No thanks", "", "", delegate (bool callback, string inputText) {
                UpdatePlayerUI();
                if (callback)
                {
                    bool isRewardReceived = false;
                    Debug.Log("OnWatchVideoBtnCLicked()");
                    if (AdmobAdsManager.Instance.IsVideoAdAvailable())
                    {
                        AdmobAdsManager._OnRewardedReceived += delegate ()
                        {
                            Debug.Log("isRewardReceived  TERU: " + isRewardReceived);
                            if (!isRewardReceived)
                            {
                                isRewardReceived = true;
                                Player.PlayerData.TotalCoins += GameData.Instance.freeReward;
                                Debug.Log("OnWatchVideoBtnCLicked: " + Player.PlayerData.TotalCoins);
                                Player.SaveDataToPrefs();
                            }
                        };
                        AdmobAdsManager._AdClosed += delegate () {
                            Debug.Log("isRewardReceived  FALSE: " + isRewardReceived);
                            if (!isRewardReceived)
                            {
                                isRewardReceived = true;
                            }
                        };
                        AdmobAdsManager.Instance.ShowVideoAd();
                    }
                    else
                    {
                        Invoke(nameof(ShowNoVideoAvailable), .5f);
                    }
                }
            });
            return;
        }
        else
        {
            Debug.Log(itemType + " BuyItem: " + buyAmt + " INDEX: " + index);
            if (itemType == ItemType.PUCKS)
                Player.PlayerData.boughtCarromMen.Add(index);
            else if (itemType == ItemType.STRIKER)
                Player.PlayerData.boughtStrikers.Add(index);
            else
                Player.PlayerData.boughtAvatar.Add(index);
            Player.PlayerData.TotalCoins -= buyAmt;
            SelectItem(itemType, index);
        }
    }

    private void ShowNoVideoAvailable()
    {
        MainMenuManager.Instance.popupManager.ShowPopup(true, Player.PlayerData.TotalCoins, PopupManager.PopupType.OK, Constants.POPUP_NO_ADS_TITLE, Constants.POPUP_REWARD_NO_ADS_AVAILABLE, "", "", "OK", Player.PlayerData.playerName, delegate (bool callback1, string inputText1) { });
    }

    private void SelectItem(ItemType itemType, int selectedIndex)
    {
        Debug.Log(itemType+" SelectItem: " + selectedIndex);

        if (itemType == ItemType.PUCKS)
            Player.PlayerData.currentCarromMen = selectedIndex;
        else if (itemType == ItemType.STRIKER)
            Player.PlayerData.currentStriker = selectedIndex;
        else
            Player.PlayerData.currentAvatar = selectedIndex;
        Player.SaveDataToPrefs();
        PopulateItems(curentTab);
        UpdatePlayerUI();
    }

    private void UpdatePlayerUI()
    {
        coinsText.text = Player.PlayerData.TotalCoins.ToString();
        MainMenuManager.Instance.UpdatePlayerUI();
    }

    public void OnTabClicked(int curentTab)
    {
        this.curentTab = (ItemType) curentTab;
        MainMenuManager.Instance.PlayButtonClickSound();
        PopulateItems(this.curentTab);
    }

    public void OnCloseBtnClicked()
    {
        //gameObject.SetActive(false);    
        MainMenuManager.Instance.PlayButtonClickSound();
        HideScreen(true);
    }
}