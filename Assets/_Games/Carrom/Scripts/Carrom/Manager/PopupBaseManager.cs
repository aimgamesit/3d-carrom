﻿using DG.Tweening;
using UnityEngine;

public class PopupBaseManager : MonoBehaviour
{
    public Transform animationTransform;

    private void DoScale(bool isScaleIn, bool scaleWithPivot = true)
    {
        Debug.Log(isScaleIn+" DOSCALE: "+scaleWithPivot);
        if(isScaleIn)
            gameObject.SetActive(isScaleIn);
        if (scaleWithPivot && isScaleIn)
        {
            Vector2 pivot = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            animationTransform.GetComponent<RectTransform>().pivot = pivot;
        }
        animationTransform.localScale = isScaleIn ? Vector3.zero : Vector3.one;
        animationTransform.DOScale(isScaleIn ? Vector3.one : Vector3.zero, GameData.Instance.popupScaleTime).onComplete += delegate () {
            gameObject.SetActive(isScaleIn);
        };
    }

    public void HideScreen(bool withAnimaton)
    {
        if (withAnimaton)
            DoScale(false, false);
        else
            gameObject.SetActive(false);
    }
    public void ShowScreen(bool withAnimaton, bool scaleWithPivot = true)
    {
        Debug.Log(withAnimaton + " DOSCsdsALE: " + scaleWithPivot);
        if (withAnimaton)
            DoScale(withAnimaton, scaleWithPivot);
        else
            gameObject.SetActive(true);
    }
}