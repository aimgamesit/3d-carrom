﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using DG.Tweening;
using com.TicTok.managers;

namespace AIMCarrom
{
    public class UiManager : BManager<UiManager>
    {
        public Text versionText;
        [SerializeField] Sprite[] difficultySprites;
        [SerializeField] Image difficultyImage;
        [SerializeField] Text difficultyText;
        public Texture defaultProfilePic;
        public Sprite unknownFlagSprite;
        [SerializeField] Color[] AllColourForTimer;
        [SerializeField] AudioSource TimerTickingSource;
        [SerializeField] private Image slider1;
        [SerializeField] private Image slider2;
        #region UI REFERENCES
        public Text playerCoinsText;
        public GameObject loadingScreen;
        public HUDScreenUI hUDScreenUI;
        public GameplayTheme gameplayTheme;
        public WinnerScreenManager winnerScreenManager;
        public PopupManager popupManager;
        #endregion
        internal int currentSelectedTheme;
        internal int currentTournamentTheme;
        private int opponenetAvatarIndex;
        private Coroutine avatarCoroutine;
        private Coroutine searchingTextCoroutine;
        private Coroutine onOpponentFoundCoroutine;
        [Space(20)]
        [SerializeField] private UnityEngine.U2D.SpriteAtlas flagAtlas;
        [SerializeField] private Sprite defaultFlagSprite;
        public Toggle soundToggle;

        void Start()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            loadingScreen.SetActive(true);
            UpdateUserDetails();
            SelectTheme(GameData.Instance.selectedThemeIndex);
            difficultyImage.sprite = difficultySprites[(int)GameData.Instance.aIDifficultyType];
            if (GameData.Instance.aIDifficultyType == AIDifficultyType.HARD)
                difficultyText.text = "HARD";
            else if (GameData.Instance.aIDifficultyType == AIDifficultyType.MEDIUM)
                difficultyText.text = "MEDIUM";
            else
                difficultyText.text = "EASY";
            soundToggle.isOn = !GameManager.Instance.AudioManager.IsMuted();
            GameManager.Instance.AudioManager.SetSoundVolume(GameManager.Instance.AudioManager.IsMuted());
            soundToggle.onValueChanged.AddListener((bool isOn) =>
            {
                GameManager.Instance.AudioManager.PlaySfx(SfxClips.ButtonClick);
                GameManager.Instance.AudioManager.SetSoundVolume(!isOn);
            });
            SetVersionText();
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        private void SetVersionText() => versionText.text = "Version " + Application.version;

        public void SelectTheme(int theme = 0)
        {
            currentSelectedTheme = theme;
            GameManager.Instance.OpponentFound = false;
            GameData gData = GameData.Instance;
            Player.PlayerData.bet = gData.themeData[currentSelectedTheme].themeEntryFee;
            Debug.Log($"Theme: {theme} Current Bet: {Player.PlayerData.bet}");
            StartGame(theme);
        }

        public void StartGame(int themeIndex)
        {
            GameManager.Instance.isLocal = true;
            LoadGamePlayTheme(themeIndex);
            StartCoroutine(GameManager.Instance.JoinOfflineCoroutine());
        }

        public void LoadGamePlayTheme(int themeIndex)
        {
            GameData gData = GameData.Instance;
            Debug.LogFormat($"<color=red>{themeIndex} theme loaded</color>");
            //gameplayTheme.watermarkSprite.sprite = gData.themeData[themeIndex].watermark;
            hUDScreenUI.strikerGlow.color = gData.themeData[themeIndex].glowColor;
            hUDScreenUI.strikerGlow2.color = gData.themeData[themeIndex].glowColor;
            hUDScreenUI.prizeMoney.text = gData.themeData[themeIndex].themePrize.ToString();
            hUDScreenUI.prizeImage.SetActive(true);
            hUDScreenUI.playerStrikerSlider.gameObject.SetActive(true);
            hUDScreenUI.opponentStrikerSlider.gameObject.SetActive(false);
        }

        public void OnExitGame()
        {
            Debug.Log("StopGame");
            SetTimerTickSound(false);
            if (onOpponentFoundCoroutine != null)
                StopCoroutine(onOpponentFoundCoroutine);
            Debug.LogFormat("opponent found Coroutine stopped");
        }

        #region GAMEPLAY UPDATES
        internal void OnUpdateTime(float time01)
        {
            float fillTimerAmount = 1 - time01;
            Color fillColor = Color.white;

            if (GameManager.Instance.IsMyTurn)
            {
                if (fillTimerAmount > 0.97f)
                {
                    fillColor = Color.white;
                    //CanPlayTimer = true;
                    if (!CanPlayTimer)
                    {
                        Debug.Log("Timer Controller: <color=magenta> CanPlayTimer set to true</color>");
                        CanPlayTimer = true;
                    }

                    if (IsSoundPlaying)
                        SetTimerTickSound(false);
                }

                if (fillTimerAmount < 0.3f)
                {
                    fillColor = Color.Lerp(slider1.color, AllColourForTimer[2], Time.deltaTime);
                    slider1.gameObject.GetComponent<Animator>().SetBool("Play", true);
                    SetTimerTickSound(true);
                }
                else if (fillTimerAmount < 0.55f)
                {
                    fillColor = Color.Lerp(slider1.color, AllColourForTimer[1], Time.deltaTime * 5f);
                }
                else if (fillTimerAmount < 0.97f)
                {
                    fillColor = Color.Lerp(slider1.color, AllColourForTimer[0], Time.deltaTime * 2f);
                }
                slider1.color = fillColor;
                slider1.fillAmount = fillTimerAmount;
                slider2.fillAmount = 0;
            }
            else
            {
                if (fillTimerAmount > 0.97f)
                {
                    fillColor = Color.white;
                    if (!CanPlayTimer)
                    {
                        Debug.Log("Timer Controller: <color=magenta> CanPlayTimer set to true</color>");
                        CanPlayTimer = true;
                    }

                    if (IsSoundPlaying)
                        SetTimerTickSound(false);
                }
                if (fillTimerAmount < 0.3f)
                {
                    fillColor = Color.Lerp(slider2.color, AllColourForTimer[2], Time.deltaTime);
                    slider2.gameObject.GetComponent<Animator>().SetBool("Play", true);
                    SetTimerTickSound(true);
                }
                else if (fillTimerAmount < 0.55f)
                {
                    fillColor = Color.Lerp(slider2.color, AllColourForTimer[1], Time.deltaTime * 5f);
                }
                else if (fillTimerAmount < 0.97f)
                {
                    if (!CanPlayTimer)
                    {
                        Debug.Log("Timer Controller: <color=magenta> CanPlayTimer set to true</color>");
                        CanPlayTimer = true;
                    }

                    if (IsSoundPlaying)
                        SetTimerTickSound(false);
                    fillColor = Color.Lerp(slider2.color, AllColourForTimer[0], Time.deltaTime * 2f);
                }
                slider1.fillAmount = 0;
                slider2.color = fillColor;
                slider2.fillAmount = fillTimerAmount;
            }
        }

        public void UpdatePlayerTimer(int id, float fillAmmount)
        {
            if (id < 0)
                return;
            hUDScreenUI.playerHudInfoUIs[id].playerTimer.fillAmount = fillAmmount;
            if (fillAmmount > 0.97f)
            {
                hUDScreenUI.playerHudInfoUIs[id].playerTimer.color = Color.white;
                CanPlayTimer = true;
            }
            else if (fillAmmount < 0.3f)
            {
                //hUDScreenUI.playerHudInfoUIs[id].playerTimer.color = AllColourForTimer[2];
                hUDScreenUI.playerHudInfoUIs[id].playerTimer.color = Color.Lerp(hUDScreenUI.playerHudInfoUIs[id].playerTimer.color, AllColourForTimer[2], Time.deltaTime);
                hUDScreenUI.playerHudInfoUIs[id].playerTimer.GetComponent<Animator>().SetBool("Play", true);
                SetTimerTickSound(true);
            }
            else if (fillAmmount < 0.55f)
            {
                hUDScreenUI.playerHudInfoUIs[id].playerTimer.color = Color.Lerp(hUDScreenUI.playerHudInfoUIs[id].playerTimer.color, AllColourForTimer[1], Time.deltaTime * 5f);
                //hUDScreenUI.playerHudInfoUIs[id].playerTimer.color = AllColourForTimer[1];
            }
            else if (fillAmmount < 0.97f)
            {
                hUDScreenUI.playerHudInfoUIs[id].playerTimer.color = Color.Lerp(hUDScreenUI.playerHudInfoUIs[id].playerTimer.color, AllColourForTimer[0], Time.deltaTime * 2f);
                //hUDScreenUI.playerHudInfoUIs[id].playerTimer.color = AllColourForTimer[0];
            }
        }
        internal bool IsSoundPlaying;
        internal bool CanPlayTimer;
        public void SetTimerTickSound(bool SetTo)
        {
            //return;
            if (IsSoundPlaying != SetTo)
            {
                IsSoundPlaying = SetTo;
                if (IsSoundPlaying)
                {
                    if (CanPlayTimer)
                    {
                        TimerTickingSource.Play();
                        CanPlayTimer = false;
                    }
                }
                else
                {
                    TimerTickingSource.Stop();
                }
            }
        }

        float playTime;

        public void SetPlayTime(float time01)
        {
            if (time01 < 1.0f)
            {
                playTime = time01;
                OnUpdateTime(playTime);
            }
            else
            {
                EndTime();
            }
        }
        private void EndTime()
        {

            Debug.Log("<color=red>Sending event for EndTime stopped timer sound</color>");
            SetTimerTickSound(false);
            playTime = 1.0f;
        }
        public void OnTurnEnd()
        {
            for (int i = 0; i < hUDScreenUI.playerHudInfoUIs.Length; i++)
            {
                hUDScreenUI.playerHudInfoUIs[i].playerTimer.fillAmount = 0;
                hUDScreenUI.playerHudInfoUIs[i].playerTimer.GetComponent<Animator>().SetBool("Play", false);
            }
            SetTimerTickSound(false);
        }
        #endregion

        public void UpdateUserDetails()
        {
            //playerInfoBarUI.playerName.text = Player.PlayerData.playerName;
            hUDScreenUI.playerHudInfoUIs[0].playerName.text = Player.PlayerData.playerName;
            hUDScreenUI.playerHudInfoUIs[0].playerAvatar.sprite = GameData.Instance.avatars[Player.PlayerData.currentAvatar].sprite;
            playerCoinsText.text = Player.PlayerData.TotalCoins.ToString();
            Debug.Log($"Updating player name to :{Player.PlayerData.playerName}");
        }

        public void UpdateScore()
        {
            Debug.Log("Update Score Called!!");
            var game = GameManager.Instance.GamePlayHandler;
            for (int i = 0; i < game.playerInfo.Count; i++)
            {
                hUDScreenUI.playerHudInfoUIs[i].playerScore.text = game.playerInfo[GameManager.Instance.GamePlayHandler.GetUiIndex(i)].pocketedCarromMen.Length.ToString();
                hUDScreenUI.playerHudInfoUIs[i].queenToggle.isOn = game.playerInfo[GameManager.Instance.GamePlayHandler.GetUiIndex(i)].hasQueen;
            }
        }

        public void UpdateScoreBoard()
        {
            Debug.Log("Update Score Called!!");
            var game = GameManager.Instance.GamePlayHandler;
            for (int i = 0; i < game.playerInfo.Count; i++)
            {
                hUDScreenUI.playerHudInfoUIs[i].playerScore.text = game.playerInfo[GameManager.Instance.GamePlayHandler.GetUiIndex(i)].pocketedCarromMen.Length.ToString();
                hUDScreenUI.playerHudInfoUIs[i].queenToggle.isOn = game.playerInfo[GameManager.Instance.GamePlayHandler.GetUiIndex(i)].hasQueen;
            }
            UpdatePlayerInfoBox(hUDScreenUI.playerHudInfoUIs[0], GameManager.Instance.GamePlayHandler.playerInfo, 0, Player.PlayerData.currentAvatar);
            opponenetAvatarIndex = UnityEngine.Random.Range(0, GameData.Instance.avatars.Length - 1);
            UpdatePlayerInfoBox(hUDScreenUI.playerHudInfoUIs[1], GameManager.Instance.GamePlayHandler.playerInfo, 1, opponenetAvatarIndex);
        }

        public void UpdatePlayerInfoBox(PlayerHudInfoUI playerHudInfoUI, IList<PlayerInfo> players, int index, int avatarIndex)
        {
            Debug.Log($"Old index: {index}");
            index = GameManager.Instance.GamePlayHandler.GetUiIndex(index);
            Debug.Log("Updated index " + index);
            var playerData = players[index].playerData;
            Debug.Log("Player Data: " + playerData.imageURL + " " + playerData.localName);
            playerHudInfoUI.playerAvatar.sprite = GameData.Instance.avatars[avatarIndex].sprite;
            playerHudInfoUI.playerName.text = playerData.localName;
            playerHudInfoUI.betAmount.text = playerData.bet.ToString();
            //if(index < 1)
            //opponentProfile.opponentViewInfoUI.playerPhoto.texture = Player.GetUserImage(playerData.imageURL);
            Debug.Log($"playerHudInfo: {playerHudInfoUI.playerCarromMenImage == null}");
            GameData gData = GameData.Instance;
            if (playerHudInfoUI.playerCarromMenImage != null)
            {
                int num = GameManager.Instance.playersPuckIndices[index];
                //for(int i=0; i< pucks.Length; i++)
                //{
                //    Debug.Log($"puck:{i}:: {pucks[i].name}");
                //}
                playerHudInfoUI.playerCarromMenImage.sprite = gData.pucks[num].sprite;
                Debug.Log($"num: {num}, updating puck: {gData.pucks[num].sprite.name} for player {index}");
            }
        }

        #region PICTURE
        private IEnumerator FetchProfilePic(Uri url, Action<bool, string> callback, Action<bool, string, string> callback2, string ID)
        {
            UnityWebRequest unityWebRequest = UnityWebRequestTexture.GetTexture(url);
            yield return unityWebRequest.SendWebRequest();

            if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
            {
                callback?.Invoke(false, "");
            }
            else
            {
                Texture2D myTexture = ((DownloadHandlerTexture)unityWebRequest.downloadHandler).texture;
                var image = Convert.ToBase64String(myTexture.EncodeToPNG());
                Debug.Log("AVATAR UPDATED");
                callback?.Invoke(true, image);
                callback2?.Invoke(true, image, ID);
            }
        }
        #endregion

        #region OPPONENT SEARCH SCREEN FUNCTIONS
        int counterSearchText = 0;
        private void AnimateSearchingText(float time, Text tMP_Text)
        {
            counterSearchText = counterSearchText > 2 ? 0 : counterSearchText;
            searchingTextCoroutine = StartCoroutine(AnimateSearchingTextEnumerator(time, tMP_Text));
        }

        IEnumerator AnimateSearchingTextEnumerator(float second, Text tMP_Text)
        {
            yield return new WaitForSeconds(second);
            Text text_ = tMP_Text;
            text_.text = Constants.SEARCHING_SCREEN_TEXT[counterSearchText];
            text_.gameObject.GetComponent<CanvasGroup>().alpha = 0;
            text_.gameObject.GetComponent<CanvasGroup>().DOFade(1, 1f);
            counterSearchText++;
            AnimateSearchingText(second, tMP_Text);
        }

        public void OnOpponentFound(IList<PlayerInfo> playerInfos)
        {
            Debug.Log($"<color=green>Calling Coroutine On Opponent Found.</color>");
            //GameManager.Instance.AudioManager.StopSfx();
            GameManager.Instance.AudioManager.PlaySfx(SfxClips.opponentFound);
            StopAvatarAnim();
            GameManager.Instance.GamePlayHandler.StartCarromGame();
            //Player.PlayerData.TotalCoins -= Player.PlayerData.bet;
            //Player.SaveDataToPrefs();
            UpdateUserDetails();
            Invoke(nameof(HideLoadingScreen), .1f);
        }

        private void HideLoadingScreen()
        {
            loadingScreen.SetActive(false);
            //GameManager.Instance.AudioManager.StopSfx();
        }

        private void StopAvatarAnim()
        {
            if (avatarCoroutine != null)
                StopCoroutine(avatarCoroutine);

            if (searchingTextCoroutine != null)
                StopCoroutine(searchingTextCoroutine);
        }
        #endregion

        public Sprite GetSprite(int index, bool isStriker = false)
        {
            GameData gData = GameData.Instance;
            return isStriker ? gData.strikers[index].sprite : gData.pucks[index].sprite;
        }

        public void ChangeSliderStriker()
        {
            //Debug.Log($"Player Index: {GameManager.Instance.GamePlayHandler.MyIndex}, Count:{GameManager.Instance.GamePlayHandler.playerInfo.Count}");
            int strikerIndex = GameManager.Instance.GamePlayHandler.playerInfo[GameManager.Instance.GamePlayHandler.MyIndex].playerData.currentSelectedStriker;
            //hUDScreenUI.sliderHandle.sprite = strikers[IsEnable ?  strikerIndex: 0];
            hUDScreenUI.sliderHandle.sprite = GameData.Instance.strikers[strikerIndex].sprite;
        }

        #region Click Button Event
        public void OnShareBtnClicked()
        {
            GameManager.Instance.AudioManager.PlaySfx(SfxClips.ButtonClick);
            AIMPluginManager.Instance.ShareApp(true);
            FirebaseManager.Instance.SendEvent(Constants.CARROM_SHARE);
        }

        public void OnRateUsBtnClicked()
        {
            AIMPluginManager.Instance.ShowRateUsPopup();
        }

        public void OnExitBtnClicked()
        {
            GameManager.Instance.AudioManager.PlaySfx(SfxClips.ButtonClick);
            popupManager.ShowPopup(false, Player.PlayerData.TotalCoins, PopupManager.PopupType.YES_NO, Constants.POPUP_EXIT_GAME_TITLE, Constants.POPUP_EXIT_GAME_MESSAGE, "YES", "NO", "OK", "", delegate (bool callback, string inputText)
            {
                if (callback)
                {
                    GameManager.Instance.OnLeaveGameByClickExitBtn(true);
                    FirebaseManager.Instance.SendEvent(Constants.CARROM_GAME_EXIT_YES);
                }
                else
                {
                    FirebaseManager.Instance.SendEvent(Constants.CARROM_GAME_EXIT_NO);
                }
            });
            FirebaseManager.Instance.SendEvent(Constants.CARROM_GAME_EXIT);
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        public void ShowWinnerScreen(List<PlayerInfo> playerList, int winIndex)
        {
            long prize = GameData.Instance.themeData[GameData.Instance.selectedThemeIndex].themePrize;
            Debug.Log(winIndex + "  PRIZE AMT: " + prize + " Winner: " + playerList[winIndex].playerData.localName + " pocketedCarromMen: " + playerList[winIndex].pocketedCarromMen.Length + " hasQueen: " + playerList[winIndex].hasQueen);
            winnerScreenManager.ShowWinnerScreen(playerList, winIndex, prize, opponenetAvatarIndex);
            UpdateUserDetails();
            GameManager.Instance.AudioManager.PlaySfx(winIndex == 0 ? SfxClips.Winner : SfxClips.Looser);
        }
        #endregion
    }


    [System.Serializable]
    public struct HUDScreenUI
    {
        public GameObject hudScreenView;
        public Image sliderHandle;
        public GameObject prizeImage;
        public Text prizeMoney;
        public PlayerHudInfoUI[] playerHudInfoUIs;
        public Image strikerGlow;
        public Image strikerGlow2;
        public Slider playerStrikerSlider;
        public Slider opponentStrikerSlider;
        public CanvasGroup[] strikerSlidersCanvasGroups;
    }

    [System.Serializable]
    public struct PlayerHudInfoUI
    {
        public Text playerName;
        public Image playerAvatar;
        public Text playerScore;
        public Image playerCarromMenImage;
        public Toggle queenToggle;
        public Image playerTimer;
        public Text playerLevel;
        public Text betAmount;
    }

    [System.Serializable]
    public struct GameplayTheme
    {
        public Image gameplayBackground;
        public SpriteRenderer carromBoard;
        public SpriteRenderer carromBoardBorder;
        public SpriteRenderer watermarkSprite;
    }

    [System.Serializable]
    public struct BotInfo
    {
        public int botId;
        public string userName;
        public string localName;
        public string flag;
    }

    public struct BotInfoArray
    {
        public BotInfo[] infos;
    }
}