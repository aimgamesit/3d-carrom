﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Mirror;
using System;
using DG.Tweening;

namespace AIMCarrom
{
    public class TransformSync : MonoBehaviour      //Initially inheriting NetworkBehaviour.cs
    {
        public bool syncRotationOnFixedUpdate;
        public bool syncRotation;
        public bool syncPositionAtEndTurn;
        public float sendInterval = 0.05f;

        private Rigidbody2D carromBody;

        private float lastDragTime;
        private bool cachedLastPostion;
        private float currentOpponentDragTime;
        private Vector3 lastPos;
        private Vector3 lastVel;
        private bool isSync;
        public bool isPot;
        private bool isServerInSync;

        //[SyncVar(hook = nameof(OnPosChangeClient))]
        private Vector3 OnPosChange;
        //[SyncVar(hook = nameof(OnRotChangeClient))]
        private Vector3 OnRotChange;

        private void Awake()
        {
            carromBody = GetComponent<Rigidbody2D>();

        }

        public void OnStartServer()
        {
            ///base.OnStartServer();        //NetworkBehaviour.cs
            Debug.Log("Transform Sync:sOnStartServer()");
        }

        void Start()
        {
            OnPosChange = transform.position;
            OnRotChange = transform.eulerAngles;

            carromBody.velocity = Vector3.zero;
            carromBody.angularVelocity = 0;
        }

        private void OnEnable()
        {
            GamePlayHandler.onTurnEnd += OnTurnEnd;
        }

        private void OnDisable()
        {
            GamePlayHandler.onTurnEnd -= OnTurnEnd;
        }

        /*
         * make it a photon Rpc called on position change
        private void OnPosChangeClient(Vector3 oldPos, Vector3 pos)
        {
            StopObject();

            if (syncPositionAtEndTurn)
                transform.position = pos;


        }*/

        /*[ServerCallback]
        private void FixedUpdate()
        {
            if (syncRotationOnFixedUpdate)
                //OnRotChange = transform.eulerAngles;
                StartCoroutine(RotationSyncCoroutine(transform.eulerAngles, 0.5f));
                
        }*/

            /*
             * make it a photon Rpc callback .. called when other player updated rotation
        IEnumerator RotationSyncCoroutine(Vector3 finalRot, float lerpTime)
        {
            float timeElapsed = 0;
            Vector3 startRot = OnRotChange;
            while (timeElapsed < lerpTime)
            {
                OnRotChange = Vector3.Lerp(startRot, finalRot, 0.5f);
                yield return null;
            }
        }*/

        /*
         * make it a photon rpc called pn rotation change
        private void OnRotChangeClient(Vector3 oldPos, Vector3 pos)
        {
            carromBody.velocity = Vector3.zero;
            carromBody.angularVelocity = 0;
            transform.eulerAngles = pos;
        }*/

        private void OnTurnEnd()
        {
            OnPosChange = transform.position;
            if (syncRotation)
                OnRotChange = transform.eulerAngles;
        }

        /*[ServerCallback]  Track carrom body of other player using Photon RPC
        private void Update()
        {
            if (!carromBody.IsSleeping())
            {
                isServerInSync = true;
                if (Time.time - lastDragTime > sendInterval)
                {
                    lastDragTime = Time.time;
                   // RpcMove(carromBody.position, carromBody.velocity);
                }
            }
            else if (isServerInSync)
            {
                isServerInSync = false;
                //RPC callback for photon to stop Sync
               // RpcStopSync(transform.position);
            }
        }*/

        //[ClientRpc(channel = Channels.DefaultUnreliable)]
        private void RpcMove(Vector2 pos, Vector2 vel)
        {
            isSync = true;
            if (!cachedLastPostion)
            {
                cachedLastPostion = true;
            }
            else
            {
                carromBody.isKinematic = true;
                currentOpponentDragTime = Vector3.Distance(transform.position, pos);
              //  carromBody.MovePosition(pos);
            }
            lastVel = vel;
            lastPos = pos;
        }

       /* [ClientRpc(channel = Channels.DefaultReliable)]
        private void RpcStopSync(Vector3 pos)
        {
            //if (isSync)
            //{
                StopObject();
                transform.position = pos;
            //}
        }*/

       /* [ClientCallback]
        private void LateUpdate()
        {
            if (isSync)
            {
                transform.position = Vector3.MoveTowards(transform.position, lastPos, currentOpponentDragTime / sendInterval * Time.deltaTime);
                if(!isPot)
                    carromBody.velocity = Vector3.MoveTowards(carromBody.velocity, lastVel, currentOpponentDragTime / sendInterval * Time.deltaTime);
            }
        }*/

        private void StopObject()
        {
            StopRigidbody();
            cachedLastPostion = false;
            isSync = false;
            currentOpponentDragTime = 0;
        }

        public void StopRigidbody()
        {
            lastVel = Vector3.zero;
            carromBody.velocity = Vector3.zero;
            carromBody.angularVelocity = 0;
        }

        public void ResetSync()
        {
            isPot = false;
        }
    }
}