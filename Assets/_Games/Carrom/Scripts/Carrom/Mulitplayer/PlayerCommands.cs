﻿using UnityEngine;

namespace AIMCarrom
{
    public class PlayerCommands : MonoBehaviour
    {
        void Start()
        {
            Debug.Log($"PlayerCommands.cs :{gameObject.name}");
        }
 
        public void Shoot(Vector3 pos, Vector2 vec, int index)
        {
            CmdShootStriker(pos, vec, index);
        }

        //[Command]
        public void CmdShootStriker(Vector3 pos, Vector2 vec, int index)
        {
            GameManager.Instance.GamePlayHandler.StrikerControls.Shoot(pos, vec, index);            

        }

        public void MoveStriker(float val, int index, bool isInstant, float lerpRate = 0.15f)
        {
            CmdMoveStriker(val, index, isInstant, lerpRate);
        }

        //[Command(channel = Channels.DefaultReliable)]
        public void CmdMoveStriker(float val, int index, bool isInstant, float lerpRate)
        {
            GameManager.Instance.GamePlayHandler.StrikerControls.MoveStriker(val, index, isInstant, lerpRate);
        }

        public void MoveDirection(Vector3 dir, float size, int index)
        {
            CmdMoveDirection(dir, size, index);
        }

        //[Command(channel = Channels.DefaultUnreliable)]
        public void CmdMoveDirection(Vector3 dir, float size, int index)
        {
            GameManager.Instance.GamePlayHandler.StrikerControls.MoveDirection(dir, size, index);
        }
    }
}
