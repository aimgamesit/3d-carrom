﻿using UnityEngine;
using UnityEngine.UI;

public class ThemeItemInfo : MonoBehaviour
{
    public ScrollItemSmoothScale scrollItemSmoothScale;
    public Button clickButton;
    public Button clickButton_1;
    public Text themeName;
    public Text themeEntryFee;
    public Text themePrize;
    public Text queenPrize;
    public Image bg;
    public Image titleIMG;
    public Image watermark;
    public Color glowColor;
}
