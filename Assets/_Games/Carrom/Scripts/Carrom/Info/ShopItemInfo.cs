﻿using UnityEngine;
using UnityEngine.UI;

public class ShopItemInfo : MonoBehaviour
{
    public Button buyButton;
    public Button fullBuyButton;
    public Button selectButton;
    public Image itemImg;
    public Image itemImgFull;
    public GameObject lockObj;
    public Text buyCoinsText;
    public Text selectText;
    public Text selectedText;
}