using UnityEngine;
using UnityEngine.UI;

public class SplashManager : MonoBehaviour
{
    public Text versionText;

    void Start()
    {
        SetVersionText();
    }

    private void SetVersionText() => versionText.text = "Version " + Application.version;
}