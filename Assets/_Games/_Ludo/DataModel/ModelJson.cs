﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class ModelJson : MonoBehaviour
{

    [Serializable]
    public class ProfilePic
    {       
        public ProfilePicData[] data;
    }

    [Serializable]
    public class ProfilePicData
    {
        public string imageUrl;
        public string Path;
    }

    [Serializable]
    public class CountryData
    {
        public string CountryId;
        public string CountryName;
        public string CountryFlag;
    }

    [Serializable]
    public class CurrentRoomData
    {
        public string Room;
        public string UserId;
        public string Lobby;
        public string Boot;
        public byte MaxPlayer;
        public string GameMode;
    }

    [Serializable]
    public class UploadMediaModel
    {
        public string Msg;
        public bool status;
        public string userId;
        public string picUrl;
    }

    [Serializable]
    public class CountryInputData
    {
        public int CountryId;
      
    }

    [Serializable]
    public class UpdateProfilePicName
    {
        public string success;
        public string message;
        public UpdateProfilePicNameData[] data;
    }
     
    [Serializable]
    public class UpdateProfilePicNameData
    {
        public string Path;
        public string UserName;
        public string imageUrl;
    }

    [Serializable]
    public class GoldInAppPurchase
    {
        public string success;
        public string message;
        public GoldInAppPurchaseData[] data;
    }

    [Serializable]
    public class GoldInAppPurchaseData
    {
        public string PurType;
        public Int32 PlanId;
        public string PlanName;
        public string PlanFor;
        public decimal PlanPrice;
        public decimal Extra;
        public long Coins;
        public string StoreKey;
        public string SpAction;
        public bool IsActive;
        public string Msg;
        public bool Status;
        public string PlanType;
        public string UserId;
        public string Name;
        public string PlayerId;
        public string PaymentKey;
        public string PurchaseDate;
        public string paymenttype;
        public string Platform;

    }


    [Serializable]
    public class LedgerDataModel
    {
        public string success;
        public string message;
    }

    [Serializable]
    public class NotificationDataModel
    {
        public string success;
        public string message;
    }

    [Serializable]
    public class MessageNotification
    {
        public string success;
        public string message;
        public MessageData[] data;
    }


    [Serializable]
    public class MessageData
    {

        public string id;
        public string title;
        public string msg;
        public string startDate;
        public string stopDate;
        public string type;
        public bool isForcefully;
    }


    [Serializable]
    public class ReedemVideoBonus
    {
        public string success;
        public string message;
        public ReedemVideoBonusData data;
    }

    [Serializable]
    public class ReedemVideoBonusData
    {
        public string userId;
        public string bonusGold;
        public string totalGold;
        public string lastRedeemed;
        public string currentDate;
        public string upComingRedeemOn;
    }

    [Serializable]
    public class GetUserTotalGold
    {
        public string success;
        public string message;
        public GetUserTotalGoldData data;
    }

    [Serializable]
    public class GetUserTotalGoldData
    {
        public string UserId;
        public string Gold;
        public string TotalGold;
        public string Level;
        public string TotalEarning;
        public string League;
        public string GamesWon;
        public string GamePlayed;
        public string Player4Win;
        public string Player2Win;
        public string WinRate;
        public string PlayerId;
    }


    public class UserProfile
    {
        public string UserId;
        public string Name;
        public string Photo;
        public string CountryFlag;
        public string Gold;
        public string TotalGold;
        public string Level;
        public string TotalEarning;
        public string League;
        public string GamesWon;
        public string GamePlayed;
        public string Player4Win;
        public string Player2Win;
        public string WinRate;
        public string PlayerId;
        public string LobbyName;
        public string RoomName;
        public string GameVariation;
    }

    [Serializable]
    public class FreindListResponseData
    {
        public string userId ;
        public string facebookId ;
        public string name ;
        public string pic ;
    }

    [Serializable]
    public class RoomProperties
    {
        public string LobbyName;
        public string BetId;
        public string BetType;
        public string GameMode;
        public string BetAmount;
        public int MaxPlayer;
        public string GameRoom;
    }

    [Serializable]
    public class FreindsList
    {       
        public List<FreindListResponseData> Data ;
    }

    [Serializable]
    public class BuddyListResponseData
    {
        public string userId;
        public string facebookId;
        public string name;
        public string pic;
    }

    [Serializable]
    public class BuddyList
    {
        public List<BuddyListResponseData> data;
    }

    [Serializable]
    public class ChallengeFreind
    {
        public bool success;
        public string message;
    }

    [Serializable]
    public class ChallengeFreindNotificationResponseData
    {
        public string TableCode;
        public string BetAmt;
        public string UserName;
        public string Pic;
    }

    [Serializable]
    public class GoldTransferResponse
    {
        public string Msg;
        public string UserName;
        public string DeviceToken;
        public string Gold;
        public string UserId;
        public string PlayerId;
    }

    [Serializable]
    public class LeaguePlayersList
    {
        public string success;
        public string message;
        public LeaguePlayersListData[] data;
    }

    [Serializable]
    public class LeaguePlayersListData
    {
        public string userId;
        public string userName;
        public string photo;
        public string countryName;
        public string countryFlag;
        public string currentDate;
        public string leagueDate;
        public string roomName;
        public string goldPrize;
        public Int16 waitMinute; 
    }

    [Serializable]
    public class PlayerPinProperties
    {
        public int PlayerId;
        public int PinIndex;
        public int CurrentBoardIndex;
    }

    [Serializable]
    public class PlayerWinProperties
    {
        public int PlayerWinCount;
        public int PlayerId;
    }

    [Serializable]
    public class PlayerGameProperties
    {
        public int PlayerID;
        public bool OpponentPinBite;
        public bool IsContainerAlreadyWins;
        public int PlayerWinCount;
        public List<bool> PinWinStatus;
    }


    [Serializable]
    public class PlayerBiteData
    {
        public string SelfUserId { get; set; }
        public string OpponentUserId { get; set; }
        public List<string> myPlayerPinIndex { get; set; }
        public List<string> OpponentPinIndex { get; set; }
        public bool IsOpponentPinBite { get; set; }
        public ModelSocialProfile.PinMoveDoneModel OpponetData { get; set; }

    }

    [Serializable]
    public class PlayerDetailsGamePlay
    {
        public string player2Win;
        public string winRate;
        public string countryName;
        public string countryFlag;
        public string isCanBuddy;
        public string isDirectBuddy;
        public string userName;
        public string level;
        public string totalEarning;
        public string gold;
        public string playerId;
        public string league;
        public string gamesWon;
        public string player4Win;
    }

    [Serializable]
    public class PinToMove
    {
        public string UserId;
        public int Dice_Number;
        public int PinIndex;
        public int PosFrom;
        public int PosTo;
        public string ActionTime;
        public List<PinStartAndEndPositions> PinStartEndPos;
    }

    [Serializable]
    public class PinStartAndEndPositions
    {
        public string PinIndex;
    }

    [Serializable]
    public class PlayerGamePlayerResult
    {      
        public PlayerDetailsGamePlay data;
    }

    public class BuddyRemoveResult
    {
        public string success;
        public string message;
        public PlayerDetailsGamePlay data;
    }

    [Serializable]
    public class WithdrawalResponseModel
    {
        public string success;
        public string message;
        public WithdrawalResponseModelData data;
    }

    [Serializable]
    public class WithdrawalResponseModelData
    {
        public string userId;
        public string gold;
        public string msg;
    }

    [Serializable]
    public class Bet
    {
        public string success;
        public string message;
        public BetData[] data;
    }

    [Serializable]
    public class BetData
    {
        public string BetAmt;
    }

    [Serializable]
    public class IntentDataString
    {
        public string intentUrl;
    }

    [Serializable]
    public class IntentDataModel
    {
        public bool success;
        public string message;
        public IntentDataString data;
    }



    [Serializable]
    public class ContactInfoModel
    {
        public string phoneNo;
        public string email;
        public string twiter;
        public string facebook;
        public string fbmessanger;
        public string instagram;
        public string telegram;
        public string youTube;
        public string web;
        public string privacyUrl;
        public string termsUrl;
    }


    [Serializable]
    public class InAppPurchase
    {
        
        public InAppPurchaseData data;
    }

    [Serializable]
    public class InAppPurchaseData
    {
        public string gold;        
        public string userId;
    }
    [Serializable]
    public class VideoBonusGold
    {
        public string success;
        public string message;
        public VideoBonusGoldData data;
    }

    [Serializable]
    public class VideoBonusGoldData
    {
        public string BonusGold;      
    }

    [Serializable]
    public class TeamUp
    {
        public string success;
        public string message;
        public TeamUpData data;
    }

    [Serializable]
    public class TeamUpData
    {
        public string Lobby;
        public string GameVariation;
        public string Bet;
        public string UserId;
        public string ShareCode;
        public string RoomName;
        public string IsUsed;
    }

    [Serializable]
    public class MovablePins
    {
        public List<int> MovablePin;
    }

    [Serializable]
    public class CheckUserStatus
    {
        public string success;
        public string message;
        public CheckUserStatusData data;
    }

    [Serializable]
    public class CheckUserStatusData
    {
        public string msg;
        public string status;       
    }

    [Serializable]
    public class ReferalAmountUpdate
    {
        public string EarnGold;
        public string UserName;
        public string deviceToken;
        public string gold;      
    }

    [Serializable]
    public class WithdrawlReject
    {
        public string UserName;
        public string deviceToken;
        public string gold;
    }
    [Serializable]
    public class ReferalStatus
    {
        public bool Status;
        public string Msg;
    }

    [Serializable]
    public class WithdrawalResponseData
    {
        public string UserId;
        public long CurrentGold;

        public long WinningBal;
        public long BonusBal;
        public long DepositBal;
        public long Gold;
    }

    [Serializable]
    public class ShopDataModel
    {
        public string Id;
        public int Coins;
        public int Cost;
        public string Url;
    }

    public class SignUpVerificationDataModel
    {
        public string OTP;
        public string UserId;
        public string MobileNo;
    }

    [Serializable]
    public class GameSliderModel
    {
        public string Id;
        public string ImageUrl;
        public string SliderText;
        public string Url;
    }
    
}

