﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class ModelSocialProfile : MonoBehaviour
{

    [Serializable]
    public class Playerdata
    {
        public string UserId;
        public string UserName;
        public long Chips;
        public string ColorIndex;
        public int BetAmount;
        public int position;
        public int UndoCount;
    }

    [Serializable]
    public class PlayerdataOffline
    {
        public string UserName;
        public int ColorIndex;
        public int position;
    }

    [Serializable]
    public class PositionModel
    {
        public int Pos;
    }

    [Serializable]
    public class PinMoveDoneModel
    {
        public List<PinIndex> PinIndexes;
        public string UserId;
        public int Dice_Number;
        public bool IsMove;
        public bool IsRepeatMyTurn = false;
    }

    [Serializable]
    public class UpdatedPlayerPosition
    {
        public string UserId;
        public List<PinIndex> PinIndexes;
        public int BgCount;
        public bool IsBackGround;
    }

    [Serializable]
    public class PinIndex
    {
        public int PinName;
        public int _PinIndex;
    }

    [Serializable]
    public class PresentRoomName
    {
        public string UserId;
        public string RoomName;
    }
}