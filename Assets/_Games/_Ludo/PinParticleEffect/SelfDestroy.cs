﻿using UnityEngine;

public class SelfDestroy : MonoBehaviour
{
    public float delay = 1f;

    void Start()
    {
        Invoke(nameof(DestroySelf), delay);
    }

    private void DestroySelf()
    {
        DestroyImmediate(gameObject);
    }
}