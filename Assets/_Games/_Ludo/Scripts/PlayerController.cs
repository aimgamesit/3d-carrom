﻿using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ModelSocialProfile;
using static AIMLudo.Utility;
using com.TicTok.managers;

namespace AIMLudo
{
    public class PlayerController : BManager<PlayerController>
    {
        [SerializeField] Text TxtRank;
        [SerializeField] Text TxtName;
        public Image ImgProfilePic;
        [SerializeField] GameObject crownObj;
        [SerializeField] Image ImgProfileContainer;
        public GameObject[] Cell;
        public GameObject[] WinBoxPath;
        [SerializeField] GameObject Home_Panel;
        [SerializeField] GameObject CellHome;
        [SerializeField] GameObject LockCell;
        [SerializeField] GameObject WinBox;
        [SerializeField] List<GameObject> PinBottom;
        [SerializeField] GameObject PinsHome;
        public ActionTimeController PlayerTimer;
        public GameObject DiceObject;
        public List<GameObject> Pins;
        public int GetPlayerTurnCount = 0;
        public GameObject[] RedBall;
        public int PlayerIndex = -1;

        public string Name { get { return TxtName.text; } }

        public string SetName { set { TxtName.text = value; } }

        public Sprite SetInfoPanel { set { ImgProfileContainer.sprite = value; } }

        public Material SetCellsMaterial
        {
            set
            {
                foreach (var cells in Cell)
                {
                    cells.GetComponent<MeshRenderer>().material = value;
                }
            }
        }
        public Material SetColoredCellMaterial
        {
            set
            {
                foreach (var cells in WinBoxPath)
                {
                    cells.GetComponent<MeshRenderer>().material = value;
                }
            }
        }

        public Material SetPinMaterial
        {
            set
            {
                foreach (var cells in PinBottom)
                {
                    cells.GetComponent<MeshRenderer>().material = value;
                }
            }
        }

        public Material SetPinTexture
        {
            set
            {
                foreach (var cells in Pins)
                {
                    cells.GetComponent<MeshRenderer>().material = value;
                }
            }
        }

        public Material SetCellHomeMaterial
        {
            set
            {
                CellHome.GetComponent<MeshRenderer>().material = value;
            }
        }
        public Material SetHomePanelMaterial
        {
            set
            {
                Home_Panel.GetComponent<MeshRenderer>().material = value;
            }
        }

        public Material SetLockCellMaterial
        {
            set
            {
                LockCell.GetComponent<MeshRenderer>().material = value;
            }
        }


        public Material SetWinBoxMaterial
        {
            set
            {
                WinBox.GetComponent<MeshRenderer>().material = value;
            }
        }

        public Material SetPinHomeBgMaterial
        {
            set
            {
                PinsHome.GetComponent<MeshRenderer>().material = value;
            }
        }

        void SetPinColorCode()
        {
            foreach (var cells in Pins)
            {
                cells.GetComponent<PinHandeller>().color = ColorIndex;
            }
        }

        public ColorIndex SetColorIndex
        {
            set
            {
                _ColorIndex = value;
                SetPinColorCode();
            }
        }

        [SerializeField] ColorIndex _ColorIndex;

        public ColorIndex ColorIndex { get { return _ColorIndex; } }

        [SerializeField] int _Position;
        public int Position { get { return _Position; } }

        public int SetPosition { set { _Position = value; } }

        [SerializeField] PlayerAlignment _Alignment;

        public PlayerAlignment Alignment { get { return _Alignment; } }

        public PlayerAlignment SetAlignment { set { _Alignment = value; } }

        [SerializeField] DiceAlignment _DiceAlignment;

        public DiceAlignment DiceAlignment { get { return _DiceAlignment; } }

        public DiceAlignment SetDiceAlignment { set { _DiceAlignment = value; } }

        bool _IsPlayer;
        public bool IsPlayer { get { return _IsPlayer; } }
        public bool SetIsPlayer { set { _IsPlayer = value; } }

        public bool SetActive { set { gameObject.SetActive(value); } }

        public void SetData(Playerdata data, Sprite profilePic, int playerIndex = -1)
        {
            ImgProfilePic.sprite = profilePic;
            SetIsPlayer = true;
            SetName = data.UserName;
            PlayerIndex = playerIndex;
            foreach (var pin in Pins)
            {
                pin.gameObject.SetActive(true);
            }
            Debug.Log($"Player ::Position::{Position}::::Name:::{Name}".Colored(Colors.red));
        }

        private void Start()
        {
            TxtRank.gameObject.SetActive(false);
        }

        public void SetWinRank(int rank)
        {
            TxtRank.gameObject.SetActive(true);
            TxtRank.text = rank.ToString();
        }

        public void RemoveData(PlayerController data)
        {
            SetIsPlayer = false;
            SetName = "";
            foreach (var pin in Pins)
            {
                pin.gameObject.SetActive(false);
            }
        }

        public void ShowCrown()
        {
            crownObj.SetActive(true);
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        public void UpdatePinsPosition(UpdatedPlayerPosition data)
        {
            Debug.Log($"Update Token Position::::{data.UserId}:::data::\n:{JsonWriter.Serialize(data)}".Colored(Colors.red));

            if (data.BgCount < 6)
            {
                for (int i = 1; i <= data.BgCount; i++)
                {
                    RedBall[i - 1].SetActive(true);
                }
            }

            var PinHome = GameController.Instance.GameObject_PinContainer.FirstOrDefault(x => x.GetComponent<PinContainerHandller>().ContanerIndex == Position).GetComponent<PinContainerHandller>();
            var PinPath = PinHome.PinPath;

            if (data.PinIndexes.Count > 0)
            {
                bool NeedToUpdate = false;
                foreach (var info in data.PinIndexes)
                {
                    if (info._PinIndex != Pins[info.PinName].GetComponent<PinHandeller>().CurrentBoardIndex)
                    {
                        NeedToUpdate = true;
                        if (info._PinIndex >= 0)
                        {
                            Transform dest = PinPath[info._PinIndex].transform;

                            Pins[info.PinName].transform.position = dest.position;
                            Pins[info.PinName].GetComponent<PinHandeller>().CurrentBoardIndex = info._PinIndex;
                            Pins[info.PinName].GetComponent<PinHandeller>().CurrentBoardName = dest.gameObject;
                            Pins[info.PinName].GetComponent<PinHandeller>().CorrespondingPositionX = Pins[info.PinName].transform.localPosition.x;
                            Pins[info.PinName].GetComponent<PinHandeller>().CorrespondingPositionZ = Pins[info.PinName].transform.localPosition.z;
                            Debug.Log($"UserId:::{data.UserId}:::PinIndex:::{info.PinName}:::CurrentBoardIndex:::::{info._PinIndex}".Colored(Colors.green));

                            if (info._PinIndex > 56)
                            {
                                Pins[info.PinName].GetComponent<PinHandeller>().PinWinStatus = true;
                                //BoardPins[info.PinName].PinWinStatus = true;
                                PinMovement.Instance.ResizeAllWinPins();
                            }

                        }
                        else
                        {
                            Pins[info.PinName].transform.position = Pins[info.PinName].GetComponent<PinHandeller>().PinHomePosition.position;
                            Pins[info.PinName].GetComponent<PinHandeller>().CurrentBoardIndex = -1;
                            Pins[info.PinName].GetComponent<PinHandeller>().CurrentBoardName = null;
                            Pins[info.PinName].GetComponent<PinHandeller>().CorrespondingPositionX = Pins[info.PinName].transform.localPosition.x;
                            Pins[info.PinName].GetComponent<PinHandeller>().CorrespondingPositionZ = Pins[info.PinName].transform.localPosition.z;
                            Debug.Log($"UserId:::{data.UserId}:::PinIndex:::{info.PinName}:::CurrentBoardIndex:::::{info._PinIndex}".Colored(Colors.green));
                        }
                    }
                }

                if (NeedToUpdate)
                {
                    PinMovement.Instance.ResizeAllPins();
                }
            }
        }
    }
}