﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class PopUpHandeler : MonoBehaviour
    {

        public static PopUpHandeler instance;


        public Button Button_Close, Button_Yes;
        public Text text_msg, txt_header;

        // Use this for initialization
        void Start()
        {
            instance = this;

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void LoadMessage(string msg)
        {
            text_msg.text = msg;
            // txt_header.text = header;
        }


    }
}