﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace AIMLudo
{
    public class AnimationScroll : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            Scroll();
        }

        // Update is called once per frame
        void Update()
        {

        }

        void Scroll()
        {
            transform.DOMoveY(-50f, 10f);
            transform.DORestart(true);
        }
    }
}