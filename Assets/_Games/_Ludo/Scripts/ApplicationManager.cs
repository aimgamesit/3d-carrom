﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using static AIMLudo.Utility;
namespace AIMLudo
{
    public class ApplicationManager : MonoBehaviour
    {
        public static string CachePath;
        private void Start()
        {

        }

        #region GET IMAGE FROM URL AND DEFAULT LOCAL

        public static IEnumerator GetImageFromURL(string defaultImagePath, Image image)
        {
            if (image != null)
            {
                CachePath = Application.temporaryCachePath + "/Resources/Cache/";
                //Debug.Log("CachePath Start: " + CachePath);
                if (!Directory.Exists(CachePath))
                {
                    Directory.CreateDirectory(CachePath);
                }
                //Download Image From URL and Cache
                if (!string.IsNullOrEmpty(defaultImagePath) && defaultImagePath != "")
                {
                    string ImagePath;
                    bool IsGetFromCache = true;

                    ImagePath = CachePath + GetFileName(defaultImagePath);
                    WWW www;
                    if (System.IO.File.Exists(ImagePath))
                    {
                        string pathforwww = "file:///" + ImagePath;
                        www = new WWW(pathforwww);
                    }
                    else
                    {
                        IsGetFromCache = false;
                        www = new WWW(defaultImagePath);
                    }

                    yield return www;
                    if (www.error == null)
                    {
                        if (!IsGetFromCache)
                        {
                            File.WriteAllBytes(ImagePath, www.bytes);
                        }
                        else
                        {
                            //Debug.Log("SUCCESS CACHE LOAD OF " + www.url);
                        }

                        if (www.texture != null)
                        {
                            Sprite sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
                            if (sprite != null)
                            {
                                image.sprite = sprite;

                            }
                            else
                            {
                                image.sprite = Resources.Load<Sprite>("no_avatar");

                            }
                        }



                    }
                    else
                    {
                        Debug.Log("WWW ERROR texture " + www.error);
                    }
                }
                else
                {
                    image.sprite = Resources.Load<Sprite>("no_avatar");
                }
            }

        }
        public static IEnumerator GetImageFromURL(string defaultImagePath, Image image, System.Action OnDownload)
        {
            if (image != null)
            {
                CachePath = Application.temporaryCachePath + "/Resources/Cache/";
                //Debug.Log("CachePath Start: " + CachePath);
                if (!Directory.Exists(CachePath))
                {
                    Directory.CreateDirectory(CachePath);
                }
                //Download Image From URL and Cache
                if (!string.IsNullOrEmpty(defaultImagePath) && defaultImagePath != "")
                {
                    string ImagePath;
                    bool IsGetFromCache = true;

                    ImagePath = CachePath + GetFileName(defaultImagePath);
                    WWW www;
                    if (System.IO.File.Exists(ImagePath))
                    {
                        string pathforwww = "file:///" + ImagePath;
                        www = new WWW(pathforwww);
                    }
                    else
                    {
                        IsGetFromCache = false;
                        www = new WWW(defaultImagePath);
                    }

                    yield return www;
                    if (www.error == null)
                    {
                        if (!IsGetFromCache)
                        {
                            File.WriteAllBytes(ImagePath, www.bytes);
                        }
                        else
                        {
                            //Debug.Log("SUCCESS CACHE LOAD OF " + www.url);
                        }

                        if (www.texture != null)
                        {
                            Sprite sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
                            if (sprite != null)
                            {
                                image.sprite = sprite;
                                if (OnDownload != null) OnDownload();
                            }
                            else
                            {
                                image.sprite = Resources.Load<Sprite>("banner_notfound");
                                if (OnDownload != null) OnDownload();
                            }
                        }



                    }
                    else
                    {
                        Debug.Log("WWW ERROR texture " + www.error);
                        if (OnDownload != null) OnDownload();
                    }
                }
                else
                {
                    image.sprite = Resources.Load<Sprite>("banner_notfound");
                    if (OnDownload != null) OnDownload();
                }
            }

        }
        static string GetFileName(string hrefLink)
        {
            string[] parts = hrefLink.Split('/');
            string fileName = "";

            if (parts.Length > 0)
                fileName = parts[parts.Length - 1];
            else
                fileName = hrefLink;
            return fileName;
        }
        public static void DeleteCache()
        {
            foreach (string file in Directory.GetFiles(CachePath))
            {
                File.Delete(file);
            }
            Debug.Log("Clear Temp Data....");
        }
        #endregion

    }
}