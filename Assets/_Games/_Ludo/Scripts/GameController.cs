﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static ModelSocialProfile;
using static AIMLudo.Utility;
using UnityEngine.SceneManagement;
using System;
using com.TicTok.managers;
using UnityEngine.UIElements;
using AIMCarrom;

namespace AIMLudo
{
    public class GameController : BManager<GameController>
    {
        public GameObject loadingScreen;
        [SerializeField] PopupManager popupManager;
        public UnityEngine.UI.Toggle soundToggle;
        public GameObject[] Player_Win;
        public List<PlayerController> gamePlayers;
        public List<Sprite> PlayerInfoPanelLeft;
        public List<Sprite> PlayerInfoPanelRight;
        public Text[] Text_PlayerName;
        public List<Material> CellMaterial;
        public List<Material> LockMaterials;
        public List<Material> PinMaterial;
        public List<Material> PinsHome;
        [HideInInspector] public GameObject[] PlayerTeamName;
        public GameObject SingleMovablePin = null;
        [Header("GameBoard Components")]
        public GameObject[] GameObject_BoardContainerCells;
        public GameObject[] GameObject_PinContainer;
        public GameObject[] GameObject_Dice;
        public GameObject[] GameBoardPins;
        public GameObject WinPartical;
        public GameObject PrefabCircleRoundPin;
        public bool GameFinished = false;
        public Animator TurnAnimation;
        public float TimeDuration;
        public List<GameObject> PinsOfPlayer1;
        public List<GameObject> PinsOfPlayer2;
        public List<GameObject> PinsOfPlayer3;
        public List<GameObject> PinsOfPlayer4;
        [Header("GameBoardColor Components")]
        public Material[] Material_BoardContainerCellColors;
        public Material[] Material_WinBoxAndPin;
        public Material[] Material_BoardBlockCells;
        [Header("GameBoardVariables Components")]
        public bool ValidToBlinkContainer;
        public static int PlayerTurnIndex;
        public bool Allow;
        public static int NumberOfMovablePins;
        public static bool IsFirstTurn;
        public static int CurrentTurn;
        public Coroutine _CoroutineBlinkContainer;
        public static bool IsGameStart;
        [Header("GamePLayPopups")]
        public UnityEngine.UI.Button ButtonMenu;
        public GameObject LoaderPanel;
        public int posFrom, posTo;
        public long ActionTime;
        public WinLooseDialogCongroller WinLooseDialog;
        private List<int> ColorIndexList = new List<int>();
        [SerializeField] DiceNumberMasterController diceNumberMasterPanel;
        [SerializeField] GameMoveMode gameMoveMode;
        [SerializeField] UndoDiceNumber UndoDice;
        PinHandeller defaultToken;
        public int maxPlayers = 0;

        void OnEnable()
        {
            maxPlayers = PlayerPrefs.GetInt("Players");
            //GameSoundManger.Instance.SetSound(GameSoundManger.Instance.IsSoundEnable());
            GameSoundManger.Instance.GameStart.Play();
            diceNumberMasterPanel.gameObject.SetActive(false);
            ColorIndexList = GetBoardColorOrder(GameData.Instance.colorIndex);
            AdmobAdsManager.Instance.ShowBannerAd(false);
        }

        void Start()
        {
            LoaderPanel.SetActive(true);
            IsFirstTurn = true;
            PlayerTurnIndex = 0;
            NumberOfMovablePins = 0;
            CurrentTurn = 0;
            Allow = true;
            gameMoveMode.SetActive = false;
            ValidToBlinkContainer = true;
            UndoDice.SetActive = false;
            InitPlayers();
            soundToggle.isOn = !GameSoundManger.Instance.IsSoundEnable();
            soundToggle.onValueChanged.AddListener(delegate (bool isOn)
            {
                GameSoundManger.Instance.SetSound(!isOn);
            });
            LoaderPanel.SetActive(false);
            GC.Collect();
            Invoke(nameof(HideLoadingScreen), .5f);
        }

        private void HideLoadingScreen() => loadingScreen.SetActive(false);
        public void ShowLoadingScreen() => loadingScreen.SetActive(true);

        private void InitPlayers()
        {
            if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
            {
                List<PlayerdataOffline> playerdataOfflines = new List<PlayerdataOffline>();
                List<PositionModel> PositionModel = new List<PositionModel>();
                List<KeyValuePair<int, string>> GetPlayerNameListData = GetPlayerNameList();
                for (int i = 0; i < 4; i++)
                {
                    if (Utility.NumberOfPlayersPlaying == (int)Utility.PlayerList.Player2)
                    {
                        if (i == 0)
                        {
                            playerdataOfflines.Add(new PlayerdataOffline()
                            {
                                UserName = GetPlayerNameListData[0].Value,
                                ColorIndex = ColorIndexList[i],
                                position = i
                            });
                        }
                        if (i == 2)
                        {
                            playerdataOfflines.Add(new PlayerdataOffline()
                            {
                                UserName = GetPlayerNameListData[1].Value,
                                ColorIndex = ColorIndexList[i],
                                position = i
                            });
                        }

                    }
                    else if (Utility.NumberOfPlayersPlaying == (int)Utility.PlayerList.Player3)
                    {
                        if (i == 0)
                        {
                            playerdataOfflines.Add(new PlayerdataOffline()
                            {
                                UserName = GetPlayerNameListData[0].Value,
                                ColorIndex = ColorIndexList[i],
                                position = i
                            });
                        }
                        if (i == 1)
                        {
                            playerdataOfflines.Add(new PlayerdataOffline()
                            {
                                UserName = GetPlayerNameListData[1].Value,
                                ColorIndex = ColorIndexList[i],
                                position = i
                            });
                        }
                        if (i == 2)
                        {
                            playerdataOfflines.Add(new PlayerdataOffline()
                            {
                                UserName = GetPlayerNameListData[2].Value,
                                ColorIndex = ColorIndexList[i],
                                position = i
                            });
                        }
                    }
                    else
                    {
                        playerdataOfflines.Add(new PlayerdataOffline()
                        {
                            UserName = GetPlayerNameListData[i].Value,
                            ColorIndex = ColorIndexList[i],
                            position = i
                        });
                    }

                    PositionModel.Add(new PositionModel() { Pos = i });
                }

                SetPlayerPositions(PositionModel);

                SetPlayerDataForOfflineMode(playerdataOfflines);
                SetColor(PositionModel);

                TurnIndicator();
            }
        }

        public List<PlayerController> GetPlayers() => gamePlayers.Where(x => x.IsPlayer).ToList();
        public PlayerController GetPlayer(int pos) => gamePlayers.FirstOrDefault(x => x.Position == pos);

        public void SetPlayerPositions(List<PositionModel> positionsData)
        {
            int index = 0;
            foreach (var player in gamePlayers)
            {
                player.SetPosition = positionsData[index].Pos;
                GameObject_PinContainer[index].GetComponent<PinContainerHandller>().ContanerIndex = positionsData[index].Pos;
                Debug.Log($"SetPlayerPositions:::::{positionsData[index].Pos}".Colored(Colors.red));

                if (index > 1)
                {
                    player.SetAlignment = PlayerAlignment.Right;
                }
                else
                {
                    player.SetAlignment = PlayerAlignment.Left;
                }
                if (index == 1 || index == 2)
                {
                    player.SetDiceAlignment = DiceAlignment.Top;
                }
                else
                {
                    player.SetDiceAlignment = DiceAlignment.Bottom;
                }
                index++;

            }
        }

        List<int> GetBoardColorOrder(ColorIndex ColorIndex)
        {
            //Get all colors
            List<int> ColorIndexs = Enum.GetValues(typeof(ColorIndex)).Cast<int>().ToList();
            //set color list according to mine player color
            List<int> FinalColorIndexs = ColorIndexs.Where(x => x >= (int)ColorIndex).OrderBy(y => y).ToList();
            FinalColorIndexs.AddRange(ColorIndexs.Where(x => x < (int)ColorIndex).OrderBy(y => y).ToList());
            return FinalColorIndexs;
        }

        public void SetColor(List<PositionModel> positionModel)
        {
            int i = 0;
            foreach (var data in positionModel)
            {
                PlayerController player = GetPlayer(data.Pos);
                if (player != null)
                {
                    PlayerController localplayer = gamePlayers[i];
                    localplayer.SetColorIndex = (ColorIndex)ColorIndexList[i];
                    // localplayer.SetColorIndex = (ColorIndex)data.Pos;
                    localplayer.SetCellsMaterial = GetMaterialTexture(MaterialType.Cell, localplayer.ColorIndex);
                    localplayer.SetPinMaterial = GetMaterialTexture(MaterialType.Pin_Box, localplayer.ColorIndex);
                    localplayer.SetPinTexture = GetMaterialTexture(MaterialType.PinTexture, localplayer.ColorIndex);
                    localplayer.SetCellHomeMaterial = GetMaterialTexture(MaterialType.Cell_Home, localplayer.ColorIndex);
                    localplayer.SetWinBoxMaterial = GetMaterialTexture(MaterialType.WinBox, localplayer.ColorIndex);
                    localplayer.SetPinHomeBgMaterial = GetMaterialTexture(MaterialType.Pin_Home_Bg, localplayer.ColorIndex);
                    Debug.Log($"Player Color ::::::::{localplayer.ColorIndex.ToString()}".Colored(Colors.red));
                }
                i++;
            }
        }

        public void SetPlayerDataForOfflineMode(List<PlayerdataOffline> playerdataOfflines)
        {
            foreach (var data in playerdataOfflines)
            {
                PlayerController player = gamePlayers[data.position];
                if (player != null)
                {
                    var Container = GameObject_PinContainer.FirstOrDefault(x => x.GetComponent<PinContainerHandller>().ContanerIndex == data.position);
                    Container.SetActive(true);
                    Debug.Log($"SetPlayerData::::::::::{Container.name} ,  INDEX: {data.position}");
                    player.SetActive = true;
                    Sprite avatar = data.position == 0 ? GameData.Instance.avatars[Player.PlayerData.currentAvatar].sprite : GameData.Instance.defaultAvatar;
                    player.SetData(new Playerdata() { UserName = data.UserName, position = data.position, ColorIndex = data.ColorIndex.ToString() }, avatar, data.position);
                    player.SetInfoPanel = GetPlayerInfoPanel(player.Alignment, player.ColorIndex);
                }
            }
        }
        public Sprite GetPlayerInfoPanel(PlayerAlignment align, ColorIndex colorIndex)
        {
            Sprite sprite = null;
            if (align == PlayerAlignment.Left)
            {

                sprite = PlayerInfoPanelLeft[(int)colorIndex];
            }
            else
            {
                sprite = PlayerInfoPanelRight[(int)colorIndex];
            }
            return sprite;
        }
        public Material GetMaterialTexture(MaterialType materialType, ColorIndex colorIndex)
        {
            Material material = null;
            switch (materialType)
            {
                case MaterialType.Cell:
                    material = CellMaterial[(int)colorIndex];
                    break;
                case MaterialType.Cell_Home:
                    material = CellMaterial[(int)colorIndex];
                    break;
                case MaterialType.Pin_Box:
                    material = CellMaterial[(int)colorIndex];
                    break;
                case MaterialType.Pin_Home_Bg:
                    material = CellMaterial[(int)colorIndex];
                    break;
                case MaterialType.WinBox:
                    material = CellMaterial[(int)colorIndex];
                    break;
                case MaterialType.PinTexture:
                    material = PinMaterial[(int)colorIndex];
                    break;
                case MaterialType.Lock:
                    material = LockMaterials[(int)colorIndex];
                    break;
            }
            return material;
        }

        public int CurrentPlayerIndex;
        public IEnumerator BoardCellBlinkAnimation(int _PlayerTurnIndex)
        {
            CurrentPlayerIndex = _PlayerTurnIndex;
            Debug.Log("BoardCellBlinkAnimation::: _PlayerTurnIndex: " + _PlayerTurnIndex);
            ValidToBlinkContainer = true;
            PlayerController currentPlayer = GetPlayer(_PlayerTurnIndex);
            //int ColorIndex = (byte)currentPlayer.ColorIndex;
            //int ColorIndex = GameObject_BoardContainerCells[_PlayerTurnIndex].GetComponent<PlayerContainerHandller>().ColorIndex;
            float count = 1f;
            int status = 0;
            float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
            while (ValidToBlinkContainer)
            {
                deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
                realTimeLastFrame = Time.realtimeSinceStartup;
                if (count > 0.9f)
                {
                    status = 0;
                }
                else if (count < 0.4)
                {
                    status = 1;
                }
                if (status == 0)
                {
                    count -= 0.03f + deltaTime;
                    yield return new WaitForSeconds(0f);
                    //foreach (GameObject child in GameObject_BoardContainerCells)
                    //{
                    //    child.gameObject.GetComponent<MeshRenderer>().material.color = new Color(count, count, count, 255);
                    //}
                    foreach (GameObject child in currentPlayer.Cell)
                    {
                        child.gameObject.GetComponent<MeshRenderer>().material.color = new Color(count, count, count, 255);
                    }
                }
                else
                {
                    count += 0.03f + deltaTime;
                    yield return new WaitForSeconds(0f);
                    foreach (GameObject child in currentPlayer.Cell)
                    {
                        child.gameObject.GetComponent<MeshRenderer>().material.color = new Color(count, count, count, 255);
                    }
                }
            }
            yield break;
        }
        public void StopBlinking()
        {
            ValidToBlinkContainer = false;
            foreach (var item in GetPlayer(CurrentPlayerIndex).Cell)
            {
                item.gameObject.GetComponent<MeshRenderer>().material.color = CellMaterial[CurrentPlayerIndex].color;
            }

        }
        public void HideDice(int index)
        {
            Debug.Log("HideDice");
            GetPlayer(index).DiceObject.SetActive(false);
        }

        public int PlayerOnTurnPosition;

        public void SetDiceColor(int PlayerIndex)
        {
            PlayerOnTurnPosition = PlayerIndex;
            Debug.Log("SetDiceColor " + PlayerIndex);
            PlayerController playerController = GetPlayer(PlayerIndex);

            //Debug.Log("DiceObject is active");
            playerController.DiceObject.SetActive(true);

            playerController.DiceObject.transform.GetChild(0).rotation = Quaternion.Euler(0f, 0f, 0f);

            if (DiceController.instance != null) DiceController.instance.SetLastDiceNumber();
            DisableEnablePlayerPinsOrDice(true, playerController.DiceObject.transform.GetChild(0).gameObject);
        }

        public void DisableEnablePlayerPinsOrDice(bool Bool, GameObject Dice)
        {
            Debug.Log("DisableEnablePlayerPinsOrDice");
            Dice.GetComponent<BoxCollider>().enabled = Bool;
        }

        public int GetStatusOfPinsUnderContainer()
        {
            int UnderContainerPinsCount = 0;
            foreach (Transform child in GameObject_PinContainer[PlayerTurnIndex].transform)
            {
                if (child.gameObject.GetComponent<PinHandeller>().CurrentBoardIndex < 58)
                {
                    if (child.gameObject.GetComponent<PinHandeller>().CurrentBoardIndex == -1)
                    {
                        UnderContainerPinsCount++;
                    }
                }
            }
            if (UnderContainerPinsCount == 4)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        // changed by shani
        bool IsMasterType { get { return (Constants.GameMode == Constants.GameMode_Master); } }

        List<PinHandeller> GetMovablePins(int PlayerTurnIndex)
        {
            //Get Active Player Tokesn
            var ActivePins = gamePlayers[PlayerTurnIndex].Pins.Select(x => x.GetComponent<PinHandeller>());

            List<PinHandeller> MovablePins = new List<PinHandeller>();

            #region Find Token For All Type Variation
            //Get all tokens lessthen index value
            if (DiceController.instance.DiceNumber == 6)
            {
                MovablePins = ActivePins.Where(x => (x.CurrentBoardIndex + DiceController.instance.DiceNumber) < 58).ToList();
            }
            else
            {
                MovablePins = ActivePins.Where(x => x.CurrentBoardIndex != -1 && (x.CurrentBoardIndex + DiceController.instance.DiceNumber) < 58).ToList();
            }
            #endregion

            #region Validate and Find Token for Master Type Game
            if (IsMasterType)
            {
                //Get all group tokens if not movable then 
                var pinsGroup = ActivePins.Where(x => x.CurrentBoardIndex != -1).GroupBy(x => x.CurrentBoardName).ToList();
                if (pinsGroup.Count > 0)
                {
                    foreach (var PinGroup in pinsGroup)
                    {
                        if (PinGroup != null && PinGroup.Count() > 1)
                        {
                            int index = 1;
                            foreach (var GroupPin in PinGroup)
                            {
                                if ((DiceController.instance.DiceNumber % 2 == 0) && GroupPin.CurrentBoardName.tag != "StopPoint")
                                {
                                    if ((GroupPin.CurrentBoardIndex + (DiceController.instance.DiceNumber / 2)) < 58)
                                    {
                                        if (MovablePins.FirstOrDefault(x => x.Index == GroupPin.Index) == null)
                                            MovablePins.Add(GroupPin);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                else
                                {
                                    if (MovablePins.FirstOrDefault(x => x.Index == GroupPin.Index) != null && GroupPin.CurrentBoardName.tag != "StopPoint")
                                    {
                                        if (PinGroup.Count() == 3 && index == 3)
                                        {
                                            //MovablePins.Remove(GroupPin);
                                        }
                                        else
                                        {
                                            MovablePins.Remove(GroupPin);
                                        }
                                    }
                                    index++;
                                }
                            }
                        }
                    }
                }
            }

            #endregion

            return MovablePins;
        }

        public delegate void UndoDiceTimeOut();
        public static event UndoDiceTimeOut OnUndoDiceTimeOut;
        int TokenGroup = 0;

        public void FindMovablePins(int PlayerTurnIndex, bool IsRollOnClick = true)
        {
            Debug.Log("FindMovablePinsClass!!!!!!!!!!!!!!!!".Colored(Colors.red) + PlayerTurnIndex);
            NumberOfMovablePins = 0;
            TokenGroup = 0;
            List<PinHandeller> MovablePins = GetMovablePins(PlayerTurnIndex);

            #region Enable Token
            PinMovement.PinMove = false;
            foreach (var Pin in MovablePins)
            {
                GameObject Roundclone = Instantiate(PrefabCircleRoundPin, Pin.transform, false) as GameObject;
                Roundclone.transform.SetAsLastSibling();
                Roundclone.SetActive(true);
                Pin.IsPinValidToClick = true;
                Pin.enabled = true;

                // To resize and scale the pin on turn
                Pin.transform.localScale = new Vector3(1f, 1f, 1f);
                Pin.gameObject.GetComponent<BoxCollider>().enabled = true;

                if (Pin.CurrentBoardIndex == -1)
                {
                    Pin.CurrentBoardName = null;
                }
            }
            #endregion

            if ((Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp) && PlayerTurnIndex != 0 && Constants.GameType == Constants.GAMETYPE_PLAY_WITH_COMPUTER)
            {
                StartCoroutine(BotMovement.Instance.OnAutoPlayBotSelectPinToMove01(MovablePins));
            }

            NumberOfMovablePins = MovablePins.Count;
            defaultToken = null;

            if (Constants.GamePlay != Constants.GamePlay_MultiPlayer && NumberOfMovablePins > 0)
            {
                TokenGroup = MovablePins.GroupBy(x => x.CurrentBoardName).Count();

                if ((NumberOfMovablePins == 1 || TokenGroup == 1))
                {
                    if (!IsRollOnClick)
                    {
                        if (Constants.GameMode != Constants.GameMode_FreeToPlay)
                        {
                            OnClickToken(MovablePins[0]);
                        }
                        else
                        {
                            if ((MovablePins[0].CurrentBoardIndex < DiceController.instance.DiceNumber) || (MovablePins[0].CurrentBoardIndex == 1 && DiceController.instance.DiceNumber == 1))
                                OnClickToken(MovablePins[0]);
                        }
                    }
                    else
                    {
                        defaultToken = MovablePins[0];
                    }
                }
            }
        }

        void PlayDefaultToken()
        {
            if (defaultToken != null && TokenGroup == 1)
            {
                if (Constants.GameMode != Constants.GameMode_FreeToPlay)
                {
                    OnClickToken(defaultToken);
                }
                else
                {
                    if ((defaultToken.CurrentBoardIndex < DiceController.instance.DiceNumber) || (defaultToken.CurrentBoardIndex == 1 && DiceController.instance.DiceNumber == 1))
                        OnClickToken(defaultToken);
                }
            }
        }

        public void OnClickToken(PinHandeller selectedPin, bool IsBackMove = false)
        {
            UndoDice.SetInteractable = false;

            //Disable Active Token
            foreach (var pin in gamePlayers[PlayerTurnIndex].Pins)
            {
                pin.GetComponent<PinHandeller>().IsPinValidToClick = false;

                for (int i = 1; i < pin.transform.childCount; i++)
                {
                    var circle = pin.transform.GetChild(i);
                    Destroy(circle.gameObject);
                }

                pin.GetComponent<BoxCollider>().enabled = false;
                pin.GetComponent<Animator>().enabled = false;
                pin.transform.localScale = new Vector3(1f, 1f, 1f);
            }

            //Validate Master Mode 
            //Group
            //Even
            //Stop !=
            //selected pin group to get two pin and move
            // Onclick Move

            List<int> PinIndexes = new List<int>();
            posFrom = selectedPin.CurrentBoardIndex;
            posTo = selectedPin.CurrentBoardIndex + DiceController.instance.DiceNumber;
            PinIndexes.Add(selectedPin.Index);

            if (IsMasterType)
            {
                if (selectedPin.CurrentBoardName != null && selectedPin.CurrentBoardName.tag != "StopPoint")
                {
                    var ActivePins = gamePlayers[PlayerTurnIndex].Pins.Select(x => x.GetComponent<PinHandeller>()).Where(y => y.CurrentBoardIndex != -1).ToList();
                    //Get all group tokens if not movable then 
                    if (ActivePins.Count > 1)
                    {
                        var pinsGroup = ActivePins.Where(x => x.CurrentBoardName == selectedPin.CurrentBoardName).GroupBy(x => x.CurrentBoardName).ToList();
                        bool NeedToBreak = false;
                        if (pinsGroup.Count > 0 && (DiceController.instance.DiceNumber % 2 == 0))
                        {
                            foreach (var PinGroup in pinsGroup)
                            {
                                if (PinGroup != null && PinGroup.Count() > 1)
                                {
                                    int index = 0;
                                    PinIndexes.Clear();
                                    foreach (var GroupPin in PinGroup)
                                    {
                                        posFrom = selectedPin.CurrentBoardIndex;
                                        posTo = selectedPin.CurrentBoardIndex + (DiceController.instance.DiceNumber / 2);
                                        PinIndexes.Add(GroupPin.Index);
                                        index++;

                                        if (index == 2)
                                        {
                                            NeedToBreak = true;
                                            break;
                                        };
                                    }
                                    if (NeedToBreak) break;
                                }
                            }
                        }
                    }
                }
            }
            else if (Constants.GameMode == Constants.GameMode_FreeToPlay)
            {
                //PinMovement.Instance.DisableCircleAnimation();
                if (posFrom > DiceController.instance.DiceNumber)
                {
                    if (Constants.GamePlay == Constants.GamePlay_Vs_Comp && PlayerTurnIndex != 0)
                    {
                        if (IsBackMove)
                        {
                            posTo = selectedPin.CurrentBoardIndex - DiceController.instance.DiceNumber;
                            PinMovement.Instance.OnPinClick(new List<GameObject>() { selectedPin.gameObject }, posFrom, posTo);
                        }
                        else
                        {
                            PinMovement.Instance.OnPinClick(new List<GameObject>() { selectedPin.gameObject }, posFrom, posTo);
                        }
                    }
                    else
                    {
                        gameMoveMode.Init(posFrom, DiceController.instance.DiceNumber, delegate (bool _IsBackMove)
                        {
                            //EventController.Instance.PinSelected(UserID, SelectedPinIndex, PosFrom, PosTo, DiceNumber, flag, IsRepeatMyTurn, IsBackMove);
                            if (_IsBackMove)
                            {
                                posTo = selectedPin.CurrentBoardIndex - DiceController.instance.DiceNumber;
                                PinMovement.Instance.OnPinClick(new List<GameObject>() { selectedPin.gameObject }, posFrom, posTo);
                            }
                            else
                            {
                                PinMovement.Instance.OnPinClick(new List<GameObject>() { selectedPin.gameObject }, posFrom, posTo);
                            }
                        }, delegate
                        {
                            FindMovablePins(PlayerTurnIndex);
                        });
                    }
                }
                else
                {
                    PinMovement.Instance.OnPinClick(new List<GameObject>() { selectedPin.gameObject }, posFrom, posTo);
                }
            }

            if (Constants.GameMode != Constants.GameMode_FreeToPlay)
            {
                List<GameObject> selectedPins = new List<GameObject>();
                foreach (var pinIndex in PinIndexes)
                {
                    Debug.Log("SelctedPinMove!!!!!!!!!!!!!!!!!");
                    selectedPins.Add((GameObject_PinContainer[PlayerTurnIndex].GetComponent<PinContainerHandller>().Pins[pinIndex]));
                }
                PinMovement.Instance.OnPinClick(selectedPins, posFrom, posTo);
            }
        }

        public void ResetPreviousTurnBoardColor(int PlayerTurnIndex)
        {
            foreach (Transform child in GameObject_BoardContainerCells[PlayerTurnIndex].transform)
            {
                child.gameObject.GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1, 255);
            }
        }

        // for offline gameplay
        public void TurnIndicator()
        {
            Debug.Log("TurnIndicator................. " + PlayerTurnIndex);
            if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
            {
                HideDice(PlayerTurnIndex);
                CurrentTurn = GetPlayerTurn();
                StopBlinking();  // for reset the colors;
                AssignPlayerTurn(CurrentTurn);
            }
            else
            {
                GetPlayerTurn();
            }
        }

        // for offline gameplay
        public int GetPlayerTurn()
        {
            Debug.Log($"GetPlayerTurn().................Offline {DiceController.instance == null}, {PinMovement.Instance == null}");
            if (DiceController.instance != null)
                Debug.Log($"DiceNumber: {DiceController.instance.DiceNumber}");
            Debug.Log($"PlayerTurnIndex: {PlayerTurnIndex},  IsFirstTurn: {IsFirstTurn},  Allow:{Allow},  PinHit:{PinMovement.Instance.PinHit},  PinWin:{PinMovement.Instance.PinWin}".Colored(Colors.yellow));


            if (!IsFirstTurn)
            {
                if (Allow)
                {
                    if ((DiceController.instance.DiceNumber != 6) && !PinMovement.Instance.PinHit && !PinMovement.Instance.PinWin)
                    {
                        Debug.Log($"IF :{(DiceController.instance.DiceNumber != 6)}");
                        Turn();
                    }
                    else
                    {
                        Debug.Log($"ELSE :{(DiceController.instance.DiceNumber != 6)}, EmptyMovablePins: {PinMovement.Instance.EmptyMovablePins}");
                        if (!PinMovement.Instance.EmptyMovablePins)
                        {
                            PinMovement.Instance.PinHit = false;
                            PinMovement.Instance.PinWin = false;
                            PinMovement.Instance.EmptyMovablePins = false;
                            if (GameObject_PinContainer[PlayerTurnIndex].GetComponent<PinContainerHandller>().IsContaineralreadyWins)
                            {
                                Turn();
                            }
                        }
                        else
                        {
                            Debug.Log($"ELSE ELSE:{(DiceController.instance.DiceNumber != 6)}");
                            Turn();
                        }
                    }
                }
                else
                {
                    Turn();
                }
            }
            Debug.Log($"GetPlayerTurn()....RETURN.............PlayerTurnIndex{PlayerTurnIndex}");
            return PlayerTurnIndex;
        }
        // for offline gameplay

        public void AssignPlayerTurn(int CurrentTurn)
        {
            Debug.Log($"AssignPlayerTurn!!!: {CurrentTurn}");
            SetDiceColor(CurrentTurn);
            DiceController.ValidToRoll = true;
            PinHandeller.SinglePinMove = false;
            PinMovement.PinMove = false;

            if (PinMovement.Instance != null)
            {
                PinMovement.Instance.SelectedPin = null;
            }
            if (_CoroutineBlinkContainer != null) StopCoroutine(_CoroutineBlinkContainer);
            {
                _CoroutineBlinkContainer = StartCoroutine(BoardCellBlinkAnimation(CurrentTurn));
            }

            //Start Here for offline Bot dice Roll
            if (Constants.GameType == Constants.GAMETYPE_PLAY_WITH_COMPUTER && CurrentTurn != 0)
            {
                DiceController.instance.CheckToRollDice();
            }
        }

        // for offline gameplay

        public void Turn()
        {
            Debug.Log($"Turn(): NumberOfPlayersPlaying : {NumberOfPlayersPlaying}".Colored(Colors.red));
            Debug.Log($"Turn(IF): PlayerTurnIndex : {PlayerTurnIndex}, IsContaineralreadyWins: {GameObject_PinContainer[PlayerTurnIndex].GetComponent<PinContainerHandller>().IsContaineralreadyWins}".Colored(Colors.red));
            if (NumberOfPlayersPlaying == (int)PlayerList.Player2)
            {
                if (PlayerTurnIndex == 2)
                {
                    if (GameObject_PinContainer[PlayerTurnIndex].GetComponent<PinContainerHandller>().IsContaineralreadyWins)
                    {
                        Turn();
                    }
                    else
                    {
                        PlayerTurnIndex = 0;
                    }
                }
                else
                {
                    if (GameObject_PinContainer[PlayerTurnIndex].GetComponent<PinContainerHandller>().IsContaineralreadyWins)
                    {
                        Turn();
                    }
                    else
                    {
                        PlayerTurnIndex = 2;
                    }
                }
            }
            else
            {
                Debug.Log($"Turn(ELSE): PlayerTurnIndex : {PlayerTurnIndex}, NumberOfPlayersPlaying: {NumberOfPlayersPlaying}".Colored(Colors.red));
                if (PlayerTurnIndex < (NumberOfPlayersPlaying - 1))
                {
                    PlayerTurnIndex += 1;
                    if (GameObject_PinContainer[PlayerTurnIndex].GetComponent<PinContainerHandller>().IsContaineralreadyWins)
                    {
                        Turn();
                    }
                }
                else
                {
                    PlayerTurnIndex = 0;
                    if (GameObject_PinContainer[PlayerTurnIndex].GetComponent<PinContainerHandller>().IsContaineralreadyWins)
                    {
                        Turn();
                    }
                }
            }
            Debug.Log($"Turn(INDEX): PlayerTurnIndex : {PlayerTurnIndex}".Colored(Colors.blue));
            //if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
            //{
            //    if (PlayerPrefs.GetInt("VibrationOn") == 1)
            //    {
            //        //Handheld.Vibrate();
            //    }
            //}
        }

        public IEnumerator PlayWinPartical()
        {
            GameSoundManger.Instance.WinBox.Play();
            WinPartical.SetActive(true);
            yield return new WaitForSeconds(.8f);
            WinPartical.SetActive(false);
        }

        public void LoadMenuScene()
        {
            StartCoroutine(LoadScene());
        }

        IEnumerator LoadScene()
        {
            ShowLoadingScreen();
            yield return new WaitForSeconds(.2f);
            SceneManager.LoadSceneAsync(GameData.Instance.MENU_SCENE);
        }

        public void PinEnableDisableByMode()
        {
            // By default pins of player2 & player4 will be off
            GameObject_PinContainer[0].SetActive(true);
            GameObject_PinContainer[2].SetActive(true);

            if (PlayerPrefs.GetInt("Players") == 2)
            {
                GameObject_PinContainer[0].SetActive(true);
                GameObject_PinContainer[2].SetActive(true);
            }
            if (PlayerPrefs.GetInt("Players") == 3)
            {
                GameObject_PinContainer[0].SetActive(true);
                GameObject_PinContainer[1].SetActive(true);
                GameObject_PinContainer[2].SetActive(true);
            }
            if (PlayerPrefs.GetInt("Players") == 4)
            {
                GameObject_PinContainer[0].SetActive(true);
                GameObject_PinContainer[1].SetActive(true);
                GameObject_PinContainer[2].SetActive(true);
                GameObject_PinContainer[3].SetActive(true);
            }
            PlayerPrefs.DeleteKey("Players");
        }

        public void ResetMasterPanel()
        {
            diceNumberMasterPanel.ResetToggles();
        }

        public void StartUndoTimer(int TimeSecond)
        {
            if (TimeSecond > 0)
            {
                UndoDice.SetUndoTimer(TimeSecond, delegate
                {
                    UndoDice.SetInteractable = false;

                    foreach (var pin in gamePlayers[PlayerTurnIndex].Pins)
                    {
                        pin.GetComponent<PinHandeller>().IsPinValidToClick = false;

                        for (int i = 1; i < pin.transform.childCount; i++)
                        {
                            var circle = pin.transform.GetChild(i);
                            Destroy(circle.gameObject);
                        }

                        pin.GetComponent<BoxCollider>().enabled = false;
                        pin.GetComponent<Animator>().enabled = false;
                        pin.transform.localScale = new Vector3(1f, 1f, 1f);
                    }
                    if (Constants.GameType == Constants.GAMETYPE_PLAY_WITH_COMPUTER)
                    {
                        DiceController.instance.GetDiceNumber(false);
                    }
                }, delegate
                {
                    if (Constants.GameType == Constants.GAMETYPE_PLAY_WITH_COMPUTER)
                    {
                        if (NumberOfMovablePins == 0)
                        {
                            OnUndoDiceTimeOut();
                        }
                        else
                        {
                            //if (PlayerTurnIndex == 0)
                            PlayDefaultToken();
                        }
                    }
                });
            }
        }

        public void OnBackBrnClick()
        {
            popupManager.ShowPopup(true, -1, PopupManager.PopupType.YES_NO, AIMCarrom.Constants.POPUP_EXIT_GAME_TITLE, AIMCarrom.Constants.POPUP_GAME_EXIT_MESSAGE, "Yes", "No", "", "", delegate (bool callback, string inputText)
            {
                if (callback)
                {
                    FirebaseManager.Instance.SendEvent(AIMCarrom.Constants.LUDO_EXIT_YES);

                    AdmobAdsManager._AdClosed += delegate ()
                    {
                        LoadMenuScene();
                    };
                    AdmobAdsManager.Instance.ShowInterstitial();
                }
                else
                {
                    FirebaseManager.Instance.SendEvent(AIMCarrom.Constants.LUDO_EXIT_NO);
                }
            });
        }
    }
}