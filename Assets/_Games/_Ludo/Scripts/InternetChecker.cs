﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace AIMLudo
{
    public class InternetChecker
    {

        public System.Action<string> IsConnectionSlow;
        public System.Action<string> IsConnectionGood;
        public static bool CheckInternet()
        {
            bool IsIneternetConnected = false;
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                IsIneternetConnected = false;
            }
            else
            {
                IsIneternetConnected = true;
            }
            return IsIneternetConnected;
        }

        public IEnumerator CheckConnectionSpeed(System.Action<string> IsConnectionGood, System.Action<string> IsConnectionSlow)
        {
            this.IsConnectionSlow = IsConnectionSlow;
            this.IsConnectionGood = IsConnectionGood;
            WWW www = new WWW("https://www.google.com");
            yield return www;
            if (www.isDone)
            {
                this.IsConnectionGood("Connection Speed Good!");
                yield break;
            }
            else
            {
                this.IsConnectionSlow("Connection Speed Slow!");
                yield break;
            }
        }
    }
}