﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class ToastController : MonoBehaviour
    {
        public GameObject goToast;
        public void Show(string ToastMessage, bool IsAutoHide = true)
        {
            goToast.transform.GetComponentInChildren<Text>().text = ToastMessage;
            goToast.SetActive(true);
            goToast.transform.GetComponent<CanvasGroup>().DOFade(1f, 0f).SetEase(Ease.Linear);
            goToast.transform.localScale = new Vector3(.6f, .6f, .6f);

            Sequence sequence = DOTween.Sequence();
            sequence.Join(goToast.transform.DOScale(1f, .35f).SetEase(Ease.OutBounce));

            if (IsAutoHide)
            {
                sequence.AppendInterval(5);
                sequence.Append(goToast.transform.GetComponent<CanvasGroup>().DOFade(0.0f, .35f).SetEase(Ease.Linear));
                sequence.OnComplete(() => Destroy(goToast));
            }

        }
        public void Hide()
        {
            goToast.SetActive(false);
        }
    }
}