﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
namespace AIMLudo
{
    namespace JsonData
    {
        public class JsonUtils
        {

            [Serializable]
            public class FreindListResponseData
            {
                public string UserId;
                public string FacebookId;
                public string Name;
                public string Pic;
            }
            [Serializable]
            public class FreindsList
            {
                public string success;
                public string message;
                public FreindListResponseData[] Data;
            }
            [Serializable]

            public class UserDataModel
            {
                public string UserProfilePicUrl;
                public string UserName;
                public string CountryPicUrl;
                public string Level;
                public string TotalEarning;
                public string CurrentGold;
                public string PlayerId;
                public string League;

                public string GamesWon;
                public string WinRate;
                public string TwoPlayerWins;
                public string FourPlayerWins;
                public string PvtTableWins;
            }
        }
    }
}