﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using UnityEngine.SceneManagement;
using AIMCarrom;

namespace AIMLudo
{
    public class ComputerModeDialogController : PopupBaseManager
    {
        [Header("Components")]
        public Button BtnClose;
        public Button BtnStart;

        public Toggle toggle2Player;
        public Toggle toggle3Player;
        public Toggle toggle4Player;

        public Toggle toggleColorRed;
        public Toggle toggleColorGreen;
        public Toggle toggleColorYellow;
        public Toggle toggleColorBlue;

        bool SetActive { set { gameObject.SetActive(value); } }

        void OnEnable()
        {
            InitDialog();
            BtnClose.onClick.AddListener(delegate
            {
                PlayClickSound();
                //SetActive = false;
                HideScreen(true);
            });
            BtnStart.onClick.AddListener(delegate
            {
                PlayClickSound();
                OnClickStart();
            });
        }
        private void OnDisable()
        {
            BtnClose.onClick.RemoveAllListeners();
            BtnStart.onClick.RemoveAllListeners();
            toggle2Player.onValueChanged.RemoveAllListeners();
            toggle3Player.onValueChanged.RemoveAllListeners();
            toggle4Player.onValueChanged.RemoveAllListeners();
        }

        public void InitDialog()
        {
            //SetActive = true;
            SetMaxPlayer(2);

            toggleColorRed.isOn = false;
            toggleColorGreen.isOn = false;
            toggleColorYellow.isOn = false;
            toggleColorBlue.isOn = true;

            toggle2Player.onValueChanged.AddListener(delegate
            {
                PlayClickSound();
                if (toggle2Player.isOn)
                {
                    SetMaxPlayer(2);
                }
            });
            toggle3Player.onValueChanged.AddListener(delegate
            {
                PlayClickSound();
                if (toggle3Player.isOn)
                {
                    SetMaxPlayer(3);
                }
            });
            toggle4Player.onValueChanged.AddListener(delegate
            {
                PlayClickSound();
                if (toggle4Player.isOn)
                {
                    SetMaxPlayer(4);
                }
            });
        }

        void SetMaxPlayer(int MaxPlayer)
        {
            switch (MaxPlayer)
            {
                case 2:
                    toggle4Player.isOn = false;
                    toggle3Player.isOn = false;
                    toggle2Player.isOn = true;
                    break;
                case 3:
                    toggle3Player.isOn = true;
                    toggle4Player.isOn = false;
                    toggle2Player.isOn = false;
                    break;
                case 4:
                    toggle2Player.isOn = false;
                    toggle3Player.isOn = false;
                    toggle4Player.isOn = true;
                    break;
            }

            //Utility Flags
            PlayerPrefs.SetInt("Players", MaxPlayer);
            Constants.Player_Mode_Number = MaxPlayer;
            Utility.NumberOfPlayersPlaying = MaxPlayer;
        }

        void OnClickStart()
        {
            GameData.Instance.undoCount = 1;
            Utility.SetPlayerNameList(GetPlayersName());
            SetPinColor();
            SceneManager.LoadSceneAsync(GameData.Instance.LUDO_GAME_SCENE);
        }
        void SetPinColor()
        {
            GameData gData = GameData.Instance;
            if (toggleColorRed.isOn)
            {
                gData.colorIndex = Utility.ColorIndex.Red;
            }
            else if (toggleColorGreen.isOn)
            {
                gData.colorIndex = Utility.ColorIndex.Green;
            }
            else if (toggleColorYellow.isOn)
            {
                gData.colorIndex = Utility.ColorIndex.Yellow;
            }
            else
            {
                gData.colorIndex = Utility.ColorIndex.Blue;
            }
        }

        List<KeyValuePair<int, string>> GetPlayersName()
        {
            List<KeyValuePair<int, string>> PlayerNameList = new List<KeyValuePair<int, string>>();
            if (Utility.NumberOfPlayersPlaying == 2)
            {
                PlayerNameList.Add(new KeyValuePair<int, string>(0, "You"));
                PlayerNameList.Add(new KeyValuePair<int, string>(2, "Computer 1"));
            }
            else if (Utility.NumberOfPlayersPlaying == 3)
            {
                PlayerNameList.Add(new KeyValuePair<int, string>(0, "You"));
                PlayerNameList.Add(new KeyValuePair<int, string>(1, "Computer 1"));
                PlayerNameList.Add(new KeyValuePair<int, string>(2, "Computer 2"));
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    string name = i == 0 ? "You" : $"Computer {i}";
                    PlayerNameList.Add(new KeyValuePair<int, string>(i, name));
                }
            }
            return PlayerNameList;
        }
        private void PlayClickSound()
        {
            MainMenuManager.Instance.PlayButtonClickSound();
        }
    }
}