﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using UnityEngine.SceneManagement;
using AIMCarrom;

namespace AIMLudo
{
    public class OfflineModeDialogController : PopupBaseManager
    {
        [Header("Components")]
        public Button BtnClose;
        public Button BtnStart;

        public InputField[] InputPlayerNames;
        public Toggle toggle2Player;
        public Toggle toggle3Player;
        public Toggle toggle4Player;

        bool SetActive { set { gameObject.SetActive(value); } }
        
        void OnEnable()
        {
            InitDialog();
            BtnClose.onClick.AddListener(delegate
            {
                PlayClickSound();
                HideScreen(true);
                //SetActive = false;
            });
            BtnStart.onClick.AddListener(delegate
            {
                PlayClickSound();
                OnClickStart();
            });
        }
        private void OnDisable()
        {
            BtnClose.onClick.RemoveAllListeners();
            BtnStart.onClick.RemoveAllListeners();
            toggle2Player.onValueChanged.RemoveAllListeners();
            toggle3Player.onValueChanged.RemoveAllListeners();
            toggle4Player.onValueChanged.RemoveAllListeners();
        }

        public void InitDialog()
        {
            //SetActive = true;
            SetMaxPlayer(2);

            toggle2Player.onValueChanged.AddListener(delegate
            {
                PlayClickSound();
                if (toggle2Player.isOn)
                {
                    SetMaxPlayer(2);
                }
            });
            toggle3Player.onValueChanged.AddListener(delegate
            {
                PlayClickSound();
                if (toggle3Player.isOn)
                {
                    SetMaxPlayer(3);
                }
            });
            toggle4Player.onValueChanged.AddListener(delegate
            {
                PlayClickSound();
                if (toggle4Player.isOn)
                {
                    SetMaxPlayer(4);
                }
            });
        }

        void SetMaxPlayer(int MaxPlayer)
        {
            switch (MaxPlayer)
            {
                case 2:
                    toggle4Player.isOn = false;
                    toggle3Player.isOn = false;
                    toggle2Player.isOn = true;
                    InputPlayerNames[0].gameObject.SetActive(true); InputPlayerNames[0].text = "Player 1";
                    InputPlayerNames[1].gameObject.SetActive(false);
                    InputPlayerNames[2].gameObject.SetActive(true); InputPlayerNames[2].text = "Player 2";
                    InputPlayerNames[3].gameObject.SetActive(false);
                    break;
                case 3:
                    toggle2Player.isOn = false;
                    toggle3Player.isOn = true;
                    toggle4Player.isOn = false;
                    InputPlayerNames[0].gameObject.SetActive(true); InputPlayerNames[0].text = "Player 1";
                    InputPlayerNames[1].gameObject.SetActive(true); InputPlayerNames[1].text = "Player 2";
                    InputPlayerNames[2].gameObject.SetActive(true); InputPlayerNames[2].text = "Player 3";
                    InputPlayerNames[3].gameObject.SetActive(false);
                    break;
                case 4:
                    toggle2Player.isOn = false;
                    toggle3Player.isOn = false;
                    toggle4Player.isOn = true;
                    InputPlayerNames[0].gameObject.SetActive(true); InputPlayerNames[0].text = "Player 1";
                    InputPlayerNames[1].gameObject.SetActive(true); InputPlayerNames[1].text = "Player 2";
                    InputPlayerNames[2].gameObject.SetActive(true); InputPlayerNames[2].text = "Player 3";
                    InputPlayerNames[3].gameObject.SetActive(true); InputPlayerNames[3].text = "Player 4";
                    break;
            }

            //Utility Flags
            GameData.Instance.colorIndex = Utility.ColorIndex.Blue;
            PlayerPrefs.SetInt("Players", MaxPlayer);
            Constants.Player_Mode_Number = MaxPlayer;
            Utility.NumberOfPlayersPlaying = MaxPlayer;
        }

        void OnClickStart()
        {
            Utility.SetPlayerNameList(GetPlayersName());
            SceneManager.LoadSceneAsync(GameData.Instance.LUDO_GAME_SCENE);
        }

        List<KeyValuePair<int, string>> GetPlayersName()
        {
            List<KeyValuePair<int, string>> PlayerNameList = new List<KeyValuePair<int, string>>();

            if (Utility.NumberOfPlayersPlaying == 2)
            {
                PlayerNameList.Add(new KeyValuePair<int, string>(0, string.IsNullOrWhiteSpace(InputPlayerNames[0].text) ? "Player 1" : InputPlayerNames[0].text));
                PlayerNameList.Add(new KeyValuePair<int, string>(0, string.IsNullOrWhiteSpace(InputPlayerNames[2].text) ? "Player 2" : InputPlayerNames[2].text));
            }
            else if (Utility.NumberOfPlayersPlaying == 3)
            {
                PlayerNameList.Add(new KeyValuePair<int, string>(0, string.IsNullOrWhiteSpace(InputPlayerNames[0].text) ? "Player 1" : InputPlayerNames[0].text));
                PlayerNameList.Add(new KeyValuePair<int, string>(0, string.IsNullOrWhiteSpace(InputPlayerNames[1].text) ? "Player 2" : InputPlayerNames[1].text));
                PlayerNameList.Add(new KeyValuePair<int, string>(0, string.IsNullOrWhiteSpace(InputPlayerNames[2].text) ? "Player 3" : InputPlayerNames[2].text));
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    PlayerNameList.Add(new KeyValuePair<int, string>(0, string.IsNullOrWhiteSpace(InputPlayerNames[i].text) ? $"Player {(i + 1)}" : InputPlayerNames[i].text));
                }
            }
            return PlayerNameList;
        }
        private void PlayClickSound()
        {
            MainMenuManager.Instance.PlayButtonClickSound();
        }
    }
}