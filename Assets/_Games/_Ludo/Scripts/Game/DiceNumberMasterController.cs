﻿using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class DiceNumberMasterController : MonoBehaviour
    {
        [SerializeField] Toggle toggle_1, toggle_2, toggle_3, toggle_4, toggle_5, toggle_6;
        [SerializeField] ToggleGroup toggleGroup;

        System.Action<int> OnSelectNumber;
        public void OnInit(System.Action<int> OnSelectNumber)
        {
            this.OnSelectNumber = OnSelectNumber;
            SelectedNumber = 0;
            RemoveListners();
            SetToggleState(true);
            OnInitListners();
        }
        public void ResetToggles()
        {
            RemoveListners();
            SetToggleState(false);
            toggle_1.isOn = false;
            toggle_2.isOn = false;
            toggle_3.isOn = false;
            toggle_4.isOn = false;
            toggle_5.isOn = false;
            toggle_6.isOn = false;
        }

        void OnInitListners()
        {
            toggle_1.onValueChanged.AddListener(delegate
            {
                if (toggle_1.isOn) SelectNumber(1);
                else
                {
                    toggle_2.isOn = false;
                    toggle_3.isOn = false;
                    toggle_4.isOn = false;
                    toggle_5.isOn = false;
                    toggle_6.isOn = false;
                }
            });
            toggle_2.onValueChanged.AddListener(delegate
            {
                if (toggle_2.isOn) SelectNumber(2);
                else
                {
                    toggle_1.isOn = false;
                    toggle_3.isOn = false;
                    toggle_4.isOn = false;
                    toggle_5.isOn = false;
                    toggle_6.isOn = false;
                }
            });
            toggle_3.onValueChanged.AddListener(delegate
            {
                if (toggle_3.isOn) SelectNumber(3);
                else
                {
                    toggle_1.isOn = false;
                    toggle_2.isOn = false;
                    toggle_4.isOn = false;
                    toggle_5.isOn = false;
                    toggle_6.isOn = false;
                }
            });
            toggle_4.onValueChanged.AddListener(delegate
            {
                if (toggle_4.isOn) SelectNumber(4);
                else
                {
                    toggle_1.isOn = false;
                    toggle_2.isOn = false;
                    toggle_3.isOn = false;
                    toggle_5.isOn = false;
                    toggle_6.isOn = false;
                }
            });
            toggle_5.onValueChanged.AddListener(delegate
            {
                if (toggle_5.isOn) SelectNumber(5);
                else
                {
                    toggle_1.isOn = false;
                    toggle_2.isOn = false;
                    toggle_3.isOn = false;
                    toggle_4.isOn = false;
                    toggle_6.isOn = false;
                }
            });
            toggle_6.onValueChanged.AddListener(delegate
            {
                if (toggle_6.isOn) SelectNumber(6);
                else
                {
                    toggle_1.isOn = false;
                    toggle_2.isOn = false;
                    toggle_3.isOn = false;
                    toggle_4.isOn = false;
                    toggle_5.isOn = false;
                }
            });
        }
        void RemoveListners()
        {
            toggle_1.onValueChanged.RemoveAllListeners();
            toggle_2.onValueChanged.RemoveAllListeners();
            toggle_3.onValueChanged.RemoveAllListeners();
            toggle_4.onValueChanged.RemoveAllListeners();
            toggle_5.onValueChanged.RemoveAllListeners();
            toggle_6.onValueChanged.RemoveAllListeners();
        }
        void SetToggleState(bool value)
        {
            toggleGroup.allowSwitchOff = true;
            if (!value) RemoveListners();
            toggle_1.interactable = value;
            toggle_2.interactable = value;
            toggle_3.interactable = value;
            toggle_4.interactable = value;
            toggle_5.interactable = value;
            toggle_6.interactable = value;

            if (value)
            {
                toggle_1.isOn = false;
                toggle_2.isOn = false;
                toggle_3.isOn = false;
                toggle_4.isOn = false;
                toggle_5.isOn = false;
                toggle_6.isOn = false;
                toggleGroup.allowSwitchOff = false;
            }
        }

        int SelectedNumber = 0;
        void SelectNumber(int number)
        {
            if (SelectedNumber == number) return;
            SelectedNumber = number;
            Debug.Log($"Selected Dice Number Is:{number}");
            if (OnSelectNumber != null) OnSelectNumber(number);
        }
    }
}