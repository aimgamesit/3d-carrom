﻿using AIMCarrom;
using com.TicTok.managers;
using UnityEngine;
namespace AIMLudo
{
    public class LudoSelection : MonoBehaviour
    {
        [SerializeField] OfflineModeDialogController OfflineModeDialog;
        [SerializeField] ComputerModeDialogController ComputerModeDialog;
 
        void Start()
        {
            Input.multiTouchEnabled = false;
            System.GC.Collect();
        }

        public void OnPlayWithPC()
        {
            PlayClickSound();
            Constants.GamePlay = Constants.GamePlay_Vs_Comp;
            Constants.GameType = Constants.GAMETYPE_PLAY_WITH_COMPUTER;
            //ComputerModeDialog.InitDialog();
            ComputerModeDialog.ShowScreen(true, false);
            FirebaseManager.Instance.SendEvent(AIMCarrom. Constants.LUDO_PC_PLAYER_BUTTON);
        }

        public void OnPlayWithFriends()
        {
            PlayClickSound();
            Constants.GamePlay = Constants.GamePlay_Local;
            Constants.GameType = "";
            //OfflineModeDialog.InitDialog();
            OfflineModeDialog.ShowScreen(true, false);
            FirebaseManager.Instance.SendEvent(AIMCarrom.Constants.LUDO_LOCAL_PLAYER_BUTTON);
        }

        private void PlayClickSound()
        {
            MainMenuManager.Instance.PlayButtonClickSound();
        }
    }
}