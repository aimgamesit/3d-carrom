﻿using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class UndoDiceNumber : MonoBehaviour
    {
        [SerializeField] Button BtnUndo;
        [SerializeField] Image TimerFrame;
        [SerializeField] GameObject UndoCounter;

        public bool SetActive { set { gameObject.SetActive(value); } }
        public bool SetInteractable
        {
            set
            {
                BtnUndo.interactable = value;
                TimerFrame.gameObject.SetActive(value);

                if (!value)
                {
                    OnUndoTimeOut = null;
                    OnClickUndo = null;
                    IsActiveTimer = false;
                }
            }
        }
        int SetUndoCount
        {
            set
            {
                UndoCount = value;
                UndoCounter.SetActive((value > 0));
                UndoCounter.GetComponentInChildren<Text>().text = value.ToString();

                if (Constants.GameType == Constants.GAMETYPE_PLAY_WITH_COMPUTER)
                {
                    GameData.Instance.undoCount = UndoCount;
                }
            }
        }
        int UndoCount;
        public void Init(int _UndoCount)
        {
            SetActive = true;
            SetInteractable = false;
            SetUndoCount = _UndoCount;

            BtnUndo.onClick.RemoveAllListeners();
            BtnUndo.onClick.AddListener(delegate
            {
                OnUndoTimeOut = null;

                if (UndoCount > 0)
                {
                    SetUndoCount = (UndoCount - 1);
                }
                else
                {
                    SetUndoCount = 0;
                }

                OnClickUndo?.Invoke();
            });
        }

        System.Action OnClickUndo;
        System.Action OnUndoTimeOut;
        Coroutine coroutine;
        public void SetUndoTimer(int Time, System.Action OnClickUndo, System.Action OnUndoTimeOut)
        {
            OnUndoTimeOut();
            //if (UndoCount > 0)
            //{
            //    this.OnClickUndo = OnClickUndo;
            //    this.OnUndoTimeOut = OnUndoTimeOut;
            //    SetInteractable = true;
            //    //Set component default values
            //    TotalActionTime = Time;
            //    AnimationTimeFrame = 0;
            //    TimerFrame.fillAmount = 1;

            //    //Star.SetActive(true);
            //    IsActiveTimer = true;
            //}
        }


        float AnimationTimeFrame = 0;
        float TotalActionTime = 0;
        [HideInInspector] public bool IsActiveTimer = false;

        private void FixedUpdate()
        {
            if (IsActiveTimer)
            {
                AnimationTimeFrame += Time.fixedDeltaTime;
                TimerFrame.fillAmount = (1 - (AnimationTimeFrame / TotalActionTime));

                if (AnimationTimeFrame >= TotalActionTime)
                {
                    //Time Out
                    OnClickUndo = null;
                    if (OnUndoTimeOut != null)
                    {
                        OnUndoTimeOut();
                        OnUndoTimeOut = null;
                    }
                    SetInteractable = false;
                    IsActiveTimer = false;
                }
            }
        }

    }
}