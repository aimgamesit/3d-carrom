﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class WaitingDialogController : MonoBehaviour
    {
        [SerializeField] Text TxtTimer;
        [SerializeField] Button BtnCancelRequest;

        public bool SetActive
        {
            set
            {
                gameObject.SetActive(value);
                if (coroutine != null) StopCoroutine(coroutine);
            }
        }

        System.Action OnCancelRequest;
        public void Init(int TotalSeconds, System.Action OnCancelRequest)
        {
            SetActive = true;
            this.OnCancelRequest = OnCancelRequest;
            coroutine = StartCoroutine(Timer(TotalSeconds));
        }

        Coroutine coroutine;
        IEnumerator Timer(int TotalSeconds)
        {
            while (TotalSeconds > 0)
            {
                System.TimeSpan timeSpan = System.TimeSpan.FromSeconds(TotalSeconds);
                TxtTimer.text = string.Format("{0:00}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
                yield return new WaitForSeconds(1f);
            }

            if (TotalSeconds <= 0)
            {
                if (OnCancelRequest != null)
                {
                    OnCancelRequest();
                }
            }
        }
    }
}