﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AIMLudo
{
    public class ToggleButton : MonoBehaviour
    {
        public ButtonState buttonState;

        public ButtonState SetState
        {
            set
            {
                buttonState = value;
            }
        }

        public ButtonState State
        {
            get
            {
                return buttonState;
            }
        }
    }

    public enum ButtonState
    {
        LOCK = 1,
        UNLOCK = 2,
        AUTO = 3,
        MANUAL = 4
    }
}