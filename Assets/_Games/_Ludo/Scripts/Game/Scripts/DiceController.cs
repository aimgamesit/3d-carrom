﻿using System.Collections;
using UnityEngine;
namespace AIMLudo
{
    public class DiceController : MonoBehaviour
    {
        public static DiceController instance;

        [Header("Dice Components")]
        public GameObject Dice;
        public static bool ValidToRoll;
        public static bool IsRolling;
        public int DiceNumber;
        public GameObject ParticlesOnSix;
        public Animator DiceRollAnimation;
        public PlayerController PlayerOnTurn;

        public static int LastDiceNumber;

        private void OnEnable()
        {
            ValidToRoll = true;
            IsRolling = true;
            instance = this;
            PlayerOnTurn = GameController.Instance.GetPlayer(GameController.Instance.PlayerOnTurnPosition);
            GameController.OnUndoDiceTimeOut += GameController_OnUndoDiceTimeOut;
        }
        private void OnDisable()
        {
            GameController.OnUndoDiceTimeOut -= GameController_OnUndoDiceTimeOut;
        }


        private void GameController_OnUndoDiceTimeOut()
        {
            Debug.Log("GameController_OnUndoDiceTimeOut AssignNextTurn ");
            PinMovement.Instance.EmptyMovablePins = true;
            StartCoroutine(AssignNextTurn());
        }

        void Update()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (Input.GetMouseButton(0))
                {
                    if (hit.transform.tag == "Dice" && ValidToRoll)
                    {
                        ValidToRoll = false;
                        // to disable animation of turn
                        GameController.Instance.TurnAnimation.gameObject.SetActive(false);

                        if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
                        {
                            GetDiceNumber();
                        }
                    }
                }
            }
        }

        public void CheckToRollDice()
        {
            Debug.Log("CheckToRollDice" + ValidToRoll);
            if (ValidToRoll)
            {
                ValidToRoll = false;

                if (Constants.GamePlay == Constants.GamePlay_MultiPlayer)
                {
                }
                else
                {
                    GetDiceNumber();
                }
            }
        }
        bool isAutoMove;
        public IEnumerator RollDice(int dicenumber, bool _isAutoMove = false, System.Action OnRolledDice = null)
        {
            Debug.Log($"RollDice!!!!!!!!!!!!!!!!!!!!!!  {dicenumber} GameController.PlayerTurnIndex: {GameController.PlayerTurnIndex}");
            if (GameController.PlayerTurnIndex != 0 && Constants.GamePlay == Constants.GamePlay_Vs_Comp)
            {
                yield return new WaitForSeconds(.8f);
            }
            DiceNumber = dicenumber;
            ValidToRoll = false;
            IsRolling = true;
            isAutoMove = _isAutoMove;
            //float evaluteimte = .2f;
            //float t = 0;
            //float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
            GameSoundManger.Instance.DiceRoll.Play();
            //DiceRollAnimation.enabled = true;
            DiceRollAnimation.SetTrigger("" + dicenumber);
            yield return new WaitForSeconds(1f);
            //while (t < evaluteimte)
            //{

            //    deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
            //    realTimeLastFrame = Time.realtimeSinceStartup;

            //    t += deltaTime;
            //    float rollDelay = (t < evaluteimte) ? 0.02f : 0f;
            //    //Debug.Log($"Roll Delay Time::::{rollDelay}");
            //    if (t < evaluteimte / 2)
            //    {
            //        Dice.transform.localScale = new Vector3(Dice.transform.localScale.x + 0.05f,
            //            Dice.transform.localScale.y + 0.05f,
            //            Dice.transform.localScale.z + 0.05f);
            //        //yield return new WaitForSeconds(0.015f);
            //        yield return new WaitForSeconds(rollDelay);
            //    }
            //    else
            //    {
            //        Dice.transform.localScale = new Vector3(Dice.transform.localScale.x - 0.05f,
            //            Dice.transform.localScale.y - 0.05f,
            //            Dice.transform.localScale.z - 0.05f);
            //        yield return new WaitForSeconds(rollDelay);

            //    }
            //}

            Debug.Log("Dice Roll Done");
            SetDiceNumber(DiceNumber);
            LastDiceNumber = DiceNumber;

            if (OnRolledDice != null)
            {
                OnRolledDice();
                OnRolledDice = null;
            }

        }

        bool IsRollOnClick;
        public void GetDiceNumber(bool IsRollOnClick = true)
        {
            if ((Constants.GamePlay == Constants.GamePlay_Vs_Comp && GameData.Instance.undoCount == 0) || Constants.GamePlay == Constants.GamePlay_Local)
                IsRollOnClick = false;

            this.IsRollOnClick = IsRollOnClick;

            Debug.Log($"GetDiceNumber {IsRollOnClick}");

            int index = GameController.PlayerTurnIndex;
            //List<int> number = new List<int> { 6,6,6,6,6,6,6,6,6,2,2,2,2,2,3,3,3,3,1};
            int RandomDiceNumber = 0;
            //RandomDiceNumber = number[Random.Range(1, 7)];
            RandomDiceNumber = Random.Range(1, 7);
            Debug.Log($"RandomDiceNumber: {RandomDiceNumber}");

            if (GameController.Instance.GetStatusOfPinsUnderContainer() == 0)
            {
                DiceNumber = RandomDiceNumber;
            }
            else
            {
                if (RandomDiceNumber != 6)
                {
                    GameController.Instance.GameObject_PinContainer[index].GetComponent<PinContainerHandller>().GetPlayerTurnCount++;
                }
                else
                {
                    GameController.Instance.GameObject_PinContainer[index].GetComponent<PinContainerHandller>().GetPlayerTurnCount = 0;
                }

                if (GameController.Instance.GameObject_PinContainer[index].GetComponent<PinContainerHandller>().GetPlayerTurnCount < 4)
                {
                    //Debug.Log("Player Turn Without Six No..... Less Then 3.................");
                    DiceNumber = RandomDiceNumber;
                }
                else
                {
                    //Debug.Log("Player Turn Without Six No.....More Then 3.................");
                    DiceNumber = 6;
                    GameController.Instance.GameObject_PinContainer[index].GetComponent<PinContainerHandller>().GetPlayerTurnCount = 0;
                }
            }
            StartCoroutine(RollDice(DiceNumber, false, delegate
            {
                if (GameController.PlayerTurnIndex == 0 && IsRollOnClick)
                    GameController.Instance.StartUndoTimer(2);
            }));
        }

        public void SetDiceNumber(int DiceNumber)
        {
            Debug.Log("SetDiceNumber:::DiceNumber: " + DiceNumber);
            IsRolling = false;
            Dice.transform.localPosition = new Vector3(Dice.transform.localPosition.x, 0f, Dice.transform.localPosition.z);
            Dice.transform.localScale = new Vector3(1f, 1f, 1f);
            //DiceRollAnimation.enabled = false;

            switch (DiceNumber)
            {
                case 1:
                    Dice.transform.localEulerAngles = new Vector3(0f, 0f, 90f);
                    break;
                case 2:
                    Dice.transform.localEulerAngles = new Vector3(-90f, 0f, 0f);
                    break;
                case 3:
                    Dice.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
                    break;
                case 4:
                    Dice.transform.localEulerAngles = new Vector3(180f, 0f, 0f);
                    break;
                case 5:
                    Dice.transform.localEulerAngles = new Vector3(90f, 0f, 0f);
                    break;
                case 6:
                    Dice.transform.localEulerAngles = new Vector3(0f, 90f, -90f);
                    break;
            }
            CheckDiceNumberConditions();
        }

        public void SetLastDiceNumber()
        {
            Debug.Log("Set Last DiceNumber");
            Dice.transform.localPosition = new Vector3(Dice.transform.localPosition.x, 0f, Dice.transform.localPosition.z);
            Dice.transform.localScale = new Vector3(1f, 1f, 1f);

            switch (LastDiceNumber)
            {
                case 1:
                    Dice.transform.localEulerAngles = new Vector3(0f, 0f, 90f);
                    break;
                case 2:
                    Dice.transform.localEulerAngles = new Vector3(-90f, 0f, 0f);
                    break;
                case 3:
                    Dice.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
                    break;
                case 4:
                    Dice.transform.localEulerAngles = new Vector3(180f, 0f, 0f);
                    break;
                case 5:
                    Dice.transform.localEulerAngles = new Vector3(90f, 0f, 0f);
                    break;
                case 6:
                    Dice.transform.localEulerAngles = new Vector3(0f, 90f, -90f);
                    break;
            }
        }

        public void CheckDiceNumberConditions()
        {
            Debug.Log("CheckDiceNumberConditions...............");
            int index = 0;
            PinMovement.Instance.EmptyMovablePins = false;
            index = GameController.PlayerTurnIndex;

            if (DiceNumber == 6)
            {
                //GameSoundManger.instance.Got6OnDice.Play();
                GameController.Instance.GameObject_PinContainer[index].GetComponent<PinContainerHandller>().GetPlayerSixCount++;
            }
            else
            {
                GameController.Instance.GameObject_PinContainer[index].GetComponent<PinContainerHandller>().GetPlayerSixCount = 0;
            }

            if (GameController.Instance.GameObject_PinContainer[index].GetComponent<PinContainerHandller>().GetPlayerSixCount < 3)
            {
                Debug.Log("GetPlayerSixCount < 3  !!!!!!!!!!!!!!!isAutoMove!" + isAutoMove);
                GameController.Instance.Allow = true;
                if (isAutoMove == false)
                {
                    Debug.Log("isAutoMove!!!!!!!!!!!!!!!!!!!!!!!!!!!" + isAutoMove);

                    GameController.Instance.FindMovablePins(index, IsRollOnClick);


                    if (IsPinValidToMove())
                    {
                        if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
                        {
                            PinMovement.Instance.EmptyMovablePins = false;

                        }
                        StartCoroutine(ParticleDiceAnimation());
                        PinMovement.IsRepeatMyTurn = true;
                    }
                    else
                    {
                        //Debug.Log("AssignNextTurn From 1");
                        if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
                        {
                            if (GameController.PlayerTurnIndex != 0 || GameData.Instance.undoCount == 0 || !IsRollOnClick || Constants.GamePlay == Constants.GamePlay_Local)
                            {
                                PinMovement.Instance.EmptyMovablePins = true;
                                StartCoroutine(AssignNextTurn());
                            }
                            //GameController_OnUndoDiceTimeOut();
                        }
                    }
                }
            }
            else
            {
                Debug.Log("AssignNextTurn From 2");
                GameController.Instance.Allow = false;
                GameController.Instance.GameObject_PinContainer[index].GetComponent<PinContainerHandller>().GetPlayerSixCount = 0;
                StartCoroutine(AssignNextTurn());
            }
        }

        public bool IsPinValidToMove()
        {
            if (GameController.NumberOfMovablePins > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerator ParticleDiceAnimation()
        {
            if (DiceNumber == 6)
            {
                //ParticlesOnSix.SetActive(true);
                yield return new WaitForSeconds(.6f);
                DisableAnimationOnSix();
            }
        }

        public void DisableAnimationOnSix()
        {
            //ParticlesOnSix.SetActive(false);
        }

        public IEnumerator AssignNextTurn()
        {
            Debug.Log("AssignNextTurn.............");
            if (Constants.GamePlay_Local == Constants.GamePlay || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
            {
                GameController.Instance.ValidToBlinkContainer = false;
                yield return new WaitForSeconds(Constants.GamePlay == Constants.GamePlay_Vs_Comp ? 0.5f : 0.01f);  // Delay for assigning next turn
                GameController.Instance.ResetPreviousTurnBoardColor(GameController.PlayerTurnIndex);
                PinHandeller.SinglePinMove = false;
                PinMovement.Instance.SelectedPin = null;
                ValidToRoll = true;
                GameController.IsFirstTurn = false;
                GameController.Instance.HideDice(GameController.PlayerTurnIndex);
                GameController.Instance.TurnIndicator();
            }
        }
    }
}