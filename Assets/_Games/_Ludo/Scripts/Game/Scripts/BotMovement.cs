﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace AIMLudo
{
    public class BotMovement : MonoBehaviour
    {
        public static BotMovement Instance;

        void Awake()
        {
            Instance = this;
        }

        public IEnumerator OnAutoPlayBotSelectPinToMove01(List<PinHandeller> MovablePinsList = null)
        {
            yield return new WaitForSeconds(1f);
            Debug.Log("OnAutoPlayBotSelectPinToMove!!!!!!!!!!!!!");
            Dictionary<PinHandeller, GameObject> MatchedPinsPinList = new Dictionary<PinHandeller, GameObject>();
            foreach (var PinObject in GameController.Instance.GameBoardPins)
            {
                if (PinObject != null && PinObject.transform.parent.GetComponent<PinContainerHandller>().ContanerIndex != GameController.Instance.GameObject_PinContainer[GameController.PlayerTurnIndex].GetComponent<PinContainerHandller>().ContanerIndex && PinObject.GetComponent<PinHandeller>().CurrentBoardName != null && !PinObject.GetComponent<PinHandeller>().PinWinStatus)
                {
                    //if (!Constants.IsTeamUpEnabled)
                    //{
                        MatchedPinsPinList.Add(PinObject.GetComponent<PinHandeller>(), PinObject);
                    //}
                    //else
                    //{
                    //    if (GameController.PlayerTurnIndex == 0 && PinObject.transform.parent.GetComponent<PinContainerHandller>().ContanerIndex != 2)
                    //    {
                    //        MatchedPinsPinList.Add(PinObject.GetComponent<PinHandeller>(), PinObject);
                    //    }
                    //    else if (GameController.PlayerTurnIndex == 1 && PinObject.transform.parent.GetComponent<PinContainerHandller>().ContanerIndex != 3)
                    //    {
                    //        MatchedPinsPinList.Add(PinObject.GetComponent<PinHandeller>(), PinObject);
                    //    }
                    //    else if (GameController.PlayerTurnIndex == 2 && PinObject.transform.parent.GetComponent<PinContainerHandller>().ContanerIndex != 0)
                    //    {
                    //        MatchedPinsPinList.Add(PinObject.GetComponent<PinHandeller>(), PinObject);
                    //    }
                    //    else if (GameController.PlayerTurnIndex == 3 && PinObject.transform.parent.GetComponent<PinContainerHandller>().ContanerIndex != 1)
                    //    {
                    //        MatchedPinsPinList.Add(PinObject.GetComponent<PinHandeller>(), PinObject);
                    //    }
                    //}
                }
            }

            if (MovablePinsList.Count > 0)
            {
                if (CheckIsDestinationPinOpponentPin(MovablePinsList, MatchedPinsPinList, out bool IsBackMove))
                {
                    Debug.Log("CheckIsDestinationPinOpponentPin");
                    PinMovement.Instance.SetSelectedPin(IsBackMove);
                }
                else if (CheckPinIsInHome(MovablePinsList))
                {
                    Debug.Log("CheckPinIsInHome");
                    PinMovement.Instance.SetSelectedPin();
                }
                else if (CheckMovablePinEnterToWinBox(MovablePinsList, MatchedPinsPinList))
                {
                    Debug.Log("CheckMovablePinEnterToWinBox");
                    PinMovement.Instance.SetSelectedPin();
                }
                else if (CheckOnBackOpponentPinPositionISClose(MovablePinsList, MatchedPinsPinList))
                {
                    Debug.Log("CheckOnBackOpponentPinPositionISClose");
                    PinMovement.Instance.SetSelectedPin();
                }
                else if (CheckOnFrontOpponentPinPositionISClose(MovablePinsList, MatchedPinsPinList))
                {
                    Debug.Log("CheckOnFrontOpponentPinPositionISClose");
                    PinMovement.Instance.SetSelectedPin();
                }
                else if (CheckMovablePinCloseToWinBox(MovablePinsList, MatchedPinsPinList))
                {
                    Debug.Log("CheckMovablePinCloseToWinBox");
                    PinMovement.Instance.SetSelectedPin();
                }
            }
        }

        List<PinHandeller> GetGroupPins(List<PinHandeller> MovablePinsList)
        {
            //MovablePinsList
            List<PinHandeller> pins = new List<PinHandeller>();
            if (MovablePinsList.Count() > 1 && (DiceController.instance.DiceNumber % 2 == 0))
            {
                var pinsGroup = MovablePinsList.Where(y => y.CurrentBoardIndex != -1).GroupBy(x => x.CurrentBoardName);
                if (pinsGroup != null && pinsGroup.Count() > 0)
                {
                    foreach (var PinGroup in pinsGroup)
                    {
                        if (PinGroup != null && PinGroup.Count() > 1)
                        {
                            int index = 1;

                            foreach (var GroupPin in PinGroup)
                            {
                                if (PinGroup.Count() == 3 && index == 3)
                                {
                                    //MovablePins.Remove(GroupPin);
                                }
                                else
                                {
                                    pins.Add(GroupPin);
                                }

                                index++;
                            }
                        }
                    }
                }
            }

            return pins;
        }

        public bool CheckIsDestinationPinOpponentPin(List<PinHandeller> MovablePinsList, Dictionary<PinHandeller, GameObject> MatchedPinsPinList, out bool IsBackMove)
        {
            IsBackMove = false;

            if (MatchedPinsPinList.Count > 0)
            {
                //Master Game Mode
                if (Constants.GameMode == Constants.GameMode_Master)
                {
                    var pinsGroup = GetGroupPins(MovablePinsList);

                    foreach (var groupPin in pinsGroup)
                    {
                        MovablePinsList.Remove(groupPin);

                        int Destination = groupPin.CurrentBoardIndex + (DiceController.instance.DiceNumber / 2);
                        Transform dest = groupPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[Destination].transform;

                        foreach (var MatchedPins in MatchedPinsPinList)
                        {
                            if (dest.gameObject.name == MatchedPins.Key.CurrentBoardName.name)
                            {
                                PinMovement.Instance.SelectedPin = groupPin.gameObject;
                                return true;
                            }
                        }
                    }
                }

                if (Constants.GameMode == Constants.GameMode_FreeToPlay)
                {
                    foreach (var MovablePins in MovablePinsList)
                    {
                        int Destination = MovablePins.CurrentBoardIndex + DiceController.instance.DiceNumber;
                        Transform dest = MovablePins.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[Destination].transform;

                        foreach (var MatchedPins in MatchedPinsPinList)
                        {
                            //Move Forward
                            if (dest.gameObject.name == MatchedPins.Key.CurrentBoardName.name)
                            {
                                PinMovement.Instance.SelectedPin = MovablePins.gameObject;
                                return true;
                            }
                            else
                            {
                                //Move Backward
                                if (MovablePins.CurrentBoardIndex > DiceController.instance.DiceNumber)
                                {
                                    Destination = MovablePins.CurrentBoardIndex - DiceController.instance.DiceNumber;
                                    dest = MovablePins.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[Destination].transform;
                                    if (dest.gameObject.name == MatchedPins.Key.CurrentBoardName.name)
                                    {
                                        PinMovement.Instance.SelectedPin = MovablePins.gameObject;
                                        IsBackMove = true;
                                        return true;
                                    }
                                }
                            }
                        }
                    }

                }
                else
                {
                    foreach (var MovablePins in MovablePinsList)
                    {
                        int Destination = MovablePins.CurrentBoardIndex + DiceController.instance.DiceNumber;
                        Transform dest = MovablePins.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[Destination].transform;
                        foreach (var MatchedPins in MatchedPinsPinList)
                        {
                            if (dest.gameObject.name == MatchedPins.Key.CurrentBoardName.name)
                            {
                                PinMovement.Instance.SelectedPin = MovablePins.gameObject;
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        public bool CheckPinIsInHome(List<PinHandeller> MovablePinsList)
        {
            foreach (var MovablePins in MovablePinsList)
            {
                if (MovablePins.CurrentBoardIndex == -1)
                {
                    PinMovement.Instance.SelectedPin = MovablePins.gameObject;
                    return true;
                }
            }
            return false;
        }

        public bool CheckOnBackOpponentPinPositionISClose(List<PinHandeller> MovablePinsList1, Dictionary<PinHandeller, GameObject> MatchedPinsPinList)
        {
            if (MatchedPinsPinList.Count > 0)
            {
                //Master Game Mode
                if (Constants.GameMode == Constants.GameMode_Master)
                {
                    var pinsGroup = GetGroupPins(MovablePinsList1);
                    foreach (var groupPin in pinsGroup)
                    {
                        MovablePinsList1.Remove(groupPin);
                    }
                }
                var MovablePinsList = MovablePinsList1.Where(x => x.CurrentBoardName.tag != "StopPoint").Select(x => x);
                Dictionary<GameObject, int> FindSortMovablePinOnPath = new Dictionary<GameObject, int>();
                foreach (var MatchedPins in MatchedPinsPinList)
                {
                    if (MatchedPins.Key.CurrentBoardIndex < 51)
                    {
                        Dictionary<GameObject, int> FindMovablePinOnPath = new Dictionary<GameObject, int>();
                        int i = MatchedPins.Key.CurrentBoardIndex;
                        int Destination = i + 6;
                        if (Destination > 51)
                        {
                            int CountRemainingBlock = 51 - i;
                            Destination = i + CountRemainingBlock;
                        }
                        while (i < Destination)
                        {
                            Transform dest = MatchedPins.Value.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i].transform;
                            var MovablePinsListFind = MovablePinsList.Where(x => x.CurrentBoardName.name == dest.gameObject.name).Select(x => x).FirstOrDefault();
                            if (MovablePinsListFind != null)
                            {
                                if (MovablePinsListFind.GetComponent<PinHandeller>().CurrentBoardIndex < 46)
                                {
                                    if (FindMovablePinOnPath.Where(x => x.Key == MovablePinsListFind).Select(x => x.Key).Count() < 1)
                                    {
                                        FindMovablePinOnPath.Add(MovablePinsListFind.gameObject, i);
                                        break;
                                    }
                                }
                                //else if (MovablePinsListFind.transform.parent.GetComponent<PinContainerHandller>().OpponentPinBite)
                                //{
                                if (FindMovablePinOnPath.Where(x => x.Key == MovablePinsListFind).Select(x => x.Key).Count() < 1)
                                {
                                    FindMovablePinOnPath.Add(MovablePinsListFind.gameObject, i);
                                    break;
                                }
                                //}
                            }
                            i++;
                        }
                        if (FindMovablePinOnPath.Count > 0)
                        {
                            var FindMovablePin = FindMovablePinOnPath.OrderBy(x => x.Value).Select(x => x).FirstOrDefault();

                            if (FindSortMovablePinOnPath.Where(x => x.Key == FindMovablePin.Key).Select(x => x.Key).Count() < 1)
                            {
                                FindSortMovablePinOnPath.Add(FindMovablePin.Key, FindMovablePin.Value);
                            }
                        }
                    }
                }
                if (FindSortMovablePinOnPath.Count > 0)
                {
                    var FindSortMovablePin = FindSortMovablePinOnPath.OrderBy(x => x.Value).Select(x => x).FirstOrDefault();
                    PinMovement.Instance.SelectedPin = FindSortMovablePin.Key;
                    return true;
                }
                return false;
            }

            else
            {
                return false;
            }
        }

        public bool CheckOnFrontOpponentPinPositionISClose(List<PinHandeller> MovablePinsList, Dictionary<PinHandeller, GameObject> MatchedPinsPinList)
        {
            if (MatchedPinsPinList.Count > 0)
            {

                if (Constants.GameMode == Constants.GameMode_Master)
                {
                    var pinsGroup = GetGroupPins(MovablePinsList);
                    foreach (var groupPin in pinsGroup)
                    {
                        MovablePinsList.Remove(groupPin);
                    }
                }

                Dictionary<GameObject, int> FindSortMovablePinOnPath = new Dictionary<GameObject, int>();
                foreach (var MovablePins in MovablePinsList)
                {
                    if (MovablePins.CurrentBoardIndex < 52)
                    {
                        Dictionary<GameObject, int> FindOpponentPinOnPath = new Dictionary<GameObject, int>();
                        int i = MovablePins.CurrentBoardIndex;
                        int Destination = i + 6;
                        if (Destination > 51)
                        {
                            int CountRemainingBlock = 51 - i;
                            Destination = i + CountRemainingBlock;
                        }
                        int CheckForDiceNumber = 0;
                        while (i < Destination)
                        {
                            Transform dest = MovablePins.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i].transform;
                            if (MatchedPinsPinList.Where(x => x.Key.CurrentBoardName == dest.gameObject).Count() > 0)
                            {
                                if (FindOpponentPinOnPath.Where(x => x.Key == MovablePins).Select(x => x.Key).Count() < 1)
                                {
                                    if (CheckForDiceNumber >= DiceController.instance.DiceNumber)
                                    {
                                        FindOpponentPinOnPath.Add(MovablePins.gameObject, i);
                                    }
                                    break;
                                }
                            }
                            CheckForDiceNumber++;
                            i++;
                        }
                        if (FindOpponentPinOnPath.Count > 0)
                        {
                            var FindMovablePin = FindOpponentPinOnPath.OrderBy(x => x.Value).Select(x => x).FirstOrDefault();
                            if (FindSortMovablePinOnPath.Where(x => x.Key == FindMovablePin.Key).Select(x => x.Key).Count() < 1)
                            {
                                FindSortMovablePinOnPath.Add(FindMovablePin.Key, FindMovablePin.Value);
                            }
                        }
                    }
                }
                if (FindSortMovablePinOnPath.Count > 0)
                {
                    var FindSortMovablePin = FindSortMovablePinOnPath.OrderBy(x => x.Value).Select(x => x).FirstOrDefault();
                    PinMovement.Instance.SelectedPin = FindSortMovablePin.Key;
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        public bool CheckMovablePinEnterToWinBox(List<PinHandeller> MovablePinsList, Dictionary<PinHandeller, GameObject> MatchedPinsPinList)
        {
            if (MatchedPinsPinList.Count > 0)
            {
                if (Constants.GameMode == Constants.GameMode_Master)
                {
                    var pinsGroup = GetGroupPins(MovablePinsList);
                    foreach (var groupPin in pinsGroup)
                    {
                        MovablePinsList.Remove(groupPin);
                        if (groupPin.CurrentBoardIndex + (DiceController.instance.DiceNumber / 2) == 57)
                        {
                            PinMovement.Instance.SelectedPin = groupPin.gameObject;
                            return true;
                        }
                    }
                }

                //if (Constants.GameMode == Constants.GameMode_Classic || ((Constants.GameMode == Constants.GameMode_Master || Constants.GameMode == Constants.GameMode_Quick) && GameController.instance.GameObject_PinContainer[GameController.PlayerTurnIndex].GetComponent<PinContainerHandller>().OpponentPinBite))
                //{
                int count = 0;
                Dictionary<GameObject, int> FindSortMovablePinOnPath = new Dictionary<GameObject, int>();
                foreach (var MatchedPins in MovablePinsList)
                {
                    int Destination = 58;
                    int i = MatchedPins.CurrentBoardIndex;
                    int CheckForDiceNumber = 0;
                    if (i > 45)
                    {
                        Dictionary<GameObject, int> FindMovablePinOnPath = new Dictionary<GameObject, int>();
                        while (i < Destination)
                        {
                            Transform dest = MatchedPins.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i].transform;

                            if (MatchedPinsPinList.Where(x => x.Key.CurrentBoardName == dest.gameObject).Count() > 0 && CheckForDiceNumber >= DiceController.instance.DiceNumber)
                            {
                                if (FindMovablePinOnPath.Where(x => x.Key == MatchedPins).Select(x => x.Key).Count() < 1)
                                {
                                    FindMovablePinOnPath.Add(MatchedPins.gameObject, i);
                                    break;
                                }
                            }
                            CheckForDiceNumber++;
                            i++;
                        }
                        if (FindMovablePinOnPath.Count > 0)
                        {
                            var FindMovablePin = FindMovablePinOnPath.OrderBy(x => x.Value).Select(x => x).FirstOrDefault();
                            if (FindSortMovablePinOnPath.Where(x => x.Key == FindMovablePin.Key).Select(x => x.Key).Count() < 1)
                            {
                                FindSortMovablePinOnPath.Add(FindMovablePin.Key, FindMovablePin.Value);
                            }
                        }
                        else
                        {
                            count++;
                            FindSortMovablePinOnPath.Add(MatchedPins.gameObject, count);
                        }
                    }
                }
                if (FindSortMovablePinOnPath.Count > 0)
                {
                    var FindSortMovablePin = FindSortMovablePinOnPath.OrderBy(x => x.Value).Select(x => x).FirstOrDefault();
                    PinMovement.Instance.SelectedPin = FindSortMovablePin.Key;
                    return true;
                }
                else
                {
                    return false;
                }
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }
        }

        public bool CheckMovablePinCloseToWinBox(List<PinHandeller> MovablePinsList, Dictionary<PinHandeller, GameObject> MatchedPinsPinList)
        {

            if (Constants.GameMode == Constants.GameMode_Master)
            {
                var pinsGroup = GetGroupPins(MovablePinsList);
                foreach (var groupPin in pinsGroup)
                {
                    MovablePinsList.Remove(groupPin);
                    if (groupPin.CurrentBoardIndex + (DiceController.instance.DiceNumber / 2) < 52)
                    {
                        PinMovement.Instance.SelectedPin = groupPin.gameObject;
                        return true;
                    }
                }
            }


            Dictionary<GameObject, int> FindMovable = new Dictionary<GameObject, int>();
            foreach (var MatchedPins in MovablePinsList)
            {
                int Destination = 58;
                int i = MatchedPins.CurrentBoardIndex;
                int SortDistance = Destination - i;

                if ((i > 45))// && !MatchedPins.transform.parent.GetComponent<PinContainerHandller>().OpponentPinBite) && (Constants.GameMode == Constants.GameMode_Quick || Constants.GameMode == Constants.GameMode_Master || Constants.GameMode_Classic == Constants.GameMode))
                {
                    if (MatchedPins.GetComponent<PinHandeller>().CurrentBoardName.tag != "StopPoint")
                    {
                        if (i + DiceController.instance.DiceNumber < 52)
                        {
                            FindMovable.Add(MatchedPins.gameObject, SortDistance);
                        }
                    }
                }
                else
                {
                    FindMovable.Add(MatchedPins.gameObject, SortDistance);
                }
            }
            if (FindMovable.Count > 0)
            {
                var FindSortMovablePin = FindMovable.OrderBy(x => x.Value).Select(x => x);
                foreach (var FindSortMovablePinList in FindSortMovablePin)
                {
                    int i = FindSortMovablePinList.Key.GetComponent<PinHandeller>().CurrentBoardIndex;
                    int Destination = i + DiceController.instance.DiceNumber;

                    int CheckForOpponentPin = 0;
                    if (MatchedPinsPinList.Count > 0)
                    {
                        while (i < Destination)
                        {
                            Transform dest = FindSortMovablePinList.Key.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i].transform;

                            if (MatchedPinsPinList.Where(x => x.Key.CurrentBoardName == dest.gameObject).Count() > 0)
                            {
                                CheckForOpponentPin++;
                                break;
                            }
                            i++;
                        }
                    }
                    if (CheckForOpponentPin < 1)
                    {
                        PinMovement.Instance.SelectedPin = FindSortMovablePinList.Key;
                        return true;
                    }
                }
                if (PinMovement.Instance.SelectedPin == null)
                {
                    var FindMovableList = FindMovable.Reverse().FirstOrDefault();
                    PinMovement.Instance.SelectedPin = FindMovableList.Key;
                    return true;
                }
            }
            return false;
        }
    }
}