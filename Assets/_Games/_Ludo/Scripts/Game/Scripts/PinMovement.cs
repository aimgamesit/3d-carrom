﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using com.TicTok.managers;
using static ModelSocialProfile;

namespace AIMLudo
{
    public class PinMovement : BManager<PinMovement>
    {
        public GameObject SelectedPin;
        public Dictionary<PinHandeller, GameObject> MatchedPinsList = new Dictionary<PinHandeller, GameObject>();
        Dictionary<PinHandeller, GameObject> MatchedPinsPinList = new Dictionary<PinHandeller, GameObject>();
        public Dictionary<int, int> PlayerWinnerList = new Dictionary<int, int>();
        public bool PinHit = false;
        public bool PinWin = false;
        public GameObject OpponentSinglePin;
        public int PlayerWinCount = 0;
        public GameObject[] PlayersToAssignRank;
        public static bool PinMove;
        public static bool IsRepeatMyTurn;
        public bool EmptyMovablePins;

        public int StartPos;
        public int PinPosition1 = -1;
        public int PinPosition2 = -1;
        public int PinPosition3 = -1;
        public int PinPosition4 = -1;
        public int destination;
        public GameObject pinMoveEffectPrefab;

        void Start()
        {
            PinMove = false;
            IsRepeatMyTurn = false;
            SetPositionsOfPins();
            EmptyMovablePins = false;
        }
        public void SetSelectedPin(bool IsBackMove = false)
        {

            Debug.Log("SetSelectedPin!!!!!!!!!!!!!!!!!!!!!!!!!!");
            if (SelectedPin != null)
            {
                if (SelectedPin.transform.childCount > 1)
                {
                    SelectedPin.GetComponent<PinHandeller>().ValidToMove = true;
                    if (SelectedPin.GetComponent<PinHandeller>().ValidToMove)
                    {
                        // Debug.Log("ValidToMove.........." + SelectedPin.GetComponent<PinHandeller>().ValidToMove);
                        SelectedPin.GetComponent<PinHandeller>().ValidToMove = false;
                        if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
                        {
                            //OnPinClick();
                            GameController.Instance.OnClickToken(SelectedPin.GetComponent<PinHandeller>(), IsBackMove);
                        }
                    }
                }
            }
        }

        public void DisableCircleAnimation()
        {
            if (SelectedPin != null)
            {
                for (int i = 0; i < 4; i++)
                {
                    GameObject SelectedPinContainer = SelectedPin.transform.GetComponentInParent<PinContainerHandller>().Pins[i];
                    if (SelectedPinContainer.transform.childCount > 1)
                    {
                        SelectedPinContainer.transform.GetChild(1).gameObject.SetActive(false);
                    }

                }
            }

            foreach (var item in GameController.Instance.gamePlayers)
            {
                foreach (var pins in item.Pins)
                {
                    pins.GetComponent<BoxCollider>().enabled = false;
                    pins.GetComponent<Animator>().enabled = false;
                    pins.transform.localScale = new Vector3(1f, 1f, 1f);
                }
            }

            OffAllPinsCollider();
        }

        public void OnPinClick()
        {
            Debug.Log("OnPinClick!!!!!!!!!!!!!!!!!!!!");
            DisableRoundAnimationFromPins();
            OffAllPinsCollider();

            foreach (var item in GameController.Instance.gamePlayers)
            {
                foreach (var pins in item.Pins)
                {
                    pins.GetComponent<BoxCollider>().enabled = false;
                    pins.GetComponent<Animator>().enabled = false;
                    pins.transform.localScale = new Vector3(1f, 1f, 1f);
                }
            }

            ////PinMovement
            if (SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex < 0)
            {
                Debug.Log("<color=cyan>Move from home to Board</color>");
                StartCoroutine(MovePinContainerToHome());
            }
            else
            {
                Debug.Log("<color=cyan>Move on Board oNly</color>");
                StartCoroutine(MovePinHomeToDestination());
            }
        }

        public void OnPinClick(List<GameObject> selectedPins, int PosFrom, int PosTo)
        {
            Debug.Log("OnPinClick!!!!!!!!!!!!!!!!!!!!");
            int index = 1;
            foreach (var SelectedPin in selectedPins)
            {
                this.SelectedPin = SelectedPin;
                //PinMovement
                if (SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex < 0)
                {
                    Debug.Log("<color=cyan>Move from home to Board</color>");
                    StartCoroutine(MovePinContainerToHome());
                }
                else
                {
                    Debug.Log("<color=cyan>Move on Board oNly</color>");
                    StartCoroutine(MovePinHomeToDestination01(SelectedPin, PosFrom, PosTo, (index == selectedPins.Count)));
                }
                index++;
            }

        }

        public IEnumerator MovePinContainerToHome()
        {
            Debug.Log("MovePinContainerToHome!!!!!!!!!!!!!!!!!!!");
            GameSoundManger.Instance.StopPoint.Play();
            Transform Destination = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[1].transform;
            Debug.Log(Destination.gameObject.name);
            SelectedPin.transform.position = Vector3.Lerp(SelectedPin.transform.position, Destination.position, 1f);

            SelectedPin.transform.localScale = new Vector3(1f, 1f, 1f);
            SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex = 1;
            SelectedPin.GetComponent<PinHandeller>().CurrentBoardName = Destination.gameObject;
            SelectedPin.GetComponent<PinHandeller>().CorrespondingPositionX = SelectedPin.transform.localPosition.x;
            SelectedPin.GetComponent<PinHandeller>().CorrespondingPositionZ = SelectedPin.transform.localPosition.z;
            ResizeAllPins();
            PinMoveDone();
            yield return new WaitForSeconds(5f);
        }

        public IEnumerator MovePinHomeToDestination()
        {
            Debug.Log("MovePinHomeToDestination!!!!!!!!!!!!!!!!!!!!!!".Colored(Colors.red));
            int Destination;
            if (Constants.GamePlay == Constants.GamePlay_MultiPlayer)
            {
                StartPos = GameController.Instance.posFrom;
                Destination = GameController.Instance.posTo;
            }
            else
            {
                Destination = SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex + DiceController.instance.DiceNumber;
                StartPos = SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex;
            }
            SelectedPin.transform.localScale = new Vector3(1f, 1f, 1f);
            int i = SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex;

            if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
            {
                PlayerController playerdata = GameController.Instance.gamePlayers[GameController.PlayerTurnIndex];
                PlayerPrefs.SetString("Color", playerdata.ColorIndex.ToString());
            }

            if (Destination > i)
            {
                #region Move Forward
                while (i < Destination)
                {
                    GameSoundManger.Instance.PinMove.Play();
                    float evaluteimte;
                    if (Constants.GamePlay == Constants.GamePlay_MultiPlayer) evaluteimte = GameController.Instance.ActionTime / 1200f;
                    else
                        evaluteimte = 0.15f;

                    Transform dest;

                    dest = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i + 1].transform;
                    SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex = i + 1;

                    float t = 0;
                    float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
                    while (t < evaluteimte)
                    {
                        deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
                        realTimeLastFrame = Time.realtimeSinceStartup;
                        t += deltaTime;
                        if (t < evaluteimte / 2)
                            SelectedPin.transform.localScale = new Vector3(SelectedPin.transform.localScale.x + 0.02f, SelectedPin.transform.localScale.y + 0.02f, SelectedPin.transform.localScale.z + 0.02f);
                        else
                            SelectedPin.transform.localScale = new Vector3(SelectedPin.transform.localScale.x - 0.02f, SelectedPin.transform.localScale.y - 0.02f, SelectedPin.transform.localScale.z - 0.02f);

                        SelectedPin.transform.position = Vector3.Lerp(SelectedPin.transform.position, dest.position, (t / evaluteimte * 1f));
                        yield return new WaitForSeconds(0);
                    }
                    SelectedPin.transform.position = dest.position;
                    SelectedPin.transform.localScale = new Vector3(1f, 1f, 1f);

                    i++;
                    SelectedPin.GetComponent<PinHandeller>().CurrentBoardName = dest.gameObject;

                }
                #endregion
            }
            else
            {
                #region Move Backward
                while (i > Destination)
                {
                    GameSoundManger.Instance.PinMove.Play();
                    float evaluteimte;
                    if (Constants.GamePlay == Constants.GamePlay_MultiPlayer) evaluteimte = GameController.Instance.ActionTime / 1200f;
                    else
                        evaluteimte = 0.15f;

                    Transform dest;
                    dest = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i - 1].transform;
                    SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex = i - 1;

                    float t = 0;
                    float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
                    while (t < evaluteimte)
                    {
                        deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
                        realTimeLastFrame = Time.realtimeSinceStartup;
                        t += deltaTime;
                        if (t < evaluteimte / 2)
                            SelectedPin.transform.localScale = new Vector3(SelectedPin.transform.localScale.x + 0.02f, SelectedPin.transform.localScale.y + 0.02f, SelectedPin.transform.localScale.z + 0.02f);
                        else
                            SelectedPin.transform.localScale = new Vector3(SelectedPin.transform.localScale.x - 0.02f, SelectedPin.transform.localScale.y - 0.02f, SelectedPin.transform.localScale.z - 0.02f);

                        SelectedPin.transform.position = Vector3.Lerp(SelectedPin.transform.position, dest.position, (t / evaluteimte * 1f));
                        yield return new WaitForSeconds(0);
                    }
                    SelectedPin.transform.position = dest.position;
                    SelectedPin.transform.localScale = new Vector3(1f, 1f, 1f);

                    i--;
                    SelectedPin.GetComponent<PinHandeller>().CurrentBoardName = dest.gameObject;

                }
                #endregion
            }
            SelectedPin.GetComponent<PinHandeller>().CorrespondingPositionX = SelectedPin.transform.localPosition.x;
            SelectedPin.GetComponent<PinHandeller>().CorrespondingPositionZ = SelectedPin.transform.localPosition.z;

            if (SelectedPin.GetComponent<PinHandeller>().CurrentBoardName.tag == "StopPoint")
            {
                GameSoundManger.Instance.StopPoint.Play();
            }
            if (Destination > 56 && SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex > 56)
            {
                PinWin = true;
                IsRepeatMyTurn = true;
                StartCoroutine(GameController.Instance.PlayWinPartical());
                if (!SelectedPin.GetComponent<PinHandeller>().PinWinStatus)
                {
                    SelectedPin.GetComponent<PinHandeller>().PinWinStatus = true;
                }
                ResizeAllWinPins();
            }
            else
            {
                Debug.Log("Calllllllll");
                ResizeAllPins();
            }

            destination = Destination;
            PinMoveDone();
        }

        private Color GetCurrentPinColor()
        {
            switch (SelectedPin.GetComponent<PinHandeller>().color)
            {
                case Utility.ColorIndex.Red:
                    return new Color32(196, 21, 0, 255);
                case Utility.ColorIndex.Green:
                    return new Color32(0, 148, 56, 255);
                case Utility.ColorIndex.Blue:
                    return new Color32(20, 121, 216, 255);
                default:
                    return new Color32(217, 183, 41, 255);
            }
        }

        public IEnumerator MovePinHomeToDestination01(GameObject SelectedPin, int posFrom, int posTo, bool IsFinalPin = true)
        {
            Debug.Log("MovePinHomeToDestination!!!!!!!!!!!!!!!!!!!!!!".Colored(Colors.red));
            int Destination;

            StartPos = posFrom;
            Destination = posTo;
            SelectedPin.transform.localScale = new Vector3(1f, 1f, 1f);
            int i = SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex;

            if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
            {
                PlayerController playerdata = GameController.Instance.gamePlayers[GameController.PlayerTurnIndex];
                PlayerPrefs.SetString("Color", playerdata.ColorIndex.ToString());
            }

            if (Destination > i)
            {
                #region Move Forward
                while (i < Destination)
                {
                    GameSoundManger.Instance.PinMove.Play();
                    float evaluteimte;
                    if (Constants.GamePlay == Constants.GamePlay_MultiPlayer) evaluteimte = GameController.Instance.ActionTime / 1200f;
                    else
                        evaluteimte = 0.15f;

                    Transform dest;

                    dest = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i + 1].transform;
                    GameObject pinMoveObj = Instantiate(pinMoveEffectPrefab, dest);
                    pinMoveObj.transform.localPosition = new Vector3(0, -2f, 0);
                    pinMoveObj.GetComponent<ParticleSystem>().startColor = GetCurrentPinColor();
                    SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex = i + 1;

                    float t = 0;
                    float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
                    while (t < evaluteimte)
                    {
                        deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
                        realTimeLastFrame = Time.realtimeSinceStartup;
                        t += deltaTime;
                        if (t < evaluteimte / 2)
                            SelectedPin.transform.localScale = new Vector3(SelectedPin.transform.localScale.x + 0.02f, SelectedPin.transform.localScale.y + 0.02f, SelectedPin.transform.localScale.z + 0.02f);
                        else
                            SelectedPin.transform.localScale = new Vector3(SelectedPin.transform.localScale.x - 0.02f, SelectedPin.transform.localScale.y - 0.02f, SelectedPin.transform.localScale.z - 0.02f);

                        SelectedPin.transform.position = Vector3.Lerp(SelectedPin.transform.position, dest.position, (t / evaluteimte * 1f));
                        yield return new WaitForSeconds(0);
                    }
                    SelectedPin.transform.position = dest.position;
                    SelectedPin.transform.localScale = new Vector3(1f, 1f, 1f);

                    i++;
                    SelectedPin.GetComponent<PinHandeller>().CurrentBoardName = dest.gameObject;

                }
                #endregion
            }
            else
            {
                #region Move Backward
                while (i > Destination)
                {
                    GameSoundManger.Instance.PinMove.Play();
                    float evaluteimte;
                    if (Constants.GamePlay == Constants.GamePlay_MultiPlayer) evaluteimte = GameController.Instance.ActionTime / 1200f;
                    else
                        evaluteimte = 0.15f;

                    Transform dest;
                    dest = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i - 1].transform;
                    GameObject pinMoveObj = Instantiate(pinMoveEffectPrefab, dest);
                    pinMoveObj.transform.localPosition = new Vector3(0, -2f, 0);
                    pinMoveObj.GetComponent<ParticleSystem>().startColor = GetCurrentPinColor();
                    SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex = i - 1;

                    float t = 0;
                    float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
                    while (t < evaluteimte)
                    {
                        deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
                        realTimeLastFrame = Time.realtimeSinceStartup;
                        t += deltaTime;
                        if (t < evaluteimte / 2)
                            SelectedPin.transform.localScale = new Vector3(SelectedPin.transform.localScale.x + 0.02f, SelectedPin.transform.localScale.y + 0.02f, SelectedPin.transform.localScale.z + 0.02f);
                        else
                            SelectedPin.transform.localScale = new Vector3(SelectedPin.transform.localScale.x - 0.02f, SelectedPin.transform.localScale.y - 0.02f, SelectedPin.transform.localScale.z - 0.02f);

                        SelectedPin.transform.position = Vector3.Lerp(SelectedPin.transform.position, dest.position, (t / evaluteimte * 1f));
                        yield return new WaitForSeconds(0);
                    }
                    SelectedPin.transform.position = dest.position;
                    SelectedPin.transform.localScale = new Vector3(1f, 1f, 1f);

                    i--;
                    SelectedPin.GetComponent<PinHandeller>().CurrentBoardName = dest.gameObject;

                }
                #endregion
            }
            SelectedPin.GetComponent<PinHandeller>().CorrespondingPositionX = SelectedPin.transform.localPosition.x;
            SelectedPin.GetComponent<PinHandeller>().CorrespondingPositionZ = SelectedPin.transform.localPosition.z;

            if (SelectedPin.GetComponent<PinHandeller>().CurrentBoardName.tag == "StopPoint")
            {
                GameSoundManger.Instance.StopPoint.Play();
            }
            if (Destination > 56 && SelectedPin.GetComponent<PinHandeller>().CurrentBoardIndex > 56)
            {
                PinWin = true;
                IsRepeatMyTurn = true;
                StartCoroutine(GameController.Instance.PlayWinPartical());
                if (!SelectedPin.GetComponent<PinHandeller>().PinWinStatus)
                {
                    SelectedPin.GetComponent<PinHandeller>().PinWinStatus = true;
                }
                ResizeAllWinPins();
            }
            else
            {
                Debug.Log("Calllllllll");
                ResizeAllPins();
            }

            destination = Destination;
            Debug.Log($"Pin Move Done:::::::::::::::::::::{IsFinalPin}".Colored(Colors.red));

            if (IsFinalPin)
            {
                PinMoveDone();
            }
        }

        /// </summary>
        /// <returns></returns>
        public void SetPositionsOfPins()  // To Set the positions of pins
        {
            if (SelectedPin != null)
            {
                PinPosition1 = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().Pins[0].GetComponent<PinHandeller>().CurrentBoardIndex;
                PinPosition2 = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().Pins[1].GetComponent<PinHandeller>().CurrentBoardIndex;
                PinPosition3 = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().Pins[2].GetComponent<PinHandeller>().CurrentBoardIndex;
                PinPosition4 = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().Pins[3].GetComponent<PinHandeller>().CurrentBoardIndex;
            }
            else
            {
                PinPosition1 = -1;
                PinPosition2 = -1;
                PinPosition3 = -1;
                PinPosition4 = -1;
            }
        }

        public IEnumerator ResetBoardColor(Transform dest, float waitTime)
        {
            yield return new WaitForSeconds(waitTime);

            GameObject gameObject_Cell = GameObject.Find(dest.gameObject.name).gameObject;
            MeshRenderer meshRenderer = gameObject_Cell.GetComponent<MeshRenderer>();
            if (gameObject_Cell.tag == "3D")
            {

                // Debug.Log("OffHighlight...............");
                Color myColor = new Color();
                UnityEngine.ColorUtility.TryParseHtmlString("#FFFFFFFF", out myColor);
                meshRenderer.material.color = myColor;

            }
        }

        public void DisableRoundAnimationFromPins()
        {
            // to disable the collider
            if (SelectedPin != null)
            {
                Debug.Log("DisableRoundAnimationFromPins......");
                for (int i = 0; i < 4; i++)
                {
                    GameObject SelectedPinContainer = SelectedPin.transform.GetComponentInParent<PinContainerHandller>().Pins[i];
                    if (SelectedPinContainer.transform.childCount > 1)
                    {
                        SelectedPinContainer.transform.GetChild(1).gameObject.SetActive(false);
                    }

                }
            }
        }
        //public IEnumerator PinMoveDone()
        public void PinMoveDone()
        {
            Debug.Log("PinMoveDoneFunction!!!!!!!!!!!!!!!!!!!!!!!!");
            for (int i = 0; i < 4; i++)
            {
                GameObject SelectedPinContainer = SelectedPin.transform.parent.gameObject.GetComponent<PinContainerHandller>().Pins[i];
                if (SelectedPinContainer.transform.childCount > 1)
                {
                    Destroy(SelectedPinContainer.transform.GetChild(1).gameObject);
                }
            }
            //For Pin Bite
            if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
                GetMatchedPinsOnSameBoardCell();

            // CheckWinner();
            bool WinStatus = CheckWinningStatus(GameController.PlayerTurnIndex);

            OffAllPinsCollider();
            if (!WinStatus)
                StartCoroutine(DiceController.instance.AssignNextTurn());
        }

        public void OffAllPinsCollider()
        {
            foreach (var Pins in GameController.Instance.GameBoardPins)
            {
                Pins.GetComponent<PinHandeller>().IsPinValidToClick = false;
            }
        }
        public void GetMatchedPinsOnSameBoardCell()
        {
            Debug.Log("GetMatchedPinsOnSameBoardCell!!!!!!!!!!!!!!!!!");
            MatchedPinsPinList.Clear();
            MatchedPinsList.Clear();
            foreach (var PinObject in GameController.Instance.GameBoardPins)
            {
                if (PinObject != null && PinObject.GetComponent<PinHandeller>().CurrentBoardName != null && !PinObject.GetComponent<PinHandeller>().PinWinStatus)
                {
                    //Get Pins Which are Out Side From Home......
                    //PinObject.transform.localScale = new Vector3(1f, 1f, 1f);
                    MatchedPinsPinList.Add(PinObject.GetComponent<PinHandeller>(), PinObject);
                }
            }
            //  Debug.Log("MatchedPinsPinList......................" + MatchedPinsPinList.Count());
            if (MatchedPinsPinList.Count > 0)
            {
                var PinDictionaryList = MatchedPinsPinList.GroupBy(x => x.Key.CurrentBoardName.name).ToList().Where(x => x.Count() > 1).ToList();
                //    Debug.Log("PinDictionaryList......................" + PinDictionaryList.Count());
                if (PinDictionaryList.Count() > 0)
                {
                    foreach (var pinObject_Dictionary in PinDictionaryList)
                    {
                        foreach (var pinObject in pinObject_Dictionary)
                        {
                            //Get Pins Cells Which are Containing More then One Pin......
                            MatchedPinsList.Add(pinObject.Key.gameObject.GetComponent<PinHandeller>(), pinObject.Key.gameObject);
                        }
                    }
                    PinBite();
                }
            }
        }

        public void PinBite()
        {
            Debug.Log("PinBite!!!!!!!!!!!!!!!!!!!!!");
            Dictionary<PinHandeller, GameObject> CurrentBoardPinList = new Dictionary<PinHandeller, GameObject>();
            foreach (var matchedPinsList in MatchedPinsList)
            {
                if (matchedPinsList.Key.CurrentBoardName.name == SelectedPin.GetComponent<PinHandeller>().CurrentBoardName.name)
                {
                    CurrentBoardPinList.Add(matchedPinsList.Key, matchedPinsList.Key.gameObject);
                }
            }
            Dictionary<PinHandeller, GameObject> OpponentContainerPinCount = new Dictionary<PinHandeller, GameObject>();
            int SameContainerPinCount = 0;
            foreach (var pinBiteObject_Dictionary in CurrentBoardPinList)
            {
                if (SelectedPin.gameObject.GetComponent<PinHandeller>().CurrentBoardName.tag != "StopPoint")
                {
                    if (pinBiteObject_Dictionary.Key.CorrespondingPinContainerIndex != SelectedPin.GetComponent<PinHandeller>().CorrespondingPinContainerIndex)
                    {
                        OpponentContainerPinCount.Add(pinBiteObject_Dictionary.Key, pinBiteObject_Dictionary.Key.gameObject);
                    }
                    else
                    {
                        SameContainerPinCount++;
                    }
                }
            }

            if (OpponentContainerPinCount.Count() > 0)
            {
                if (OpponentContainerPinCount.Count() == 1)
                {
                    StartCoroutine(PinBackToHomeAnimation(OpponentContainerPinCount, true));
                }
            }
        }

        string playerBiteIndex = "";
        string oppBiteIndex;
        bool pinbite;

        public IEnumerator PinBackToHomeAnimation(Dictionary<PinHandeller, GameObject> OpponentContainerPinCount, bool val)
        {
            pinbite = true;  // to repeat turn
            PinHit = true;
            var data = OpponentContainerPinCount.Select(x => x.Key).ToArray();
            GameObject opponentPin = OpponentContainerPinCount.Select(y => y.Value).FirstOrDefault();
            int oppPos = opponentPin.GetComponentInParent<PinContainerHandller>().ContanerIndex;
            oppBiteIndex = opponentPin.name.Split('_')[1];
            playerBiteIndex = SelectedPin.name.Split('_')[1];

            ModelSocialProfile.PinMoveDoneModel oppData = new ModelSocialProfile.PinMoveDoneModel();
            PinContainerHandller opponentContainer = opponentPin.GetComponentInParent<PinContainerHandller>();

            Debug.Log("<color=yellow>Opponent Container= " + opponentContainer.name + " </color>");
            Debug.Log("Opponent Container Pins ChildCount= " + opponentContainer.Pins.Length);

            foreach (var pin in OpponentContainerPinCount)
            {
                pin.Key.transform.localScale = new Vector3(1f, 1f, 1f);
            }

            int i = data[0].GetComponent<PinHandeller>().CurrentBoardIndex;
            int Destination = 1;

            GameSoundManger.Instance.BiteSound.Play();

            while (i > Destination)
            {
                Transform dest = data[0].transform.parent.gameObject.GetComponent<PinContainerHandller>().PinPath[i - 1].transform;
                foreach (var pin in OpponentContainerPinCount)
                {
                    pin.Key.transform.position = dest.position;
                }
                if (GameObject.Find(dest.gameObject.name).gameObject.tag == "3D")
                {
                    //Debug.Log("OnHighlight...............");
                    Color myColor = new Color();
                    UnityEngine.ColorUtility.TryParseHtmlString("#FFFFFFFF", out myColor);
                    GameObject.Find(dest.gameObject.name).GetComponent<MeshRenderer>().material.color = myColor;
                }
                //    StartCoroutine(ResetBoardColor(dest, 0.05f));
                yield return new WaitForSeconds(0.02f);
                i--;
            }
            foreach (var pin in OpponentContainerPinCount)
            {
                Transform DestinationHome = pin.Key.transform.gameObject.GetComponent<PinHandeller>().PinHomePosition;
                pin.Key.transform.position = Vector3.Lerp(pin.Key.transform.position, DestinationHome.position, 1);
                pin.Key.GetComponent<PinHandeller>().CurrentBoardName = null;
                pin.Key.GetComponent<PinHandeller>().CurrentBoardIndex = -1;
                pin.Key.transform.localScale = new Vector3(1f, 1f, 1f);
            }
            oppData.PinIndexes = new List<ModelSocialProfile.PinIndex>();
            oppData.PinIndexes.Add(new ModelSocialProfile.PinIndex { PinName = 0, _PinIndex = opponentContainer.Pins[0].gameObject.GetComponent<PinHandeller>().CurrentBoardIndex });
            oppData.PinIndexes.Add(new ModelSocialProfile.PinIndex { PinName = 1, _PinIndex = opponentContainer.Pins[1].gameObject.GetComponent<PinHandeller>().CurrentBoardIndex });
            oppData.PinIndexes.Add(new ModelSocialProfile.PinIndex { PinName = 2, _PinIndex = opponentContainer.Pins[2].gameObject.GetComponent<PinHandeller>().CurrentBoardIndex });
            oppData.PinIndexes.Add(new ModelSocialProfile.PinIndex { PinName = 3, _PinIndex = opponentContainer.Pins[3].gameObject.GetComponent<PinHandeller>().CurrentBoardIndex });
            Debug.Log("Calllllllll");
            ResizeAllPins();
            IsRepeatMyTurn = true;
        }

        public void ResizeAllPins()
        {
            Debug.Log("ResizeAllPins!!!!!!!!!!!!!!!!!!!!!!!");
            Dictionary<PinHandeller, GameObject> OutsidePinList = new Dictionary<PinHandeller, GameObject>();
            foreach (var PinObject in GameController.Instance.GameBoardPins)
            {
                if (PinObject != null && PinObject.GetComponent<PinHandeller>().CurrentBoardName != null && !PinObject.GetComponent<PinHandeller>().PinWinStatus)
                {
                    //Debug.Log("Cell Contain Pins outside from the home..............");
                    OutsidePinList.Add(PinObject.GetComponent<PinHandeller>(), PinObject);
                }
            }
            var PinDictionaryOnMatchCell = OutsidePinList.GroupBy(x => x.Key.CurrentBoardName).Where(x => x.Count() > 1);
            foreach (var matchedPinsList in PinDictionaryOnMatchCell)
            {
                var data = matchedPinsList.Select(x => x.Key).ToArray();

                if (data.Length == 2)
                {
                    data[0].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[0].transform.localPosition = new Vector3((data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[1].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[1].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[1].transform.localPosition = new Vector3((data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                }
                if (data.Length == 3)
                {
                    data[0].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[0].transform.localPosition = new Vector3((data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[1].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[1].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[1].transform.localPosition = new Vector3((data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[2].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[2].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[2].transform.localPosition = new Vector3((data[2].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);
                }
                if (data.Length == 4)
                {
                    data[0].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[0].transform.localPosition = new Vector3((data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[1].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[1].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[1].transform.localPosition = new Vector3((data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[2].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[2].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[2].transform.localPosition = new Vector3((data[2].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[3].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[3].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[3].transform.localPosition = new Vector3((data[3].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);
                }
                if (data.Length == 5)
                {
                    data[0].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[0].transform.localPosition = new Vector3((data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[1].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[1].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[1].transform.localPosition = new Vector3((data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[2].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[2].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[2].transform.localPosition = new Vector3((data[2].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[3].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[3].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[3].transform.localPosition = new Vector3((data[3].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[4].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[4].transform.localPosition = new Vector3(data[4].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[4].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    data[4].transform.localPosition = new Vector3((data[4].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);
                }
                if (data.Length == 6)
                {
                    data[0].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[0].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    data[0].transform.localPosition = new Vector3((data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[1].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[1].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    data[1].transform.localPosition = new Vector3((data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[2].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[2].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    data[2].transform.localPosition = new Vector3((data[2].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[3].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[3].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    data[3].transform.localPosition = new Vector3((data[3].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[4].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[4].transform.localPosition = new Vector3(data[4].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[4].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    data[4].transform.localPosition = new Vector3((data[4].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[5].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[5].transform.localPosition = new Vector3(data[5].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[5].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[5].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    data[5].transform.localPosition = new Vector3((data[5].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[5].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);
                }
                if (data.Length == 7)
                {
                    data[0].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[0].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[0].transform.localPosition = new Vector3((data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[1].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[1].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[1].transform.localPosition = new Vector3((data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[2].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[2].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[2].transform.localPosition = new Vector3((data[2].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[3].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[3].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[3].transform.localPosition = new Vector3((data[3].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[4].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[4].transform.localPosition = new Vector3(data[4].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[4].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[4].transform.localPosition = new Vector3((data[4].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[5].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[5].transform.localPosition = new Vector3(data[5].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[5].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[5].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[5].transform.localPosition = new Vector3((data[5].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[5].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[6].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[6].transform.localPosition = new Vector3(data[6].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[6].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[6].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[6].transform.localPosition = new Vector3((data[6].GetComponent<PinHandeller>().CorrespondingPositionX), -2f, data[6].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);
                }
                if (data.Length == 8)
                {
                    data[0].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[0].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[0].transform.localPosition = new Vector3((data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[1].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[1].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[1].transform.localPosition = new Vector3((data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[2].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[2].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[2].transform.localPosition = new Vector3((data[2].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[3].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[3].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[3].transform.localPosition = new Vector3((data[3].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[4].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[4].transform.localPosition = new Vector3(data[4].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[4].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[4].transform.localPosition = new Vector3((data[4].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[5].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[5].transform.localPosition = new Vector3(data[5].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[5].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[5].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[5].transform.localPosition = new Vector3((data[5].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[5].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[6].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[6].transform.localPosition = new Vector3(data[6].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[6].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[6].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[6].transform.localPosition = new Vector3((data[6].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[6].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[7].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[7].transform.localPosition = new Vector3(data[7].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[7].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[7].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[7].transform.localPosition = new Vector3((data[7].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[7].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);
                }
                if (data.Length == 9)
                {
                    data[0].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[0].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[0].transform.localPosition = new Vector3((data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[1].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[1].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[1].transform.localPosition = new Vector3((data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[2].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[2].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[2].transform.localPosition = new Vector3((data[2].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ - 1f);

                    data[3].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[3].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[3].transform.localPosition = new Vector3((data[3].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[3].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[3].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[3].transform.localPosition = new Vector3((data[3].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[4].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[4].transform.localPosition = new Vector3(data[4].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[4].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[4].transform.localPosition = new Vector3((data[4].GetComponent<PinHandeller>().CorrespondingPositionX - 1f), 2f, data[4].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[5].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[5].transform.localPosition = new Vector3(data[5].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[5].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[5].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[5].transform.localPosition = new Vector3((data[5].GetComponent<PinHandeller>().CorrespondingPositionX), 2f, data[5].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                    data[6].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[6].transform.localPosition = new Vector3(data[6].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[6].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[6].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[6].transform.localPosition = new Vector3((data[6].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[6].GetComponent<PinHandeller>().CorrespondingPositionZ);

                    data[7].transform.localScale = new Vector3(1f, 1f, 1f);
                    data[7].transform.localPosition = new Vector3(data[7].GetComponent<PinHandeller>().CorrespondingPositionX, 2f, data[7].GetComponent<PinHandeller>().CorrespondingPositionZ);
                    data[7].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    data[7].transform.localPosition = new Vector3((data[7].GetComponent<PinHandeller>().CorrespondingPositionX + 1f), 2f, data[7].GetComponent<PinHandeller>().CorrespondingPositionZ + 1f);

                }
            }
            //For one Pin In One Cell.............
            var PinDictionaryOnMatchCell1 = OutsidePinList.GroupBy(x => x.Key.CurrentBoardName).Where(x => x.Count() == 1);
            // Debug.Log("PinDictionaryOnMatchCell1!!!!!!!!!!!!" + PinDictionaryOnMatchCell1.Count());
            foreach (var matchedPinsList1 in PinDictionaryOnMatchCell1)
            {
                foreach (var matchedPinsList in matchedPinsList1)
                {
                    matchedPinsList.Key.transform.localScale = new Vector3(1f, 1f, 1f);
                    matchedPinsList.Key.transform.localPosition = new Vector3(matchedPinsList.Key.GetComponent<PinHandeller>().CorrespondingPositionX, 2f, matchedPinsList.Key.GetComponent<PinHandeller>().CorrespondingPositionZ);
                }
            }
        }

        private List<int> winnerList = new List<int>();

        public bool CheckWinningStatus(int playerTurnIndex)
        {
            Debug.Log("CheckWinningStatus.............");
            Debug.Log($"Constants.GamePlay: {Constants.GamePlay}, Constants.GameType: {Constants.GameType}, Constants.GameMode: {Constants.GameMode}");
            Dictionary<PinHandeller, GameObject> WinPinObjects = new Dictionary<PinHandeller, GameObject>();
            Transform PinsToCheckForWin = GameController.Instance.GameObject_PinContainer[playerTurnIndex].gameObject.transform;

            List<PlayerController> activePlayers = GameController.Instance.gamePlayers.FindAll(x => x.PlayerIndex != -1);
            Dictionary<int, bool> playerWonInfo = new Dictionary<int, bool>();

            for (int i = 0; i < activePlayers.Count; i++)
            {
                Debug.Log($"ACTIVE: {activePlayers[i].PlayerIndex}");
                int pinstatusCount = 0;
                Transform playerTrans = GameController.Instance.GameObject_PinContainer[activePlayers[i].PlayerIndex].gameObject.transform;
                foreach (Transform child in playerTrans)
                {
                    if (child.gameObject.GetComponent<PinHandeller>().PinWinStatus)
                    {
                        pinstatusCount++;
                    }
                }
                playerWonInfo.Add(activePlayers[i].PlayerIndex, pinstatusCount == 4);
            }

            int totalWinners = playerWonInfo.Count(x => x.Value == true);

            foreach (KeyValuePair<int, bool> item in playerWonInfo)
            {
                if (item.Value)
                {
                    int winnerIndex = item.Key;
                    if (!winnerList.Contains(winnerIndex))
                    {
                        winnerList.Add(winnerIndex);
                        PlayerController playerdata = GameController.Instance.gamePlayers[winnerIndex];
                        playerdata.SetWinRank(winnerList.IndexOf(winnerIndex));
                        GameController.Instance.GameObject_PinContainer[winnerIndex].GetComponent<PinContainerHandller>().IsContaineralreadyWins = true;
                        playerdata.ShowCrown();
                    }
                }
            }

            if (GameController.Instance.maxPlayers - 1 == totalWinners)
            {
                Debug.Log($"Total Winners: {totalWinners}");
                string[] playersName = new string[GameController.Instance.maxPlayers - 1];
                Sprite[] playersPic = new Sprite[GameController.Instance.maxPlayers - 1];
                for (int i = 0; i < winnerList.Count; i++)
                {
                    PlayerController playerdata = GameController.Instance.gamePlayers[winnerList[i]];
                    playersName[i] = playerdata.Name;
                    playersPic[i] = playerdata.ImgProfilePic.sprite;
                }
                GameSoundManger.Instance.WinnerScreen.Play();
                GameController.Instance.StopBlinking();
                GameController.Instance.WinLooseDialog.Init(playersName, playersPic);
                return true;
            }
            return false;
        }

        public void ResizeAllWinPins()
        {
            List<PinHandeller> WinPinObjects = new List<PinHandeller>();

            foreach (GameObject pins in GameController.Instance.GameBoardPins)
            {
                if (pins.gameObject.GetComponent<PinHandeller>().PinWinStatus)
                {
                    WinPinObjects.Add(pins.GetComponent<PinHandeller>());
                }
            }
            Debug.Log("ResizeWinPinDoneWinPinObjects:::::::::::" + WinPinObjects.Count);
            //GroupBy WINPINS On The Basis Of their ContainerIndex
            if (WinPinObjects.Count > 0)
            {
                var GroupOfPins_SameContainer = WinPinObjects.GroupBy(x => x.CorrespondingPinContainerIndex).Select(grp => grp.ToList()).ToList();

                foreach (var groups in GroupOfPins_SameContainer)
                {
                    var data = groups.ToArray();
                    int ContanerIndex = data[0].CorrespondingPinContainerIndex;
                    if (data.Length == 1)
                    {
                        if (ContanerIndex == 0)
                        {
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 1.6f, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                        }
                        else if (ContanerIndex == 1)
                        {
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ + 1.6f);
                        }
                        else if (ContanerIndex == 2)
                        {
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX + 1.6f, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                        }
                        else if (ContanerIndex == 3)
                        {
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 1.6f);
                        }
                    }
                    if (data.Length == 2)
                    {
                        if (ContanerIndex == 0)
                        {
                            data[0].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                            data[1].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX - 3f, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                        }
                        else if (ContanerIndex == 1)
                        {
                            data[0].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                            data[1].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ + 3f);
                        }
                        else if (ContanerIndex == 2)
                        {
                            data[0].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                            data[1].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 3f, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                        }
                        else if (ContanerIndex == 3)
                        {
                            data[0].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                            data[1].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 3f);
                        }
                    }
                    if (data.Length == 3)
                    {
                        if (ContanerIndex == 0)
                        {
                            data[0].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[1].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[2].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX + 0.6f, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX - 3.8f, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                            data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX - 1.6f, 2.8f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);
                        }
                        else if (ContanerIndex == 1)
                        {
                            data[0].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[1].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[2].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 0.6f);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ + 3.8f);
                            data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ + 1.6f);
                        }
                        else if (ContanerIndex == 2)
                        {
                            data[0].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[1].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[2].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 0.6f, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 3.8f, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ);
                            data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX + 1.6f, 2.8f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ);

                        }
                        else if (ContanerIndex == 3)
                        {
                            data[0].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[1].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[2].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ + 0.6f);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 3.8f);
                            data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX, 2.8f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ - 1.6f);

                        }
                    }
                    if (data.Length == 4)
                    {
                        if (ContanerIndex == 0)
                        {
                            data[0].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[1].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[2].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[3].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX + 0.6f, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 0.5f);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX - 3.8f, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 0.5f);
                            data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX - 1.6f, 2.8f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ + 2f);
                            data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX - 1.6f, 2.8f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ - 0.5f);
                        }
                        else if (ContanerIndex == 1)
                        {
                            data[0].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[1].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[2].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[3].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 0.5f, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ - 0.6f);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX - 0.5f, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ + 3.8f);
                            data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX + 2f, 2.8f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ + 1.6f);
                            data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX - 0.5f, 2.8f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ + 1.6f);

                        }
                        else if (ContanerIndex == 2)
                        {
                            data[0].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[1].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[2].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[3].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX - 0.6f, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ + 0.5f);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 3.8f, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ + 0.5f);
                            data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX + 1.6f, 2.8f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ - 2f);
                            data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX + 1.6f, 2.8f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ + 0.5f);
                        }
                        else if (ContanerIndex == 3)
                        {
                            data[0].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[1].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[2].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[3].transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                            data[0].transform.localPosition = new Vector3(data[0].GetComponent<PinHandeller>().CorrespondingPositionX + 0.5f, 2.8f, data[0].GetComponent<PinHandeller>().CorrespondingPositionZ + 0.6f);
                            data[1].transform.localPosition = new Vector3(data[1].GetComponent<PinHandeller>().CorrespondingPositionX + 0.5f, 2.8f, data[1].GetComponent<PinHandeller>().CorrespondingPositionZ - 3.8f);
                            data[2].transform.localPosition = new Vector3(data[2].GetComponent<PinHandeller>().CorrespondingPositionX - 2f, 2.8f, data[2].GetComponent<PinHandeller>().CorrespondingPositionZ - 1.6f);
                            data[3].transform.localPosition = new Vector3(data[3].GetComponent<PinHandeller>().CorrespondingPositionX + 0.5f, 2.8f, data[3].GetComponent<PinHandeller>().CorrespondingPositionZ - 1.6f);
                        }
                    }
                    Debug.Log("ResizeWinPinDone:::::::::::" + data.Length);
                }
            }
        }
    }
}