﻿using UnityEngine;
using static AIMLudo.Utility;

namespace AIMLudo
{
    public class PinHandeller : MonoBehaviour
    {
        public static PinHandeller Instance;
        public bool ValidToMove;
        public bool IsPinValidToClick;
        public int CurrentBoardIndex;
        public GameObject CurrentBoardName;
        public int CorrespondingPinContainerIndex;
        public Transform PinHomePosition;
        public static bool SinglePinMove;
        public GameObject BaseBoard;
        public bool PinWinStatus;
        public float CorrespondingPositionX;
        public float CorrespondingPositionZ;
        public int Index;
        public ColorIndex color;

        private void Awake()
        {
            Instance = this;
            CurrentBoardIndex = -1;
            IsPinValidToClick = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (CurrentBoardIndex == -1)
            {
                BaseBoard.SetActive(true);
            }
            else
            {
                BaseBoard.SetActive(false);
            }
            if (!PinWinStatus)
            {
                if (CurrentBoardIndex > 56)
                {
                    PinWinStatus = true;
                }
            }
            // Code to Click on Pin
            if (IsPinValidToClick)
            {
                if (Constants.GamePlay == Constants.GamePlay_Local || Constants.GamePlay == Constants.GamePlay_Vs_Comp)
                {
                    GetComponent<BoxCollider>().enabled = true;
                }

                if (Constants.GamePlay == Constants.GamePlay_Vs_Comp && GameController.PlayerTurnIndex != 0 && Constants.GameType == Constants.GAMETYPE_PLAY_WITH_COMPUTER)
                {
                    //PinMovement.Instance.OffAllPinsCollider();
                    //GameController.instance.FindMovablePins(GameController.PlayerTurnIndex);
                    //StartCoroutine(BotMovement.Instance.OnAutoPlayBotSelectPinToMove01(null));
                }
                else
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit))
                    {
                        if (Input.GetMouseButton(0))
                        {
                            Debug.Log("HIT");
                            if (hit.transform.tag == "Pin")
                            {
                                Debug.Log(hit.transform.gameObject.name + "     " + gameObject.name);
                                if (hit.transform.gameObject == gameObject)
                                {
                                    SinglePinMove = true;
                                    IsPinValidToClick = false;
                                    Debug.Log("SinglePinMove" + SinglePinMove);
                                    Debug.Log("IsPinValidToClick" + IsPinValidToClick);
                                    PinMovement.Instance.SelectedPin = hit.transform.gameObject;
                                    GameController.Instance.SingleMovablePin = hit.transform.gameObject;

                                    if (Constants.GamePlay != Constants.GamePlay_MultiPlayer)
                                    {
                                        GameController.Instance.OnClickToken(this);
                                    }
                                    else
                                    {
                                        PinMovement.Instance.SetSelectedPin();
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }
}