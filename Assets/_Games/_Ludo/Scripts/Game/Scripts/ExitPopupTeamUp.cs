﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AIMLudo
{
    public class ExitPopupTeamUp : MonoBehaviour
    {

        public static ExitPopupTeamUp Instance;
        public GameObject popup;

        private void Awake()
        {
            Instance = this;
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                ExitGame();
            }
        }

        public void ExitGame()
        {
            popup.SetActive(true);


        }
    }
}