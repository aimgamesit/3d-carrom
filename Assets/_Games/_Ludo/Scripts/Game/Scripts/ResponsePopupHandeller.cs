﻿using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class ResponsePopupHandeller : MonoBehaviour
    {
        public static ResponsePopupHandeller instance;
        public Text TextMsg;
        public Button ok;
        public GameObject FooterPanel;
        public GameObject MainPanel;
        // Use this for initialization

        private void Awake()
        {
            instance = this;
        }
        private void OnEnable()
        {
            ok.onClick.AddListener(delegate
            {
                GameSoundManger.Instance.SoundButtonClick.Play();
                if (IsNotification)
                {
                    gameObject.SetActive(false);
                    return;
                }

                if (Application.loadedLevelName == GameData.Instance.LUDO_GAME_SCENE)
                {
                    GameController.Instance.LoadMenuScene();
                }
                else
                {
                    gameObject.SetActive(false);
                }

            });

            if (Application.loadedLevelName == GameData.Instance.MENU_SCENE)
            {
                if (MainPanel != null) MainPanel.transform.localScale = new Vector3(.7f, .7f, 1f);
            }
            if (Application.loadedLevelName == GameData.Instance.LUDO_GAME_SCENE)
            {
                if (MainPanel != null) MainPanel.transform.localScale = new Vector3(.7f, .7f, 1f);
            }
            else
            {
                if (MainPanel != null) MainPanel.transform.localScale = new Vector3(1f, 1f, 1f);
            }
        }

        private void OnDisable()
        {
            ok.onClick.RemoveAllListeners();

        }
        bool IsNotification;
        public void InitDialog(bool IsEnableFooter, bool IsNotification = false)
        {
            this.IsNotification = IsNotification;
            FooterPanel.SetActive(IsEnableFooter);


        }
    }
}