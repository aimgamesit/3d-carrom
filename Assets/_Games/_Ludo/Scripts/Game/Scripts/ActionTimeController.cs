﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class ActionTimeController : MonoBehaviour
    {
        float AnimationTimeFrame = 0;
        float TotalActionTime = 0;
        [HideInInspector] public bool IsActiveTimer = false;
        bool IsStartWarnTime = false;

        System.Action OnWarning;
        System.Action OnTimeOut;

        public void OnStartActionTime(System.Action _OnWarning, System.Action _OnTimeOut, float _ActionTime, float _RemainingTime)
        {
            Debug.Log("OnStartActionTime!!!!!!!!!!!!!");
            OnWarning = _OnWarning;
            OnTimeOut = _OnTimeOut;
            Debug.Log("_ActionTime  " + _ActionTime);
            Debug.Log("_RemainingTime  " + _RemainingTime);

            //Set component default values
            TotalActionTime = _ActionTime;
            AnimationTimeFrame = _ActionTime - (_RemainingTime == _ActionTime ? _ActionTime : (_RemainingTime));
            gameObject.GetComponent<Image>().fillAmount = 0;
            // Star.SetActive(true);
            IsActiveTimer = true;
            IsStartWarnTime = false;
        }

        private void FixedUpdate()
        {
            if (IsActiveTimer)
            {
                AnimationTimeFrame += Time.fixedDeltaTime;
                gameObject.GetComponent<Image>().fillAmount = (AnimationTimeFrame / TotalActionTime);
                float AnimMoveSpeed = ((AnimationTimeFrame / TotalActionTime) * 360);
                //Move start with time layer
                //Star.transform.localEulerAngles = new Vector3(0f, 0f, -AnimMoveSpeed);

                if (Math.Round(AnimationTimeFrame, 0) >= Math.Round(((TotalActionTime * 75f) / 100f)))
                {
                    //Warning Time
                    if (!IsStartWarnTime)
                    {
                        IsStartWarnTime = true;
                        OnWarning();
                    }
                }
                if (AnimationTimeFrame >= TotalActionTime)
                {
                    //Time Out
                    // OnTimeOut();
                    IsActiveTimer = false;
                }
            }
        }

    }
}