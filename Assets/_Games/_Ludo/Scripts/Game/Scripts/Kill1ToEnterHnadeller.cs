﻿using UnityEngine;
namespace AIMLudo
{
    public class Kill1ToEnterHnadeller : MonoBehaviour
    {
        public GameObject PopupOpenkill1enter;
        public PinContainerHandller PinContainerHandller;
        public Material[] MaterialsCellnormalcolor;

        void Update()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (Input.GetMouseButton(0))
                {
                    if ((hit.transform.gameObject.GetComponent<MeshRenderer>().material.name == "Red_Block (Instance)") ||
                        (hit.transform.gameObject.GetComponent<MeshRenderer>().material.name == "Yellow_Block (Instance)") ||
                        (hit.transform.gameObject.GetComponent<MeshRenderer>().material.name == "Green_Block (Instance)") ||
                        (hit.transform.gameObject.GetComponent<MeshRenderer>().material.name == "Blue_Block (Instance)")
                         )
                    {
                        PopupOpenkill1enter.SetActive(true);
                    }
                }
            }

            if (Constants.GameMode != Constants.GameMode_Classic)
            {
                //if (PinContainerHandller.OpponentPinBite)
                //{
                if ((this.transform.gameObject.GetComponent<MeshRenderer>().material.name == "Red_Block (Instance)") ||
                     (this.transform.gameObject.GetComponent<MeshRenderer>().material.name == "Yellow_Block (Instance)") ||
                     (this.transform.gameObject.GetComponent<MeshRenderer>().material.name == "Green_Block (Instance)") ||
                     (this.transform.gameObject.GetComponent<MeshRenderer>().material.name == "Blue_Block (Instance)")
                      )
                {
                    changecolor(this.transform.gameObject.GetComponent<MeshRenderer>().material.name);
                }
                //}
                //else
                //{
                //    if ((this.transform.gameObject.GetComponent<MeshRenderer>().material.name == "RedCell (Instance)") ||
                //     (this.transform.gameObject.GetComponent<MeshRenderer>().material.name == "YellowCell (Instance)") ||
                //     (this.transform.gameObject.GetComponent<MeshRenderer>().material.name == "GreenCell (Instance)") ||
                //     (this.transform.gameObject.GetComponent<MeshRenderer>().material.name == "BlueCell (Instance)")
                //      )
                //    {
                //        changecolortoblock(this.transform.gameObject.GetComponent<MeshRenderer>().material.name);
                //    }
                //}
            }
        }

        public void changecolor(string currentcolor)
        {
            switch (currentcolor)
            {
                case "Red_Block (Instance)":
                    this.gameObject.GetComponent<MeshRenderer>().material = MaterialsCellnormalcolor[0];
                    break;

                case "Yellow_Block (Instance)":
                    this.gameObject.GetComponent<MeshRenderer>().material = MaterialsCellnormalcolor[2];
                    break;

                case "Green_Block (Instance)":
                    this.gameObject.GetComponent<MeshRenderer>().material = MaterialsCellnormalcolor[1];
                    break;

                case "Blue_Block (Instance)":
                    this.gameObject.GetComponent<MeshRenderer>().material = MaterialsCellnormalcolor[3];
                    break;
            }
        }
        public void changecolortoblock(string currentcolor)
        {
            switch (currentcolor)
            {
                case "RedCell (Instance)":
                    this.gameObject.GetComponent<MeshRenderer>().material = GameController.Instance.Material_BoardBlockCells[0];
                    break;

                case "YellowCell (Instance)":
                    this.gameObject.GetComponent<MeshRenderer>().material = GameController.Instance.Material_BoardBlockCells[2];
                    break;

                case "GreenCell (Instance)":
                    this.gameObject.GetComponent<MeshRenderer>().material = GameController.Instance.Material_BoardBlockCells[1];
                    break;

                case "BlueCell (Instance)":
                    this.gameObject.GetComponent<MeshRenderer>().material = GameController.Instance.Material_BoardBlockCells[3];
                    break;
            }
        }
    }
}