﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class GameMoveMode : MonoBehaviour
    {
        [SerializeField] Button BtnBack;
        [SerializeField] Button BtnForward;
        [SerializeField] Button BtnOff;
        [SerializeField] Transform MainPanel;

        public bool SetActive { set { gameObject.SetActive(value); } }
        public void Init(int FromPos, int DiceNumber, System.Action<bool> OnAction, System.Action OnCancel)
        {
            SetActive = true;
            BtnBack.interactable = (FromPos >= DiceNumber);
            MainPanel.DOScale(1f, 0);

            BtnBack.onClick.AddListener(delegate
            {
                OnAction(true);
                SetActive = false;
            });
            BtnForward.onClick.AddListener(delegate
            {
                OnAction(false);
                SetActive = false;
            });

            BtnOff.onClick.AddListener(delegate
            {
                SetActive = false;
                if (OnCancel != null) OnCancel();
            });

            MainPanel.DOScale(1.05f, 1f).SetLoops(-1, LoopType.Yoyo);
        }

        private void OnDisable()
        {
            BtnBack.onClick.RemoveAllListeners();
            BtnForward.onClick.RemoveAllListeners();
            BtnOff.onClick.RemoveAllListeners();
        }
    }
}