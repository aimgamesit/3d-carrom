﻿using AIMCarrom;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class WinLooseDialogCongroller : PopupBaseManager
    {
        [Header("Ads")]
        [SerializeField] GameObject adNativePanel;
        [SerializeField] RawImage adIcon;
        [SerializeField] RawImage adChoices;
        [SerializeField] Text adHeadline;
        [SerializeField] Text adCallToAction;
        [SerializeField] Text adAdvertiser;
        [SerializeField] Text[] nameText;
        [SerializeField] Image[] image;
        [SerializeField] GameObject[] winObj;
        [SerializeField] Button BtnLobby;

        void OnEnable()
        {
            BtnLobby.onClick.AddListener(delegate
            {
                GameController.Instance.ShowLoadingScreen();
                GameSoundManger.Instance.SoundButtonClick.Play();
                AdmobAdsManager._AdClosed += delegate ()
                {
                    StartCoroutine(LoadMenuScene());
                };
                AdmobAdsManager.Instance.ShowInterstitial();

            });
        }

        private IEnumerator LoadMenuScene()
        {
            yield return new WaitForEndOfFrame();
            FirebaseManager.Instance.SendEvent(AIMCarrom.Constants.LUDO_GO_TO_HOME);
            GameController.Instance.LoadMenuScene();
        }

        public void Init(string[] playerName, Sprite[] playersPic)
        {
            for (int i = 0; i < winObj.Length; i++)
            {
                winObj[i].SetActive(false);
            }
            //SetActive = true;
            for (int i = 0; i < playerName.Length; i++)
            {
                nameText[i].text = playerName[i];
                winObj[i].SetActive(true);
                if (playersPic != null)
                {
                    image[i].sprite = playersPic[i];
                }
            }
            ShowScreen(true, false);
            AdmobAdsManager.Instance.ShowNativeAds(adNativePanel, adIcon, adChoices, adHeadline, adCallToAction, adAdvertiser, true);
        }
    }
}