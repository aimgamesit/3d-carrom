﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class MenuColorSetup : MonoBehaviour
    {
        public Button ButtonRedColor, ButtonYellowColor, ButtonGreenColor, ButtonBlueColor;
        public int SelectedPlayerIndex;
        public static MenuColorSetup instance;


        // Use this for initialization
        void Start()
        {

            ButtonRedColor.onClick.AddListener(delegate
            {
                GameSoundManger.Instance.SoundButtonClick.Play();
                ButtonColorClick(Utility.ColorIndex.Red);
            });
            ButtonYellowColor.onClick.AddListener(delegate
            {
                GameSoundManger.Instance.SoundButtonClick.Play();
                ButtonColorClick(Utility.ColorIndex.Yellow);
            });
            ButtonGreenColor.onClick.AddListener(delegate
            {
                GameSoundManger.Instance.SoundButtonClick.Play();
                ButtonColorClick(Utility.ColorIndex.Green);
            });
            ButtonBlueColor.onClick.AddListener(delegate
            {
                GameSoundManger.Instance.SoundButtonClick.Play();
                ButtonColorClick(Utility.ColorIndex.Blue);
            });
        }

        // Update is called once per frame
        void Update()
        {
            instance = this;
        }
        void ButtonColorClick(Utility.ColorIndex ButtonClickedColor)
        {
            SelectedPlayerIndex = this.gameObject.GetComponent<MenuColorSetup>().SelectedPlayerIndex;
            Utility.SetPlayerSeletedColorList(GetPlayerColorList(SelectedPlayerIndex, ButtonClickedColor));
            //OfflineModeDialogController.instance.PlayerColorSetup();
        }

        public List<KeyValuePair<int, int>> GetPlayerColorList(int SelectedPlayerIndexValue, Utility.ColorIndex SelectedColorIndex)
        {
            List<KeyValuePair<int, int>> PlayerColorList = new List<KeyValuePair<int, int>>();
            Debug.Log("SelectedPlayerIndexValue:::::::::::::::::::::::" + SelectedPlayerIndexValue);
            Debug.Log("SelectedColorIndex:::::::::::::::::::::::" + SelectedColorIndex);
            int SelectedColorIndexValue = (int)SelectedColorIndex;
            Debug.Log("SelectedColorIndexValue:::::::::::::::::::::::" + SelectedColorIndexValue);
            for (int i = 0; i < 4; i++)
            {
                PlayerColorList.Add(new KeyValuePair<int, int>(SelectedPlayerIndexValue, SelectedColorIndexValue));
                //For PlayerIndex
                if (SelectedPlayerIndexValue < 3)
                {
                    SelectedPlayerIndexValue++;
                }
                else
                {
                    SelectedPlayerIndexValue = 0;
                }
                //For PlayerColorIndex
                if (SelectedColorIndexValue < 3)
                {
                    SelectedColorIndexValue++;
                }
                else
                {
                    SelectedColorIndexValue = 0;
                }
            }
            return PlayerColorList;
        }
    }
}