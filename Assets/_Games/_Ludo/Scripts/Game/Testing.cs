﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Testing : MonoBehaviour
{
    [SerializeField] Button BtnTest;
    [SerializeField] Button BtnReset;
    [SerializeField] GameObject CardPanel;
    [SerializeField] int Count;
    [SerializeField] Ease ease;
    
    //[SerializeField] Transform[] transforms;
    //[SerializeField] Transform[] Positions;

    //[SerializeField] GameObject[] goldCoins;
    //[SerializeField] Transform[] GoldPositions;


    [SerializeField] Button[] Buttons;
    [SerializeField] Transform highlighter;

    [SerializeField] GameObject box;
    [SerializeField] GameObject source;
    [SerializeField] float speed=.5f;
    [SerializeField] Image ImgTimer;

    void RewardAnim()
    {
        //box.SetActive(true);
        //box.transform.DOScale(1.5f, .2f).OnComplete(() =>
        //{
        //    box.transform.DOScale(1f, .3f).SetEase(ease);
        //}).SetEase(ease);

        box.transform.DOMoveY(BtnTest.transform.position.y, speed);
        box.transform.DOScale(1.5f, speed/2).OnComplete(()=> {
            box.transform.DOScale(1f, speed/2).SetEase(ease,5f);
        }).SetEase(ease);

    }

    private void OnEnable()
    {
        BtnTest.onClick.AddListener(delegate
        {
            //GameSoundManger.instance.SoundButtonClick.Play();
            //Test(Count);
            //OnClick();
            //PlayGoldAnim();
            // RewardAnim();
            Timer();

        });

        BtnReset.onClick.AddListener(delegate
        {
            box.transform.DOMoveY(source.transform.position.y, .5f);
        });


        foreach (var button in Buttons)
        {
            button.onClick.AddListener(delegate {
                OnClickAnim(button.transform);
            });
        }

    }
    private void Start()
    {
        OnClickAnim(Buttons[2].transform);
    }

    private void OnDisable()
    {
        BtnTest.onClick.RemoveAllListeners();
        foreach (var button in Buttons)
        {
            button.onClick.RemoveAllListeners();
        }
    }



    void OnClickAnim(Transform trans)
    {
        var temp = highlighter.GetComponent<RectTransform>().rect;
        float to = trans.GetComponent<RectTransform>().rect.width;
        DOTween.To(() => temp.width, x => temp.width = x, to, .15F).OnUpdate(()=>{
            highlighter.GetComponent<RectTransform>().sizeDelta=new Vector2(temp.width, temp.height);
        });
        highlighter.DOMoveX(trans.position.x, .2f).SetEase(ease).OnComplete(() => {
            trans.GetChild(0).transform.DOPunchRotation(new Vector3(20f, 20f, 100f), .5f,10,10f);
        });
    }

    bool IsEnable = false;
    void OnClick()
    {
        //test
        //ArrangeCard02();
        //SetPingPositions();
        if (IsEnable)
        {
            //stop
            IsEnable = false;
        }
        else
        {
            //Enable
            IsEnable = true;
           // PlayAnimation();
        }
    }

    int index = 0;
    int i = 0;
    void PlayAnimation()
    {
        if (i > 10){i = 0;}

        //Transform transform = transforms[index];
        //transform.GetComponentInChildren<Text>().text = i.ToString();
        //transform.gameObject.SetActive(true);

        //transform.DOMove(Positions[1].position, .5f).SetEase(Ease.Linear).OnComplete(() =>
        //{
        //    //Move on 2 position
        //    transform.DOMove(Positions[2].position, .5f).SetEase(Ease.Linear).OnComplete(() =>{
        //        //Reset Frame
        //        transform.DOMove(Positions[0].position, 0f);
        //    });

        //    index++;
        //    i++;
        //    //Find Next Frame
        //    if (index == transforms.Length) { index = 0; }
        //    if (IsEnable) 
        //    {
        //        PlayAnimation(); 
        //    }
        //    else
        //    {
        //        //Stop Anim
        //        OnStopAnimation(transforms[index]);
        //    }
        //});
    }
    void OnStopAnimation(Transform transform)
    {
        //transform.GetComponentInChildren<Text>().text = i.ToString();
        //transform.DOMove(Positions[1].position, .7f).SetEase(Ease.OutBounce);
    }


    void PlayGoldAnim() {
        PlayAnim();
    }

    float delay = 0f;
    void PlayAnim() {
        //float y = 0f;
        //Vector2 chipPosition = new Vector2((GoldPositions[3].position.x), GoldPositions[3].position.y);
        //foreach (var chip in goldCoins)
        //{
        //    chip.SetActive(true);
        //    Vector2 position = new Vector2(chipPosition.x, chipPosition.y + (y));
        //    chip.transform.DOMove(position, .5f).SetDelay(delay).OnComplete(() =>
        //    {
        //        //chip.SetActive(false);
        //    });
        //    y +=8f;
        //    delay += .03f;
        //}
    }



    #region Positioning
    /// <summary>
    /// Get Device Screen Size
    /// </summary>
    float _ScreenWidth;
    float _ScreenHeight;
    public List<GameObject> _cards;
    void GetScreenRatio()
    {
        Vector2 vector = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        _ScreenWidth = vector.x * 2f;
        _ScreenHeight = vector.y * 2f;

        ArrangeCard();
    }

    void ArrangeCard()
    {

        Rect rect = CardPanel.GetComponent<RectTransform>().rect;

        //Spacing
        float num = -20f;
        //Get Default Position
        float num2 = rect.width / 40f - rect.height / 20f;//user for *landscap
        float num3 = 150;

        //int index = 0;
        //float X = 40;
        //foreach (var card in _cards)
        //{
        //    num2 = rect.width / 40f - rect.height / 20f;
        //    //should be ---like -> 1[0]1       x     y                         z
        //    card.GetComponent<RectTransform>().DOAnchorPos(new Vector2(num2, num3- rect.height / 2f), .1f);
        //    num3-=num;
        //    //num -= rect.width / 13f;
        //    num2 += rect.width / (float)(_cards.Count + 1);
        //}

        int index = 0;
        float X = 40;
        float spcing = 0;

        List<GameObject> _cards2 = new List<GameObject>();
        foreach (var card in _cards)
        {

            num2 = rect.width / 40f - rect.height / 20f;
            foreach (var item in _cards2)
            {
                //card.GetComponent<RectTransform>().DOAnchorPosX(card.transform.position.x- spcing, .1f);
                spcing = -50;
            }

            //should be ---like -> 1[0]1       x     y                         z
            card.GetComponent<RectTransform>().DOAnchorPos(new Vector2(num2, 0f), .1f);
            num3 -= num;
            //num -= rect.width / 13f;
            num2 += (card.GetComponent<RectTransform>().rect.width - 10);
            _cards2.Add(card);
        }

    }

    //Testing
    void ArrangeCard02()
    {
        int num = 0;
        float num2 = 10f;
        float num3 = 150f; //-70//50f

        foreach (var card in _cards)
        {
            Vector3 position = CardPanel.transform.position;

            card.transform.position = new Vector3(position.x + num2, position.y, position.z);
            card.transform.SetSiblingIndex(1 + num);
            card.transform.position += new Vector3(0f, -1f, 0f);

            num = num - 7;
            num2 += 30f;
            num++;
        }

    }

    private void SetPingPositions()
    {
        float num = 1.5f;
        for (int j = 0; j < this._cards.Count; j++)
        {
            float num4 = num / (float)this._cards.Count;
            float num5 = 500f / (float)(this._cards.Count + 1);
            //float num5 = 1f / (float)(this.PawnList.Count + 1);
            float x2 = CardPanel.transform.position.x + Mathf.Sign(CardPanel.transform.position.x) * 0.215f + Mathf.Sign(CardPanel.transform.position.x) * -1f * num5 * (float)(j);
            //float x2 = base.transform.position.x + Mathf.Sign(base.transform.position.x) * 0.215f + Mathf.Sign(base.transform.position.x) * -1f * num5 * (float)(j);
            float y2 = base.transform.position.y;

            this._cards[j].SetActive(true);
            this._cards[j].transform.DOMoveX(x2, .1f);
        }
    }

    void Test(int value)
    {
        int num = 10;
        for (int i = 0; i < value; i++)
        {
            float x2 = 0 + Mathf.Sign(5) * 0.215f + Mathf.Sign(5) * -i;
            Debug.Log($"value::::::{x2}");
            num += 10;
        }

    }

    #endregion

    #region Timer 
   
    public void Timer()
    {
            //Set component default values
            TotalActionTime = 5;
            AnimationTimeFrame = 0;
            ImgTimer.fillAmount = 1;

            //Star.SetActive(true);
            IsActiveTimer = true;
    }


    float AnimationTimeFrame = 0;
    float TotalActionTime = 0;
    [HideInInspector] public bool IsActiveTimer = false;

    private void FixedUpdate()
    {
        if (IsActiveTimer)
        {
            AnimationTimeFrame += Time.fixedDeltaTime;
            ImgTimer.fillAmount = (1-(AnimationTimeFrame / TotalActionTime));
            float AnimMoveSpeed = ((AnimationTimeFrame / TotalActionTime) * 360);

            if (AnimationTimeFrame >= TotalActionTime)
            {
                //Time Out
                IsActiveTimer = false;
            }
        }
    }
    #endregion

}
