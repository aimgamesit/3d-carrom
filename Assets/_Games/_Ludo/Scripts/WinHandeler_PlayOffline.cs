﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace AIMLudo
{
    public class WinHandeler_PlayOffline : MonoBehaviour
    {
        public static WinHandeler_PlayOffline instance;

        public Text[] WinnerPlayersName;
        public Image[] WinnerPlayersPhoto;
        public Button Button_Lobby, Button_Share;
        public GameObject Partical;
        // Use this for initialization
        private void Awake()
        {
            instance = this;
        }
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Partical.transform.Rotate(Vector3.forward * Time.deltaTime * 100f);
        }


    }
}