﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net;
using System.Xml.Linq;
using System.Linq;
namespace AIMLudo
{
    public class LocationManager : MonoBehaviour
    {

        string baseUri = "http://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false";
        public static LocationManager Instance;

        public void Awake()
        {
            Instance = this;
        }
        public void RetrieveAddress(string lat, string lng)
        {
            string requestUri = string.Format(baseUri, lat, lng);
            using (WebClient wc = new WebClient())
            {
                string result = wc.DownloadString(requestUri);
                XElement responseElement = XElement.Parse(result);
                //ParseLocation(responseElement);
            }
        }
        public void ParseLocation(XElement responseElement)
        {
            IEnumerable<XElement> statusElement = from st in responseElement.Elements("status") select st;
            if (statusElement.FirstOrDefault() != null)
            {
                string status = statusElement.FirstOrDefault().Value;
                if (status.ToLower() == "ok")
                {
                    IEnumerable<XElement> resultElement = from rs in responseElement.Elements("result") select rs;
                    if (resultElement.FirstOrDefault() != null)
                    {
                        IEnumerable<XElement> addressElement = from ad in resultElement.FirstOrDefault().Elements("address_component") select ad;
                        foreach (XElement element in addressElement)
                        {
                            IEnumerable<XElement> typeElement = from te in element.Elements("type") select te;
                            string type = typeElement.FirstOrDefault().Value;
                            if (type == "locality")
                            {
                                IEnumerable<XElement> cityElement = from ln in element.Elements("long_name") select ln;
                                string city = cityElement.FirstOrDefault().Value;
                                PlayerPrefs.SetString("city", city);
                            }
                            if (type == "premise")
                            {
                                IEnumerable<XElement> premiseElement = from ln in element.Elements("long_name") select ln;
                                string premise = premiseElement.FirstOrDefault().Value;
                                PlayerPrefs.SetString("premise", premise);
                            }
                            if (type == "country")
                            {
                                IEnumerable<XElement> countryElement = from ln in element.Elements("long_name") select ln;
                                string country = countryElement.FirstOrDefault().Value;
                                Debug.Log("country" + country);
                                PlayerPrefs.SetString("country", country);
                            }
                            if (type == "administrative_area_level_1")
                            {
                                IEnumerable<XElement> administrative_areaElement = from ln in element.Elements("long_name") select ln;
                                string administrative_area = administrative_areaElement.FirstOrDefault().Value;
                                PlayerPrefs.SetString("state", administrative_area);
                            }
                            if (type == "postal_code")
                            {
                                IEnumerable<XElement> postal_codeElement = from ln in element.Elements("long_name") select ln;
                                string postal_code = postal_codeElement.FirstOrDefault().Value;
                                PlayerPrefs.SetString("postal_code", postal_code);
                            }
                            if (type == "political")
                            {
                                IEnumerable<XElement> LocalityElement = from ln in element.Elements("long_name") select ln;
                                string Locality = LocalityElement.FirstOrDefault().Value;
                                PlayerPrefs.SetString("locality", Locality);
                            }

                        }
                    }
                }
            }
        }
    }
}