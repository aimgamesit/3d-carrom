﻿using UnityEngine;
using DG.Tweening;
namespace AIMLudo
{
    public class ButtonsAniamtion : MonoBehaviour
    {
        void OnEnable()
        {
            Shake();
        }
        
        public void Shake()
        {
            transform.DOPunchScale(new Vector3(0.1f, 0.1f, 0.1f), .3f, 3, 1);
            transform.localScale = new Vector3(1f, 1, 1f);
        }
    }
}