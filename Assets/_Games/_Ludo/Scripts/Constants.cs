﻿using System.Collections.Generic;
using UnityEngine;
namespace AIMLudo
{
    public class Constants
    {
        #region "GameModes"
        public static string GameMode = "Classic";
        public const string GameMode_Classic = "Classic";
        public const string GameMode_Master = "Master";
        public const string GameMode_Quick = "Quick";
        public const string GameMode_FreeToPlay = "FreeToPlay";

        #endregion

        #region GameVariableData   
        public const int IncrementBetAmount = 100;
        public static int Player_Mode_Number = 2;
        #endregion

        #region "GameTypes"
        public static string GameType = "four_player";   // change by shani  03/04/2019
        public const string GAMETYPE_PLAY_WITH_COMPUTER = "play_with_computer";

        public const string GamePlay_Local = "GamePlay_Local";
        public const string GamePlay_MultiPlayer = "GamePlay_MultiPlayer";
        public static string GamePlay = "GamePlay_Local";
        public static string GamePlay_Vs_Comp = "GamePlay_Vs_Comp";
        #endregion

        public const string CREATE_USER_PROFILE = "createuserprofile";
        public const string KEY_SOUND = "SOUND_KEY";
    }

    public class Utility
    {
        public enum GameMode
        {
            None,
            Online,
            Private
        }

        public enum ColorIndex
        {
            Red = 0,
            Green = 1,
            Yellow = 2,
            Blue = 3
        }

        public enum MaterialType
        {
            Cell,
            Cell_Home,
            Pin_Box,
            Pin_Home_Bg,
            WinBox,
            PinTexture,
            Lock,
        }

        public enum PlayerList
        {
            Player2 = 2,
            Player3 = 3,
            Player4 = 4
        }

        public enum PlayerAlignment
        {
            Left,
            Right
        }

        public enum DiceAlignment
        {
            Bottom,
            Top
        }


        public static List<KeyValuePair<int, int>> _PlayerSeletedColorList = new List<KeyValuePair<int, int>>();
        /// <summary>
        ///Get Default PlayerSeletedColorList Return a list as List<KeyValuePair<int, int>>
        /// </summary>
        /// <returns></returns>
        public static List<KeyValuePair<int, int>> GetPlayerSeletedColorList()
        {
            if (_PlayerSeletedColorList.Count == 0)
            {
                for (int i = 0; i < 4; i++)
                {
                    _PlayerSeletedColorList.Add(new KeyValuePair<int, int>(i, i));
                }
            }
            return _PlayerSeletedColorList;
        }

        /// <summary>
        /// Set Here Selected PlayerSeletedColorList to Utilty Static Color List here 
        /// </summary>
        /// <returns></returns>
        public static void SetPlayerSeletedColorList(List<KeyValuePair<int, int>> PlayerSeletedColorList)
        {
            _PlayerSeletedColorList = PlayerSeletedColorList;
        }

        public static int NumberOfPlayersPlaying = (int)Utility.PlayerList.Player2;

        public static List<KeyValuePair<int, string>> _PlayerNameList = new List<KeyValuePair<int, string>>();

        public static List<KeyValuePair<int, string>> GetPlayerNameList()
        {
            if (_PlayerNameList.Count == 0)
            {
                for (int i = 0; i < 4; i++)
                {
                    _PlayerNameList.Add(new KeyValuePair<int, string>(i, "Player " + (i + 1)));
                }
            }
            return _PlayerNameList;
        }
        public static void SetPlayerNameList(List<KeyValuePair<int, string>> UserDefinedPlayerNameList)
        {
            _PlayerNameList = UserDefinedPlayerNameList;
        }
    }
}