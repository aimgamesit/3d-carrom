﻿using com.TicTok.managers;
using UnityEngine;
namespace AIMLudo
{
    public class GameSoundManger : BManager<GameSoundManger>
    {
        public AudioSource SoundButtonClick, WinnerScreen, PinMove, DiceRoll, StopPoint, WinBox, BiteSound, Got6OnDice, GameStart;

        public void PlayButtonClick()
        {
            SoundButtonClick.Play();
        }

        private void OnEnable()
        {
            SetSound(IsSoundEnable());
        }

        public bool IsSoundEnable() => PlayerPrefs.GetInt(Constants.KEY_SOUND, 1) == 1;

        public void SetSound(bool isOn=true)
        {
            PlayerPrefs.SetInt(Constants.KEY_SOUND, isOn ? 1 : 0);
            if (isOn)
                PlayGameSounds();
            else
                StopGameSounds();
        }

        public void PlayGameSounds()
        {
            PinMove.volume = 1f;
            DiceRoll.volume = 1f;
            StopPoint.volume = 1f;
            WinBox.volume = 1f;
            BiteSound.volume = 1f;
            WinnerScreen.volume = 1f;
            Got6OnDice.volume = 1f;
            GameStart.volume = 1f;
            SoundButtonClick.volume = 1f;
        }

        public void StopGameSounds()
        {
            PinMove.volume = 0f;
            DiceRoll.volume = 0f;
            StopPoint.volume = 0f;
            WinBox.volume = 0f;
            BiteSound.volume = 0f;
            WinnerScreen.volume = 0f;
            Got6OnDice.volume = 0f;
            GameStart.volume = 0f;
            SoundButtonClick.volume = 0f;
        }
    }
}