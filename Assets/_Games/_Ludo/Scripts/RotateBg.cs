﻿using UnityEngine;
using System.Collections;
namespace AIMLudo
{
    public class RotateBg : MonoBehaviour
    {
        Vector3 angle;
        void Start()
        {
            angle = transform.eulerAngles;
        }

        void Update()
        {
            angle.z -= Time.deltaTime * 50;
            transform.eulerAngles = angle;
        }

    }
}