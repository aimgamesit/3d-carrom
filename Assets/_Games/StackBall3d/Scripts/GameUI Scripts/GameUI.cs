﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using com.TicTok.managers;

namespace StackBall3D
{
    public class GameUI : BManager<GameUI>
    {
        [SerializeField] GameObject loadingScreen;
        public GameObject homeUI, inGameUI, finishUI, gameOverUI;
        [Header("InGame")]
        public Image levelSlider;
        public Image currentLevetImg;
        public Image nextLevelImg;
        public Text currentLevelText, nextLevelText;
        [Header("Finish")]
        public Text finishLevelText;
        [Header("GameOver")]
        public Text gameOverScoreText;
        public Text gameOverBestText;
        [HideInInspector]
        private Material playerMat;
        private Player player;
        public static byte adsCounter = 0;

        private void Start()
        {
            playerMat = FindObjectOfType<Player>().transform.GetChild(0).GetComponent<MeshRenderer>().material;
            player = FindObjectOfType<Player>();
            levelSlider.transform.parent.GetComponent<Image>().color = playerMat.color + Color.gray;
            levelSlider.color = playerMat.color;
            currentLevetImg.color = playerMat.color;
            nextLevelImg.color = playerMat.color;
            AdmobAdsManager.Instance.ShowBannerAd(false);
            currentLevelText.text = FindObjectOfType<LevelSpawner>().level.ToString();
            nextLevelText.text = FindObjectOfType<LevelSpawner>().level + 1 + "";
            Invoke(nameof(HideLoadingScreen), .4f);
        }

        private void HideLoadingScreen()
        {
            loadingScreen.SetActive(false);
            Player.isReady = true;
        }
        public void ShowLoadingScreen()
        {
            loadingScreen.SetActive(true);
            Player.isReady = false;
        }

        void Update()
        {
            if (!Player.isReady)
                return;
            if (Input.GetMouseButtonDown(0) && !IgnoreUI() && player.playerState == Player.PlayerState.Prepare)
            {
                player.playerState = Player.PlayerState.Playing;
                inGameUI.SetActive(true);
                homeUI.SetActive(false);
                finishUI.SetActive(false);
                gameOverUI.SetActive(false);
            }

            if (player.playerState == Player.PlayerState.Finish)
            {
                homeUI.SetActive(false);
                inGameUI.SetActive(false);
                finishUI.SetActive(true);
                gameOverUI.SetActive(false);
                finishLevelText.text = "Level " + FindObjectOfType<LevelSpawner>().level;
            }

            if (player.playerState == Player.PlayerState.Died)
            {

            }
        }

        public void Died()
        {
            homeUI.SetActive(false);
            inGameUI.SetActive(false);
            finishUI.SetActive(false);
            gameOverUI.SetActive(true);
            gameOverScoreText.text = ScoreManager.Instance.score.ToString();
            gameOverBestText.text = PlayerPrefs.GetInt("HighScore").ToString();

            adsCounter++;
            if (adsCounter >= 2)
            {
                adsCounter = 0;
                AdmobAdsManager._AdClosed += delegate ()
                {
                    FirebaseManager.Instance.SendEvent(AIMCarrom.Constants.STACK_BALL_FULL_ADS_BUTTON);
                    StartCoroutine(GameOver());
                };
                AdmobAdsManager.Instance.ShowInterstitial();
            }
            else
            {
                StartCoroutine(GameOver());
            }
        }

        private IEnumerator GameOver()
        {
            ScoreManager.Instance.ResetScore();
            yield return new WaitForSeconds(.2f);
            SceneManager.LoadScene(GameData.Instance.STACK_BALL_GAME_SCENE);
        }

        public void BackButtonClicked()
        {
            StartCoroutine(LoadMenuScene());
        }
        private IEnumerator LoadMenuScene()
        {
            GameUI.Instance.ShowLoadingScreen();
            yield return new WaitForSeconds(.2f);
            SceneManager.LoadSceneAsync(GameData.Instance.MENU_SCENE);
        }

        private bool IgnoreUI()
        {
            PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
            pointerEventData.position = Input.mousePosition;

            List<RaycastResult> raycastResultList = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerEventData, raycastResultList);
            for (int i = 0; i < raycastResultList.Count; i++)
            {
                if (raycastResultList[i].gameObject.GetComponent<Ignore>() != null)
                {
                    raycastResultList.RemoveAt(i);
                    i--;
                }
            }
            return raycastResultList.Count > 0;
        }

        public void LevelSliderFill(float fillAmount)
        {
            levelSlider.fillAmount = fillAmount;
        }
    }
}