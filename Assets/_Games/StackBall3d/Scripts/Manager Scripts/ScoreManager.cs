﻿using com.TicTok.managers;
using UnityEngine;
using UnityEngine.UI;
namespace StackBall3D
{
    public class ScoreManager : BManager<ScoreManager>
    {
        [SerializeField] Text scoreText;

        public int score = 0;

        void OnEnable()
        {
            AddScore(0);
        }

        public void AddScore(int amount)
        {
            score += amount;
            if (score > PlayerPrefs.GetInt("HighScore", 0))
                PlayerPrefs.SetInt("HighScore", score);
            scoreText.text = score.ToString();
        }

        public void ResetScore()
        {
            score = 0;
        }
    }
}