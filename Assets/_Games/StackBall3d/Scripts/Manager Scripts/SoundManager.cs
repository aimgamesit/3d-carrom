﻿using com.TicTok.managers;
using UnityEngine;
namespace StackBall3D
{
    public class SoundManager : BManager<SoundManager>
    {
        private AudioSource audioSource;
        public bool sound = true;

        void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        public void SoundOnOff()
        {
            sound = !sound;
        }

        public void PlaySoundFX(AudioClip clip, float voulme)
        {
            if (sound)
                audioSource.PlayOneShot(clip, voulme);
        }
    }
}