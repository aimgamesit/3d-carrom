﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace StackBall3D
{
    public class Rotator : MonoBehaviour
    {
        public float speed = 100;

        void Update()
        {
            transform.Rotate(new Vector3(0, speed * Time.deltaTime, 0));
        }
    }
}